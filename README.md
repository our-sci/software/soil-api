# Soil API

### Requirements
  - `node` (see version in `package.json` `engines` entry)
  - `yarn` `>=1.21.1`

### Local Development
  - `yarn`: install dependencies
  - `yarn start`: run local development server on port 3000, watching for changes
  - `yarn debug`: like `yarn start` but also exposes node debugger
  - `yarn test --watch`: run tests
  - `yarn test:e2e`: runs e2e tests
  - `yarn generate-types`: generate typescript types from open api schema

### Build
  - `yarn build`: output bundle to `dist` directory

### Deploy
  - see gitlab ci/cd pipelines

### API Endpoints Documentation
  Documentation can be found at the path `/api-docs`.

  The production docs are at [api.soilstack.io/api-docs](https://api.soilstack.io/api-docs)

### Getting Started With Local Development
  - Install node modules: `yarn`
  - Setup Postgres
    - create a postgres docker container (https://hub.docker.com/_/postgres/)
      - ex. `docker run --name soil-api-postgres -p 5432:5432 -e POSTGRES_PASSWORD=mysecretpassword -d postgres:14.7`
    - add a `DATABASE_URL` variable to `.env`
      - ex. `DATABASE_URL="postgresql://postgres:mysecretpassword@localhost:5432/postgres?schema=public"`
    - sync prisma with your local database: `yarn prisma migrate dev`
  - Start the dev server: `yarn start`


