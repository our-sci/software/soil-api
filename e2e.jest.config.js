module.exports = {
	verbose: true,
	testMatch: ['**/?(*.)+(spec.e2e).ts'],
	resetModules: true,
	resetMocks: true,
	moduleFileExtensions: ['js', 'ts', 'd.ts'],
	preset: 'ts-jest',
	setupFilesAfterEnv: ['<rootDir>/src/test-utils/e2e/clearDatabase.ts', 'jest-expect-message'],
	globals: {
		__E2E_TESTS__: true,
	},
};
