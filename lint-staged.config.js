module.exports = {
	'*.{js,ts}': 'eslint --fix',
	'prisma/schema.prisma': [() => 'yarn prisma format', () => 'yarn detect-schema-drift'],
	// '*.ts', '**/*.ts', '**/**/*.ts', etc. will match any .ts file
	// we specify multiple equivalent matchers to allow the tasks to run in parallel
	'*.ts': () => 'tsc -p tsconfig.json --noEmit',
	'**/*.ts': () => 'yarn test',
	// bail on the first test failure to reduce total precommit time, since this is the longest task
	'**/**/*.ts': () => 'yarn test:e2e --bail',
	'**/**/**/*.ts': () => 'yarn detect-types-drift',
};
