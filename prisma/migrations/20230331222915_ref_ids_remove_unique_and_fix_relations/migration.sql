/*
  Warnings:

  - You are about to drop the `_FieldToReferenceId` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `_ReferenceIdToSamplingCollection` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "_FieldToReferenceId" DROP CONSTRAINT "_FieldToReferenceId_A_fkey";

-- DropForeignKey
ALTER TABLE "_FieldToReferenceId" DROP CONSTRAINT "_FieldToReferenceId_B_fkey";

-- DropForeignKey
ALTER TABLE "_ReferenceIdToSamplingCollection" DROP CONSTRAINT "_ReferenceIdToSamplingCollection_A_fkey";

-- DropForeignKey
ALTER TABLE "_ReferenceIdToSamplingCollection" DROP CONSTRAINT "_ReferenceIdToSamplingCollection_B_fkey";

-- DropIndex
DROP INDEX "ReferenceId_owner_value_key";

-- AlterTable
ALTER TABLE "ReferenceId" ADD COLUMN     "fieldId" TEXT,
ADD COLUMN     "samplingCollectionId" TEXT;

-- DropTable
DROP TABLE "_FieldToReferenceId";

-- DropTable
DROP TABLE "_ReferenceIdToSamplingCollection";

-- AddForeignKey
ALTER TABLE "ReferenceId" ADD CONSTRAINT "ReferenceId_fieldId_fkey" FOREIGN KEY ("fieldId") REFERENCES "Field"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "ReferenceId" ADD CONSTRAINT "ReferenceId_samplingCollectionId_fkey" FOREIGN KEY ("samplingCollectionId") REFERENCES "SamplingCollection"("id") ON DELETE CASCADE ON UPDATE CASCADE;
