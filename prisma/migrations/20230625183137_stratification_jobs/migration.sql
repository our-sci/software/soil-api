-- CreateEnum
CREATE TYPE "ExternalJobStatus" AS ENUM ('PENDING', 'SUCCESS', 'FAILURE');

-- CreateEnum
CREATE TYPE "StratificationJobCloseReason" AS ENUM ('APPROVED', 'CANCELLED', 'REJECTED', 'TIMED_OUT', 'FAILED_TO_CREATE');

-- CreateTable
CREATE TABLE "StratificationJob" (
    "id" TEXT NOT NULL,
    "jobId" TEXT,
    "areaId" TEXT NOT NULL,
    "error" TEXT,
    "isClosed" BOOLEAN NOT NULL DEFAULT false,
    "closedReason" "StratificationJobCloseReason",
    "externalJobStatus" "ExternalJobStatus",
    "jobResult" JSONB,
    "stratificationConfig" JSONB NOT NULL,
    "stratificationId" TEXT,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "StratificationJob_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "StratificationJob_jobId_key" ON "StratificationJob"("jobId");

-- CreateIndex
CREATE UNIQUE INDEX "StratificationJob_stratificationId_key" ON "StratificationJob"("stratificationId");

-- CreateIndex
CREATE INDEX "StratificationJob_jobId_idx" ON "StratificationJob" USING HASH ("jobId");

-- AddForeignKey
ALTER TABLE "StratificationJob" ADD CONSTRAINT "StratificationJob_areaId_fkey" FOREIGN KEY ("areaId") REFERENCES "Area"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "StratificationJob" ADD CONSTRAINT "StratificationJob_stratificationId_fkey" FOREIGN KEY ("stratificationId") REFERENCES "Stratification"("id") ON DELETE SET NULL ON UPDATE CASCADE;
