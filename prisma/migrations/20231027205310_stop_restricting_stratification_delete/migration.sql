-- DropForeignKey
ALTER TABLE "Sampling" DROP CONSTRAINT "Sampling_locationId_fkey";

-- DropForeignKey
ALTER TABLE "SamplingCollection" DROP CONSTRAINT "SamplingCollection_locationCollectionId_fkey";

-- AddForeignKey
ALTER TABLE "SamplingCollection" ADD CONSTRAINT "SamplingCollection_locationCollectionId_fkey" FOREIGN KEY ("locationCollectionId") REFERENCES "LocationCollection"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Sampling" ADD CONSTRAINT "Sampling_locationId_fkey" FOREIGN KEY ("locationId") REFERENCES "Location"("id") ON DELETE CASCADE ON UPDATE CASCADE;
