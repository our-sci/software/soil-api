-- DropForeignKey
ALTER TABLE "StratificationJob" DROP CONSTRAINT "StratificationJob_areaId_fkey";

-- AddForeignKey
ALTER TABLE "StratificationJob" ADD CONSTRAINT "StratificationJob_areaId_fkey" FOREIGN KEY ("areaId") REFERENCES "Area"("id") ON DELETE CASCADE ON UPDATE CASCADE;
