// This is your Prisma schema file,
// learn more about it in the docs: https://pris.ly/d/prisma-schema

generator client {
  provider = "prisma-client-js"
}

datasource db {
  provider = "postgresql"
  url      = env("DATABASE_URL")
}

model Field {
  id                  String               @id @default(cuid())
  name                String
  alternateName       String?
  producerName        String?
  streetAddress       String?
  addressLocality     String?
  addressRegion       String?
  addressCountry      String?
  postalCode          String?
  contactPoints       Json?
  areas               Area[]
  referenceIds        ReferenceId[]
  createdAt           DateTime             @default(now())
  updatedAt           DateTime             @updatedAt
  group               Group                @relation(fields: [groupId], references: [id], onDelete: Restrict)
  groupId             String
  locationCollections LocationCollection[]
  samplingCollections SamplingCollection[]
  samples             Sample[]
}

model Area {
  id                  String               @id @default(cuid())
  type                String               @default("Feature")
  geometry            Json
  properties          Json?
  createdAt           DateTime             @default(now())
  updatedAt           DateTime             @updatedAt
  group               Group                @relation(fields: [groupId], references: [id], onDelete: Restrict)
  groupId             String
  field               Field                @relation(fields: [fieldId], references: [id], onDelete: Cascade)
  fieldId             String
  stratifications     Stratification[]
  locationCollections LocationCollection[]
  stratificationJob   StratificationJob[]
}

model Stratification {
  id                      String              @id @default(cuid())
  name                    String?
  agent                   String?
  provider                String?
  area                    Area                @relation(fields: [areaId], references: [id], onDelete: Restrict)
  areaId                  String
  algorithm               Algorithm           @relation(fields: [algorithmId], references: [id])
  algorithmId             String
  isDemo                  Boolean             @default(false)
  input                   Json?
  result                  Json?
  group                   Group               @relation(fields: [groupId], references: [id], onDelete: Restrict)
  groupId                 String
  locationCollection      LocationCollection?
  createdAt               DateTime            @default(now())
  updatedAt               DateTime            @updatedAt
  originStratificationJob StratificationJob?
}

model StratificationJob {
  id                   String                        @id @default(cuid())
  jobId                String?                       @unique
  area                 Area                          @relation(fields: [areaId], references: [id], onDelete: Cascade)
  areaId               String
  error                String?
  isClosed             Boolean                       @default(false)
  closedReason         StratificationJobCloseReason?
  externalJobStatus    ExternalJobStatus?
  jobResult            Json?
  autoApprove          Boolean                       @default(false)
  stratificationConfig Json
  stratification       Stratification?               @relation(fields: [stratificationId], references: [id])
  stratificationId     String?                       @unique
  createdAt            DateTime                      @default(now())
  updatedAt            DateTime                      @updatedAt

  @@index([jobId], type: Hash)
}

enum ExternalJobStatus {
  PENDING
  SUCCESS
  FAILURE
}

enum StratificationJobCloseReason {
  APPROVED
  CANCELLED
  REJECTED
  TIMED_OUT
  FAILED_TO_CREATE
}

model Algorithm {
  id              String           @id @default(cuid())
  name            String?
  alternateName   String?
  codeRepository  String?
  version         String?
  doi             String?
  stratifications Stratification[]
  createdAt       DateTime         @default(now())
  updatedAt       DateTime         @updatedAt
}

model LocationCollection {
  id                  String               @id @default(cuid())
  area                Area                 @relation(fields: [areaId], references: [id])
  areaId              String
  stratification      Stratification       @relation(fields: [stratificationId], references: [id], onDelete: Cascade)
  stratificationId    String               @unique
  field               Field                @relation(fields: [fieldId], references: [id], onDelete: Restrict)
  fieldId             String
  group               Group                @relation(fields: [groupId], references: [id], onDelete: Restrict)
  groupId             String
  locations           Location[]
  createdAt           DateTime             @default(now())
  updatedAt           DateTime             @updatedAt
  samplingCollections SamplingCollection[]
}

model Location {
  id                   String             @id @default(cuid())
  locationCollection   LocationCollection @relation(fields: [locationCollectionId], references: [id], onDelete: Cascade)
  locationCollectionId String
  type                 String             @default("Feature")
  geometry             Json
  properties           Json?
  group                Group              @relation(fields: [groupId], references: [id], onDelete: Restrict)
  groupId              String
  createdAt            DateTime           @default(now())
  updatedAt            DateTime           @updatedAt
  samplings            Sampling[]
}

model SamplingCollection {
  id                   String             @id @default(cuid())
  name                 String?
  fieldId              String
  field                Field              @relation(fields: [fieldId], references: [id], onDelete: Restrict)
  locationCollectionId String
  locationCollection   LocationCollection @relation(fields: [locationCollectionId], references: [id], onDelete: Cascade)
  samplings            Sampling[]
  groupId              String
  group                Group              @relation(fields: [groupId], references: [id], onDelete: Restrict)
  soDepth              Json?
  sampleDepths         Json?
  referenceIds         ReferenceId[]
  createdAt            DateTime           @default(now())
  updatedAt            DateTime           @updatedAt
  externalCreatedAt    DateTime
  externalUpdatedAt    DateTime
}

model Sampling {
  id                   String              @id @default(cuid())
  resultTime           DateTime
  geometry             Json
  samples              Sample[]
  locationId           String
  location             Location            @relation(fields: [locationId], references: [id], onDelete: Cascade)
  samplingCollection   SamplingCollection? @relation(fields: [samplingCollectionId], references: [id], onDelete: Cascade)
  samplingCollectionId String?
  comment              String?
  procedures           Json?
  properties           Json?
  soDepth              Json?
  groupId              String
  group                Group               @relation(fields: [groupId], references: [id], onDelete: Restrict)
  createdAt            DateTime            @default(now())
  updatedAt            DateTime            @updatedAt
}

model Sample {
  id         String   @id @default(cuid())
  name       String
  soDepth    Json?
  fieldId    String
  field      Field    @relation(fields: [fieldId], references: [id], onDelete: Restrict)
  samplingId String
  sampling   Sampling @relation(fields: [samplingId], references: [id], onDelete: Cascade)
  groupId    String
  group      Group    @relation(fields: [groupId], references: [id], onDelete: Restrict)
  createdAt  DateTime @default(now())
  updatedAt  DateTime @updatedAt
}

model Group {
  id                  String               @id @default(cuid())
  name                String
  apiOnly             Boolean              @default(false)
  memberships         Membership[]
  subgroups           Group[]              @relation(name: "subgroups")
  parent              Group?               @relation(name: "subgroups", fields: [parentId], references: [id], onDelete: Cascade)
  parentId            String?
  fields              Field[]
  areas               Area[]
  stratifications     Stratification[]
  locationCollections LocationCollection[]
  locations           Location[]
  samplingCollections SamplingCollection[]
  samplings           Sampling[]
  samples             Sample[]
  createdAt           DateTime             @default(now())
  updatedAt           DateTime             @updatedAt
}

model Membership {
  id        String   @id @default(cuid())
  user      User     @relation(fields: [userId], references: [id], onDelete: Cascade)
  group     Group    @relation(fields: [groupId], references: [id], onDelete: Cascade)
  role      Role     @default(USER)
  createdAt DateTime @default(now())
  updatedAt DateTime @updatedAt
  userId    String
  groupId   String

  @@unique([userId, groupId])
}

enum Role {
  USER
}

model User {
  id                  String               @id @default(cuid())
  email               String               @unique
  hashedPassword      String?
  isSuperAdmin        Boolean              @default(false)
  // seen indicates that the user has logged in before
  // seen would be false if a user has been added to a group by another user but never logged in themselves, yet.
  seen                Boolean              @default(false)
  createdAt           DateTime             @default(now())
  updatedAt           DateTime             @updatedAt
  memberships         Membership[]
  apiKeys             ApiKey[]
  authTokens          AuthToken[]
  passwordResetTokens PasswordResetToken[]

  @@index([email], type: Hash)
}

model ReferenceId {
  id                   String              @id @default(cuid())
  owner                String
  value                String
  createdAt            DateTime            @default(now())
  updatedAt            DateTime            @updatedAt
  field                Field?              @relation(fields: [fieldId], references: [id], onDelete: Cascade)
  fieldId              String?
  samplingCollection   SamplingCollection? @relation(fields: [samplingCollectionId], references: [id], onDelete: Cascade)
  samplingCollectionId String?
}

model ApiKey {
  id        String   @id @default(cuid())
  label     String
  hashedKey String   @unique
  userId    String
  user      User     @relation(fields: [userId], references: [id], onDelete: Cascade)
  createdAt DateTime @default(now())
  updatedAt DateTime @updatedAt

  @@unique([id, userId])
  @@index([hashedKey], type: Hash)
}

model AuthToken {
  id          String   @id @default(cuid())
  hashedToken String   @unique
  userId      String
  user        User     @relation(fields: [userId], references: [id], onDelete: Cascade)
  createdAt   DateTime @default(now())
  updatedAt   DateTime @updatedAt

  @@index([hashedToken], type: Hash)
}

model PasswordResetToken {
  id          String   @id @default(cuid())
  hashedToken String   @unique
  expiresAt   DateTime
  userId      String
  user        User     @relation(fields: [userId], references: [id], onDelete: Cascade)
  createdAt   DateTime @default(now())
  updatedAt   DateTime @updatedAt

  @@index([hashedToken], type: Hash)
}
