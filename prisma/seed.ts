// we want the same behavior that the e2e tests have for creating users
(global as any).__E2E_TESTS__ = true;

import prisma from '../src/prismaClient';
import {
	createMockStratificationConfig,
	postMockField,
	postMockSamplingCollectionAndDependencies,
	postMockStratificationAndDependencies,
} from '../src/test-utils';
import { seedDatabase } from '../src/test-utils/e2e';
import { stratServiceJobToPrismaStratificationJobCreate } from '../src/transforms';

async function main() {
	// setup 2 groups and 2 users
	await seedDatabase();

	const fieldResponses: any[] = [];
	for (let i = 0; i < 20; i++) {
		try {
			const fieldResponse = await postMockField();
			fieldResponses.push(fieldResponse);
		} catch (e) {
			console.error('Error seeding field:', e);
		}
	}

	for (let i = 0; i < 20; i++) {
		try {
			await postMockStratificationAndDependencies();
		} catch (e) {
			console.error('Error seeding stratified field:', e);
		}
	}

	for (let i = 0; i < 20; i++) {
		try {
			await postMockSamplingCollectionAndDependencies();
		} catch (e) {
			console.error('Error seeding sampled field:', e);
		}
	}

	// closed, successful stratification job
	await prisma.stratificationJob.create({
		data: stratServiceJobToPrismaStratificationJobCreate({
			jobId: 'mock-job-id',
			jobStatus: 'SUCCESS',
			areaId: fieldResponses[0].body.areas[0].id,
			stratificationConfig: createMockStratificationConfig(),
			isClosed: true,
			closedReason: 'CANCELLED',
		}),
	});
	// pending stratification job
	await prisma.stratificationJob.create({
		data: stratServiceJobToPrismaStratificationJobCreate({
			jobId: 'mock-job-id-2',
			jobStatus: 'PENDING',
			areaId: fieldResponses[1].body.areas[0].id,
			stratificationConfig: createMockStratificationConfig(),
			isClosed: false,
		}),
	});
	// open, successful stratification job
	await prisma.stratificationJob.create({
		data: {
			...stratServiceJobToPrismaStratificationJobCreate({
				jobId: 'mock-job-id-3',
				jobStatus: 'SUCCESS',
				areaId: fieldResponses[2].body.areas[0].id,
				stratificationConfig: createMockStratificationConfig(),
				isClosed: false,
			}),
			jobResult: {
				locations: [
					{
						type: 'Feature',
						geometry: {
							type: 'Point',
							coordinates: [0, 0],
						},
					},
				],
				algorithm: {},
				input: [],
				result: [],
			},
		},
	});
}

main()
	.then(async () => {
		console.log('Seed script completed successfully');
	})
	.catch(async (e) => {
		console.error('Seed script failed', e);
	})
	.finally(async () => {
		console.log('Exiting seed script');
	});
