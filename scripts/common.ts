const GENERATED_TYPES_PATH = '../src/types/generated/openapi-types.ts';
const GENERATED_STRATIFICATION_SERVICE_TYPES_PATH = '../src/types/generated/stratification-service-types.ts';

export { GENERATED_STRATIFICATION_SERVICE_TYPES_PATH, GENERATED_TYPES_PATH };
