import fs from 'fs';
import path from 'path';
import openapiTS from 'openapi-typescript';
import StratificationServiceOpenApiSpec from '@soilstack/stratification-service/api/open-api-spec/open-api-spec.json';
import { openApiSpecDocument } from '../src/open-api-spec/index';
import { GENERATED_STRATIFICATION_SERVICE_TYPES_PATH, GENERATED_TYPES_PATH } from './common';

const soilstackTypesPath = path.join(__dirname, GENERATED_TYPES_PATH);
const stratificationServiceTypesPath = path.join(__dirname, GENERATED_STRATIFICATION_SERVICE_TYPES_PATH);

const pathAndSpecToCheck = [
	{ path: soilstackTypesPath, openApiSpec: openApiSpecDocument },
	{ path: stratificationServiceTypesPath, openApiSpec: StratificationServiceOpenApiSpec },
];

(async () => {
	pathAndSpecToCheck.forEach(async ({ path, openApiSpec }) => {
		const latestGeneratedTypes = await openapiTS(openApiSpec as any);
		const currentGeneratedTypes = fs.readFileSync(path, 'utf8');
		if (latestGeneratedTypes !== currentGeneratedTypes) {
			throw new Error('Drift detected in types generated from open api spec. Run `yarn generate-types` to update.');
		}
	});
})();
