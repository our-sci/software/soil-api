import fs from 'fs';
import path from 'path';
import openapiTS from 'openapi-typescript';
import StratificationServiceOpenApiSpec from '@soilstack/stratification-service/api/open-api-spec/open-api-spec.json';
import { openApiSpecDocument } from '../src/open-api-spec/index';
import { GENERATED_STRATIFICATION_SERVICE_TYPES_PATH, GENERATED_TYPES_PATH } from './common';

(async () => {
	const soilstackTypesOutput = await openapiTS(openApiSpecDocument as any);
	const soilstackTypesAbsolutePath = path.join(__dirname, GENERATED_TYPES_PATH);

	const stratificationServiceTypesOutput = await openapiTS(StratificationServiceOpenApiSpec as any);
	const stratificationServiceTypesAbsolutePath = path.join(__dirname, GENERATED_STRATIFICATION_SERVICE_TYPES_PATH);

	fs.writeFileSync(soilstackTypesAbsolutePath, soilstackTypesOutput);
	fs.writeFileSync(stratificationServiceTypesAbsolutePath, stratificationServiceTypesOutput);
})();
