import compression from 'compression';
import cors from 'cors';
import express from 'express';
import { errorHandler } from './errors';
import { initLogging } from './logging';
import { authenticateUser } from './middlewares';
import setupOpenApiSpec from './open-api-spec';
import { routes } from './routes';

function createApp() {
	const app = express();

	app.use(compression());

	app.use(initLogging);

	app.use(cors());

	app.use('/health', (req, res) => {
		res.status(200).send();
		return;
	});

	// set up body parsers prior to registering endpoints
	app.use(express.json({ limit: '300kb' }));

	setupOpenApiSpec(app);

	app.use(authenticateUser);

	// register endpoints
	Object.entries(routes).forEach(([path, router]) => {
		app.use(path, router);
	});

	// register error handler last
	app.use(errorHandler);

	return app;
}

export default createApp;
