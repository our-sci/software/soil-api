/*
	NOTE: Environment variables are not configured in the same way for local development and actual deployments.
	
	For local devleopment, dotenv.config() will pull in environment variables from a .env file in the root of
	the project if one is present. If environment variables specified in the .env file are already set on the machine,
	dotenv.config() will NOT OVERRIDE the variables set on the machine with the variables in the .env file.
	
	For deployed environments, environment variables will NOT come directly from a .env file. They will be present on
	the machine and configured at the time of deployment by ci/cd scripts. This means dotenv.config() will have no
	effect for deployed environments.
*/
import dotenv from 'dotenv';
dotenv.config({
	path: process.env.NODE_ENV === 'test' ? '.env.jest' : '.env',
});

const expectedEnvironmentVariables = [
	'elasticEnableLogging',
	'elasticIndexName',
	'elasticCloudId',
	'elasticAuthUsername',
	'elasticAuthPassword',
	'internalApiDocsUrl',
	'DATABASE_URL',
	'saltRounds',
	'sesAccessKeyId',
	'sesSecretAccessKey',
	'enableEmail',
	'appUrl',
	'stratificationServiceUrl',
	'stratificationServiceKey',
	'selfUrl',
];

expectedEnvironmentVariables.forEach((envVar) => {
	if (process.env[envVar] === '' || process.env[envVar] === undefined) {
		throw new Error(
			`Required environment variable ${envVar} is not defined. Make sure it is defined in either a .env file or as an environment variable.`
		);
	}
});

export const ELASTIC_ENABLE_LOGGING: boolean = process.env.elasticEnableLogging === 'true';
export const ELASTIC_INDEX_NAME: string = process.env.elasticIndexName as string;
export const ELASTIC_CLOUD_ID: string = process.env.elasticCloudId as string;
export const ELASTIC_AUTH_USERNAME: string = process.env.elasticAuthUsername as string;
export const ELASTIC_AUTH_PASSWORD: string = process.env.elasticAuthPassword as string;

export const INTERNAL_API_DOCS_URL: string = process.env.internalApiDocsUrl as string;

export const DATABASE_URL: string = process.env.DATABASE_URL as string;

export const SALT_ROUNDS: number = parseInt(process.env.saltRounds as string, 10);

export const SES_ACCESS_KEY_ID: string = process.env.sesAccessKeyId as string;
export const SES_SECRET_ACCESS_KEY: string = process.env.sesSecretAccessKey as string;
export const ENABLE_EMAIL: boolean = process.env.enableEmail === 'true';

export const APP_URL: string = process.env.appUrl as string;

export const STRATIFICATION_SERVICE_URL: string = process.env.stratificationServiceUrl as string;
export const STRATIFICATION_SERVICE_KEY: string = process.env.stratificationServiceKey as string;
export const STRATIFICATION_JOB_TIMEOUT_MS: number = process.env.stratificationJobTimeoutMs
	? parseInt(process.env.stratificationJobTimeoutMs as string, 10)
	: 48 * 60 * 60 * 1000; // 48 hours

export const SELF_URL: string = process.env.selfUrl as string;

export enum ResourceTypes {
	Area = 'Area',
	Field = 'Field',
	Stratification = 'Stratification',
	LocationCollection = 'LocationCollection',
	Location = 'Location',
	Sample = 'Sample',
	Sampling = 'Sampling',
	SamplingCollection = 'SamplingCollection',
}
