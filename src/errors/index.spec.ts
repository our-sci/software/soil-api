import request from 'supertest';
import { getDefaultUserBearerToken, testApp } from '../test-utils';

describe('errorHandler', () => {
	it('returns open api schema validation errors as json with "message" and "errors" properties', (done) => {
		request(testApp)
			.post('/fields')
			.set('Authorization', getDefaultUserBearerToken())
			.send({})
			.end((err, res) => {
				const errorObject = JSON.parse(res.text);
				expect(errorObject).toHaveProperty('errors');
				expect(errorObject).toHaveProperty('message');
				done();
			});
	});

	it('returns 400 when requests violate open api schema validation', (done) => {
		request(testApp).post('/fields').set('Authorization', getDefaultUserBearerToken()).send({}).expect(400, done);
	});

	describe('routes requiring authorization', () => {
		it('returns 401 when Authorization header is not included', (done) => {
			request(testApp).post('/fields').send({}).expect(401, done);
		});
	});
});
