import { NextFunction, Request, Response } from 'express';

// eslint-disable-next-line unused-imports/no-unused-vars
const errorHandler = (err: any, req: Request, res: Response, next: NextFunction): any => {
	const isReturnableHttpError = Boolean(err?.status && err?.message);

	if (isReturnableHttpError) {
		res.status(err.status).json({
			message: err.message,
			errors: err.errors,
		});
	} else {
		// log errors during development and in production
		// but, do not log them during testing as it pollutes the test output
		if (process.env.NODE_ENV !== 'test') {
			console.log('Unhandled Error:', err);
		}
		res.status(500).send();
	}
};

export { errorHandler };
