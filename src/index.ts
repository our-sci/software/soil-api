import createApp from './app';

async function main() {
	const app = createApp();

	app.listen(3000, () => {
		console.log('Soil API listening at http://localhost:3000');
	});
}

main();
