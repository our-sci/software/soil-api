import { NextFunction, Request, Response } from 'express';
import mung from 'express-mung';
import { v4 as uuidv4 } from 'uuid';
import { ELASTIC_ENABLE_LOGGING } from '../constants';
import { log } from '../services/logging';

function initializeLoggingMessage(req: Request) {
	if (req?.body) {
		if (req?.body?.password) {
			req.body.password = 'REDACTED';
		}
		if (req?.body?.newPassword) {
			req.body.newPassword = 'REDACTED';
		}
		if (req?.body?.currentPassword) {
			req.body.currentPassword = 'REDACTED';
		}
		if (req?.body?.token) {
			req.body.token = 'REDACTED';
		}
	}
	return {
		method: req.method,
		path: req.path,
		requestBody: JSON.stringify(req.body),
		query: req.query,
		ip: req.ip,
		originalUrl: req.originalUrl,
		userAgent: req.get('User-Agent'),
		authorizationHeaderScheme: req.get('Authorization')?.split(' ')[0],
		correlationId: req.get('X-Correlation-Id') ?? uuidv4(),
		timestamp: new Date(),
		responseBody: null,
		httpStatus: null,
		duration: null,
		userEmail: null,
	};
}

function initLogging(req: Request, res: Response, next: NextFunction): void {
	if (ELASTIC_ENABLE_LOGGING) {
		res.locals.loggingMessage = initializeLoggingMessage(req);
		res.on('finish', function () {
			if (res?.locals?.user?.email) {
				res.locals.loggingMessage.userEmail = res.locals.user.email;
			}
			res.locals.loggingMessage.httpStatus = res.statusCode;
			res.locals.loggingMessage.duration = Date.now() - res.locals.loggingMessage.timestamp.valueOf();
			log(res.locals.loggingMessage);
		});
	}
	next();
}

// Logging response bodies can use significant amounts of storage.
// Consider log volume, index lifecycle policy (duration to keep logs), and available storage before using this middleware.
const appendResponseBodyToLoggingMessage = mung.json(
	(resBody, req, res) => {
		if (ELASTIC_ENABLE_LOGGING) {
			const addressedThisTodo = false;
			if (!addressedThisTodo) {
				throw new Error('The TODO below must be resolved before using this');
			}
			// TODO: redact sensitive information from response bodies before adding to logging message
			res.locals.loggingMessage.resBody = JSON.stringify(resBody);
		}
	},
	{ mungError: true }
);

export { appendResponseBodyToLoggingMessage, initLogging };
