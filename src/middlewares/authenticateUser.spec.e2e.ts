import request from 'supertest';
import { testApp } from '../test-utils';
import { randomHex } from '../utils';

describe('authenticateUser middleware', () => {
	describe('when Authorization header scheme is apikey', () => {
		it('returns a 401 if apikey is not found', async (done) => {
			request(testApp).get('/user').set('Authorization', `apikey ${randomHex()}`).send().expect(401, done);
		});
	});

	describe('when Authorization header scheme is bearer', () => {
		it('returns a 401 if authtoken is not found', async (done) => {
			request(testApp).get('/user').set('Authorization', `bearer ${randomHex()}`).send().expect(401, done);
		});
	});
});
