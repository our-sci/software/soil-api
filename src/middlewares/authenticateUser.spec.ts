import { prismaMock } from '../singleton';
import {
	createMockPrismaApiKeyWithUserWithMemberships,
	createMockPrismaAuthTokenWithUserWithMemberships,
} from '../test-utils';
import { hash, randomHex } from '../utils';
import authenticateUser from './authenticateUser';

// TODO: move this utility to a test utils file
// TODO: find places we're creating mock request by hand and use this utility instead
const createMockRequest = ({
	headers = {},
	path = '/',
	method = 'GET',
}): {
	headers: { [key: string]: string };
	get: (header: string) => string | undefined;
	path?: string;
	method?: string;
	openapi: {
		openApiRoute: string;
	};
} => ({
	headers,
	path,
	method,
	get(header: string) {
		return this.headers[header];
	},
	openapi: {
		openApiRoute: path,
	},
});

describe('authenticateUser middleware', () => {
	describe('when endpoint is unsecured (has no security schemes defined in the open api spec)', () => {
		it('short circuits', async () => {
			const mockRequest = createMockRequest({
				headers: {},
				path: '/auth/register',
				method: 'POST',
			});
			const nextSpy = jest.fn();

			await authenticateUser(mockRequest as any, {} as any, nextSpy);

			// assert that next is called with nothing
			// authenticateUser will call next with an error if you make a request to
			// a secured endpoint without an Authorization header
			expect(nextSpy).toHaveBeenCalledWith();
		});
	});

	describe('when Authorization header scheme is apikey', () => {
		it('looks up the user by apikey and puts the user and authorization scheme on res.locals', async () => {
			const mockApiKey = randomHex();
			const mockHashedApiKey = hash(mockApiKey);
			prismaMock.apiKey.findFirst.mockImplementation(((args: any) => {
				if (args.where.hashedKey === mockHashedApiKey) {
					return createMockPrismaApiKeyWithUserWithMemberships();
				} else {
					return Promise.resolve(null);
				}
			}) as any);
			const mockRequest = createMockRequest({
				headers: {
					authorization: `apikey ${mockApiKey}`,
				},
			});
			const mockResponse = {
				status: jest.fn(() => ({ send: jest.fn() })),
				locals: {},
			};
			const nextSpy = jest.fn();

			await authenticateUser(mockRequest as any, mockResponse as any, nextSpy);

			expect(mockResponse.locals).toHaveProperty(
				'user',
				expect.objectContaining({
					id: 'seed-user-1',
					email: 'seedUser1@soilstack.io',
				})
			);
			expect(mockResponse.locals).toHaveProperty('authorizationScheme', 'apikey');
			expect(nextSpy).toHaveBeenCalled();
		});

		it('calls next with error if prisma.apiKey.findFirst throws an unexpected error', async (done) => {
			const mockError = new Error('Unexpected error');
			prismaMock.apiKey.findFirst.mockRejectedValueOnce(mockError);

			const mockRequest = createMockRequest({
				headers: {
					authorization: `apikey ${randomHex()}`,
				},
			});
			const mockResponse = {
				status: jest.fn(() => ({ send: jest.fn() })),
				locals: {},
			};
			const nextSpy = jest.fn();

			await authenticateUser(mockRequest as any, mockResponse as any, nextSpy);

			expect(nextSpy).toHaveBeenCalledWith(mockError);
			done();
		});
	});

	describe('when Authorization header scheme is bearer', () => {
		it('looks up the user by auth token and puts the user and authorization scheme on res.locals', async (done) => {
			const mockAuthToken = randomHex();
			const mockHashedAuthToken = hash(mockAuthToken);
			prismaMock.authToken.findFirst.mockImplementation(((args: any) => {
				if (args.where.hashedToken === mockHashedAuthToken) {
					return createMockPrismaAuthTokenWithUserWithMemberships();
				} else {
					return Promise.resolve(null);
				}
			}) as any);
			const mockRequest = createMockRequest({
				headers: {
					authorization: `bearer ${mockAuthToken}`,
				},
			});
			const mockResponse = {
				status: jest.fn(() => ({ send: jest.fn() })),
				locals: {},
			};
			const nextSpy = jest.fn();

			await authenticateUser(mockRequest as any, mockResponse as any, nextSpy);

			expect(mockResponse.locals).toHaveProperty(
				'user',
				expect.objectContaining({
					id: 'seed-user-1',
					email: 'seedUser1@soilstack.io',
				})
			);
			expect(mockResponse.locals).toHaveProperty('authorizationScheme', 'bearer');
			expect(nextSpy).toHaveBeenCalled();
			done();
		});

		it('throws an error if prisma.authToken.findFirst throws an unexpected error', async (done) => {
			const mockError = new Error('Unexpected error');
			prismaMock.authToken.findFirst.mockRejectedValueOnce(mockError);

			const mockRequest = createMockRequest({
				headers: {
					authorization: `bearer ${randomHex()}`,
				},
			});
			const mockResponse = {
				status: jest.fn(() => ({ send: jest.fn() })),
				locals: {},
			};
			const nextSpy = jest.fn();

			await authenticateUser(mockRequest as any, mockResponse as any, nextSpy);

			expect(nextSpy).toHaveBeenCalledWith(mockError);
			done();
		});
	});
});
