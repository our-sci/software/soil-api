import { NextFunction, Request, Response } from 'express';
import { isUnsecuredEndpoint } from '../open-api-spec';
import prisma from '../prismaClient';
import { hash } from '../utils';

const authenticateUser = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
	try {
		if (isUnsecuredEndpoint(req as any)) {
			next();
			return;
		}

		const [scheme, token] = req.get('authorization')?.split(' ') ?? [];
		res.locals.authorizationScheme = scheme.toLowerCase();

		if (res.locals.authorizationScheme === 'bearer') {
			const hashedToken = hash(token);
			const authTokenWithUser = await prisma.authToken.findFirst({
				where: { hashedToken },
				include: {
					user: {
						include: {
							memberships: true,
						},
					},
				},
			});
			if (authTokenWithUser?.user) {
				res.locals.user = authTokenWithUser.user;
			}
		}

		if (res.locals.authorizationScheme === 'apikey') {
			const hashedKey = hash(token);
			const apiKeyWithUser = await prisma.apiKey.findFirst({
				where: { hashedKey },
				include: {
					user: {
						include: {
							memberships: true,
						},
					},
				},
			});
			if (apiKeyWithUser?.user) {
				res.locals.user = apiKeyWithUser.user;
			}
		}

		if (!res.locals.user) {
			res.status(401).send();
			return;
		}

		next();
	} catch (error) {
		next(error);
	}
};

export default authenticateUser;
