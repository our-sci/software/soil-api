import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';

export const groupIdQueryParam: OpenAPIV3.ParameterObject = {
	name: 'groupId',
	in: 'query',
	required: true,
	schema: {
		type: 'string',
	},
};
