import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';

const Algorithm: OpenAPIV3.SchemaObject = {
	type: 'object',
	additionalProperties: false,
	properties: {
		name: { type: 'string' },
		alternateName: { type: 'string' },
		codeRepository: { type: 'string' },
		version: { type: 'string' },
		doi: { type: 'string' },
	},
};

export { Algorithm };
