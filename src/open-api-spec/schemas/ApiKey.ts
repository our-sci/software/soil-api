import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';

export const ApiKey: OpenAPIV3.SchemaObject = {
	type: 'object',
	required: ['id', 'label'],
	additionalProperties: false,
	properties: {
		id: { type: 'string' },
		label: { type: 'string' },
	},
};

export const PostApiKeyResponse: OpenAPIV3.SchemaObject = {
	type: 'object',
	required: [...(ApiKey.required ?? []), 'apiKey'],
	additionalProperties: false,
	properties: {
		...ApiKey.properties,
		apiKey: { type: 'string' },
	},
};
