import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';

const drawn: OpenAPIV3.SchemaObject = { type: 'boolean' };

const EditableAreaProperties: OpenAPIV3.SchemaObject = {
	type: 'object',
	additionalProperties: false,
	properties: {
		name: { type: 'string' },
		description: { type: 'string' },
	},
};

const AreaProperties: OpenAPIV3.SchemaObject = {
	type: 'object',
	additionalProperties: false,
	properties: {
		...EditableAreaProperties.properties,
		drawn,
	},
};

const ResponseAreaProperties: OpenAPIV3.SchemaObject = {
	type: 'object',
	additionalProperties: false,
	properties: {
		...AreaProperties.properties,
		drawn,
		featureOfInterest: {
			type: 'string',
			description: 'The Field (id) that the area belongs to.',
		},
	},
};

export { AreaProperties, EditableAreaProperties, ResponseAreaProperties };
