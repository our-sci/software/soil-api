import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';

const ResponseGroup: OpenAPIV3.SchemaObject = {
	type: 'object',
	required: ['id', 'path'],
	additionalProperties: false,
	properties: {
		id: { type: 'string' },
		name: { type: 'string' },
		path: { type: 'string' },
		apiOnly: { type: 'boolean' },
		meta: {
			type: 'object',
			additionalProperties: false,
			properties: {
				submittedAt: { type: 'string', format: 'date-time' },
			},
		},
	},
};

const CreateGroup: OpenAPIV3.SchemaObject = {
	type: 'object',
	required: ['name'],
	additionalProperties: false,
	minProperties: 1,
	properties: {
		name: { type: 'string', minLength: 1 },
		apiOnly: {
			'x-internal': true,
			type: 'boolean',
		} as OpenAPIV3.NonArraySchemaObject,
	},
};

const { required, ...PatchGroup } = CreateGroup;

export { CreateGroup, PatchGroup, ResponseGroup };
