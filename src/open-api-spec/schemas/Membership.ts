import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';

const ResponseMembership: OpenAPIV3.SchemaObject = {
	type: 'object',
	required: ['id', 'groupId', 'role', 'user'],
	additionalProperties: false,
	properties: {
		id: { type: 'string' },
		groupId: { type: 'string' },
		role: { type: 'string' },
		user: {
			type: 'object',
			additionalProperties: false,
			properties: {
				id: { type: 'string' },
				email: { type: 'string', format: 'email' },
				seen: { type: 'boolean' },
			},
		},
	},
};

export { ResponseMembership };
