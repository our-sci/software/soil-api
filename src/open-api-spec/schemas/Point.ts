import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';

const Point: OpenAPIV3.SchemaObject = {
	type: 'object',
	additionalProperties: false,
	required: ['type', 'coordinates'],
	properties: {
		type: { type: 'string', default: 'Point', enum: ['Point'] },
		coordinates: { $ref: '#/components/schemas/Position' },
	},
};

export { Point };
