import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';

const Position: OpenAPIV3.SchemaObject = {
	type: 'array',
	minItems: 2,
	maxItems: 3,
	items: {
		type: 'number',
	},
};

export default Position;
