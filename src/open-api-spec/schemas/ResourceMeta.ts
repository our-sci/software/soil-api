import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';

const ReferenceIdOwners: OpenAPIV3.SchemaObject = {
	type: 'string',
	enum: [
		'surveystack',
		'heartland',
		'soilstack-draft',
		'carbofarm',
		'flurosense-field',
		'flurosense-producer',
		'flurosense-program',
		'flurosense-year',
		'holganix',
		'holganix-company',
		'holganix-farm',
		'holganix-name',
	],
};

const ResourceReferenceIds: OpenAPIV3.SchemaObject = {
	type: 'array',
	items: {
		type: 'object',
		required: ['owner', 'id'],
		properties: {
			owner: {
				$ref: '#/components/schemas/ReferenceIdOwners',
			},
			id: { type: 'string' },
		},
	},
};

const metaProperties: { [key: string]: OpenAPIV3.SchemaObject } = {
	groupId: { type: 'string' },
	submittedAt: { type: 'string', format: 'date-time' },
};

const ResourceMeta: OpenAPIV3.SchemaObject = {
	type: 'object',
	additionalProperties: false,
	properties: {
		referenceIds: {
			$ref: '#/components/schemas/ResourceReferenceIds',
		},
	},
};

const ResponseResourceMeta: OpenAPIV3.SchemaObject = {
	type: 'object',
	additionalProperties: false,
	properties: {
		referenceIds: { $ref: '#/components/schemas/ResourceReferenceIds' },
		...metaProperties,
	},
};

const ResponseResourceMetaNoRefIds: OpenAPIV3.SchemaObject = {
	type: 'object',
	additionalProperties: false,
	properties: metaProperties,
};

export { ReferenceIdOwners, ResourceMeta, ResourceReferenceIds, ResponseResourceMeta, ResponseResourceMetaNoRefIds };
