import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';

const Sample: OpenAPIV3.SchemaObject = {
	type: 'object',
	required: ['soDepth', 'name'],
	additionalProperties: false,
	properties: {
		name: { type: 'string' },
		soDepth: { $ref: '#/components/schemas/SoDepth' },
	},
};

const ResponseSample: OpenAPIV3.SchemaObject = {
	...Sample,
	required: ['id', 'name', 'sampleOf', 'resultOf', 'soDepth', 'meta'],
	properties: {
		id: { type: 'string' },
		...Sample.properties,
		resultOf: {
			type: 'string',
			description: 'The Sampling the sample belongs to.',
		},
		sampleOf: {
			type: 'string',
			description: 'The Field the sample was taken in.',
		},
		meta: { $ref: '#/components/schemas/ResponseResourceMetaNoRefIds' },
	},
};

const EditableSample: OpenAPIV3.SchemaObject = {
	type: 'object',
	additionalProperties: false,
	properties: {
		name: Sample.properties?.name as OpenAPIV3.SchemaObject,
		soDepth: { $ref: '#/components/schemas/SoDepth' },
	},
};

export { EditableSample, ResponseSample, Sample };
