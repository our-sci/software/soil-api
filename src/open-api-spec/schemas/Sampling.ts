import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';

const Sampling: OpenAPIV3.SchemaObject = {
	type: 'object',
	required: ['geometry', 'resultTime', 'soDepth', 'featureOfInterest'],
	additionalProperties: false,
	properties: {
		resultTime: { type: 'string', format: 'date-time' },
		geometry: { $ref: '#/components/schemas/Point' },
		featureOfInterest: {
			type: 'string',
			description: 'The Location (id) this sampling was created at.',
		},
		results: {
			type: 'array',
			items: { type: 'string' },
			description: 'The Samples (ids) belonging to the sampling.',
		},
		comment: { type: 'string' },
		procedures: { type: 'array', items: { type: 'string' } },
		properties: { type: 'object' },
		soDepth: { $ref: '#/components/schemas/SoDepth' },
	},
};

const ResponseSampling: OpenAPIV3.SchemaObject = {
	...Sampling,
	required: [
		'id',
		'resultTime',
		'geometry',
		'featureOfInterest',
		'memberOf',
		'results',
		'comment',
		'procedures',
		'properties',
		'soDepth',
		'meta',
	],
	properties: {
		id: { type: 'string' },
		...Sampling.properties,
		memberOf: {
			type: 'string',
			description: 'The Sampling Collection (id) this sampling belongs to.',
		},
		meta: { $ref: '#/components/schemas/ResponseResourceMetaNoRefIds' },
	},
};

const PopulatedSampling: OpenAPIV3.SchemaObject = {
	...Sampling,
	properties: {
		...Sampling.properties,
		results: {
			type: 'array',
			items: { $ref: '#/components/schemas/Sample' },
			description: 'The Samples belonging to the sampling.',
		},
	},
};

export { PopulatedSampling, ResponseSampling, Sampling };
