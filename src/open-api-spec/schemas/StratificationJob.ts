import { ExternalJobStatus, StratificationJobCloseReason } from '@prisma/client';
import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';

const StratificationJob: OpenAPIV3.SchemaObject = {
	type: 'object',
	required: ['id', 'areaId', 'isClosed', 'stratificationConfig', 'createdAt', 'updatedAt', 'autoApprove'],
	additionalProperties: false,
	properties: {
		id: { type: 'string' },
		areaId: { type: 'string' },
		error: { type: 'string' },
		isClosed: { type: 'boolean' },
		closedReason: { type: 'string', enum: Object.keys(StratificationJobCloseReason) },
		externalJobStatus: { type: 'string', enum: Object.keys(ExternalJobStatus) },
		jobResult: { $ref: '#/components/schemas/StratificationServiceStratification' },
		autoApprove: { type: 'boolean' },
		stratificationConfig: { type: 'string' },
		stratificationId: { type: 'string' },
		createdAt: { type: 'string', format: 'date-time' },
		updatedAt: { type: 'string', format: 'date-time' },
	},
};

export { StratificationJob };
