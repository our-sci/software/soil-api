import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';

const StratificationResult: OpenAPIV3.SchemaObject = {
	type: 'object',
	additionalProperties: false,
	properties: {
		name: { type: 'string' },
		/*
			Specifying an empty object here allows `value` to be any type.
			We want to accept any valid JSON for `value`, so this meets that need.
			However, `openapi-typescript` doesn't know what to do with this,
			and assigns `value` a type of `unknown` when generating typescript types from the open api schema.
			That causes us to use `as unknown as` when transforming requests to prisma inputs.
			To avoid the need for `as unknown as`, we should make the open api spec explicit
			by enumerating all valid JSON types here instead of specifying an empty object.
		*/
		value: {} as OpenAPIV3.SchemaObject,
	},
};

export { StratificationResult };
