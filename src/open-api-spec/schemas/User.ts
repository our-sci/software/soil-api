import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';

export const ResponseUser: OpenAPIV3.SchemaObject = {
	type: 'object',
	additionalProperties: false,
	required: ['id', 'email', 'seen', 'apiKeys'],
	properties: {
		id: { type: 'string' },
		email: { type: 'string', format: 'email' },
		seen: { type: 'boolean' },
		apiKeys: {
			type: 'array',
			items: { $ref: '#/components/schemas/ApiKey' },
		},
	},
};
