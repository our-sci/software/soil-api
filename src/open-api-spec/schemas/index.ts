import StratificationServiceOpenApiSpec from '@soilstack/stratification-service/api/open-api-spec/open-api-spec.json';
import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';
import Address from './Address';
import { Algorithm } from './Algorithm';
import { ApiKey, PostApiKeyResponse } from './ApiKey';
import { Area, EditableArea, ResponseArea } from './Area';
import { AreaProperties, EditableAreaProperties, ResponseAreaProperties } from './AreaProperties';
import ContactPoint from './ContactPoint';
import Error from './Error';
import { EditableField, Field, PopulatedField, ResponseField } from './Field';
import { CreateGroup, PatchGroup, ResponseGroup } from './Group';
import { Location, ResponseLocation } from './Location';
import { LocationCollection, PopulatedLocationCollection, ResponseLocationCollection } from './LocationCollection';
import { ResponseMembership } from './Membership';
import MultiPolygon from './MultiPolygon';
import { Point } from './Point';
import Polygon from './Polygon';
import Position from './Position';
import {
	ReferenceIdOwners,
	ResourceMeta,
	ResourceReferenceIds,
	ResponseResourceMeta,
	ResponseResourceMetaNoRefIds,
} from './ResourceMeta';
import { EditableSample, ResponseSample, Sample } from './Sample';
import { PopulatedSampling, ResponseSampling, Sampling } from './Sampling';
import { PopulatedSamplingCollection, ResponseSamplingCollection, SamplingCollection } from './SamplingCollection';
import { SoDepth, SoDepthValue } from './SoDepth';
import { ResponseStratification, Stratification } from './Stratification';
import { StratificationInput } from './StratificationInput';
import { StratificationJob } from './StratificationJob';
import { StratificationResult } from './StratificationResult';
import { ResponseUser } from './User';

const prefixRefs = (schemas: { [key: string]: OpenAPIV3.SchemaObject }, prefix = 'StratificationService') => {
	const prefixedTopLevel = Object.fromEntries(
		Object.entries(schemas).map(([key, value]) => [`StratificationService${key}`, value])
	);
	const json = JSON.stringify(prefixedTopLevel);
	const prefixedTopLevelAndRefs = json.replace(
		/#\/components\/schemas\/([a-zA-Z0-9]+)/g,
		`#/components/schemas/${prefix}$1`
	);
	return JSON.parse(prefixedTopLevelAndRefs);
};

const namespacedStratificationServiceSchemas = prefixRefs(
	(StratificationServiceOpenApiSpec.components.schemas as unknown) as { [key: string]: OpenAPIV3.SchemaObject }
);

const schemas: { [key: string]: OpenAPIV3.SchemaObject } = {
	Address,
	ContactPoint,
	Field,
	ResponseField,
	PopulatedField,
	EditableField,
	AreaProperties,
	EditableAreaProperties,
	ResponseAreaProperties,
	Area,
	ResponseArea,
	EditableArea,
	Polygon,
	MultiPolygon,
	Position,
	Point,
	ResourceMeta,
	ResponseResourceMeta,
	ResponseResourceMetaNoRefIds,
	ResourceReferenceIds,
	ReferenceIdOwners,
	Algorithm,
	Stratification,
	StratificationInput,
	StratificationResult,
	ResponseStratification,
	LocationCollection,
	ResponseLocationCollection,
	PopulatedLocationCollection,
	Location,
	ResponseLocation,
	SoDepthValue,
	SoDepth,
	Sample,
	ResponseSample,
	EditableSample,
	Sampling,
	ResponseSampling,
	PopulatedSampling,
	SamplingCollection,
	ResponseSamplingCollection,
	PopulatedSamplingCollection,
	ResponseGroup,
	CreateGroup,
	PatchGroup,
	Error,
	ResponseUser,
	ResponseMembership,
	ApiKey,
	PostApiKeyResponse,
	StratificationJob,
	...namespacedStratificationServiceSchemas,
};

export default schemas;
