import { Request } from 'express';
import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';
import { checkAuth, createIsUnsecuredEndpoint, getUnsecuredEndpoints, removeUnusedTags } from './utils';

describe('removeUnusedTags', () => {
	it('removes tags from the top level of a spec document if they are not used by any endpoints', () => {
		const specDocWithExtraTags: OpenAPIV3.Document = {
			openapi: '3.0.1',
			info: { title: 'title', version: '0.0.1' },
			tags: [{ name: 'a' }, { name: 'b' }, { name: 'c' }],
			paths: {
				'/endpointA': {
					get: {
						tags: ['a'],
					},
				},
			},
		};

		const actual = removeUnusedTags(specDocWithExtraTags);

		expect(actual.tags).toHaveLength(1);
		expect(actual.tags).toContainEqual({ name: 'a' });
	});

	it('handles endpoints with multiple tags', () => {
		const specDocWithExtraTags: OpenAPIV3.Document = {
			openapi: '3.0.1',
			info: { title: 'title', version: '0.0.1' },
			tags: [{ name: 'a' }, { name: 'b' }, { name: 'c' }],
			paths: {
				'/endpointAB': {
					get: {
						tags: ['a', 'b'],
					},
				},
			},
		};

		const actual = removeUnusedTags(specDocWithExtraTags);

		expect(actual.tags).toHaveLength(2);
		expect(actual.tags).toContainEqual({ name: 'a' });
		expect(actual.tags).toContainEqual({ name: 'b' });
	});

	it('handles multiple endpoints with tags', () => {
		const specDocWithExtraTags: OpenAPIV3.Document = {
			openapi: '3.0.1',
			info: { title: 'title', version: '0.0.1' },
			tags: [{ name: 'a' }, { name: 'b' }, { name: 'c' }],
			paths: {
				'/endpointA': {
					get: {
						tags: ['a'],
					},
				},
				'/endpointB': {
					get: {
						tags: ['b'],
					},
				},
			},
		};

		const actual = removeUnusedTags(specDocWithExtraTags);

		expect(actual.tags).toHaveLength(2);
		expect(actual.tags).toContainEqual({ name: 'a' });
		expect(actual.tags).toContainEqual({ name: 'b' });
	});
});

describe('checkAuth', () => {
	it('returns TRUE if the Authorization header contains the specified scheme and a token, insensitive to scheme case', () => {
		const mockReq = {
			headers: { authorization: 'apikey token' },
		} as Request;

		expect(checkAuth(mockReq, 'apikey')).toBe(true);
		expect(checkAuth(mockReq, 'ApIkEy')).toBe(true);
	});

	it('throws an error if the Authorization header is empty', () => {
		const mockReq = { headers: {} } as Request;

		expect(() => checkAuth(mockReq, 'apikey')).toThrowError();
	});

	it('throws an error if the Authorization header does not contain the specified scheme', () => {
		const mockReq = {
			headers: { authorization: 'basic token' },
		} as Request;

		expect(() => checkAuth(mockReq, 'apikey')).toThrowError();
	});

	it('throws an error if the Authorization header contains more than a scheme and token', () => {
		const mockReq = {
			headers: { authorization: 'apikey token crazytown' },
		} as Request;

		expect(() => checkAuth(mockReq, 'apikey')).toThrowError();
	});
});

describe('getUnsecuredEndpoints', () => {
	const specDoc: OpenAPIV3.Document = {
		openapi: '3.0.1',
		info: { title: 'title', version: '0.0.1' },
		paths: {
			'/endpointA': {
				get: {
					security: [{ apikey: [] }],
				},
			},
			'/endpointB': {
				get: {
					security: [{ basic: [] }],
				},
				post: {
					security: [{ basic: [] }],
				},
			},
			'/endpointC': {
				get: {
					security: [],
				},
			},
			'/endpointD': {
				get: {
					security: [],
				},
				post: {
					security: [],
				},
			},
		},
	};

	const unsecuredEndpoints = getUnsecuredEndpoints(specDoc);

	it('creates a lookup of path+method combos for any endpoints without a security scheme defined in the open api document', () => {
		expect(unsecuredEndpoints).toHaveProperty('/endpointc', ['get']);
		expect(unsecuredEndpoints).toHaveProperty('/endpointd', ['get', 'post']);
	});

	describe('createIsUnsecuredEndpoint', () => {
		const createMockRequest = (path: string, method: string): any => ({
			method,
			openapi: {
				openApiRoute: path,
			},
		});
		const isUnsecuredEndpoint = createIsUnsecuredEndpoint(unsecuredEndpoints);

		it('returns true if the endpoint is in the unsecuredEndpoints object', () => {
			expect(isUnsecuredEndpoint(createMockRequest('/endpointc', 'get'))).toBe(true);
			expect(isUnsecuredEndpoint(createMockRequest('/endpointd', 'get'))).toBe(true);
			expect(isUnsecuredEndpoint(createMockRequest('/endpointd', 'post'))).toBe(true);
			expect(isUnsecuredEndpoint(createMockRequest('/endpointD', 'post'))).toBe(true);
			expect(isUnsecuredEndpoint(createMockRequest('/endpointD', 'POST'))).toBe(true);
		});

		it('returns false if the endpoint is not in the unsecuredEndpoints object', () => {
			expect(isUnsecuredEndpoint(createMockRequest('/endpointa', 'get'))).toBe(false);
			expect(isUnsecuredEndpoint(createMockRequest('/endpointb', 'get'))).toBe(false);
			expect(isUnsecuredEndpoint(createMockRequest('/endpointb', 'post'))).toBe(false);
			expect(isUnsecuredEndpoint(createMockRequest('/endpointB', 'post'))).toBe(false);
			expect(isUnsecuredEndpoint(createMockRequest('/endpointb', 'POST'))).toBe(false);
		});
	});
});
