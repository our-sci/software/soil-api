import { Request } from 'express';
import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';

const operationTypes = ['get', 'put', 'post', 'delete', 'options', 'head', 'patch', 'trace'];

const getTagsFromPath = (path: OpenAPIV3.PathItemObject) =>
	Object.entries(path)
		.filter(([key]) => operationTypes.includes(key))
		.flatMap(([_, operation]: [string, OpenAPIV3.OperationObject]) => operation.tags ?? []);

const removeUnusedTags = (specDocument: OpenAPIV3.Document): OpenAPIV3.Document => {
	const usedTags = Object.values(specDocument.paths).flatMap(getTagsFromPath);
	return {
		...specDocument,
		tags: specDocument.tags?.filter((tag) => usedTags.includes(tag.name)),
	};
};

/*
	express-openapi-validator has a bug where it doesn't return the error messages returned from security handlers
	as described in the documentation. So, the error messages below are never actually returned in responses.
	The error messages do match the format returned by express-openapi-validator by default, so do not be confused!

	The security handlers will still be run and will pass security when they return true, and return a 401 otherwise.

	https://github.com/cdimascio/express-openapi-validator/issues/539
	https://github.com/cdimascio/express-openapi-validator/issues/604
*/
const checkAuth = (req: Request, authScheme: string): boolean => {
	if (!req.headers.authorization) {
		throw Error(`Authorization header required`);
	}

	const [scheme, token, ...rest] = req.headers.authorization?.split(' ') ?? [];

	if (scheme?.toLowerCase() !== authScheme.toLowerCase()) {
		throw Error(`Authorization header with scheme '${authScheme}' required`);
	}

	if (rest.length > 0 || !token) {
		throw Error(`Authorization header format is: '${authScheme} [key]'`);
	}

	return true;
};

type UnsecuredEndpoints = {
	[path: string]: string[];
};

// We use req.openapi.openApiRoute to get the route that matches the request. instead of using the exact path of the request.
// For example, /stratification-jobs/123/resolve will have an openApiRoute of /stratification-jobs/{jobId}/resolve.
const createIsUnsecuredEndpoint = (unsecuredEndpoints: UnsecuredEndpoints) => (
	req: Request & { openapi: any }
): boolean =>
	Boolean(unsecuredEndpoints?.[req?.openapi?.openApiRoute?.toLowerCase()]?.includes(req?.method?.toLowerCase()));

function isUnsecuredOperation(operationObject: OpenAPIV3.OperationObject): boolean {
	return operationObject?.security?.length === 0;
}
/*
	Return format:
  {
	 '/api/endpoint1': ['get', 'post'],
	 '/api/endpoint2/{pathParam}': ['get'],
	};
*/
function getUnsecuredEndpoints(openApiSpec: OpenAPIV3.Document): UnsecuredEndpoints {
	const unsecuredEndpoints: [string, string[]][] = Object.entries(openApiSpec.paths)
		.map(([path, pathObject]) => [
			path.toLowerCase(),
			Object.entries(pathObject)
				.filter(([method, operationObject]) => isUnsecuredOperation(operationObject))
				.map(([method]) => method),
		])
		.filter(([_, methods]): boolean => methods.length > 0) as [string, string[]][];

	return Object.fromEntries(unsecuredEndpoints);
}

export { checkAuth, createIsUnsecuredEndpoint, getUnsecuredEndpoints, removeUnusedTags };
