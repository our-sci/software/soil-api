import request from 'supertest';
import prisma from '../prismaClient';
import {
	expect201AndValidateResponse,
	expect204AndValidateResponse,
	expect404AndValidateResponse,
	getDefaultAdminBearerToken,
	getDefaultUserBearerToken,
	getUserBearerToken,
	postMockApiKey,
	registerUser,
	testApp,
} from '../test-utils';

describe('ApiKeys Endpoints', () => {
	describe('POST', () => {
		describe('/api-keys', () => {
			it('returns 201 with new API key on success, including the raw api token', async (done) => {
				const postApiKeyRequestBody = {
					label: 'mock label',
				};

				request(testApp)
					.post('/api-keys')
					.set('Authorization', getDefaultUserBearerToken())
					.send(postApiKeyRequestBody)
					.end((err, res) => {
						expect(res.body).toHaveProperty('id');
						expect(res.body).toHaveProperty('apiKey');
						expect(res.body).toHaveProperty('label');
						expect201AndValidateResponse(done)(err, res);
					});
			});
		});
	});

	describe('DELETE', () => {
		describe('/api-keys/:id', () => {
			describe('when the requesting user is a super admin', () => {
				beforeEach(async () => {
					await registerUser({
						id: 'seed-super-admin-1',
						email: 'seedSuperAdmin1@soilstack.io',
						password: 'password',
						groupIds: [],
						isSuperAdmin: true,
					});
				});

				it('returns 204 and deletes the api key belonging to any user', async (done) => {
					const {
						body: { id },
					} = await postMockApiKey({ authToken: getDefaultUserBearerToken() });

					request(testApp)
						.delete(`/api-keys/${id}`)
						.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
						.end(async (err, res) => {
							expect204AndValidateResponse()(err, res);

							const apiKey = await prisma.apiKey.findFirst({ where: { id } });
							expect(apiKey).toBeNull();

							done();
						});
				});

				it('returns 404 if the api key does not exist', (done) => {
					request(testApp)
						.delete('/api-keys/non-existent-id')
						.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
						.end(expect404AndValidateResponse(done));
				});
			});

			describe('when the requesting user is not a super admin', () => {
				it('returns 204 and deletes the api key when it belongs to the requesting user', async (done) => {
					const {
						body: { id },
					} = await postMockApiKey({ authToken: getDefaultUserBearerToken() });

					request(testApp)
						.delete(`/api-keys/${id}`)
						.set('Authorization', getDefaultUserBearerToken())
						.end(async (err, res) => {
							expect204AndValidateResponse()(err, res);

							const apiKey = await prisma.apiKey.findFirst({ where: { id } });
							expect(apiKey).toBeNull();

							done();
						});
				});

				it('returns 404 if the api key does not belong to the requesting user', async (done) => {
					const {
						body: { id },
					} = await postMockApiKey({ authToken: getDefaultAdminBearerToken() });

					request(testApp)
						.delete(`/api-keys/${id}`)
						.set('Authorization', getDefaultUserBearerToken())
						.end(async (err, res) => {
							expect404AndValidateResponse()(err, res);

							const apiKey = await prisma.apiKey.findFirst({ where: { id } });
							expect(apiKey).not.toBeNull();

							done();
						});
				});

				it('returns 404 if the api key does not exist', (done) => {
					request(testApp)
						.delete('/api-keys/non-existent-id')
						.set('Authorization', getDefaultUserBearerToken())
						.end(expect404AndValidateResponse(done));
				});
			});
		});
	});
});
