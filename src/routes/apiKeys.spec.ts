import request from 'supertest';
import { prismaMock } from '../singleton';
import { testApp } from '../test-utils';
import { getDefaultSuperAdminBearerToken, getDefaultUserBearerToken } from '../test-utils';

describe('ApiKeys Endpoints', () => {
	describe('POST', () => {
		describe('/api-keys', () => {
			it('returns 500 when prismaClient throws an unexpected error when creating the apiKey', (done) => {
				prismaMock.apiKey.create.mockRejectedValueOnce(new Error('unexpected error'));

				const postApiKeyRequestBody = {
					label: 'mock label',
				};

				request(testApp)
					.post('/api-keys')
					.set('Authorization', getDefaultUserBearerToken())
					.send(postApiKeyRequestBody)
					.expect(500, done);
			});
		});
	});

	describe('DELETE', () => {
		describe('/api-keys/:id', () => {
			it('returns 500 when prisma.apiKey.delete throws an unexpected error', (done) => {
				prismaMock.apiKey.delete.mockRejectedValue(new Error('Unexpected error'));

				request(testApp)
					.delete('/api-keys/abcd1234-ab12-cd34-ef56-abcdef123456')
					.set('Authorization', getDefaultSuperAdminBearerToken())
					.send()
					.expect(500, done);
			});
		});
	});
});
