import { Prisma } from '@prisma/client';
import { NextFunction, Request, Response, Router } from 'express';
import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';
import prisma from '../prismaClient';
import { prismaApiKeyToPostApiKeyResponse } from '../transforms';
import { hash, randomHex } from '../utils';

const router = Router();

router.post('/', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const { label } = req.body;
		const apiKey = randomHex();
		const hashedKey = hash(apiKey);

		const prismaApiKey = await prisma.apiKey.create({
			data: {
				label,
				hashedKey,
				user: {
					connect: { id: res.locals.user.id },
				},
			},
		});

		res.status(201).json(prismaApiKeyToPostApiKeyResponse(prismaApiKey, apiKey));
	} catch (error) {
		next(error);
	}
});

router.delete('/:id', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const { id } = req.params;

		if (res.locals.user.isSuperAdmin) {
			await prisma.apiKey.delete({ where: { id } });
		} else {
			const userId = res.locals.user.id;
			await prisma.apiKey.delete({ where: { id_userId: { id, userId } } });
		}

		return res.status(204).send();
	} catch (error) {
		if (error instanceof Prisma.PrismaClientKnownRequestError) {
			// P2025 is the Prisma Client error code for RecordNotFound
			if (error.code === 'P2025') {
				return res.status(404).send();
			}
		}

		next(error);
	}
});

const openApiPaths: OpenAPIV3.PathsObject = {
	'/api-keys': {
		post: {
			tags: ['API Keys'],
			summary: 'Create an API key',
			requestBody: {
				content: {
					'application/json': {
						schema: {
							type: 'object',
							description: 'A label to distinguish this API key from others.',
							required: ['label'],
							properties: {
								label: { type: 'string' },
							},
						},
					},
				},
				required: true,
			},
			responses: {
				'201': {
					description: 'Successfully created the API key.',
					content: {
						'application/json': {
							schema: { $ref: '#/components/schemas/PostApiKeyResponse' },
						},
					},
				},
				401: { $ref: '#/components/responses/401' },
			},
		},
	},
	'/api-keys/{apiKeyId}': {
		delete: {
			tags: ['API Keys'],
			summary: 'Delete an API key.',
			parameters: [
				{
					name: 'apiKeyId',
					in: 'path',
					description: 'The id of the API key to delete.',
					required: true,
					schema: { type: 'string' },
				},
			],
			responses: {
				'204': {
					description: 'The API key was deleted successfully.',
				},
				'404': {
					description: 'No API key with the given id exists.',
				},
				401: { $ref: '#/components/responses/401' },
			},
		} as OpenAPIV3.OperationObject,
	},
};

export { openApiPaths, router };
