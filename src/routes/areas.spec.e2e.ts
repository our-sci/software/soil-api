import { Prisma } from '@prisma/client';
import request from 'supertest';
import { ResourceTypes, SELF_URL, STRATIFICATION_SERVICE_KEY } from '../constants';
import prisma from '../prismaClient';
import * as stratificationService from '../services/stratification';
import {
	createMockArea,
	createMockPolygon,
	createMockPopulatedLocationCollection,
	createMockPostStratificationRequest,
	createMockStratification,
	createMockStratifyRequest,
	createMockStratServicePostStratificationResponse,
	expect200AndValidateResponse,
	expect201AndValidateResponse,
	expect400AndValidateResponse,
	expect404AndValidateResponse,
	expect409AndValidateResponse,
	getDefaultAdminBearerToken,
	getDefaultUserBearerToken,
	getUserBearerToken,
	postMockField,
	postMockStratification,
	registerUser,
	testApp,
	testGetResourceByIdEndpoint,
} from '../test-utils';
import { components, paths } from '../types/generated/openapi-types';

jest.mock('../services/stratification');

describe('Area Endpoints', () => {
	describe('GET', () => {
		describe('/areas', () => {
			it('returns 200 with areas the user has access to on success', async (done) => {
				// add a field to a group the user belongs to
				const postFieldResponse = await postMockField();
				const postedArea = postFieldResponse.body.areas[0];
				// add a field to a group the user does NOT belong to
				await postMockField({ groupId: 'seed-group-2' });

				request(testApp)
					.get(`/areas`)
					.set('Authorization', getDefaultUserBearerToken())
					.end((err, res) => {
						expect(res.body).toHaveLength(1);
						expect(res.body[0]).toHaveProperty('id', postedArea.id);
						expect(res.body[0]).toHaveProperty('meta.groupId', 'seed-group-1');
						expect200AndValidateResponse(done)(err, res);
					});
			});

			it('returns 200 on success when no areas are present', async (done) => {
				request(testApp)
					.get(`/areas`)
					.set('Authorization', getDefaultUserBearerToken())
					.end(expect200AndValidateResponse(done));
			});
		});

		testGetResourceByIdEndpoint({
			endpointPath: '/areas',
			resourceName: 'area',
			resourceType: ResourceTypes.Area,
		});
	});

	describe('PATCH', () => {
		describe('/areas/:id', () => {
			it('returns 204 and updates the area, preserving existing properties that are not edited', async (done) => {
				const postFieldResponse = await postMockField();
				const postedArea = postFieldResponse.body.areas[0];

				const mockArea = createMockArea({
					id: postedArea.id,
					properties: {
						name: 'new name',
						description: 'new description',
					},
					geometry: createMockPolygon({
						coordinates: [
							[
								[-123.123, 45.456],
								[-123.123, 45.457],
								[-123.123, 45.456],
							],
						],
					}),
				});
				const { type, geometry, properties } = mockArea;
				const editedArea: components['schemas']['EditableArea'] = {
					type,
					geometry,
					properties,
				};

				await request(testApp)
					.patch(`/areas/${postedArea.id}`)
					.set('Authorization', getDefaultUserBearerToken())
					.send(editedArea)
					.expect(204);

				const updatedArea = await prisma.area.findUnique({ where: { id: postedArea.id } });
				expect(updatedArea).toHaveProperty('geometry', updatedArea?.geometry);
				expect(updatedArea).toHaveProperty(
					'properties',
					expect.objectContaining({
						name: (updatedArea?.properties as Prisma.InputJsonObject)?.name,
						description: (updatedArea?.properties as Prisma.InputJsonObject)?.description,
						// confirm drawn property is preserved
						drawn: true,
					})
				);

				done();
			});

			it('returns 404 when an area with the given id is not found', async (done) => {
				const mockArea = createMockArea();
				const { type, geometry, properties } = mockArea;
				const updatedArea: components['schemas']['EditableArea'] = {
					type,
					geometry,
					properties,
				};

				request(testApp)
					.patch(`/areas/non-existent-id`)
					.set('Authorization', getDefaultUserBearerToken())
					.send(updatedArea)
					.end(expect404AndValidateResponse(done));
			});

			it('returns 404 when the user does not have access to the group there area is in', async (done) => {
				const postFieldResponse = await postMockField({ groupId: 'seed-group-2' });
				const { id } = postFieldResponse.body.areas[0];

				const { type, geometry, properties } = createMockArea();
				const editedArea: components['schemas']['EditableArea'] = {
					type,
					geometry,
					properties,
				};

				request(testApp)
					.patch(`/areas/${id}`)
					.set('Authorization', getDefaultUserBearerToken())
					.send(editedArea)
					.end(expect404AndValidateResponse(done));
			});

			it('returns 409 when a stratification exists that depends on the area', async (done) => {
				const postFieldResponse = await postMockField();
				const postedArea = postFieldResponse.body.areas[0];

				await postMockStratification({ areaId: postedArea.id });

				const mockArea = createMockArea();
				const { type, geometry, properties } = mockArea;
				const updatedArea: components['schemas']['EditableArea'] = {
					type,
					geometry,
					properties,
				};

				request(testApp)
					.patch(`/areas/${postedArea.id}`)
					.set('Authorization', getDefaultUserBearerToken())
					.send(updatedArea)
					.end(expect409AndValidateResponse(done));
			});
		});
	});

	describe('POST', () => {
		describe('/areas/:id/stratifications', () => {
			it('returns 201 on success', async (done) => {
				const postFieldResponse = await postMockField();
				const areaId = postFieldResponse.body.areas[0].id;
				const fieldId = postFieldResponse.body.fields[0].id;

				const postStratificationRequestBody = createMockPostStratificationRequest();

				request(testApp)
					.post(`/areas/${areaId}/stratifications`)
					.set('Authorization', getDefaultUserBearerToken())
					.send(postStratificationRequestBody)
					.end((err, res) => {
						const locationCollections = res.body.locationCollections;
						const stratifications = res.body.stratifications;
						const locations = res.body.locations;

						expect(stratifications).toHaveLength(1);

						expect(stratifications[0], 'stratification.object should match the area id').toHaveProperty(
							'object',
							areaId
						);

						expect(
							stratifications[0],
							'stratification.featureOfInterest should match the id of the field the area belongs to'
						).toHaveProperty('featureOfInterest', fieldId);

						expect(
							stratifications[0],
							'stratification.meta.groupId should match the groupId of the area in the path'
						).toHaveProperty('meta.groupId', 'seed-group-1');

						expect(locationCollections).toHaveLength(1);

						expect(locationCollections[0], 'locationCollection.object should match the area id').toHaveProperty(
							'object',
							areaId
						);

						expect(
							locationCollections[0],
							'locationCollection.resultOf should match the id of the stratification'
						).toHaveProperty('resultOf', stratifications[0].id);

						expect(
							locationCollections[0],
							'locationCollection.featureOfInterest should match the id of the field the area belongs to'
						).toHaveProperty('featureOfInterest', fieldId);

						expect(
							locationCollections[0],
							'locationCollection.meta.groupId should match the groupId of the area in the path'
						).toHaveProperty('meta.groupId', 'seed-group-1');

						expect(locations).toHaveLength(1);

						expect(
							locationCollections[0].features,
							'locationCollection.features should contain the ids of the locations that belong to the location collection'
						).toContain(locations[0].id);

						expect(
							locations[0],
							'location.meta.groupId should match the groupId of the area in the path'
						).toHaveProperty('meta.groupId', 'seed-group-1');

						expect201AndValidateResponse(done)(err, res);
					});
			});

			it('returns 404 and does not create the stratification if the area referenced in the path does not exist', (done) => {
				const postStratificationRequestBody = {
					stratification: createMockStratification({}, { omitObject: true }),
					locationCollection: createMockPopulatedLocationCollection(),
				};

				request(testApp)
					.post('/areas/non-existent-id/stratifications')
					.set('Authorization', getDefaultUserBearerToken())
					.send(postStratificationRequestBody)
					.end(expect404AndValidateResponse(done));
			});

			it('returns 404 when the area referenced in the path belongs to a field that is in a group the user does not have access to', async (done) => {
				const {
					body: {
						areas: [{ id }],
					},
				} = await postMockField({ groupId: 'seed-group-2' });
				const postStratificationRequestBody = {
					stratification: createMockStratification({}, { omitObject: true }),
					locationCollection: createMockPopulatedLocationCollection(),
				};

				request(testApp)
					.post(`/areas/${id}/stratifications`)
					.set('Authorization', getDefaultUserBearerToken())
					.send(postStratificationRequestBody)
					.end(expect404AndValidateResponse(done));
			});

			it('returns 409 when a stratification already exists for the area', async (done) => {
				const postFieldResponse = await postMockField();
				const areaId = postFieldResponse.body.areas[0].id;

				await postMockStratification({ areaId });

				const postStratificationRequestBody = {
					stratification: createMockStratification({}, { omitObject: true }),
					locationCollection: createMockPopulatedLocationCollection(),
				};

				request(testApp)
					.post(`/areas/${areaId}/stratifications`)
					.set('Authorization', getDefaultAdminBearerToken())
					.send(postStratificationRequestBody)
					.end(expect409AndValidateResponse(done));
			});
		});

		describe('/areas/stratify', () => {
			describe('when requesting user is not a super admin', () => {
				it('returns 401', (done) => {
					request(testApp)
						.post('/areas/stratify')
						.set('Authorization', getDefaultUserBearerToken())
						.send(createMockStratifyRequest())
						.expect(401, done);
				});
			});

			describe('when requesting user is a super admin', () => {
				beforeEach(async () => {
					await registerUser({
						id: 'seed-super-admin-1',
						email: 'seedSuperAdmin1@soilstack.io',
						password: 'password',
						groupIds: [],
						isSuperAdmin: true,
					});
				});

				it('returns 404 if any of the areas referenced by areaIds do not exist', (done) => {
					request(testApp)
						.post('/areas/stratify')
						.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
						.send(createMockStratifyRequest({ areaIds: ['non-existent-id'] }))
						.expect(404, done);
				});

				it('returns 500 and does not create stratification jobs if the stratification service returns a 500 for every stratification request', async (done) => {
					(stratificationService.postStratification as jest.Mock).mockRejectedValue({
						response: {
							status: 500,
						},
					});
					const [
						{
							body: {
								areas: [area1],
							},
						},
						{
							body: {
								areas: [area2],
							},
						},
					] = await Promise.all([postMockField(), postMockField()]);
					const postStratifyRequest = createMockStratifyRequest({ areaIds: [area1.id, area2.id] });

					await request(testApp)
						.post('/areas/stratify')
						.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
						.send(postStratifyRequest)
						.expect(500);

					const stratificationJobCount = await prisma.stratificationJob.count();
					expect(stratificationJobCount).toBe(0);

					done();
				});

				it('returns 400 and passes on the error from the stratification service if all stratification requests return 400', async (done) => {
					const mock400Body = {
						message: "request/body/stratificationConfig must have required property 'inputs'",
						errors: [
							{
								path: '/body/stratificationConfig/inputs',
								message: "must have required property 'inputs'",
								errorCode: 'required.openapi.validation',
							},
						],
					};
					(stratificationService.postStratification as jest.Mock).mockRejectedValue({
						response: {
							status: 400,
							data: mock400Body,
						},
					});
					const [
						{
							body: {
								areas: [area1],
							},
						},
						{
							body: {
								areas: [area2],
							},
						},
					] = await Promise.all([postMockField(), postMockField()]);
					const postStratifyRequest = createMockStratifyRequest({ areaIds: [area1.id, area2.id] });

					request(testApp)
						.post('/areas/stratify')
						.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
						.send(postStratifyRequest)
						.end((err, res) => {
							expect(res.body).toEqual(mock400Body);
							expect400AndValidateResponse(done)(err, res);
						});
				});

				it('returns 201 with the created stratification jobs if all stratification requests return 200', async (done) => {
					let taskIdIndex = 0;
					(stratificationService.postStratification as jest.Mock).mockImplementation(() => {
						return Promise.resolve({
							status: 200,
							data: createMockStratServicePostStratificationResponse({ taskId: String(taskIdIndex++) }),
						});
					});
					const [
						{
							body: {
								areas: [area1],
							},
						},
						{
							body: {
								areas: [area2],
							},
						},
					] = await Promise.all([postMockField(), postMockField()]);
					const postStratifyRequest = createMockStratifyRequest({ areaIds: [area1.id, area2.id], autoApprove: true });

					request(testApp)
						.post('/areas/stratify')
						.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
						.send(postStratifyRequest)
						.end((err, res) => {
							expect(res.status).toBe(201);
							expect(res.body).toHaveLength(2);
							type StratificationJob = paths['/areas/stratify']['post']['responses']['201']['content']['application/json'][0];
							res.body.forEach((job: StratificationJob) => {
								expect(job).toHaveProperty('autoApprove', true);
							});
							expect(stratificationService.postStratification).toHaveBeenCalledWith(
								expect.objectContaining({
									callbackUrl: `${SELF_URL}/stratification-jobs/resolve?token=${encodeURIComponent(
										STRATIFICATION_SERVICE_KEY
									)}`,
								})
							);
							expect201AndValidateResponse(done)(err, res);
						});
				});

				it('returns 201 with the created stratification jobs if some stratification requests return 200 and some return 500', async (done) => {
					let count = 0;
					(stratificationService.postStratification as jest.Mock).mockImplementation((requestBody) => {
						// return 500 for the first request, 200 for the second
						if (count === 0) {
							count++;
							return Promise.reject({
								response: {
									status: 500,
								},
							});
						}
						return Promise.resolve({
							status: 200,
							data: createMockStratServicePostStratificationResponse(),
						});
					});
					const [
						{
							body: {
								areas: [area1],
							},
						},
						{
							body: {
								areas: [area2],
							},
						},
					] = await Promise.all([postMockField(), postMockField()]);
					const postStratifyRequest = createMockStratifyRequest({ areaIds: [area1.id, area2.id] });

					request(testApp)
						.post('/areas/stratify')
						.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
						.send(postStratifyRequest)
						.end((err, res) => {
							const body = res.body as paths['/areas/stratify']['post']['responses']['201']['content']['application/json'];
							expect(body).toHaveLength(2);
							expect(body).toContainEqual(
								expect.objectContaining({
									isClosed: false,
									externalJobStatus: 'PENDING',
								})
							);
							expect(body).toContainEqual(
								expect.objectContaining({
									isClosed: true,
									closedReason: 'FAILED_TO_CREATE',
								})
							);
							expect201AndValidateResponse(done)(err, res);
						});
				});
			});
		});
	});
});
