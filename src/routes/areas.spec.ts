import request from 'supertest';
import * as stratificationService from '../services/stratification';
import { prismaMock } from '../singleton';
import {
	createMockArea,
	createMockPopulatedLocationCollection,
	createMockPrismaArea,
	createMockPrismaField,
	createMockStratification,
	createMockStratifyRequest,
	createMockStratServicePostStratificationResponse,
	getDefaultSuperAdminBearerToken,
	getDefaultUserBearerToken,
	testApp,
} from '../test-utils';
import { components } from '../types/generated/openapi-types';

describe('Area Endpoints', () => {
	describe('GET', () => {
		describe('/areas', () => {
			it('returns 500 when prismaClient returns an error', (done) => {
				prismaMock.area.findMany.mockRejectedValueOnce(new Error('unexpected error'));

				request(testApp).get(`/areas`).set('Authorization', getDefaultUserBearerToken()).expect(500, done);
			});
		});

		describe('/areas/:id', () => {
			it('returns 500 when prisma.area.findFirst throws an unexpected error', (done) => {
				prismaMock.area.findFirst.mockRejectedValueOnce(new Error('unexpected error'));

				request(testApp)
					.get('/areas/non-existent-id')
					.set('Authorization', getDefaultUserBearerToken())
					.expect(500, done);
			});
		});
	});

	describe('PATCH', () => {
		describe('/areas/:id', () => {
			const mockArea = createMockArea();
			const { type, geometry, properties } = mockArea;
			const mockEditableArea: components['schemas']['EditableArea'] = {
				type,
				geometry,
				properties,
			};

			it('returns 500 when prisma.stratification.count throws an unexpected error', (done) => {
				prismaMock.area.findUnique.mockResolvedValue(createMockPrismaArea());
				prismaMock.stratification.count.mockRejectedValueOnce(new Error('unexpected error'));
				prismaMock.area.update.mockResolvedValueOnce(createMockPrismaArea());

				request(testApp)
					.patch('/areas/area-id')
					.set('Authorization', getDefaultUserBearerToken())
					.send(mockEditableArea)
					.expect(500, done);
			});

			it('returns 500 when prisma.area.update throws an unexpected error', (done) => {
				prismaMock.area.findUnique.mockResolvedValue(createMockPrismaArea());
				prismaMock.stratification.count.mockResolvedValueOnce(0);
				prismaMock.area.update.mockRejectedValueOnce(new Error('unexpected error'));

				request(testApp)
					.patch('/areas/area-id')
					.set('Authorization', getDefaultUserBearerToken())
					.send(mockEditableArea)
					.expect(500, done);
			});

			it('returns 500 when prisma.area.findUnique throws an unexpected error', (done) => {
				prismaMock.area.findUnique.mockRejectedValueOnce(new Error('unexpected error'));
				prismaMock.stratification.count.mockResolvedValueOnce(0);
				prismaMock.area.update.mockResolvedValueOnce(createMockPrismaArea());

				request(testApp)
					.patch('/areas/area-id')
					.set('Authorization', getDefaultUserBearerToken())
					.send(mockEditableArea)
					.expect(500, done);
			});
		});
	});

	describe('POST', () => {
		describe('/areas/:id/stratifications', () => {
			it('returns 500 when prismaClient returns an error when creating the stratification', (done) => {
				prismaMock.field.findFirst.mockResolvedValueOnce(createMockPrismaField());
				prismaMock.stratification.create.mockRejectedValueOnce(new Error('unexpected error'));

				const postStratificationRequestBody = {
					stratification: createMockStratification({}, { omitObject: true }),
					locationCollection: createMockPopulatedLocationCollection(),
				};

				request(testApp)
					.post('/areas/area-id/stratifications')
					.set('Authorization', getDefaultUserBearerToken())
					.send(postStratificationRequestBody)
					.expect(500, done);
			});

			it('returns 500 when prismaClient returns an error when finding field', (done) => {
				prismaMock.field.findFirst.mockRejectedValueOnce(new Error('unexpected error'));

				const postStratificationRequestBody = {
					stratification: createMockStratification({}, { omitObject: true }),
					locationCollection: createMockPopulatedLocationCollection(),
				};

				request(testApp)
					.post('/areas/area-id/stratifications')
					.set('Authorization', getDefaultUserBearerToken())
					.send(postStratificationRequestBody)
					.expect(500, done);
			});
		});

		describe('/areas/stratify', () => {
			it('returns 500 when prisma.area.findMany throws an unexpected error', (done) => {
				prismaMock.area.findMany.mockRejectedValueOnce(new Error('unexpected error'));

				request(testApp)
					.post('/areas/stratify')
					.set('Authorization', getDefaultSuperAdminBearerToken())
					.send(createMockStratifyRequest({ areaIds: ['area-id'] }))
					.expect(500, done);
			});

			it('returns 500 prisma.$transaction throws an unexpected error', (done) => {
				prismaMock.area.findMany.mockResolvedValueOnce([createMockPrismaArea()]);
				(stratificationService.postStratification as jest.Mock).mockResolvedValue({
					status: 200,
					data: createMockStratServicePostStratificationResponse(),
				});
				prismaMock.$transaction.mockRejectedValueOnce(new Error('unexpected error'));

				request(testApp)
					.post('/areas/stratify')
					.set('Authorization', getDefaultSuperAdminBearerToken())
					.send(createMockStratifyRequest({ areaIds: ['area-id'] }))
					.expect(500, done);
			});
		});
	});
});
