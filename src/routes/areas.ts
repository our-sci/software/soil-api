import { Membership as PrismaMembership, Prisma } from '@prisma/client';
import { NextFunction, Request, Response, Router } from 'express';
import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';
import { MultiPolygon, Polygon } from 'geojson';
import { ResourceTypes, SELF_URL, STRATIFICATION_SERVICE_KEY } from '../constants';
import prisma from '../prismaClient';
import * as stratificationService from '../services/stratification';
import {
	editableAreaToPrismaAreaUpdateInput,
	locationToPrismaCreateInput,
	populatedLocationCollectionToPrismaCreateInput,
	postStratificationRequestToPrismaCreateInput,
	prismaAreaToArea,
	prismaLocationCollectionToLocationCollection,
	prismaLocationToLocation,
	prismaStratificationJobToStratificationJob,
	prismaStratificationToStratification,
	stratServiceJobToPrismaStratificationJobCreate,
} from '../transforms';
import { components, paths } from '../types/generated/openapi-types';
import { PrismaPopulatedStratification } from '../types/prisma';
import { isFulfilled, isRejected } from '../utils';
import createGetResourceByIdController from './common/createGetResourceByIdController';

const router = Router();

router.get('/', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const userGroupIds = res.locals.user.memberships.map((m: PrismaMembership) => m.groupId);

		const prismaAreas = await prisma.area.findMany({
			where: { groupId: { in: userGroupIds } },
		});

		const areas = prismaAreas.map(prismaAreaToArea);

		res.status(200).json(areas);
	} catch (error) {
		next(error);
	}
});

router.get('/:id', createGetResourceByIdController(ResourceTypes.Area));

router.patch('/:id', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const id = req.params.id;
		const body = req.body as components['schemas']['EditableArea'];
		const userGroupIds = res.locals.user.memberships.map((m: PrismaMembership) => m.groupId);

		const area = await prisma.area.findUnique({ where: { id } });
		const isAuthorized = userGroupIds.includes(area?.groupId);

		if (!isAuthorized || !area) {
			return res.status(404).send();
		}

		const relatedStratificationCount = await prisma.stratification.count({
			where: { areaId: id },
		});

		if (relatedStratificationCount > 0) {
			return res.status(409).send({
				message: 'Cannot edit an area that has a stratification depending on it.',
			});
		}

		await prisma.area.update({
			where: { id },
			data: editableAreaToPrismaAreaUpdateInput(area, body),
		});

		return res.status(204).send();
	} catch (error) {
		if (error instanceof Prisma.PrismaClientKnownRequestError) {
			// P2025 is the Prisma Client error code for RecordNotFound
			if (error.code === 'P2025') {
				return res.status(404).send();
			}
		}

		next(error);
	}
});

router.post('/:id/stratifications', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const areaId = req.params.id;
		const body = req.body as {
			stratification: components['schemas']['Stratification'];
			locationCollection: components['schemas']['PopulatedLocationCollection'];
		};
		const userGroupIds = res.locals.user.memberships.map((m: PrismaMembership) => m.groupId);

		const field = await prisma.field.findFirst({
			where: {
				areas: {
					some: { id: areaId },
				},
			},
			select: {
				id: true,
				groupId: true,
				_count: {
					select: { locationCollections: true },
				},
			},
		});

		const isAuthorized = userGroupIds.includes(field?.groupId);

		if (!field || !isAuthorized) {
			return res.status(404).send();
		}

		if (field._count.locationCollections > 0) {
			return res.status(409).send({
				message: 'Cannot create a stratification for an area that already has one.',
			});
		}

		const { id: fieldId, groupId } = field;

		const stratification: PrismaPopulatedStratification = await prisma.stratification.create({
			data: {
				...postStratificationRequestToPrismaCreateInput(body, groupId, areaId),
				locationCollection: {
					create: {
						...populatedLocationCollectionToPrismaCreateInput(groupId, fieldId, areaId),
						locations: {
							createMany: {
								data: body.locationCollection.features.map((feature) => locationToPrismaCreateInput(feature, groupId)),
							},
						},
					},
				},
			},
			include: {
				algorithm: true,
				locationCollection: { include: { locations: true } },
				area: { select: { fieldId: true } },
			},
		});

		res.status(201).json({
			stratifications: [prismaStratificationToStratification(stratification)],
			locationCollections: stratification?.locationCollection
				? [prismaLocationCollectionToLocationCollection(stratification.locationCollection)]
				: [],
			locations: stratification.locationCollection?.locations.map(prismaLocationToLocation) ?? [],
		});
	} catch (error) {
		next(error);
	}
});

router.post('/stratify', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const {
			stratificationConfig,
			areaIds,
			autoApprove,
		} = req.body as paths['/areas/stratify']['post']['requestBody']['content']['application/json'];

		if (!res.locals.user.isSuperAdmin) {
			return res.status(401).send();
		}

		const areas = await prisma.area.findMany({
			where: { id: { in: areaIds } },
		});

		if (areas.length !== areaIds.length) {
			return res.status(404).send();
		}

		const responses = await Promise.allSettled<{
			id: string;
			axiosResult: Awaited<ReturnType<typeof stratificationService.postStratification>>;
		}>(
			areas.map(
				({ id, geometry }) =>
					new Promise(async (resolve, reject) => {
						try {
							resolve({
								id,
								axiosResult: await stratificationService.postStratification({
									stratificationConfig,
									area: { type: 'Feature', geometry: (geometry as unknown) as Polygon | MultiPolygon },
									callbackUrl: `${SELF_URL}/stratification-jobs/resolve?token=${encodeURIComponent(
										STRATIFICATION_SERVICE_KEY
									)}`,
								}),
							});
						} catch (error) {
							reject({
								id,
								axiosResult: error,
							});
						}
					})
			)
		);

		const successes = responses.filter(isFulfilled).map((success) => success.value);
		const failures = responses.filter(isRejected).map((failure) => failure.reason);

		if (successes.length === 0) {
			const requestError = failures.find((failure) => failure.axiosResult?.response?.status === 400);
			if (requestError) {
				return res.status(400).send(requestError.axiosResult.response?.data);
			}

			return res.status(500).send();
		}

		const successJobsToCreate = successes.map(({ id, axiosResult }) =>
			stratServiceJobToPrismaStratificationJobCreate({
				areaId: id,
				jobId: axiosResult.data.taskId,
				jobStatus: axiosResult.data.state,
				stratificationConfig,
				autoApprove,
			})
		);
		const failureJobsToCreate = failures.map(({ id, axiosResult }) =>
			stratServiceJobToPrismaStratificationJobCreate({
				areaId: id,
				jobId: null,
				isClosed: true,
				error: axiosResult.message,
				closedReason: 'FAILED_TO_CREATE',
				stratificationConfig,
				autoApprove,
			})
		);

		// createMany doesn't return the created records, so we have to use a transaction instead
		// https://github.com/prisma/prisma/issues/8131
		const prismaStratificationJobs = await prisma.$transaction(
			[...successJobsToCreate, ...failureJobsToCreate].map((job) => prisma.stratificationJob.create({ data: job }))
		);

		const responseStratificationJobs = prismaStratificationJobs.map(prismaStratificationJobToStratificationJob);

		res.status(201).send(responseStratificationJobs);
	} catch (error) {
		next(error);
	}
});

const openApiPaths: OpenAPIV3.PathsObject = {
	'/areas': {
		get: {
			tags: ['Areas'],
			summary: 'Get all areas.',
			responses: {
				'200': {
					description: 'An array of the areas you have access to.',
					content: {
						'application/json': {
							schema: {
								type: 'array',
								items: { $ref: '#/components/schemas/ResponseArea' },
							},
						},
					},
				},
				401: { $ref: '#/components/responses/401' },
			},
		},
	},
	'/areas/{areaId}': {
		get: {
			tags: ['Areas'],
			summary: 'Get an area by its id.',
			parameters: [
				{
					name: 'areaId',
					in: 'path',
					description: 'The id of the area to get.',
					required: true,
					schema: { type: 'string' },
				},
			],
			responses: {
				'200': {
					description: 'The area with the given id.',
					content: {
						'application/json': {
							schema: {
								$ref: '#/components/schemas/ResponseArea',
							},
						},
					},
				},
				'404': {
					description: 'No area with the given id exists.',
				},
				401: { $ref: '#/components/responses/401' },
			},
		},
		patch: {
			tags: ['Areas'],
			summary: 'Updates an area.',
			parameters: [
				{
					name: 'areaId',
					in: 'path',
					description: 'The id of the area to edit.',
					required: true,
					schema: { type: 'string' },
				},
			],
			requestBody: {
				description: 'The area object to be updated.',
				required: true,
				content: {
					'application/json': {
						schema: {
							$ref: '#/components/schemas/EditableArea',
						},
					},
				},
			},
			responses: {
				'204': {
					description: 'The area was saved successfully.',
				},
				'404': {
					description: 'No area with the given id exists.',
				},
				'409': {
					description: 'Cannot edit an area that has resources that depend on it.',
					content: {
						'application/json': {
							schema: {
								$ref: '#/components/schemas/Error',
							},
						},
					},
				},
				401: { $ref: '#/components/responses/401' },
			},
		},
	},
	'/areas/{areaId}/stratifications': {
		post: {
			tags: ['Stratifications'],
			summary: 'Saves a stratification',
			parameters: [
				{
					name: 'areaId',
					in: 'path',
					description: 'The id of the area to save the stratification to.',
					required: true,
					schema: { type: 'string' },
				},
			],
			requestBody: {
				description: 'The stratification object and its location collection object to be saved.',
				content: {
					'application/json': {
						schema: {
							type: 'object',
							required: ['stratification', 'locationCollection'],
							properties: {
								stratification: { $ref: '#/components/schemas/Stratification' },
								locationCollection: { $ref: '#/components/schemas/PopulatedLocationCollection' },
							},
						},
					},
				},
				required: true,
			},
			responses: {
				'201': {
					description: 'Successfully created the stratification.',
					content: {
						'application/json': {
							schema: {
								type: 'object',
								required: ['stratifications', 'locationCollections', 'locations'],
								properties: {
									stratifications: {
										type: 'array',
										items: { $ref: '#/components/schemas/ResponseStratification' },
									},
									locationCollections: {
										type: 'array',
										items: { $ref: '#/components/schemas/ResponseLocationCollection' },
									},
									locations: {
										type: 'array',
										items: { $ref: '#/components/schemas/ResponseLocation' },
									},
								},
							},
						},
					},
				},
				'404': {
					description: 'Area not found.',
				},
				'409': {
					description: 'Cannot create a stratification for an area that already has one.',
					content: {
						'application/json': {
							schema: {
								$ref: '#/components/schemas/Error',
							},
						},
					},
				},
				401: { $ref: '#/components/responses/401' },
			},
		},
	},
	'/areas/stratify': {
		post: {
			'x-internal': true,
			tags: ['Stratification Jobs'],
			summary: 'Initiate an automated stratification for one or more areas.',
			requestBody: {
				description: 'The area ids to stratify and a stratification configuration.',
				content: {
					'application/json': {
						schema: {
							type: 'object',
							required: ['areaIds', 'stratificationConfig'],
							properties: {
								areaIds: {
									type: 'array',
									minItems: 1,
									items: { type: 'string' },
								},
								stratificationConfig: {
									$ref: '#/components/schemas/StratificationServiceStratificationConfig',
								},
								autoApprove: {
									type: 'boolean',
									default: false,
								},
							},
						},
					},
				},
			},
			responses: {
				'201': {
					description:
						'Successfully processed the stratification jobs. Note: It is possible that some of the jobs were not created successfully. Review the response body for details.',
					content: {
						'application/json': {
							schema: {
								type: 'array',
								items: { $ref: '#/components/schemas/StratificationJob' },
							},
						},
					},
				},
				'400': {
					description: 'The request body was invalid.',
					content: {
						'application/json': {
							schema: {
								type: 'object',
							},
						},
					},
				},
				'404': {
					description: 'One or more areas were not found.',
				},
			},
		} as OpenAPIV3.OperationObject,
	},
};

export { openApiPaths, router };
