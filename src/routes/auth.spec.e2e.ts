import request from 'supertest';
import prisma from '../prismaClient';
import { sendEmail } from '../services/email';
import {
	expect200AndValidateResponse,
	expect201AndValidateResponse,
	getDefaultUserBearerToken,
	testApp,
} from '../test-utils';
import { compare, hash, randomHex } from '../utils';

jest.mock('../services/email');

describe('Auth Endpoints', () => {
	describe('/auth/register', () => {
		it('when a user with the requested email exists and user.seen is true, returns 409', (done) => {
			request(testApp)
				.post('/auth/register')
				.send({
					email: 'seedUser1@soilstack.io',
					password: 'password',
				})
				.expect(409, done);
		});

		it('when a user with the requested email exists and user.seen is false, returns 201 with token and sets user.seen to true', async (done) => {
			await prisma.user.create({
				data: {
					email: 'newuser@soilstack.io',
					seen: false,
				},
			});

			request(testApp)
				.post('/auth/register')
				.send({
					email: 'newUser@soilstack.io',
					password: 'password',
				})
				.end(async (err, res) => {
					expect201AndValidateResponse()(err, res);

					const user = await prisma.user.findUnique({
						where: { email: 'newuser@soilstack.io' },
						include: { authTokens: true },
					});

					expect(await compare('password', user?.hashedPassword as string)).toBe(true);
					expect(user?.authTokens[0].hashedToken).toEqual(hash(res.body.token));
					expect(user?.seen).toBe(true);

					done();
				});
		});

		it('when a user with the requested email does not exist, returns 201 with auth token and creates the user', async (done) => {
			request(testApp)
				.post('/auth/register')
				.send({
					email: 'newUser@soilstack.io',
					password: 'password',
				})
				.end(async (err, res) => {
					expect201AndValidateResponse()(err, res);

					const user = await prisma.user.findUnique({
						where: { email: 'newuser@soilstack.io' },
						include: { authTokens: true },
					});

					expect(await compare('password', user?.hashedPassword as string)).toBe(true);
					expect(user?.authTokens[0].hashedToken).toEqual(hash(res.body.token));
					expect(user?.seen).toBe(true);

					done();
				});
		});
	});

	describe('auth/login', () => {
		it('when password is incorrect, returns 401', (done) => {
			request(testApp)
				.post('/auth/login')
				.send({
					email: 'seedUser1@soilstack.io',
					password: 'password2',
				})
				.expect(401, done);
		});

		it('when password is correct, generates a new auth token and returns 200 with the token', async (done) => {
			request(testApp)
				.post('/auth/login')
				.send({
					email: 'seedUser1@soilstack.io',
					password: 'password',
				})
				.end(async (err, res) => {
					expect200AndValidateResponse()(err, res);

					const user = await prisma.user.findUnique({
						where: { email: 'seeduser1@soilstack.io' },
						include: { authTokens: true },
					});

					const hashedToken = hash(res.body.token);
					expect(user?.authTokens.some((token) => token.hashedToken === hashedToken)).toBe(true);

					done();
				});
		});

		it('when the user doesnt exist, returns 401', (done) => {
			request(testApp)
				.post('/auth/login')
				.send({
					email: 'notAUser@soilstack.io',
					password: 'password',
				})
				.expect(401, done);
		});

		it('when the user exists but has not been seen, return 401', async (done) => {
			await prisma.user.create({
				data: {
					email: 'unseenuser@soilstack.io',
					seen: false,
				},
			});

			request(testApp)
				.post('/auth/login')
				.send({
					email: 'unseenUser@soilstack.io',
					password: 'password',
				})
				.expect(401, done);
		});
	});

	describe('auth/request-password-reset', () => {
		it('short circuits and returns 200 when a user with the requested email does not exist', (done) => {
			request(testApp)
				.post('/auth/request-password-reset')
				.send({
					email: 'nonexistentUser@soilstack.io',
				})
				.end((err, res) => {
					expect200AndValidateResponse()(err, res);

					expect(sendEmail).not.toHaveBeenCalled();

					done();
				});
		});

		it('sends an email to the user with a password reset link, and returns 200 when a user with the requested email exists', (done) => {
			request(testApp)
				.post('/auth/request-password-reset')
				.send({
					email: 'seedUser1@soilstack.io',
				})
				.end(async (err, res) => {
					expect200AndValidateResponse()(err, res);

					expect(sendEmail).toHaveBeenCalled();

					const passwordRestToken = await prisma.passwordResetToken.findFirst({
						where: {
							user: { id: 'seed-user-1' },
						},
					});

					expect(passwordRestToken).not.toBeNull();

					done();
				});
		});
	});

	describe('/auth/reset-password', () => {
		it('returns 401 when the password reset token is not found in the database', (done) => {
			request(testApp)
				.post('/auth/reset-password')
				.send({
					token: randomHex(),
					password: 'password',
				})
				.expect(401, done);
		});

		it('returns 401 when the password reset token is expired', async (done) => {
			const passwordResetToken = randomHex();
			const hashedPasswordResetToken = hash(passwordResetToken);

			await prisma.passwordResetToken.create({
				data: {
					hashedToken: hashedPasswordResetToken,
					expiresAt: new Date(0),
					user: {
						connect: { email: 'seeduser1@soilstack.io' },
					},
				},
			});

			request(testApp)
				.post('/auth/reset-password')
				.send({
					token: passwordResetToken,
					password: 'password2',
				})
				.expect(401, done);
		});

		it("when the password reset token is valid, returns 200 with a new auth token, updates the user's password, marks the user as seen, deletes the password reset token, and deletes all preexisting auth tokens", async (done) => {
			const passwordResetToken = randomHex();
			const hashedPasswordResetToken = hash(passwordResetToken);

			await prisma.user.create({
				data: {
					email: 'unseenUser@soilstack.io',
					seen: false,
				},
			});
			await prisma.passwordResetToken.create({
				data: {
					hashedToken: hashedPasswordResetToken,
					expiresAt: new Date(Date.now() + 1000 * 60 * 60 * 24),
					user: {
						connect: { email: 'unseenUser@soilstack.io' },
					},
				},
			});

			request(testApp)
				.post('/auth/reset-password')
				.send({
					token: passwordResetToken,
					password: 'password2',
				})
				.end(async (err, res) => {
					expect200AndValidateResponse()(err, res);

					const token = res.body.token;
					const user = await prisma.user.findUnique({
						where: { email: 'unseenUser@soilstack.io' },
						include: {
							authTokens: true,
							passwordResetTokens: true,
						},
					});

					expect(user?.seen).toBe(true);
					// assert password was updated
					expect(await compare('password2', user?.hashedPassword as string)).toBe(true);
					// assert preexisting auth tokens were deleted
					expect(user?.authTokens).toHaveLength(1);
					expect(user?.authTokens[0].hashedToken).toBe(hash(token));
					// assert password reset token was deleted
					expect(user?.passwordResetTokens).toHaveLength(0);

					done();
				});
		});
	});

	describe('/auth/update-password', () => {
		it("when currentPassword is correct, returns 200 with new auth token, updates user's password, and deletes preexisting auth tokens", (done) => {
			request(testApp)
				.post('/auth/update-password')
				.set('Authorization', getDefaultUserBearerToken())
				.send({
					newPassword: 'password2',
					currentPassword: 'password',
				})
				.end(async (err, res) => {
					expect200AndValidateResponse()(err, res);

					const token = res.body.token;
					const user = await prisma.user.findUnique({
						where: { email: 'seeduser1@soilstack.io' },
						include: {
							authTokens: true,
						},
					});

					// assert password was updated
					expect(await compare('password2', user?.hashedPassword as string)).toBe(true);
					// assert preexisting auth tokens were deleted
					expect(user?.authTokens).toHaveLength(1);
					expect(user?.authTokens[0].hashedToken).toBe(hash(token));

					done();
				});
		});

		it('when currentPassword is incorrect, returns 401', (done) => {
			request(testApp)
				.post('/auth/update-password')
				.set('Authorization', getDefaultUserBearerToken())
				.send({
					newPassword: 'password2',
					currentPassword: 'wrongPassword',
				})
				.expect(401, done);
		});
	});
});
