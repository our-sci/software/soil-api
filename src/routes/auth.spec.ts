import request from 'supertest';
import { prismaMock } from '../singleton';
import {
	createMockPrismaPasswordResetTokenWithUser,
	createMockPrismaUser,
	getDefaultUserBearerToken,
	testApp,
} from '../test-utils';
import { randomHex } from '../utils';

describe('Auth Endpoints', () => {
	describe('/auth/register', () => {
		it('returns a 500 if prisma.user.findUnique throws an unexpected error', (done) => {
			prismaMock.user.findUnique.mockRejectedValue(new Error('Unexpected error'));

			request(testApp)
				.post('/auth/register')
				.send({
					email: 'newUser@soilstack.io',
					password: 'password',
				})
				.expect(500, done);
		});

		it('returns a 500 if prisma.user.upsert throws an unexpected error', async (done) => {
			prismaMock.user.findUnique.mockResolvedValue(await createMockPrismaUser({ seen: false }));
			prismaMock.user.upsert.mockRejectedValue(new Error('Unexpected error'));

			request(testApp)
				.post('/auth/register')
				.send({
					email: 'newUser@soilstack.io',
					password: 'password',
				})
				.expect(500, done);
		});
	});

	describe('/auth/login', () => {
		it('returns a 500 if prisma.user.findUnique throws an unexpected error', (done) => {
			prismaMock.user.findUnique.mockRejectedValue(new Error('Unexpected error'));

			request(testApp)
				.post('/auth/login')
				.send({
					email: 'newUser@soilstack.io',
					password: 'password',
				})
				.expect(500, done);
		});

		it('returns a 500 if prisma.authToken.create throws an unexpected error', async (done) => {
			prismaMock.user.findUnique.mockResolvedValue(await createMockPrismaUser());
			prismaMock.authToken.create.mockRejectedValue(new Error('Unexpected error'));

			request(testApp)
				.post('/auth/login')
				.send({
					email: 'newUser@soilstack.io',
					password: 'password',
				})
				.expect(500, done);
		});
	});

	describe('/auth/request-password-reset', () => {
		it('returns a 500 if prisma.user.findUnique throws an unexpected error', (done) => {
			prismaMock.user.findUnique.mockRejectedValue(new Error('Unexpected error'));

			request(testApp)
				.post('/auth/request-password-reset')
				.send({
					email: 'seedUser1@soilstack.io',
				})
				.expect(500, done);
		});

		it('returns a 500 if prisma.passwordResetRequest.create throws an unexpected error', async (done) => {
			prismaMock.user.findUnique.mockResolvedValue(await createMockPrismaUser());
			prismaMock.passwordResetToken.create.mockRejectedValue(new Error('Unexpected error'));

			request(testApp)
				.post('/auth/request-password-reset')
				.send({
					email: 'seedUser1@soilstack.io',
				})
				.expect(500, done);
		});
	});

	describe('/auth/reset-password', () => {
		it('returns a 500 if prisma.passwordResetToken.findFirst throws an unexpected error', async (done) => {
			prismaMock.passwordResetToken.findFirst.mockRejectedValue(new Error('Unexpected error'));

			request(testApp)
				.post('/auth/reset-password')
				.send({
					password: 'password',
					token: randomHex(),
				})
				.expect(500, done);
		});

		it('returns a 500 if prisma.authToken.deleteMany throws an unexpected error', async (done) => {
			prismaMock.passwordResetToken.findFirst.mockResolvedValue(await createMockPrismaPasswordResetTokenWithUser());
			prismaMock.authToken.deleteMany.mockRejectedValue(new Error('Unexpected error'));

			request(testApp)
				.post('/auth/reset-password')
				.send({
					password: 'password',
					token: randomHex(),
				})
				.expect(500, done);
		});

		it('returns a 500 if prisma.user.update throws an unexpected error', async (done) => {
			prismaMock.passwordResetToken.findFirst.mockResolvedValue(await createMockPrismaPasswordResetTokenWithUser());
			prismaMock.authToken.deleteMany.mockResolvedValue({ count: 1 });
			prismaMock.user.update.mockRejectedValue(new Error('Unexpected error'));

			request(testApp)
				.post('/auth/reset-password')
				.send({
					password: 'password',
					token: randomHex(),
				})
				.expect(500, done);
		});

		it('returns a 500 if prisma.passwordResetToken.delete throws an unexpected error', async (done) => {
			const mockUser = await createMockPrismaUser();
			prismaMock.passwordResetToken.findFirst.mockResolvedValue(await createMockPrismaPasswordResetTokenWithUser());
			prismaMock.authToken.deleteMany.mockResolvedValue({ count: 1 });
			prismaMock.user.update.mockResolvedValue(mockUser);
			prismaMock.passwordResetToken.delete.mockRejectedValue(new Error('Unexpected error'));

			request(testApp)
				.post('/auth/reset-password')
				.send({
					password: 'password',
					token: randomHex(),
				})
				.expect(500, done);
		});
	});

	describe('/auth/update-password', () => {
		it('returns a 500 if prisma.authToken.deleteMany throws an unexpected error', async (done) => {
			prismaMock.authToken.deleteMany.mockRejectedValue(new Error('Unexpected error'));

			request(testApp)
				.post('/auth/update-password')
				.set('Authorization', getDefaultUserBearerToken())
				.send({
					currentPassword: 'password',
					newPassword: 'newPassword',
				})
				.expect(500, done);
		});

		it('returns a 500 if prisma.user.update throws an unexpected error', async (done) => {
			prismaMock.authToken.deleteMany.mockResolvedValue({ count: 1 });
			prismaMock.user.update.mockRejectedValue(new Error('Unexpected error'));

			request(testApp)
				.post('/auth/update-password')
				.set('Authorization', getDefaultUserBearerToken())
				.send({
					currentPassword: 'password',
					newPassword: 'newPassword',
				})
				.expect(500, done);
		});
	});
});
