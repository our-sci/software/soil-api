import { NextFunction, Request, Response, Router } from 'express';
import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';
import { APP_URL } from '../constants';
import prisma from '../prismaClient';
import { sendEmail, singleCTA } from '../services/email';
import { paths } from '../types/generated/openapi-types';
import { compare, hash, hashAndSalt, randomHex } from '../utils';

const router = Router();

router.post('/register', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const {
			email,
			password,
		} = req.body as paths['/auth/register']['post']['requestBody']['content']['application/json'];

		const trimmedPassword = password.trim();
		const lowerCasedEmail = email.toLowerCase();

		const user = await prisma.user.findUnique({
			where: { email: lowerCasedEmail },
		});

		if (user && user.seen === true) {
			res.status(409).send();
			return;
		}

		const hashedPassword = await hashAndSalt(trimmedPassword);
		const authToken = randomHex();
		const hashedAuthToken = hash(authToken);

		const userData = {
			hashedPassword,
			authTokens: {
				create: {
					hashedToken: hashedAuthToken,
				},
			},
			seen: true,
		};

		await prisma.user.upsert({
			where: { email: lowerCasedEmail },
			update: userData,
			create: {
				email: lowerCasedEmail,
				...userData,
			},
		});

		res.status(201).json({ token: authToken });
	} catch (error) {
		next(error);
	}
});

router.post('/login', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const { email, password } = req.body as paths['/auth/login']['post']['requestBody']['content']['application/json'];
		const trimmedPassword = password.trim();
		const lowerCasedEmail = email.toLowerCase();

		const user = await prisma.user.findUnique({
			where: { email: lowerCasedEmail },
		});

		if (!user || user.seen === false) {
			res.status(401).send();
			return;
		}

		const passwordsMatch = await compare(trimmedPassword, user.hashedPassword as string);
		if (!passwordsMatch) {
			res.status(401).send();
			return;
		}

		const authToken = randomHex();
		const hashedAuthToken = hash(authToken);

		await prisma.authToken.create({
			data: {
				hashedToken: hashedAuthToken,
				user: {
					connect: { email: lowerCasedEmail },
				},
			},
		});

		res.status(200).json({ token: authToken });
	} catch (error) {
		next(error);
	}
});

router.post('/request-password-reset', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const {
			email,
		} = req.body as paths['/auth/request-password-reset']['post']['requestBody']['content']['application/json'];

		const lowerCasedEmail = email.toLowerCase();

		const user = await prisma.user.findUnique({
			where: { email: lowerCasedEmail },
		});

		if (!user) {
			res.status(200).send();
			return;
		}

		const passwordResetToken = randomHex();
		const hashedPasswordResetToken = hash(passwordResetToken);

		await prisma.passwordResetToken.create({
			data: {
				hashedToken: hashedPasswordResetToken,
				expiresAt: new Date(Date.now() + 24 * 60 * 60 * 1000),
				user: {
					connect: { email: lowerCasedEmail },
				},
			},
		});

		const body = singleCTA({
			preheaderText: 'Reset your password.',
			preCtaParagraphs: [
				`We received a request to reset your password. To do so, click the link below. This link is valid for 24 hours and can only be used once.`,
			],
			ctaText: 'Reset Your Password',
			ctaHref: `${APP_URL}/auth/reset-password?token=${encodeURIComponent(passwordResetToken)}`,
			postCtaParagraphs: [
				"If you didn't make this request, you can safely ignore this email.",
				'The SoilStack.io Team',
			],
		});
		sendEmail({
			to: lowerCasedEmail,
			subject: 'SoilStack.io - Reset your password',
			body,
		});

		res.status(200).send();
	} catch (error) {
		next(error);
	}
});

router.post('/reset-password', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const {
			token,
			password,
		} = req.body as paths['/auth/reset-password']['post']['requestBody']['content']['application/json'];
		const trimmedPassword = password.trim();

		const passwordResetToken = await prisma.passwordResetToken.findFirst({
			where: { hashedToken: hash(token) },
			include: { user: true },
		});

		if (!passwordResetToken || passwordResetToken.expiresAt < new Date()) {
			res.status(401).send();
			return;
		}

		await prisma.authToken.deleteMany({
			where: { userId: passwordResetToken.user.id },
		});

		const hashedPassword = await hashAndSalt(trimmedPassword);
		const authToken = randomHex();
		const hashedAuthToken = hash(authToken);

		await prisma.user.update({
			where: { email: passwordResetToken.user.email },
			data: {
				hashedPassword,
				authTokens: {
					create: {
						hashedToken: hashedAuthToken,
					},
				},
				seen: true,
			},
		});

		await prisma.passwordResetToken.delete({
			where: { id: passwordResetToken.id },
		});

		res.status(200).send({ token: authToken });
	} catch (error) {
		next(error);
	}
});

router.post('/update-password', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const {
			newPassword,
			currentPassword,
		} = req.body as paths['/auth/update-password']['post']['requestBody']['content']['application/json'];
		const user = res.locals.user;

		const trimmedNewPassword = newPassword.trim();
		const trimmedCurrentPassword = currentPassword.trim();

		const passwordsMatch = await compare(trimmedCurrentPassword, user.hashedPassword as string);
		if (!passwordsMatch) {
			res.status(401).send();
			return;
		}

		await prisma.authToken.deleteMany({
			where: { userId: user.id },
		});

		const hashedPassword = await hashAndSalt(trimmedNewPassword);
		const authToken = randomHex();
		const hashedAuthToken = hash(authToken);

		await prisma.user.update({
			where: { email: user.email },
			data: {
				hashedPassword,
				authTokens: {
					create: {
						hashedToken: hashedAuthToken,
					},
				},
			},
		});

		res.status(200).send({ token: authToken });
	} catch (error) {
		next(error);
	}
});

const openApiPaths: OpenAPIV3.PathsObject = {
	'/auth/register': {
		post: {
			tags: ['Auth'],
			summary: 'Register a new account.',
			security: [],
			requestBody: {
				content: {
					'application/json': {
						schema: {
							type: 'object',
							required: ['email', 'password'],
							additionalProperties: false,
							properties: {
								email: { type: 'string', format: 'email' },
								password: { type: 'string' },
							},
						},
					},
				},
			},
			responses: {
				201: {
					description: 'Account created.',
					content: {
						'application/json': {
							schema: {
								type: 'object',
								required: ['token'],
								additionalProperties: false,
								properties: {
									token: {
										type: 'string',
									},
								},
							},
						},
					},
				},
				409: {
					description: 'The email address is already in use.',
				},
			},
		},
	},
	'/auth/login': {
		post: {
			tags: ['Auth'],
			summary: 'Login to an existing account.',
			security: [],
			requestBody: {
				content: {
					'application/json': {
						schema: {
							type: 'object',
							required: ['email', 'password'],
							additionalProperties: false,
							properties: {
								email: { type: 'string', format: 'email' },
								password: { type: 'string' },
							},
						},
					},
				},
			},
			responses: {
				200: {
					description: 'Login successful.',
					content: {
						'application/json': {
							schema: {
								type: 'object',
								required: ['token'],
								additionalProperties: false,
								properties: {
									token: {
										type: 'string',
									},
								},
							},
						},
					},
				},
				401: {
					description: 'Invalid credentials.',
				},
			},
		},
	},
	'/auth/request-password-reset': {
		post: {
			tags: ['Auth'],
			summary: 'Request a password reset.',
			security: [],
			requestBody: {
				content: {
					'application/json': {
						schema: {
							type: 'object',
							required: ['email'],
							additionalProperties: false,
							properties: {
								email: { type: 'string', format: 'email' },
							},
						},
					},
				},
			},
			responses: {
				200: {
					description:
						'If an account with the given email exists, an email will be sent to the address with a password reset link.',
				},
			},
		},
	},
	'/auth/reset-password': {
		post: {
			tags: ['Auth'],
			summary: 'Reset your password.',
			security: [],
			requestBody: {
				content: {
					'application/json': {
						schema: {
							type: 'object',
							required: ['password', 'token'],
							additionalProperties: false,
							properties: {
								password: { type: 'string' },
								token: { type: 'string' },
							},
						},
					},
				},
			},
			responses: {
				200: {
					description: 'Password has been reset.',
					content: {
						'application/json': {
							schema: {
								type: 'object',
								required: ['token'],
								additionalProperties: false,
								properties: {
									token: { type: 'string' },
								},
							},
						},
					},
				},
				401: {
					description: 'The token is invalid or has expired.',
				},
			},
		},
	},
	'/auth/update-password': {
		post: {
			tags: ['Auth'],
			summary: 'Update your password.',
			requestBody: {
				content: {
					'application/json': {
						schema: {
							type: 'object',
							required: ['newPassword', 'currentPassword'],
							additionalProperties: false,
							properties: {
								newPassword: { type: 'string' },
								currentPassword: { type: 'string' },
							},
						},
					},
				},
			},
			responses: {
				200: {
					description: 'Password has been updated.',
					content: {
						'application/json': {
							schema: {
								type: 'object',
								required: ['token'],
								additionalProperties: false,
								properties: {
									token: { type: 'string' },
								},
							},
						},
					},
				},
				401: {
					description: 'Invalid credentials.',
				},
			},
		},
	},
};

export { openApiPaths, router };
