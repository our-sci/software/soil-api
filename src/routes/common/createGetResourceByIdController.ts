import { Membership as PrismaMembership, Prisma } from '@prisma/client';
import { NextFunction, Request, Response } from 'express';
import { ResourceTypes } from '../../constants';
import prisma from '../../prismaClient';
import {
	prismaAreaToArea,
	prismaFieldToField,
	prismaLocationCollectionToLocationCollection,
	prismaLocationToLocation,
	prismaSampleToSample,
	prismaSamplingCollectionToSamplingCollection,
	prismaSamplingToSampling,
	prismaStratificationToStratification,
} from '../../transforms';
import { PrismaFlatSampling } from '../../types/prisma';

const getResourceById = async ({
	resourceType,
	id,
	userGroupIds,
}: {
	resourceType: ResourceTypes;
	id: string;
	userGroupIds: string[];
}) => {
	switch (resourceType) {
		case ResourceTypes.Field: {
			const include = Prisma.validator<Prisma.FieldInclude>()({
				referenceIds: true,
				areas: { select: { id: true } },
			});

			const getFieldById = async (id: string) =>
				await prisma.field.findFirst({
					where: {
						id,
						groupId: { in: userGroupIds },
					},
					include,
				});

			type FieldReturnType = Prisma.PromiseReturnType<typeof getFieldById>;

			const resource: FieldReturnType = await getFieldById(id);

			return resource ? prismaFieldToField(resource) : null;
		}
		case ResourceTypes.Area: {
			const getAreaById = async (id: string) =>
				await prisma.area.findFirst({
					where: {
						id,
						groupId: { in: userGroupIds },
					},
				});

			type AreaReturnType = Prisma.PromiseReturnType<typeof getAreaById>;

			const resource: AreaReturnType = await getAreaById(id);

			return resource ? prismaAreaToArea(resource) : null;
		}
		case ResourceTypes.Stratification: {
			const getStratificationById = async (id: string) =>
				await prisma.stratification.findFirst({
					where: {
						id,
						groupId: { in: userGroupIds },
					},
					include: {
						algorithm: true,
						area: { select: { fieldId: true } },
					},
				});

			type StratificationReturnType = Prisma.PromiseReturnType<typeof getStratificationById>;

			const resource: StratificationReturnType = await getStratificationById(id);

			return resource ? prismaStratificationToStratification(resource) : null;
		}
		case ResourceTypes.LocationCollection: {
			const getLocationCollectionById = async (id: string) =>
				await prisma.locationCollection.findFirst({
					where: {
						id,
						groupId: { in: userGroupIds },
					},
					include: { locations: { select: { id: true } } },
				});

			type LocationCollectionReturnType = Prisma.PromiseReturnType<typeof getLocationCollectionById>;

			const resource: LocationCollectionReturnType = await getLocationCollectionById(id);

			return resource ? prismaLocationCollectionToLocationCollection(resource) : null;
		}
		case ResourceTypes.Location: {
			const getLocationById = async (id: string) =>
				await prisma.location.findFirst({
					where: {
						id,
						groupId: { in: userGroupIds },
					},
				});

			type LocationReturnType = Prisma.PromiseReturnType<typeof getLocationById>;

			const resource: LocationReturnType = await getLocationById(id);

			return resource ? prismaLocationToLocation(resource) : null;
		}
		case ResourceTypes.SamplingCollection: {
			const getSamplingCollectionById = async (id: string) =>
				await prisma.samplingCollection.findFirst({
					where: {
						id,
						groupId: { in: userGroupIds },
					},
					include: {
						samplings: { select: { id: true } },
						referenceIds: true,
					},
				});

			type SamplingCollectionReturnType = Prisma.PromiseReturnType<typeof getSamplingCollectionById>;

			const resource: SamplingCollectionReturnType = await getSamplingCollectionById(id);

			return resource ? prismaSamplingCollectionToSamplingCollection(resource) : null;
		}
		case ResourceTypes.Sampling: {
			const getSamplingById = async (id: string) =>
				await prisma.sampling.findFirst({
					where: {
						id,
						groupId: { in: userGroupIds },
					},
					include: { samples: { select: { id: true } } },
				});

			const resource = await getSamplingById(id);

			return resource ? prismaSamplingToSampling(resource as PrismaFlatSampling) : null;
		}
		case ResourceTypes.Sample: {
			const getSampleById = async (id: string) =>
				await prisma.sample.findFirst({
					where: {
						id,
						groupId: { in: userGroupIds },
					},
				});

			type SampleReturnType = Prisma.PromiseReturnType<typeof getSampleById>;

			const resource: SampleReturnType = await getSampleById(id);

			return resource ? prismaSampleToSample(resource) : null;
		}
	}
};

const createGetResourceByIdController = (resourceType: ResourceTypes) => async (
	req: Request,
	res: Response,
	next: NextFunction
): Promise<any> => {
	try {
		const id = req.params.id;
		const userGroupIds = res.locals.user.memberships.map((m: PrismaMembership) => m.groupId);

		const resource = await getResourceById({ resourceType, id, userGroupIds });

		if (resource) {
			res.status(200).json(resource);
		} else {
			res.status(404).send();
		}
	} catch (error) {
		next(error);
	}
};

export default createGetResourceByIdController;
