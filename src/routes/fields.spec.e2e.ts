import request from 'supertest';
import { ResourceTypes } from '../constants';
import prisma from '../prismaClient';
import { testApp } from '../test-utils';
import {
	createMockPopulatedField,
	createMockStratificationConfig,
	expect200AndValidateResponse,
	expect201AndValidateResponse,
	expect400AndValidateResponse,
	expect401AndValidateResponse,
	expect404AndValidateResponse,
	expect409AndValidateResponse,
	getDefaultUserBearerToken,
	getUserBearerToken,
	postMockField,
	postMockStratification,
	registerUser,
	testGetResourceByIdEndpoint,
} from '../test-utils';
import { createMockAddress, createMockContactPoint } from '../test-utils';
import { stratServiceJobToPrismaStratificationJobCreate } from '../transforms';
import { components } from '../types/generated/openapi-types';

describe('Field Endpoints', () => {
	describe('GET', () => {
		describe('/fields', () => {
			it('returns 200 with fields the user has access to on success', async (done) => {
				// add a field to a group the user belongs to
				const postFieldResponse = await postMockField();
				const postedField = postFieldResponse.body.fields[0];
				// add a field to a group the user does NOT belong to
				await postMockField({ groupId: 'seed-group-2' });

				request(testApp)
					.get(`/fields`)
					.set('Authorization', getDefaultUserBearerToken())
					.send()
					.end((err, res) => {
						// assert that the user only sees the field in the group they belong to
						expect(res.body).toHaveLength(1);
						expect(res.body[0]).toHaveProperty('id', postedField.id);
						expect(res.body[0]).toHaveProperty('meta.groupId', 'seed-group-1');
						expect200AndValidateResponse(done)(err, res);
					});
			});

			it('returns 200 on success when no fields are present', async (done) => {
				request(testApp)
					.get(`/fields`)
					.set('Authorization', getDefaultUserBearerToken())
					.end(expect200AndValidateResponse(done));
			});
		});

		testGetResourceByIdEndpoint({
			endpointPath: '/fields',
			resourceName: 'field',
			resourceType: ResourceTypes.Field,
		});

		describe('/fields/owner/:owner/:id', () => {
			it('returns 200 with an array containing fields that have the given reference id', async (done) => {
				// add 2 fields to a group the user belongs to
				const {
					body: {
						fields: [field1],
					},
				} = await postMockField({
					meta: {
						referenceIds: [{ owner: 'surveystack', id: '1234' }],
					},
				});
				const {
					body: {
						fields: [field2],
					},
				} = await postMockField({
					meta: {
						referenceIds: [{ owner: 'surveystack', id: '1234' }],
					},
				});
				// add a field to a group the user does NOT belong to
				await postMockField({
					meta: {
						referenceIds: [{ owner: 'surveystack', id: '1234' }],
					},
					groupId: 'seed-group-2',
				});

				request(testApp)
					.get(`/fields/owner/surveystack/1234`)
					.set('Authorization', getDefaultUserBearerToken())
					.send()
					.end((err, res) => {
						expect(res.body).toHaveLength(2);
						expect(res.body).toContainEqual(
							expect.objectContaining({
								id: field1.id,
								meta: expect.objectContaining({
									groupId: 'seed-group-1',
									referenceIds: [{ owner: 'surveystack', id: '1234' }],
								}),
							})
						);
						expect(res.body).toContainEqual(
							expect.objectContaining({
								id: field2.id,
								meta: expect.objectContaining({
									groupId: 'seed-group-1',
									referenceIds: [{ owner: 'surveystack', id: '1234' }],
								}),
							})
						);
						expect200AndValidateResponse(done)(err, res);
					});
			});

			it('returns 400 when the owner is not a valid reference id owner', async (done) => {
				request(testApp)
					.get(`/fields/owner/invalid-owner/1234`)
					.set('Authorization', getDefaultUserBearerToken())
					.send()
					.end(expect400AndValidateResponse(done));
			});

			it('returns 404 when no fields exists matching the owner and id', async (done) => {
				request(testApp)
					.get(`/fields/owner/surveystack/1234`)
					.set('Authorization', getDefaultUserBearerToken())
					.send()
					.end(expect404AndValidateResponse(done));
			});

			it('returns 404 when the requesting user does not have access to any of the matching fields', async (done) => {
				// add a field to a group the user does NOT belong to
				await postMockField({
					meta: {
						referenceIds: [{ owner: 'surveystack', id: '1234' }],
					},
					groupId: 'seed-group-2',
				});

				request(testApp)
					.get(`/fields/owner/surveystack/1234`)
					.set('Authorization', getDefaultUserBearerToken())
					.send()
					.end(expect404AndValidateResponse(done));
			});
		});
	});

	describe('POST', () => {
		describe('/fields', () => {
			it('returns 201 on success', (done) => {
				request(testApp)
					.post('/fields?groupId=seed-group-1')
					.set('Authorization', getDefaultUserBearerToken())
					.send(
						createMockPopulatedField({
							meta: {
								referenceIds: [
									{
										owner: 'surveystack',
										id: '1234',
									},
									{
										owner: 'soilstack-draft',
										id: '1234',
									},
								],
							},
						})
					)
					.end((err, res) => {
						const fields = res.body.fields;
						const areas = res.body.areas;

						expect(fields).toHaveLength(1);
						expect(areas).toHaveLength(1);

						expect(fields[0].areas, 'field.areas should contain the area id of the area that was created').toContain(
							areas[0].id
						);

						expect(fields[0], 'field.meta.groupId should match the groupId query param').toHaveProperty(
							'meta.groupId',
							'seed-group-1'
						);

						expect(areas[0], 'area.properties.featureOfInterest should match the id of the field').toHaveProperty(
							'properties.featureOfInterest',
							fields[0].id
						);

						expect(areas[0], 'area.meta.groupId should match the groupId query param').toHaveProperty(
							'meta.groupId',
							'seed-group-1'
						);

						expect(
							fields[0].meta.referenceIds,
							'field.meta.referenceIds should contain the passed in reference ids'
						).toEqual([
							{ id: '1234', owner: 'surveystack' },
							{
								owner: 'soilstack-draft',
								id: '1234',
							},
						]);

						expect201AndValidateResponse(done)(err, res);
					});
			});

			it('returns 404 if the user does not have access to the group referenced by the groupId query parameter', (done) => {
				request(testApp)
					.post('/fields?groupId=seed-group-2')
					.set('Authorization', getDefaultUserBearerToken())
					.send(createMockPopulatedField())
					.end(expect404AndValidateResponse(done));
			});
		});
	});

	describe('PATCH', () => {
		describe('/fields/:id', () => {
			const editedField: components['schemas']['EditableField'] = {
				name: 'modified name',
				producerName: 'modified producerName',
				address: createMockAddress({
					streetAddress: 'modified streetAddress',
					addressLocality: 'modified addressLocality',
					addressRegion: 'modified addressRegion',
					addressCountry: 'modified addressCountry',
					postalCode: 'modified postalCode',
				}),
				contactPoints: [
					createMockContactPoint({
						telephone: 'modified telephone',
						email: 'modified email',
						name: 'modified name',
						contactType: 'modified contactType',
						organization: 'modified organization',
					}),
				],
			};

			it('returns 204 and updates the field on success', async (done) => {
				const postFieldResponse = await postMockField();
				const postedField = postFieldResponse.body.fields[0];

				await request(testApp)
					.patch(`/fields/${postedField.id}`)
					.set('Authorization', getDefaultUserBearerToken())
					.send(editedField)
					.expect(204);

				const updatedField = await prisma.field.findUnique({ where: { id: postedField.id } });
				expect(updatedField).toHaveProperty('name', editedField.name);
				expect(updatedField).toHaveProperty('producerName', editedField.producerName);
				expect(updatedField).toHaveProperty('streetAddress', editedField.address?.streetAddress);
				expect(updatedField).toHaveProperty('addressLocality', editedField.address?.addressLocality);
				expect(updatedField).toHaveProperty('addressRegion', editedField.address?.addressRegion);
				expect(updatedField).toHaveProperty('addressCountry', editedField.address?.addressCountry);
				expect(updatedField).toHaveProperty('postalCode', editedField.address?.postalCode);
				expect(updatedField).toHaveProperty('contactPoints', editedField.contactPoints);

				done();
			});

			it('returns 404 when a field with the given id is not found', (done) => {
				request(testApp)
					.patch('/fields/non-existent-field-id')
					.set('Authorization', getDefaultUserBearerToken())
					.send(editedField)
					.end(expect404AndValidateResponse(done));
			});

			it('returns 404 when the user does not have access to the group the field is in', async (done) => {
				const postFieldResponse = await postMockField({ groupId: 'seed-group-2' });
				const postedField = postFieldResponse.body.fields[0];

				request(testApp)
					.patch(`/fields/${postedField.id}`)
					.set('Authorization', getDefaultUserBearerToken())
					.send(editedField)
					.end(expect404AndValidateResponse(done));
			});
		});
	});

	describe('DELETE', () => {
		describe('/fields/:id', () => {
			describe('when the requesting user is a super admin', () => {
				beforeEach(async () => {
					await registerUser({
						id: 'seed-super-admin-1',
						email: 'seedSuperAdmin1@soilstack.io',
						password: 'password',
						groupIds: [],
						isSuperAdmin: true,
					});
				});

				it('returns 204 and deletes the field, its areas, and any related closed stratification jobs when the field exists and has no dependent resources', async (done) => {
					const {
						body: {
							fields: [{ id: fieldId }],
							areas: [{ id: areaId }],
						},
					} = await postMockField();
					const [
						{ id: stratJobId1 },
						{ id: stratJobId2 },
						{ id: stratJobId3 },
						{ id: stratJobId4 },
					] = await Promise.all([
						prisma.stratificationJob.create({
							data: stratServiceJobToPrismaStratificationJobCreate({
								jobId: 'mock-job-id-1',
								jobStatus: 'SUCCESS',
								areaId,
								stratificationConfig: createMockStratificationConfig(),
								isClosed: true,
								closedReason: 'CANCELLED',
							}),
						}),
						prisma.stratificationJob.create({
							data: stratServiceJobToPrismaStratificationJobCreate({
								jobId: 'mock-job-id-2',
								jobStatus: 'SUCCESS',
								areaId,
								stratificationConfig: createMockStratificationConfig(),
								isClosed: true,
								closedReason: 'APPROVED',
							}),
						}),
						prisma.stratificationJob.create({
							data: stratServiceJobToPrismaStratificationJobCreate({
								jobId: 'mock-job-id-3',
								jobStatus: 'PENDING',
								areaId,
								stratificationConfig: createMockStratificationConfig(),
								isClosed: true,
								closedReason: 'TIMED_OUT',
							}),
						}),
						prisma.stratificationJob.create({
							data: stratServiceJobToPrismaStratificationJobCreate({
								jobId: 'mock-job-id-4',
								jobStatus: 'SUCCESS',
								areaId,
								stratificationConfig: createMockStratificationConfig(),
								isClosed: true,
								closedReason: 'REJECTED',
							}),
						}),
					]);

					await request(testApp)
						.delete(`/fields/${fieldId}`)
						.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
						.expect(204);

					const deletedField = await prisma.field.findUnique({ where: { id: fieldId } });
					const deletedArea = await prisma.area.findUnique({ where: { id: areaId } });
					const deletedStratJob1 = await prisma.stratificationJob.findUnique({ where: { id: stratJobId1 } });
					const deletedStratJob2 = await prisma.stratificationJob.findUnique({ where: { id: stratJobId2 } });
					const deletedStratJob3 = await prisma.stratificationJob.findUnique({ where: { id: stratJobId3 } });
					const deletedStratJob4 = await prisma.stratificationJob.findUnique({ where: { id: stratJobId4 } });
					expect(deletedField).toBeNull();
					expect(deletedArea).toBeNull();
					expect(deletedStratJob1).toBeNull();
					expect(deletedStratJob2).toBeNull();
					expect(deletedStratJob3).toBeNull();
					expect(deletedStratJob4).toBeNull();

					done();
				});

				it("returns 404 when the field doesn't exist", (done) => {
					request(testApp)
						.delete('/fields/non-existent-field-id')
						.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
						.end(expect404AndValidateResponse(done));
				});

				it('returns 409 when a stratification depends on the field', async (done) => {
					const {
						body: {
							fields: [{ id: fieldId }],
							areas: [{ id: areaId }],
						},
					} = await postMockField();
					await postMockStratification({ areaId });

					request(testApp)
						.delete(`/fields/${fieldId}`)
						.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
						.end(expect409AndValidateResponse(done));
				});

				it("returns 409 when any of the field's areas have an open stratification job", async (done) => {
					const {
						body: {
							fields: [{ id: fieldId }],
							areas: [{ id: areaId }],
						},
					} = await postMockField();
					await prisma.stratificationJob.create({
						data: stratServiceJobToPrismaStratificationJobCreate({
							jobId: 'mock-job-id',
							jobStatus: 'PENDING',
							areaId,
							stratificationConfig: createMockStratificationConfig(),
							isClosed: false,
						}),
					});

					request(testApp)
						.delete(`/fields/${fieldId}`)
						.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
						.end(expect409AndValidateResponse(done));
				});
			});

			describe('when the requesting user is not a super admin', () => {
				it('returns 401', (done) => {
					request(testApp)
						.delete('/fields/any-field-id')
						.set('Authorization', getDefaultUserBearerToken())
						.end(expect401AndValidateResponse(done));
				});
			});
		});
	});
});
