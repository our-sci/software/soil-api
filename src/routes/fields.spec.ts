import request from 'supertest';
import { prismaMock } from '../singleton';
import {
	createMockPrismaField,
	getDefaultSuperAdminBearerToken,
	getDefaultUserBearerToken,
	testApp,
} from '../test-utils';
import { createMockField, createMockPopulatedField } from '../test-utils';
import { components } from '../types/generated/openapi-types';

describe('Field Endpoints', () => {
	describe('POST', () => {
		describe('/fields', () => {
			it('returns 500 when prisma.field.create throws an unexpected error', (done) => {
				prismaMock.field.create.mockRejectedValueOnce(new Error('unexpected error'));

				request(testApp)
					.post(`/fields?groupId=seed-group-1`)
					.set('Authorization', getDefaultUserBearerToken())
					.send(createMockPopulatedField())
					.expect(500, done);
			});
		});
	});

	describe('GET', () => {
		describe('/fields', () => {
			it('returns 500 when prismaClient throws an unexpected error', (done) => {
				prismaMock.field.findMany.mockRejectedValueOnce(new Error('unexpected error'));

				request(testApp).get(`/fields`).set('Authorization', getDefaultUserBearerToken()).expect(500, done);
			});
		});

		describe('/fields/:id', () => {
			it('returns 500 when prisma.field.findFirst throws an unexpected error', (done) => {
				prismaMock.field.findFirst.mockRejectedValueOnce(new Error('unexpected error'));

				request(testApp)
					.get('/fields/non-existent-id')
					.set('Authorization', getDefaultUserBearerToken())
					.expect(500, done);
			});
		});

		describe('/fields/owner/:owner/:id', () => {
			it('returns 500 when prisma.field.findMany throws an unexpected error', (done) => {
				prismaMock.field.findMany.mockRejectedValueOnce(new Error('unexpected error'));

				request(testApp)
					.get('/fields/owner/surveystack/1234')
					.set('Authorization', getDefaultUserBearerToken())
					.expect(500, done);
			});
		});
	});

	describe('PATCH', () => {
		describe('/fields/:id', () => {
			const mockField = createMockField();
			const { name, producerName, address, contactPoints } = mockField;
			const mockEditableField: components['schemas']['EditableField'] = {
				name,
				producerName,
				address,
				contactPoints,
			};

			it('returns 500 when prisma.field.update throws an unexpected error', (done) => {
				prismaMock.field.findUnique.mockResolvedValue(createMockPrismaField());
				prismaMock.field.update.mockRejectedValueOnce(new Error('unexpected error'));

				request(testApp)
					.patch(`/fields/field-id`)
					.set('Authorization', getDefaultUserBearerToken())
					.send(mockEditableField)
					.expect(500, done);
			});

			it('returns 500 when prisma.field.findUnique throws an unexpected error', (done) => {
				prismaMock.field.findUnique.mockRejectedValueOnce(new Error('unexpected error'));

				request(testApp)
					.patch(`/fields/field-id`)
					.set('Authorization', getDefaultUserBearerToken())
					.send(mockEditableField)
					.expect(500, done);
			});
		});
	});

	describe('DELETE', () => {
		describe('/fields/:id', () => {
			it('returns 500 when prisma.field.delete throws an unexpected error', (done) => {
				prismaMock.field.delete.mockRejectedValue(new Error('Unexpected error'));

				request(testApp)
					.delete('/fields/123456789012345678901234')
					.set('Authorization', getDefaultSuperAdminBearerToken())
					.send()
					.expect(500, done);
			});
		});
	});
});
