import { Membership as PrismaMembership, Prisma } from '@prisma/client';
import { NextFunction, Request, Response, Router } from 'express';
import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';
import { ResourceTypes } from '../constants';
import { groupIdQueryParam } from '../open-api-spec/common/queryParameters';
import prisma from '../prismaClient';
import {
	areaToPrismaArea,
	editableFieldToPrismaFieldUpdateInput,
	populatedFieldRequestToPrismaFieldCreateInput,
	prismaAreaToArea,
	prismaFieldToField,
	referenceIdToPrismaReferenceId,
} from '../transforms';
import { components, paths } from '../types/generated/openapi-types';
import createGetResourceByIdController from './common/createGetResourceByIdController';

const router = Router();

router.post('/', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const groupId = req.query.groupId as string;

		const isAuthorized = res.locals.user.memberships.some((m: PrismaMembership) => m.groupId === groupId);
		if (!isAuthorized) {
			return res.status(404).send();
		}

		const body = req.body as components['schemas']['PopulatedField'];
		const field = populatedFieldRequestToPrismaFieldCreateInput(body, groupId);

		const prismaField = await prisma.field.create({
			data: {
				...field,
				areas: {
					createMany: {
						data: body.areas?.map((area) => areaToPrismaArea(area, groupId)),
					},
				},
				referenceIds: {
					createMany: {
						data: body.meta?.referenceIds?.map(referenceIdToPrismaReferenceId) ?? [],
					},
				},
			},
			include: {
				areas: true,
				referenceIds: true,
			},
		});

		return res.status(201).json({
			fields: [prismaFieldToField(prismaField)],
			areas: prismaField.areas.map((area) => prismaAreaToArea(area)),
		});
	} catch (error) {
		next(error);
	}
});

router.get('/', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const userGroupIds = res.locals.user.memberships.map((m: PrismaMembership) => m.groupId);

		const prismaFields = await prisma.field.findMany({
			include: { areas: true, referenceIds: true },
			where: { groupId: { in: userGroupIds } },
		});

		const fields = prismaFields.map(prismaFieldToField);

		return res.status(200).json(fields);
	} catch (error) {
		next(error);
	}
});

router.get('/:id', createGetResourceByIdController(ResourceTypes.Field));

router.get('/owner/:owner/:id', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const params = req.params as paths['/fields/owner/{owner}/{id}']['get']['parameters']['path'];
		const { owner, id } = params;
		const userGroupIds = res.locals.user.memberships.map((m: PrismaMembership) => m.groupId);

		const prismaFields = await prisma.field.findMany({
			include: { areas: { select: { id: true } }, referenceIds: true },
			where: { groupId: { in: userGroupIds }, referenceIds: { some: { owner, value: id } } },
		});

		if (prismaFields.length === 0) {
			return res.status(404).send();
		}

		const fields = prismaFields.map(prismaFieldToField);

		return res.status(200).json(fields);
	} catch (error) {
		next(error);
	}
});

router.delete('/:id', async (req: Request, res: Response, next: NextFunction) => {
	try {
		if (!res.locals.user.isSuperAdmin) {
			return res.status(401).send();
		}

		const { id } = req.params as paths['/fields/{id}']['delete']['parameters']['path'];

		// TODO: Can these 2 prisma queries be condensed into a single prisma query?
		const areas = await prisma.area.findMany({
			where: { fieldId: id },
			select: { id: true },
		});
		const openStratificationJobsCount = await prisma.stratificationJob.count({
			where: { isClosed: false, areaId: { in: areas.map(({ id }) => id) } },
		});
		if (openStratificationJobsCount > 0) {
			return res.status(409).send({
				message: 'Cannot delete a field that has open stratification jobs.',
			});
		}

		await prisma.field.delete({ where: { id } });

		return res.status(204).send();
	} catch (error) {
		if (error instanceof Prisma.PrismaClientKnownRequestError) {
			// P2025 is the Prisma Client error code for RecordNotFound
			if (error.code === 'P2025') {
				return res.status(404).send();
			}
			// P2003 is the Prisma Client error code for foreign key constraints
			if (error.code === 'P2003') {
				return res.status(409).send({
					message: 'Cannot delete a field that has a stratification depending on it.',
				});
			}
		}

		next(error);
	}
});

router.patch('/:id', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const id = req.params.id;
		const body = req.body as components['schemas']['EditableField'];
		const userGroupIds = res.locals.user.memberships.map((m: PrismaMembership) => m.groupId);

		const field = await prisma.field.findUnique({ where: { id } });
		if (!field) {
			return res.status(404).send();
		}

		const isAuthorized = userGroupIds.includes(field.groupId);
		if (!isAuthorized) {
			return res.status(404).send();
		}

		await prisma.field.update({
			where: { id },
			data: editableFieldToPrismaFieldUpdateInput(body),
		});

		return res.status(204).send();
	} catch (error) {
		if (error instanceof Prisma.PrismaClientKnownRequestError) {
			// P2025 is the Prisma Client error code for RecordNotFound
			if (error.code === 'P2025') {
				return res.status(404).send();
			}
		}

		next(error);
	}
});

const openApiPaths: OpenAPIV3.PathsObject = {
	'/fields': {
		post: {
			tags: ['Fields'],
			summary: 'Saves a field.',
			parameters: [groupIdQueryParam],
			requestBody: {
				description: 'The field object to be created.',
				content: {
					'application/json': {
						schema: {
							$ref: '#/components/schemas/PopulatedField',
						},
					},
				},
				required: true,
			},
			responses: {
				'201': {
					description: 'Successfully created the field.',
					content: {
						'application/json': {
							schema: {
								type: 'object',
								required: ['fields', 'areas'],
								properties: {
									fields: { type: 'array', items: { $ref: '#/components/schemas/ResponseField' } },
									areas: { type: 'array', items: { $ref: '#/components/schemas/ResponseArea' } },
								},
							},
						},
					},
				},
				401: { $ref: '#/components/responses/401' },
			},
		},
		get: {
			tags: ['Fields'],
			summary: 'Get all fields.',
			responses: {
				'200': {
					description: 'An array of the fields you have access to.',
					content: {
						'application/json': {
							schema: {
								type: 'array',
								items: { $ref: '#/components/schemas/ResponseField' },
							},
						},
					},
				},
				401: { $ref: '#/components/responses/401' },
			},
		},
	},
	'/fields/{id}': {
		get: {
			tags: ['Fields'],
			summary: 'Get a field by its id.',
			parameters: [
				{
					name: 'id',
					in: 'path',
					description: 'The id of the field to get.',
					required: true,
					schema: { type: 'string' },
				},
			],
			responses: {
				'200': {
					description: 'The field with the given id.',
					content: {
						'application/json': {
							schema: {
								$ref: '#/components/schemas/ResponseField',
							},
						},
					},
				},
				'404': {
					description: 'No field with the given id exists.',
				},
				401: { $ref: '#/components/responses/401' },
			},
		},
		patch: {
			tags: ['Fields'],
			summary: 'Updates a field.',
			parameters: [
				{
					name: 'id',
					in: 'path',
					description: 'The id of the field to edit.',
					required: true,
					schema: { type: 'string' },
				},
			],
			requestBody: {
				description: 'The field object to be updated.',
				required: true,
				content: {
					'application/json': {
						schema: {
							$ref: '#/components/schemas/EditableField',
						},
					},
				},
			},
			responses: {
				'204': {
					description: 'The field was saved successfully.',
				},
				'404': {
					description: 'No field with the given id exists.',
				},
				401: { $ref: '#/components/responses/401' },
			},
		},
		delete: {
			'x-internal': true,
			tags: ['Fields'],
			summary: 'Delete a field by its id.',
			parameters: [
				{
					name: 'id',
					in: 'path',
					description: 'The id of the field to delete.',
					required: true,
					schema: { type: 'string' },
				},
			],
			responses: {
				'204': {
					description: 'The field was deleted successfully.',
				},
				'404': {
					description: 'No field with the given id exists.',
				},
				'409': {
					description: 'Cannot delete a field that has resources that depend on it.',
					content: {
						'application/json': {
							schema: {
								$ref: '#/components/schemas/Error',
							},
						},
					},
				},
				401: { $ref: '#/components/responses/401' },
			},
		} as OpenAPIV3.OperationObject,
	},
	'/fields/owner/{owner}/{id}': {
		get: {
			tags: ['Fields'],
			summary: 'Get fields by reference id.',
			parameters: [
				{
					name: 'owner',
					in: 'path',
					description: 'The reference id owner.',
					required: true,
					schema: {
						$ref: '#/components/schemas/ReferenceIdOwners',
					},
				},
				{
					name: 'id',
					in: 'path',
					description: 'The reference id.',
					required: true,
					schema: { type: 'string' },
				},
			],
			responses: {
				'200': {
					description: 'An array of fields with the given reference id.',
					content: {
						'application/json': {
							schema: {
								type: 'array',
								items: {
									$ref: '#/components/schemas/ResponseField',
								},
							},
						},
					},
				},
				'404': {
					description: 'No field with the given reference id exists.',
				},
				401: { $ref: '#/components/responses/401' },
			},
		},
	},
};

export { openApiPaths, router };
