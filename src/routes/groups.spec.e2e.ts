import request from 'supertest';
import prisma from '../prismaClient';
import {
	expect200AndValidateResponse,
	expect201AndValidateResponse,
	expect204AndValidateResponse,
	expect400AndValidateResponse,
	expect401AndValidateResponse,
	expect404AndValidateResponse,
	expect409AndValidateResponse,
	getDefaultUserBearerToken,
	getUserBearerToken,
	postMockApiKey,
	postMockField,
	postMockGroup,
	postMockMembership,
	registerUser,
	testApp,
} from '../test-utils';

describe('Group Endpoints', () => {
	describe('GET', () => {
		describe('/groups', () => {
			let apiOnlyGroupId: string;
			beforeEach(async () => {
				await registerUser({
					id: 'seed-super-admin-1',
					email: 'seedSuperAdmin1@soilstack.io',
					password: 'password',
					groupIds: [],
					isSuperAdmin: true,
				});
				({
					body: { id: apiOnlyGroupId },
				} = await postMockGroup({
					name: 'API Only Group',
					parentGroupId: 'seed-group-1',
					apiOnly: true,
					authorizationHeaderValue: getUserBearerToken('seedSuperAdmin1@soilstack.io'),
				}));
			});

			describe('when the authorization scheme is bearer (frontend request)', () => {
				it('returns a 200 with the groups the user has access to except api only groups on success', (done) => {
					request(testApp)
						.get(`/groups`)
						.set('Authorization', getDefaultUserBearerToken())
						.send()
						.end((err, res) => {
							expect(res.body).toHaveLength(1);
							expect(res.body[0]).toHaveProperty('id', 'seed-group-1');
							expect(res.body[0]).toHaveProperty('name', 'Seed Group 1');
							expect(res.body[0]).toHaveProperty('path', '/seed-group-1/');
							expect200AndValidateResponse(done)(err, res);
						});
				});

				it('returns a 200 with all groups when the requesting user is a super admin', async (done) => {
					request(testApp)
						.get(`/groups`)
						.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
						.send()
						.end((err, res) => {
							expect(res.body).toHaveLength(3);
							expect(res.body).toContainEqual(
								expect.objectContaining({
									id: 'seed-group-1',
									name: 'Seed Group 1',
									path: '/seed-group-1/',
								})
							);
							expect(res.body).toContainEqual(
								expect.objectContaining({
									id: apiOnlyGroupId,
									name: 'API Only Group',
									path: '/seed-group-1/api-only-group/',
								})
							);
							expect(res.body).toContainEqual(
								expect.objectContaining({
									id: 'seed-group-2',
									name: 'Seed Group 2',
									path: '/seed-group-2/',
								})
							);
							expect200AndValidateResponse(done)(err, res);
						});
				});

				it('returns the full group path, even if the user does not have access to all of the groups in the path', async (done) => {
					const {
						body: { id: subGroupTwoId },
					} = await postMockGroup({ name: 'Subgroup 2', parentGroupId: 'seed-group-2' });
					await postMockMembership({ groupId: subGroupTwoId, userEmail: 'seedUser1@soilstack.io' });

					request(testApp)
						.get(`/groups`)
						.set('Authorization', getDefaultUserBearerToken())
						.send()
						.end((err, res) => {
							const subGroupTwo = res.body.find((group: any) => group.id === subGroupTwoId);
							expect(subGroupTwo).toHaveProperty('path', '/seed-group-2/subgroup-2/');
							expect200AndValidateResponse(done)(err, res);
						});
				});
			});

			describe('when the authorization scheme is apikey (non frontend request)', () => {
				it('returns a 200 with the groups the user has access, including api only groups on success', async (done) => {
					const {
						body: { apiKey },
					} = await postMockApiKey({ authToken: getDefaultUserBearerToken() });

					request(testApp)
						.get(`/groups`)
						.set('Authorization', `apikey ${apiKey}`)
						.send()
						.end((err, res) => {
							expect(res.body).toHaveLength(2);
							expect(res.body[0]).toHaveProperty('id', 'seed-group-1');
							expect(res.body[0]).toHaveProperty('name', 'Seed Group 1');
							expect(res.body[0]).toHaveProperty('path', '/seed-group-1/');
							expect(res.body[1]).toHaveProperty('id', apiOnlyGroupId);
							expect(res.body[1]).toHaveProperty('name', 'API Only Group');
							expect(res.body[1]).toHaveProperty('path', '/seed-group-1/api-only-group/');
							expect200AndValidateResponse(done)(err, res);
						});
				});

				it('returns a 200 with all groups, including api only groups when the requesting user is a super admin', async (done) => {
					const {
						body: { apiKey },
					} = await postMockApiKey({ authToken: getUserBearerToken('seedSuperAdmin1@soilstack.io') });

					request(testApp)
						.get(`/groups`)
						.set('Authorization', `apikey ${apiKey}`)
						.send()
						.end((err, res) => {
							expect(res.body).toHaveLength(3);
							expect(res.body).toContainEqual(
								expect.objectContaining({
									id: 'seed-group-1',
									name: 'Seed Group 1',
									path: '/seed-group-1/',
								})
							);
							expect(res.body).toContainEqual(
								expect.objectContaining({
									id: apiOnlyGroupId,
									name: 'API Only Group',
									path: '/seed-group-1/api-only-group/',
								})
							);
							expect(res.body).toContainEqual(
								expect.objectContaining({
									id: 'seed-group-2',
									name: 'Seed Group 2',
									path: '/seed-group-2/',
								})
							);
							expect200AndValidateResponse(done)(err, res);
						});
				});
			});
		});
	});

	describe('POST', () => {
		describe('/groups', () => {
			describe('when the requesting user is not a super admin', () => {
				it('returns 400 when the request body contains the apiOnly property', (done) => {
					request(testApp)
						.post(`/groups`)
						.set('Authorization', getDefaultUserBearerToken())
						.send({ name: 'New Group', apiOnly: true })
						.end(expect400AndValidateResponse(done));
				});

				describe('When parentGroupId query parameter is present', () => {
					it('returns 201 on success, creates the group within the parent group, and creates memberships to the new group for any users with membership to the parent group, and sets apiOnly to false by default', async (done) => {
						request(testApp)
							.post(`/groups?parentGroupId=seed-group-1`)
							.set('Authorization', getDefaultUserBearerToken())
							.send({ name: 'Nested Group' })
							.end(async (err, res) => {
								expect201AndValidateResponse()(err, res);
								expect(res.body).toHaveProperty('name', 'Nested Group');
								expect(res.body).toHaveProperty('path', '/seed-group-1/nested-group/');
								expect(res.body).toHaveProperty('apiOnly', false);

								const membership = await prisma.membership.findUnique({
									where: {
										userId_groupId: {
											userId: 'seed-user-1',
											groupId: res.body.id,
										},
									},
								});
								expect(membership).not.toBeNull();

								done();
							});
					});

					it('returns 404 when parentGroupId refers to a group that does not exist', (done) => {
						request(testApp)
							.post(`/groups?parentGroupId=non-existent-group`)
							.set('Authorization', getDefaultUserBearerToken())
							.send({ name: 'New Group' })
							.end(expect404AndValidateResponse(done));
					});

					it('returns 404 when parentGroupId refers to a group that the user does not belong to', (done) => {
						request(testApp)
							.post(`/groups?parentGroupId=seed-group-2`)
							.set('Authorization', getDefaultUserBearerToken())
							.send({ name: 'New Group' })
							.end(expect404AndValidateResponse(done));
					});

					it('returns 409 when a sibling group with the same name already exists', async (done) => {
						await postMockGroup({ name: 'Subgroup 1', parentGroupId: 'seed-group-1' });

						request(testApp)
							.post(`/groups?parentGroupId=seed-group-1`)
							.set('Authorization', getDefaultUserBearerToken())
							.send({ name: 'Subgroup 1' })
							.end(expect409AndValidateResponse(done));
					});
				});

				describe('When parentGroupId query parameter is NOT present', () => {
					it('returns 201, creates the group at the root level on success, and creates a membership for the requesting user', (done) => {
						request(testApp)
							.post(`/groups`)
							.set('Authorization', getDefaultUserBearerToken())
							.send({ name: 'New Root Group' })
							.end(async (err, res) => {
								expect201AndValidateResponse()(err, res);
								expect(res.body).toHaveProperty('name', 'New Root Group');
								expect(res.body).toHaveProperty('path', '/new-root-group/');
								expect(res.body).toHaveProperty('apiOnly', false);

								const membership = await prisma.membership.findUnique({
									where: {
										userId_groupId: {
											userId: 'seed-user-1',
											groupId: res.body.id,
										},
									},
								});
								expect(membership).not.toBeNull();

								done();
							});
					});
				});
			});

			describe('when the requesting user is a super admin', () => {
				beforeEach(async () => {
					await registerUser({
						id: 'seed-super-admin-1',
						email: 'seedSuperAdmin1@soilstack.io',
						password: 'password',
						groupIds: [],
						isSuperAdmin: true,
					});
				});

				describe('When parentGroupId query parameter is present', () => {
					describe('when the parent group has apiOnly=false', () => {
						it('returns 201 on success, creates the group within the parent group, and sets apiOnly to false by default, even when the requesting user is not a member of the parent group', (done) => {
							request(testApp)
								.post(`/groups?parentGroupId=seed-group-1`)
								.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
								.send({ name: 'Nested Group' })
								.end((err, res) => {
									expect201AndValidateResponse()(err, res);
									expect(res.body).toHaveProperty('name', 'Nested Group');
									expect(res.body).toHaveProperty('path', '/seed-group-1/nested-group/');
									expect(res.body).toHaveProperty('apiOnly', false);

									done();
								});
						});

						it('returns 201 and creates the group with the specified apiOnly value from the request', (done) => {
							request(testApp)
								.post(`/groups?parentGroupId=seed-group-1`)
								.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
								.send({ name: 'Nested Group', apiOnly: true })
								.end((err, res) => {
									expect201AndValidateResponse()(err, res);
									expect(res.body).toHaveProperty('name', 'Nested Group');
									expect(res.body).toHaveProperty('path', '/seed-group-1/nested-group/');
									expect(res.body).toHaveProperty('apiOnly', true);

									done();
								});
						});
					});

					describe('when the parent group has apiOnly=true', () => {
						it('returns 409 when the request contains apiOnly=false', async (done) => {
							const {
								body: { id: subGroupOneId },
							} = await postMockGroup({
								name: 'Subgroup 1',
								parentGroupId: 'seed-group-1',
								apiOnly: true,
								authorizationHeaderValue: getUserBearerToken('seedSuperAdmin1@soilstack.io'),
							});

							request(testApp)
								.post(`/groups?parentGroupId=${subGroupOneId}`)
								.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
								.send({ name: 'Subgroup 2', apiOnly: false })
								.end(expect409AndValidateResponse(done));
						});

						it('returns 201 and sets apiOnly to true to match the parent', async (done) => {
							const {
								body: { id: subGroupOneId },
							} = await postMockGroup({
								name: 'Subgroup 1',
								parentGroupId: 'seed-group-1',
								apiOnly: true,
								authorizationHeaderValue: getUserBearerToken('seedSuperAdmin1@soilstack.io'),
							});

							request(testApp)
								.post(`/groups?parentGroupId=${subGroupOneId}`)
								.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
								.send({ name: 'Subgroup 2' })
								.end(async (err, res) => {
									expect201AndValidateResponse()(err, res);
									expect(res.body).toHaveProperty('apiOnly', true);

									done();
								});
						});
					});
				});

				describe('When parentGroupId query parameter is NOT present', () => {
					it('returns 201 and creates the group with the specified apiOnly value from the request', (done) => {
						request(testApp)
							.post(`/groups`)
							.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
							.send({ name: 'New Root Group', apiOnly: true })
							.end(async (err, res) => {
								expect201AndValidateResponse()(err, res);
								expect(res.body).toHaveProperty('name', 'New Root Group');
								expect(res.body).toHaveProperty('path', '/new-root-group/');
								expect(res.body).toHaveProperty('apiOnly', true);

								done();
							});
					});
				});
			});
		});
	});

	describe('PATCH', () => {
		describe('/groups/:groupId', () => {
			describe('when the requesting user is not a super admin', () => {
				it('returns 204 on success, and updates the group name', async (done) => {
					request(testApp)
						.patch(`/groups/seed-group-1`)
						.set('Authorization', getDefaultUserBearerToken())
						.send({ name: 'Updated Group Name' })
						.end(async (err, res) => {
							expect204AndValidateResponse()(err, res);

							const group = await prisma.group.findUnique({
								where: { id: 'seed-group-1' },
							});
							expect(group).toHaveProperty('name', 'Updated Group Name');

							done();
						});
				});

				it('returns 400 when the request body contains the apiOnly property', (done) => {
					request(testApp)
						.patch(`/groups/seed-group-1`)
						.set('Authorization', getDefaultUserBearerToken())
						.send({ name: 'Updated Group Name', apiOnly: true })
						.end(expect400AndValidateResponse(done));
				});

				it('returns 404 when the group does not exist', (done) => {
					request(testApp)
						.patch(`/groups/non-existent-group`)
						.set('Authorization', getDefaultUserBearerToken())
						.send({ name: 'Updated Group Name' })
						.end(expect404AndValidateResponse(done));
				});

				it('returns 404 when the user does not belong to the group', (done) => {
					request(testApp)
						.patch(`/groups/seed-group-2`)
						.set('Authorization', getDefaultUserBearerToken())
						.send({ name: 'Updated Group Name' })
						.end(expect404AndValidateResponse(done));
				});
			});

			describe('when the requesting user is a super admin', () => {
				beforeEach(async () => {
					await registerUser({
						id: 'seed-super-admin-1',
						email: 'seedSuperAdmin1@soilstack.io',
						password: 'password',
						groupIds: [],
						isSuperAdmin: true,
					});
				});

				it('returns 204 on success, and updates the group name, even when the super admin is not a member of the group', async (done) => {
					request(testApp)
						.patch(`/groups/seed-group-1`)
						.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
						.send({ name: 'Updated Group Name' })
						.end(async (err, res) => {
							expect204AndValidateResponse()(err, res);

							const group = await prisma.group.findUnique({
								where: { id: 'seed-group-1' },
							});
							expect(group).toHaveProperty('name', 'Updated Group Name');

							done();
						});
				});

				it('returns 404 when the group does not exist', (done) => {
					request(testApp)
						.patch(`/groups/non-existent-group`)
						.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
						.send({ name: 'Updated Group Name' })
						.end(expect404AndValidateResponse(done));
				});

				describe('when the request body contains the apiOnly property', () => {
					it('updates the apiOnly property of the group and all descendant groups', async (done) => {
						const {
							body: { id: subGroupOneId },
						} = await postMockGroup({ name: 'Subgroup 1', parentGroupId: 'seed-group-1' });
						const {
							body: { id: subGroupTwoId },
						} = await postMockGroup({ name: 'Subgroup 2', parentGroupId: subGroupOneId });

						request(testApp)
							.patch(`/groups/seed-group-1`)
							.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
							.send({ apiOnly: true })
							.end(async (err, res) => {
								expect204AndValidateResponse()(err, res);

								const groups = await prisma.group.findMany({
									where: { id: { in: ['seed-group-1', subGroupOneId, subGroupTwoId] } },
								});
								expect(groups).toHaveLength(3);
								expect(groups.every((group) => group.apiOnly)).toBe(true);

								done();
							});
					});
				});
			});
		});
	});

	describe('DELETE', () => {
		describe('/groups/:groupId', () => {
			describe('when the requesting user is a super admin', () => {
				beforeEach(async () => {
					await registerUser({
						id: 'seed-super-admin-1',
						email: 'seedSuperAdmin1@soilstack.io',
						password: 'password',
						groupIds: [],
						isSuperAdmin: true,
					});
				});

				it('returns 204 on success and deletes the group, all descendant groups, and all memberships belonging to any of those groups', async (done) => {
					const {
						body: { id: subGroupOneId },
					} = await postMockGroup({ name: 'Subgroup 1', parentGroupId: 'seed-group-1' });
					const {
						body: { id: subGroupTwoId },
					} = await postMockGroup({ name: 'Subgroup 2', parentGroupId: subGroupOneId });

					request(testApp)
						.delete(`/groups/seed-group-1`)
						.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
						.end(async (err, res) => {
							expect204AndValidateResponse()(err, res);

							const groups = await prisma.group.findMany({
								where: { id: { in: ['seed-group-1', subGroupOneId, subGroupTwoId] } },
							});
							expect(groups).toHaveLength(0);

							const memberships = await prisma.membership.findMany({
								where: { groupId: { in: ['seed-group-1', subGroupOneId, subGroupTwoId] } },
							});
							expect(memberships).toHaveLength(0);

							done();
						});
				});

				it('returns 409 when the group has resources in it', async (done) => {
					await postMockField();

					request(testApp)
						.delete(`/groups/seed-group-1`)
						.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
						.end(expect409AndValidateResponse(done));
				});

				it('returns 409 when any descendant groups have resources in them', async (done) => {
					const {
						body: { id: subGroupOneId },
					} = await postMockGroup({ name: 'Subgroup 1', parentGroupId: 'seed-group-1' });
					await postMockField({ groupId: subGroupOneId });

					request(testApp)
						.delete(`/groups/seed-group-1`)
						.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
						.end(expect409AndValidateResponse(done));
				});

				it('returns 404 when the group does not exist', (done) => {
					request(testApp)
						.delete(`/groups/non-existent-group`)
						.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
						.end(expect404AndValidateResponse(done));
				});
			});

			describe('when the requesting user is not a super admin', () => {
				it('returns 401', (done) => {
					request(testApp)
						.delete('/groups/any-group-id')
						.set('Authorization', getDefaultUserBearerToken())
						.end(expect401AndValidateResponse(done));
				});
			});
		});
	});
});
