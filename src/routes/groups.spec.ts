import request from 'supertest';
import { prismaMock } from '../singleton';
import {
	createMockPrismaGroup,
	createMockPrismaMembership,
	getDefaultSuperAdminBearerToken,
	getDefaultUserBearerToken,
	testApp,
} from '../test-utils';

describe('Group Endpoints', () => {
	describe('GET', () => {
		describe('/groups', () => {
			it('returns 500 when prisma.group.findMany throws an unexpected error', (done) => {
				prismaMock.group.findMany.mockRejectedValue(new Error('unexpected error'));

				request(testApp).get(`/groups`).set('Authorization', getDefaultUserBearerToken()).expect(500, done);
			});
		});
	});

	describe('POST', () => {
		describe('groups', () => {
			describe('when parentGroupId is omitted', () => {
				it('returns 500 when prisma.group.create throws an unexpected error', (done) => {
					prismaMock.group.create.mockRejectedValue(new Error('unexpected error'));

					request(testApp)
						.post(`/groups`)
						.set('Authorization', getDefaultUserBearerToken())
						.send({ name: 'Nested Group' })
						.expect(500, done);
				});

				it('returns 500 when prisma.group.findMany throws an unexpected error', (done) => {
					prismaMock.group.create.mockResolvedValueOnce(createMockPrismaGroup());
					prismaMock.group.findMany.mockRejectedValue(new Error('unexpected error'));

					request(testApp)
						.post(`/groups`)
						.set('Authorization', getDefaultUserBearerToken())
						.send({ name: 'Nested Group' })
						.expect(500, done);
				});
			});

			describe('when parentGroupId is present', () => {
				it('returns 500 when prisma.group.create throws an unexpected error', (done) => {
					prismaMock.group.findUnique.mockResolvedValue(createMockPrismaGroup());
					prismaMock.group.create.mockRejectedValue(new Error('unexpected error'));
					prismaMock.group.findMany.mockResolvedValue([createMockPrismaGroup()]);

					request(testApp)
						.post(`/groups?parentGroupId=present`)
						.set('Authorization', getDefaultUserBearerToken())
						.send({ name: 'Nested Group' })
						.expect(500, done);
				});

				it('returns 500 when prisma.group.findUnique throws an unexpected error', (done) => {
					prismaMock.group.findUnique.mockRejectedValue(new Error('unexpected error'));
					prismaMock.group.create.mockResolvedValue(createMockPrismaGroup());
					prismaMock.group.findMany.mockResolvedValue([createMockPrismaGroup()]);

					request(testApp)
						.post(`/groups?parentGroupId=present`)
						.set('Authorization', getDefaultUserBearerToken())
						.send({ name: 'Nested Group' })
						.expect(500, done);
				});

				it('returns 500 when prisma.group.findMany throws an unexpected error', (done) => {
					prismaMock.group.findUnique.mockResolvedValue(createMockPrismaGroup());
					prismaMock.group.create.mockResolvedValue(createMockPrismaGroup());
					prismaMock.group.findMany.mockRejectedValue(new Error('unexpected error'));

					request(testApp)
						.post(`/groups?parentGroupId=present`)
						.set('Authorization', getDefaultUserBearerToken())
						.send({ name: 'Nested Group' })
						.expect(500, done);
				});
			});
		});
	});

	describe('PATCH', () => {
		describe('/groups/:groupId', () => {
			it('returns 500 when prisma.group.update throws an unexpected error', (done) => {
				prismaMock.membership.findUnique.mockResolvedValue(createMockPrismaMembership());
				prismaMock.group.update.mockRejectedValue(new Error('unexpected error'));

				request(testApp)
					.patch(`/groups/1`)
					.set('Authorization', getDefaultUserBearerToken())
					.send({ name: 'Updated Group' })
					.expect(500, done);
			});

			it('returns 500 when prisma.membership.findUnique throws an unexpected error', (done) => {
				prismaMock.membership.findUnique.mockRejectedValue(new Error('unexpected error'));

				request(testApp)
					.patch(`/groups/1`)
					.set('Authorization', getDefaultUserBearerToken())
					.send({ name: 'Updated Group' })
					.expect(500, done);
			});
		});
	});

	describe('DELETE', () => {
		describe('/groups/:groupId', () => {
			it('returns 500 when prisma.group.delete throws an unexpected error', (done) => {
				prismaMock.group.delete.mockRejectedValue(new Error('unexpected error'));

				request(testApp).delete(`/groups/1`).set('Authorization', getDefaultSuperAdminBearerToken()).expect(500, done);
			});
		});
	});
});
