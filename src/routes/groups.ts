import { Group as PrismaGroup, Membership as PrismaMembership, Prisma, User as PrismaUser } from '@prisma/client';
import { NextFunction, Request, Response, Router } from 'express';
import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';
import { APP_URL } from '../constants';
import prisma from '../prismaClient';
import { sendEmail, singleCTA } from '../services/email';
import { prismaGroupToGroup, prismaMembershipToMembership } from '../transforms';
import { components, paths } from '../types/generated/openapi-types';
import { PrismaMembershipWithIncludes } from '../types/prisma';
import { stripHtml } from '../utils';

const router = Router();

// returns the ids of the starting group and all of its descendant groups
const getDescendantGroupIds = (startingGroupId: string) => prisma.$queryRaw<Pick<PrismaGroup, 'id'>[]>`
	WITH RECURSIVE subgroups AS (
		SELECT
			id,
			"parentId"
		FROM
			public."Group"
		WHERE
			id = ${startingGroupId}
		UNION
			SELECT
				g.id,
				g."parentId"
			FROM
				public."Group" g
			INNER JOIN subgroups s ON s.id = g."parentId"
	) SELECT
		id
	FROM
		subgroups;
`;

router.get('/', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const userGroupIds = res.locals.user.memberships.map((membership: any) => membership.groupId);
		const allGroups = await prisma.group.findMany();
		const isSuperAdmin = res.locals.user.isSuperAdmin;

		const groups = allGroups
			.filter((group) => (isSuperAdmin ? true : userGroupIds.includes(group.id)))
			.filter((group) => (isSuperAdmin || res.locals.authorizationScheme === 'apikey' ? true : group.apiOnly === false))
			.map((prismaGroup) => prismaGroupToGroup(prismaGroup, allGroups));

		res.status(200).json(groups);
	} catch (error) {
		next(error);
	}
});

router.post('/', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const parentGroupId = req.query.parentGroupId;
		const body = req.body as paths['/groups']['post']['requestBody']['content']['application/json'];
		const isSuperAdmin = res.locals.user.isSuperAdmin;

		if (!isSuperAdmin && Object.hasOwn(body, 'apiOnly')) {
			// simulate an openapi-validator error response
			return res.status(400).send({
				message: 'request/body must NOT have additional properties',
				errors: [
					{
						path: '/body/apiOnly',
						message: 'must NOT have additional properties',
						errorCode: 'additionalProperties.openapi.validation',
					},
				],
			});
		}

		if (!parentGroupId) {
			const prismaGroup = await prisma.group.create({
				data: {
					...body,
					memberships: {
						create: {
							userId: res.locals.user.id,
						},
					},
				},
			});
			const allGroups = await prisma.group.findMany();

			return res.status(201).json(prismaGroupToGroup(prismaGroup, allGroups));
		}

		const parentGroup = await prisma.group.findUnique({
			where: { id: parentGroupId as string },
			include: {
				memberships: true,
				subgroups: true,
			},
		});

		if (!parentGroup) {
			return res.status(404).send();
		}

		const isAuthorized =
			isSuperAdmin || parentGroup.memberships.some((membership) => membership.userId === res.locals.user.id);
		if (!isAuthorized) {
			return res.status(404).send();
		}

		const hasNameConflictWithSibling = parentGroup.subgroups.some((subgroup) => subgroup.name === body.name);
		const isTryingToSetApiOnlyFalseWhenParentIsApiOnly = parentGroup.apiOnly && body.apiOnly === false;

		if (hasNameConflictWithSibling || isTryingToSetApiOnlyFalseWhenParentIsApiOnly) {
			return res.status(409).send();
		}

		const prismaGroup = await prisma.group.create({
			data: {
				name: body.name,
				apiOnly: parentGroup.apiOnly || body.apiOnly,
				parentId: parentGroup.id,
				memberships: {
					createMany: {
						data: parentGroup.memberships.map(({ userId }) => ({ userId })),
					},
				},
			},
		});

		const allGroups = await prisma.group.findMany();

		res.status(201).json(prismaGroupToGroup(prismaGroup, allGroups));
	} catch (error) {
		next(error);
	}
});

router.patch('/:groupId', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const groupId = req.params.groupId;
		const body = req.body as components['schemas']['PatchGroup'];
		const isSuperAdmin = res.locals.user.isSuperAdmin;

		if (!isSuperAdmin && Object.hasOwn(body, 'apiOnly')) {
			// simulate an openapi-validator error response
			return res.status(400).send({
				message: 'request/body must NOT have additional properties',
				errors: [
					{
						path: '/body/apiOnly',
						message: 'must NOT have additional properties',
						errorCode: 'additionalProperties.openapi.validation',
					},
				],
			});
		}

		const isAuthorized =
			isSuperAdmin ||
			(await prisma.membership.findUnique({
				where: {
					userId_groupId: {
						groupId,
						userId: res.locals.user.id,
					},
				},
			}));

		if (!isAuthorized) {
			return res.status(404).send();
		}

		const branchGroups = await getDescendantGroupIds(groupId);
		const hasDescendantGroups = branchGroups.length > 1;
		if (Object.hasOwn(body, 'apiOnly') && hasDescendantGroups) {
			const descendantGroupIds = branchGroups.map(({ id }) => id).filter((id) => id !== groupId);
			await prisma.$transaction([
				prisma.group.update({
					where: { id: groupId },
					data: body,
				}),
				prisma.group.updateMany({
					where: {
						id: {
							in: descendantGroupIds,
						},
					},
					data: {
						apiOnly: body.apiOnly,
					},
				}),
			]);
		} else {
			await prisma.group.update({
				where: { id: groupId },
				data: body,
			});
		}

		res.status(204).send();
	} catch (error) {
		if (error instanceof Prisma.PrismaClientKnownRequestError) {
			// P2025 is the Prisma Client error code for RecordNotFound
			if (error.code === 'P2025') {
				return res.status(404).send();
			}
		}
		next(error);
	}
});

router.delete('/:groupId', async (req: Request, res: Response, next: NextFunction) => {
	try {
		if (!res.locals.user.isSuperAdmin) {
			return res.status(401).send();
		}

		const groupId = req.params.groupId;

		await prisma.group.delete({
			where: { id: groupId },
		});

		res.status(204).send();
	} catch (error) {
		if (error instanceof Prisma.PrismaClientKnownRequestError) {
			// P2025 is the Prisma Client error code for RecordNotFound
			if (error.code === 'P2025') {
				return res.status(404).send();
			}
			// P2003 is the Prisma Client error code for foreign key constraints
			if (error.code === 'P2003') {
				return res.status(409).send({
					message: 'Cannot delete a group that has resources in it.',
				});
			}
		}
		next(error);
	}
});

router.get('/:groupId/memberships', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const groupId = req.params.groupId;

		const isAuthorized =
			res.locals.user.isSuperAdmin ||
			res.locals.user.memberships.some((membership: any) => membership.groupId === groupId);

		if (!isAuthorized) {
			return res.status(404).send();
		}

		const prismaMemberships = await prisma.membership.findMany({
			where: { groupId },
			include: { user: true },
		});

		const memberships = prismaMemberships.map(prismaMembershipToMembership);

		res.status(200).json(memberships);
	} catch (error) {
		next(error);
	}
});

router.post('/:groupId/memberships', async (req: Request, res: Response, next: NextFunction) => {
	try {
		let sendEmailOnSuccess = false;
		const groupId = req.params.groupId;
		const userEmail = req.body
			.userEmail as paths['/groups/{groupId}/memberships']['post']['requestBody']['content']['application/json']['userEmail'];
		const lowerCasedUserEmail = userEmail.toLowerCase();

		const isAuthorized =
			res.locals.user.isSuperAdmin ||
			res.locals.user.memberships.some((membership: any) => membership.groupId === groupId);
		if (!isAuthorized) {
			return res.status(404).send();
		}

		let user: PrismaUser | null;
		user = await prisma.user.findUnique({
			where: { email: lowerCasedUserEmail },
		});
		if (!user) {
			user = await prisma.user.create({
				data: {
					email: lowerCasedUserEmail,
					seen: false,
				},
			});
			sendEmailOnSuccess = true;
		}

		const branchGroups = await getDescendantGroupIds(groupId);
		if (branchGroups.length === 0) {
			return res.status(404).send();
		}
		const branchGroupIds = branchGroups.map(({ id }) => id);

		await prisma.membership.createMany({
			data: branchGroupIds.map((groupId) => ({
				userId: (user as PrismaUser).id,
				groupId,
			})),
			skipDuplicates: true,
		});

		const prismaMemberships = await prisma.membership.findMany({
			where: {
				groupId: { in: branchGroupIds },
				userId: (user as PrismaUser).id,
			},
			include: { user: true },
		});

		const memberships = prismaMemberships.map(prismaMembershipToMembership);

		// If the user being added to the group doesn't have an account yet, send them an email to register.
		if (sendEmailOnSuccess) {
			prisma.group.findUnique({ where: { id: groupId } }).then((group) => {
				if (group?.name) {
					const body = singleCTA({
						preheaderText: "You've been added to a group!",
						preCtaParagraphs: [
							`You have been added to the <b>${stripHtml(
								group.name
							)}</b> group on SoilStack.io. Create an account to view your group's resources.`,
						],
						ctaText: 'Create Account',
						ctaHref: `${APP_URL}/auth/register?email=${encodeURIComponent(lowerCasedUserEmail)}`,
						postCtaParagraphs: [
							'If you think you were added to this group by mistake, you can safely ignore this email.',
							'Happy sampling!',
							'The SoilStack.io Team',
						],
					});
					// email failures are acceptable and we continue on with the request since memberships have already been created
					sendEmail({
						to: lowerCasedUserEmail,
						subject: "SoilStack.io - You've been added to a group!",
						body,
					});
				}
			});
		}

		res.status(201).json(memberships);
	} catch (error) {
		next(error);
	}
});

router.delete('/:groupId/memberships/:membershipId', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const groupId = req.params.groupId;
		const membershipId = req.params.membershipId;

		const isAuthorized =
			res.locals.user.isSuperAdmin ||
			res.locals.user.memberships.some((membership: any) => membership.groupId === groupId);
		if (!isAuthorized) {
			return res.status(404).send();
		}

		const membership: PrismaMembershipWithIncludes | null = await prisma.membership.findUnique({
			where: { id: membershipId },
			include: {
				user: {
					include: {
						memberships: true,
					},
				},
				group: {
					select: { parentId: true },
				},
			},
		});

		if (!membership) {
			return res.status(404).send();
		}

		if (membership.groupId !== groupId) {
			return res.status(404).send();
		}

		if (membership.group.parentId) {
			// is the user whose membership is being deleted a member of the parent group?
			const isMemberOfParentGroup = membership.user.memberships.some(
				(userMembership: PrismaMembership) => userMembership.groupId === membership.group.parentId
			);
			if (isMemberOfParentGroup) {
				return res.status(409).send();
			}
		}

		const userId = membership?.userId;
		const branchGroups = await getDescendantGroupIds(groupId);
		const branchGroupIds = branchGroups.map(({ id }) => id);

		await prisma.membership.deleteMany({
			where: {
				userId,
				groupId: { in: branchGroupIds },
			},
		});

		res.status(204).send();
	} catch (error) {
		if (error instanceof Prisma.PrismaClientKnownRequestError) {
			// P2025 is the Prisma Client error code for RecordNotFound
			if (error.code === 'P2025') {
				return res.status(404).send();
			}
		}
		next(error);
	}
});

const openApiPaths: OpenAPIV3.PathsObject = {
	'/groups': {
		get: {
			tags: ['Groups'],
			summary: 'Get all groups that you are a member of.',
			responses: {
				200: {
					description: 'An array of groups.',
					content: {
						'application/json': {
							schema: {
								type: 'array',
								items: { $ref: '#/components/schemas/ResponseGroup' },
							},
						},
					},
				},
				401: { $ref: '#/components/responses/401' },
			},
		},
		post: {
			tags: ['Groups'],
			summary: 'Create a new group.',
			parameters: [
				{
					name: 'parentGroupId',
					description:
						'If present, specifies the parent group to create the new group within. If not present, the group will be created without a parent group.',
					in: 'query',
					required: false,
					schema: {
						type: 'string',
					},
				},
			],
			requestBody: {
				description: 'The group to be created.',
				content: {
					'application/json': {
						schema: {
							$ref: '#/components/schemas/CreateGroup',
						},
					},
				},
				required: true,
			},
			responses: {
				'201': {
					description: 'Successfully created the group.',
					content: {
						'application/json': {
							schema: { $ref: '#/components/schemas/ResponseGroup' },
						},
					},
				},
				'400': {
					description: 'The request body is invalid.',
					content: {
						'application/json': {
							schema: {
								$ref: '#/components/schemas/Error',
							},
						},
					},
				},
				'401': { $ref: '#/components/responses/401' },
				'409': {
					description: 'A group with the same name already exists in the parent group.',
				},
			},
		},
	},
	'/groups/{groupId}': {
		patch: {
			tags: ['Groups'],
			summary: 'Updates a group.',
			parameters: [
				{
					name: 'groupId',
					in: 'path',
					description: 'The id of the group to edit.',
					required: true,
					schema: { type: 'string' },
				},
			],
			requestBody: {
				description: 'The group object to be updated.',
				required: true,
				content: {
					'application/json': {
						schema: {
							$ref: '#/components/schemas/PatchGroup',
						},
					},
				},
			},
			responses: {
				'204': {
					description: 'The group was saved successfully.',
				},
				'400': {
					description: 'The request body is invalid.',
					content: {
						'application/json': {
							schema: {
								$ref: '#/components/schemas/Error',
							},
						},
					},
				},
				'404': {
					description: 'No group with the given id exists.',
				},
				'401': { $ref: '#/components/responses/401' },
			},
		},
		delete: {
			'x-internal': true,
			tags: ['Groups'],
			summary: 'Deletes a group, all of its descendant groups, and all memberships belonging to those groups.',
			parameters: [
				{
					name: 'groupId',
					in: 'path',
					description: 'The id of the group to edit.',
					required: true,
					schema: { type: 'string' },
				},
			],
			responses: {
				'204': {
					description: 'The group was deleted successfully.',
				},
				'404': {
					description: 'No group with the given id exists.',
				},
				'409': {
					description: 'Cannot delete a group that has resources in it or in a descendant group.',
					content: {
						'application/json': {
							schema: {
								$ref: '#/components/schemas/Error',
							},
						},
					},
				},
				'401': { $ref: '#/components/responses/401' },
			},
		} as OpenAPIV3.OperationObject,
	},
	'/groups/{groupId}/memberships': {
		get: {
			tags: ['Memberships'],
			summary: 'Get all memberships for a group.',
			parameters: [
				{
					name: 'groupId',
					in: 'path',
					description: 'The id of the group to get memberships for.',
					required: true,
					schema: { type: 'string' },
				},
			],
			responses: {
				'200': {
					description: 'An array of memberships.',
					content: {
						'application/json': {
							schema: {
								type: 'array',
								items: { $ref: '#/components/schemas/ResponseMembership' },
							},
						},
					},
				},
				401: { $ref: '#/components/responses/401' },
			},
		},
		post: {
			tags: ['Memberships'],
			summary:
				'Create a new membership (Add a user to a group). If the group has descendant groups, memberships to those groups will be created as well.',
			parameters: [
				{
					name: 'groupId',
					in: 'path',
					description: 'The id of the group to add the user to.',
					required: true,
					schema: { type: 'string' },
				},
			],
			requestBody: {
				description: 'The user to add to the group.',
				required: true,
				content: {
					'application/json': {
						schema: {
							type: 'object',
							required: ['userEmail'],
							additionalProperties: false,
							properties: {
								userEmail: {
									type: 'string',
									format: 'email',
									description: 'The email of the user to add to the group.',
								},
							},
						},
					},
				},
			},
			responses: {
				'201': {
					description: 'Successfully created the membership.',
					content: {
						'application/json': {
							schema: {
								type: 'array',
								items: { $ref: '#/components/schemas/ResponseMembership' },
							},
						},
					},
				},
				'401': { $ref: '#/components/responses/401' },
			},
		},
	},
	'/groups/{groupId}/memberships/{membershipId}': {
		delete: {
			tags: ['Memberships'],
			summary: 'Delete a membership (Remove a user from a group). The user is also removed from all descendant groups.',
			parameters: [
				{
					name: 'groupId',
					in: 'path',
					description: 'The id of the group the membership is in.',
					required: true,
					schema: { type: 'string' },
				},
				{
					name: 'membershipId',
					in: 'path',
					description: 'The id of the membership to delete.',
					required: true,
					schema: { type: 'string' },
				},
			],
			responses: {
				'204': {
					description: 'Successfully deleted the membership.',
				},
				'401': { $ref: '#/components/responses/401' },
			},
		},
	},
};

export { openApiPaths, router };
