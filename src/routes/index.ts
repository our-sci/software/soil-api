import { openApiPaths as apiKeyPaths, router as apiKeys } from './apiKeys';
import { openApiPaths as areasPaths, router as areas } from './areas';
import { openApiPaths as authPaths, router as auth } from './auth';
import { openApiPaths as fieldsPaths, router as fields } from './fields';
import { openApiPaths as groupsPaths, router as groups } from './groups';
import { openApiPaths as locationCollectionsPaths, router as locationCollections } from './locationCollections';
import { openApiPaths as locationsPaths, router as locations } from './locations';
import { openApiPaths as membershipPaths, router as memberships } from './memberships';
import { openApiPaths as reassignPaths, router as reassign } from './reassign';
import { openApiPaths as resourcesPaths, router as resources } from './resources';
import { openApiPaths as samplesPaths, router as samples } from './samples';
import { openApiPaths as samplingCollectionsPaths, router as samplingCollections } from './samplingCollections';
import { openApiPaths as samplingsPaths, router as samplings } from './samplings';
import { openApiPaths as stratificationJobPaths, router as stratificationJobs } from './stratificationJobs';
import { openApiPaths as stratificationsPaths, router as stratifications } from './stratifications';
import { openApiPaths as userPath, router as user } from './user';
import { openApiPaths as userPaths, router as users } from './users';

const routes = {
	'/auth': auth,
	'/fields': fields,
	'/areas': areas,
	'/stratifications': stratifications,
	'/location-collections': locationCollections,
	'/locations': locations,
	'/samples': samples,
	'/samplings': samplings,
	'/sampling-collections': samplingCollections,
	'/resources': resources,
	'/groups': groups,
	'/reassign': reassign,
	'/users': users,
	'/user': user,
	'/memberships': memberships,
	'/api-keys': apiKeys,
	'/stratification-jobs': stratificationJobs,
};

const paths = {
	...authPaths,
	...fieldsPaths,
	...areasPaths,
	...stratificationsPaths,
	...locationCollectionsPaths,
	...locationsPaths,
	...samplesPaths,
	...samplingsPaths,
	...samplingCollectionsPaths,
	...resourcesPaths,
	...groupsPaths,
	...reassignPaths,
	...userPaths,
	...userPath,
	...membershipPaths,
	...apiKeyPaths,
	...stratificationJobPaths,
};

export { paths, routes };
