import request from 'supertest';
import { ResourceTypes } from '../constants';
import { getDefaultUserBearerToken, testApp } from '../test-utils';
import {
	expect200AndValidateResponse,
	postMockStratificationAndDependencies,
	testGetResourceByIdEndpoint,
} from '../test-utils';

describe('Location Collection Endpoints', () => {
	describe('GET', () => {
		describe('/location-collections', () => {
			it('returns 200 with location collections the user has access to on success', async (done) => {
				// add a stratification to a group the user belongs to
				const {
					body: {
						locationCollections: [{ id: locationCollectionId }],
					},
				} = await postMockStratificationAndDependencies();
				// add a stratification to a group the user does NOT belong to
				await postMockStratificationAndDependencies({ groupId: 'seed-group-2' });

				request(testApp)
					.get(`/location-collections`)
					.set('Authorization', getDefaultUserBearerToken())
					.end((err, res) => {
						expect(res.body).toHaveLength(1);
						expect(res.body[0]).toHaveProperty('id', locationCollectionId);
						expect200AndValidateResponse(done)(err, res);
					});
			});

			it('returns 200 on success when no location collections are not present', (done) => {
				request(testApp)
					.get(`/location-collections`)
					.set('Authorization', getDefaultUserBearerToken())
					.end(expect200AndValidateResponse(done));
			});

			describe("when passing 'submittedAfter' query parameter", () => {
				it("only returns location collections created after the 'submittedAfter' time, ordered most recent first", async (done) => {
					const {
						body: {
							locationCollections: [locationCollectionOne],
						},
					} = await postMockStratificationAndDependencies();
					const {
						body: {
							locationCollections: [locationCollectionTwo],
						},
					} = await postMockStratificationAndDependencies();
					const {
						body: {
							locationCollections: [locationCollectionThree],
						},
					} = await postMockStratificationAndDependencies();

					const submittedAfter = encodeURIComponent(locationCollectionOne.meta.submittedAt);

					request(testApp)
						.get(`/location-collections?submittedAfter=${submittedAfter}`)
						.set('Authorization', getDefaultUserBearerToken())
						.end((err, res) => {
							expect(res.body).toHaveLength(2);
							expect(res.body[0]).toHaveProperty('id', locationCollectionThree.id);
							expect(res.body[1]).toHaveProperty('id', locationCollectionTwo.id);
							expect200AndValidateResponse(done)(err, res);
						});
				});
			});
		});

		testGetResourceByIdEndpoint({
			endpointPath: '/location-collections',
			resourceName: 'location collection',
			resourceType: ResourceTypes.LocationCollection,
		});
	});
});
