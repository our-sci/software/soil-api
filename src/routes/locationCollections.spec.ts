import request from 'supertest';
import { prismaMock } from '../singleton';
import { getDefaultUserBearerToken, testApp } from '../test-utils';

describe('Location Collection Endpoints', () => {
	describe('/location-collections', () => {
		it('returns 500 when prismaClient returns an unexpected error', (done) => {
			prismaMock.locationCollection.findMany.mockRejectedValueOnce(new Error('unexpected error'));

			request(testApp).get(`/location-collections`).set('Authorization', getDefaultUserBearerToken()).expect(500, done);
		});
	});

	describe('/location-collections/:id', () => {
		it('returns 500 when prisma.locationCollection.findFirst throws an unexpected error', (done) => {
			prismaMock.locationCollection.findFirst.mockRejectedValueOnce(new Error('unexpected error'));

			request(testApp)
				.get('/location-collections/non-existent-id')
				.set('Authorization', getDefaultUserBearerToken())
				.expect(500, done);
		});
	});
});
