import { Membership as PrismaMembership } from '@prisma/client';
import { NextFunction, Request, Response, Router } from 'express';
import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';
import { ResourceTypes } from '../constants';
import prisma from '../prismaClient';
import {
	postSamplingCollectionRequestToPrismaCreateInput,
	prismaLocationCollectionToLocationCollection,
	prismaSampleToSample,
	prismaSamplingCollectionToSamplingCollection,
	prismaSamplingToSampling,
	referenceIdToPrismaReferenceId,
	sampleToPrismaCreateInput,
	samplingToPrismaCreateInput,
} from '../transforms';
import { components } from '../types/generated/openapi-types';
import { PrismaPopulatedSampling, PrismaPopulatedSamplingCollection } from '../types/prisma';
import createGetResourceByIdController from './common/createGetResourceByIdController';

const router = Router();

router.get('/', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const submittedAfter = req.query.submittedAfter as string;
		const userGroupIds = res.locals.user.memberships.map((m: PrismaMembership) => m.groupId);
		const defaultWhere = { groupId: { in: userGroupIds } };

		const prismaLocationCollections = await prisma.locationCollection.findMany({
			where: submittedAfter ? { ...defaultWhere, createdAt: { gt: new Date(submittedAfter) } } : defaultWhere,
			orderBy: { createdAt: 'desc' },
			include: { locations: { select: { id: true } } },
		});

		const locationCollections = prismaLocationCollections.map(prismaLocationCollectionToLocationCollection);

		res.status(200).json(locationCollections);
	} catch (error) {
		next(error);
	}
});

router.get('/:id', createGetResourceByIdController(ResourceTypes.LocationCollection));

router.post('/:id/sampling-collections', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const locationCollectionId = req.params.id;
		const body = req.body as components['schemas']['PopulatedSamplingCollection'];
		const userGroupIds = res.locals.user.memberships.map((m: PrismaMembership) => m.groupId);

		const locationCollection = await prisma.locationCollection.findUnique({
			where: { id: locationCollectionId },
			include: {
				field: { select: { id: true } },
				locations: { select: { id: true } },
			},
		});

		const isAuthorized = userGroupIds.includes(locationCollection?.groupId);
		if (!isAuthorized || !locationCollection) {
			return res.status(404).send();
		}

		const locationsReferencedBySamplings = body.members?.map(({ featureOfInterest }) => featureOfInterest) ?? [];
		const hasValidReferences = locationsReferencedBySamplings.every((referencedId) =>
			locationCollection.locations.some(({ id }) => id === referencedId)
		);
		if (!hasValidReferences) {
			return res.status(400).send({ message: "Invalid location reference found in a sampling's featureOfInterest." });
		}

		const fieldId = locationCollection.field.id;
		const groupId = locationCollection.groupId;

		const samplingCreates = req.body.members.map((sampling: components['schemas']['PopulatedSampling']) =>
			prisma.sampling.create({
				data: {
					...samplingToPrismaCreateInput(sampling, groupId),
					samples: {
						createMany: {
							data:
								sampling.results?.map((sample: components['schemas']['Sample']) =>
									sampleToPrismaCreateInput(sample, groupId, fieldId)
								) ?? [],
						},
					},
				},
				select: { id: true },
			})
		);

		let samplings;
		try {
			samplings = await prisma.$transaction(samplingCreates);
		} catch (error) {
			return next(error);
		}

		let samplingCollection: PrismaPopulatedSamplingCollection;
		try {
			samplingCollection = await prisma.samplingCollection.create({
				data: {
					...postSamplingCollectionRequestToPrismaCreateInput(body, groupId, fieldId, locationCollectionId),
					samplings: { connect: samplings },
					referenceIds: {
						createMany: {
							data: req.body.meta?.referenceIds?.map(referenceIdToPrismaReferenceId),
						},
					},
				},
				include: {
					samplings: {
						include: {
							samples: true,
						},
					},
					referenceIds: true,
				},
			});
		} catch (error) {
			// attempt to rollback orphaned samplings when sampling collection creation fails
			await prisma.sampling.deleteMany({
				where: { id: { in: samplings.map((sampling) => sampling.id) } },
			});
			return next(error);
		}

		res.status(201).json({
			samplingCollections: [prismaSamplingCollectionToSamplingCollection(samplingCollection)],
			samplings: (samplingCollection.samplings as PrismaPopulatedSampling[]).map(prismaSamplingToSampling),
			samples: samplingCollection.samplings.flatMap((samplings) => samplings.samples).map(prismaSampleToSample),
		});
	} catch (error) {
		next(error);
	}
});

const openApiPaths: OpenAPIV3.PathsObject = {
	'/location-collections': {
		get: {
			tags: ['Location Collections'],
			summary: 'Get all location collections.',
			parameters: [
				{
					name: 'submittedAfter',
					in: 'query',
					description: 'Only return location collections submitted after this ISO8601 timestamp.',
					required: false,
					schema: { type: 'string', format: 'date-time' },
				},
			],
			responses: {
				'200': {
					description: 'An array of the location collections you have access to.',
					content: {
						'application/json': {
							schema: {
								type: 'array',
								items: { $ref: '#/components/schemas/ResponseLocationCollection' },
							},
						},
					},
				},
				401: { $ref: '#/components/responses/401' },
			},
		},
	},
	'/location-collections/{locationCollectionId}': {
		get: {
			tags: ['Location Collections'],
			summary: 'Get a location collection by its id.',
			parameters: [
				{
					name: 'locationCollectionId',
					in: 'path',
					description: 'The id of the location collection to get.',
					required: true,
					schema: { type: 'string' },
				},
			],
			responses: {
				'200': {
					description: 'The location collection with the given id.',
					content: {
						'application/json': {
							schema: {
								$ref: '#/components/schemas/ResponseLocationCollection',
							},
						},
					},
				},
				'404': {
					description: 'No location collection with the given id exists.',
				},
				401: { $ref: '#/components/responses/401' },
			},
		},
	},
	'/location-collections/{locationCollectionId}/sampling-collections': {
		post: {
			tags: ['Sampling Collections'],
			summary: 'Saves a sampling collection.',
			parameters: [
				{
					name: 'locationCollectionId',
					in: 'path',
					description: 'The id of the location collection the sampling collection relates to.',
					required: true,
					schema: { type: 'string' },
				},
			],
			requestBody: {
				description: 'The sampling collections object to be saved.',
				content: {
					'application/json': {
						schema: {
							$ref: '#/components/schemas/PopulatedSamplingCollection',
						},
					},
				},
				required: true,
			},
			responses: {
				'201': {
					description: 'Successfully created the sampling collection.',
					content: {
						'application/json': {
							schema: {
								type: 'object',
								required: ['samplingCollections', 'samplings', 'samples'],
								properties: {
									samplingCollections: {
										type: 'array',
										items: { $ref: '#/components/schemas/ResponseSamplingCollection' },
									},
									samplings: { type: 'array', items: { $ref: '#/components/schemas/ResponseSampling' } },
									samples: { type: 'array', items: { $ref: '#/components/schemas/ResponseSample' } },
								},
							},
						},
					},
				},
				'400': {
					description: 'Bad request.',
					content: {
						'application/json': {
							schema: {
								$ref: '#/components/schemas/Error',
							},
						},
					},
				},
				401: { $ref: '#/components/responses/401' },
			},
		},
	},
};

export { openApiPaths, router };
