import request from 'supertest';
import { ResourceTypes } from '../constants';
import { getDefaultUserBearerToken, testApp } from '../test-utils';
import { expect200AndValidateResponse, postMockStratificationAndDependencies } from '../test-utils';
import { testGetResourceByIdEndpoint } from '../test-utils';

describe('Location Endpoints', () => {
	describe('GET', () => {
		describe('/locations', () => {
			it('returns 200 with locations the user has access to on success', async (done) => {
				// add a stratification to a group the user belongs to
				const {
					body: {
						locations: [{ id: locationId }],
					},
				} = await postMockStratificationAndDependencies();
				// add a stratification to a group the user does NOT belong to
				await postMockStratificationAndDependencies({ groupId: 'seed-group-2' });

				request(testApp)
					.get(`/locations`)
					.set('Authorization', getDefaultUserBearerToken())
					.end((err, res) => {
						expect(res.body).toHaveLength(1);
						expect(res.body[0]).toHaveProperty('id', locationId);
						expect200AndValidateResponse(done)(err, res);
					});
			});

			it('returns 200 on success when no locations are present', (done) => {
				request(testApp)
					.get(`/locations`)
					.set('Authorization', getDefaultUserBearerToken())
					.end(expect200AndValidateResponse(done));
			});
		});

		testGetResourceByIdEndpoint({
			endpointPath: '/locations',
			resourceName: 'location',
			resourceType: ResourceTypes.Location,
		});
	});
});
