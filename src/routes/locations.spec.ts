import request from 'supertest';
import { prismaMock } from '../singleton';
import { getDefaultUserBearerToken, testApp } from '../test-utils';

describe('Location Endpoints', () => {
	describe('GET', () => {
		describe('/locations', () => {
			it('returns 500 when prismaClient returns an unexpected error', (done) => {
				prismaMock.location.findMany.mockRejectedValue(new Error('Unexpected error'));

				request(testApp).get(`/locations`).set('Authorization', getDefaultUserBearerToken()).expect(500, done);
			});
		});

		describe('/locations/:id', () => {
			it('returns 500 when prisma.location.findFirst throws an unexpected error', (done) => {
				prismaMock.location.findFirst.mockRejectedValue(new Error('Unexpected error'));

				request(testApp)
					.get('/locations/non-existent-id')
					.set('Authorization', getDefaultUserBearerToken())
					.expect(500, done);
			});
		});
	});
});
