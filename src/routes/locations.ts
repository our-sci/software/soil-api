import { Membership as PrismaMembership } from '@prisma/client';
import { NextFunction, Request, Response, Router } from 'express';
import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';
import { ResourceTypes } from '../constants';
import prisma from '../prismaClient';
import { prismaLocationToLocation } from '../transforms';
import createGetResourceByIdController from './common/createGetResourceByIdController';

const router = Router();

router.get('/', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const userGroupIds = res.locals.user.memberships.map((m: PrismaMembership) => m.groupId);

		const prismaLocations = await prisma.location.findMany({
			where: { groupId: { in: userGroupIds } },
		});

		const locations = prismaLocations.map(prismaLocationToLocation);

		res.status(200).json(locations);
	} catch (error) {
		next(error);
	}
});

router.get('/:id', createGetResourceByIdController(ResourceTypes.Location));

const openApiPaths: OpenAPIV3.PathsObject = {
	'/locations': {
		get: {
			tags: ['Locations'],
			summary: 'Get all locations.',
			responses: {
				'200': {
					description: 'An array of the locations you have access to.',
					content: {
						'application/json': {
							schema: {
								type: 'array',
								items: { $ref: '#/components/schemas/ResponseLocation' },
							},
						},
					},
				},
				401: { $ref: '#/components/responses/401' },
			},
		},
	},
	'/locations/{locationId}': {
		get: {
			tags: ['Locations'],
			summary: 'Get a location by its id.',
			parameters: [
				{
					name: 'locationId',
					in: 'path',
					description: 'The id of the location to get.',
					required: true,
					schema: { type: 'string' },
				},
			],
			responses: {
				'200': {
					description: 'The location with the given id.',
					content: {
						'application/json': {
							schema: {
								$ref: '#/components/schemas/ResponseLocation',
							},
						},
					},
				},
				'404': {
					description: 'No location with the given id exists.',
				},
				401: { $ref: '#/components/responses/401' },
			},
		},
	},
};

export { openApiPaths, router };
