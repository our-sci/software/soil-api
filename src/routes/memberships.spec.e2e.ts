import request from 'supertest';
import prisma from '../prismaClient';
import { sendEmail } from '../services/email';
import {
	expect200AndValidateResponse,
	expect201AndValidateResponse,
	expect404AndValidateResponse,
	getDefaultAdminBearerToken,
	getDefaultUserBearerToken,
	getUserBearerToken,
	postMockGroup,
	postMockMembership,
	registerUser,
	testApp,
} from '../test-utils';

jest.mock('../services/email');

describe('Membership Endpoints', () => {
	describe('GET', () => {
		describe('/groups/:groupId/memberships', () => {
			it('returns 200 with all memberships to the group', (done) => {
				request(testApp)
					.get('/groups/seed-group-1/memberships')
					.set('Authorization', getDefaultUserBearerToken())
					.end((err, res) => {
						expect200AndValidateResponse()(err, res);
						expect(res.body).toHaveLength(2);
						expect(res.body).toContainEqual(
							expect.objectContaining({
								groupId: 'seed-group-1',
								user: expect.objectContaining({
									id: 'seed-user-1',
									email: 'seeduser1@soilstack.io',
								}),
							})
						);
						expect(res.body).toContainEqual(
							expect.objectContaining({
								groupId: 'seed-group-1',
								user: expect.objectContaining({
									id: 'seed-admin-1',
									email: 'seedadmin1@soilstack.io',
								}),
							})
						);
						done();
					});
			});

			it('returns 404 if the requesting user is not a member of the group', (done) => {
				request(testApp)
					.get('/groups/seed-group-2/memberships')
					.set('Authorization', getDefaultUserBearerToken())
					.expect(404, done);
			});

			it('returns 200 with all memberships to the group when the requesting user is a super admin, even if the requesting user does not have a membership to the group', async (done) => {
				await registerUser({
					id: 'seed-super-admin-1',
					email: 'seedSuperAdmin1@soilstack.io',
					password: 'password',
					groupIds: [],
					isSuperAdmin: true,
				});

				request(testApp)
					.get('/groups/seed-group-1/memberships')
					.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
					.end((err, res) => {
						expect200AndValidateResponse()(err, res);
						expect(res.body).toHaveLength(2);
						expect(res.body).toContainEqual(
							expect.objectContaining({
								groupId: 'seed-group-1',
								user: expect.objectContaining({
									id: 'seed-user-1',
									email: 'seeduser1@soilstack.io',
								}),
							})
						);
						expect(res.body).toContainEqual(
							expect.objectContaining({
								groupId: 'seed-group-1',
								user: expect.objectContaining({
									id: 'seed-admin-1',
									email: 'seedadmin1@soilstack.io',
								}),
							})
						);
						done();
					});
			});
		});

		describe('/memberships', () => {
			it('returns 200 with all memberships of the requesting user', (done) => {
				request(testApp)
					.get('/memberships')
					.set('Authorization', getDefaultUserBearerToken())
					.end((err, res) => {
						expect200AndValidateResponse()(err, res);
						expect(res.body).toHaveLength(1);
						expect(res.body[0]).toHaveProperty('groupId', 'seed-group-1');
						done();
					});
			});
		});
	});

	describe('POST', () => {
		describe('/groups/:groupId/memberships', () => {
			describe('when the requesting user is NOT a super admin', () => {
				it('returns 201 with the new membership, creating the user when they do not exist, and sending them an email to create an account', async (done) => {
					const userBefore = await prisma.user.findUnique({ where: { email: 'seeduser2@soilstack.io' } });
					expect(userBefore).toBeNull();

					request(testApp)
						.post('/groups/seed-group-1/memberships')
						.set('Authorization', getDefaultUserBearerToken())
						.send({ userEmail: 'seedUser2@soilstack.io' })
						.end(async (err, res) => {
							expect201AndValidateResponse()(err, res);
							expect(res.body[0]).toHaveProperty('groupId', 'seed-group-1');
							expect(res.body[0]).toHaveProperty('user.email', 'seeduser2@soilstack.io');

							const userAfter = await prisma.user.findUnique({ where: { email: 'seeduser2@soilstack.io' } });
							expect(userAfter).not.toBeNull();
							expect(userAfter?.seen).toBe(false);

							expect(sendEmail).toHaveBeenCalled();

							done();
						});
				});

				it('creates a membership in all descendant groups of the group in the path', async () => {
					const {
						body: { id: levelOneId },
					} = await postMockGroup({ name: 'level-1', parentGroupId: null });
					const {
						body: { id: levelTwoId },
					} = await postMockGroup({ name: 'level-2', parentGroupId: levelOneId });
					const {
						body: { id: levelThreeId },
					} = await postMockGroup({ name: 'level-3', parentGroupId: levelTwoId });

					await request(testApp)
						.post(`/groups/${levelOneId}/memberships`)
						.set('Authorization', getDefaultAdminBearerToken())
						.send({ userEmail: 'seedUser1@soilstack.io' })
						.expect(201);

					const memberships = await prisma.membership.findMany({
						where: { userId: 'seed-user-1' },
					});

					expect(memberships).toContainEqual(expect.objectContaining({ groupId: levelOneId }));
					expect(memberships).toContainEqual(expect.objectContaining({ groupId: levelTwoId }));
					expect(memberships).toContainEqual(expect.objectContaining({ groupId: levelThreeId }));
				});

				it("does not fail when user is already a member of some descendant groups of the group they're being added to", async () => {
					const {
						body: { id: levelOneId },
					} = await postMockGroup({ name: 'level-1', parentGroupId: null });
					const {
						body: { id: levelTwoId },
					} = await postMockGroup({ name: 'level-2', parentGroupId: levelOneId });
					const {
						body: { id: levelThreeId },
					} = await postMockGroup({ name: 'level-3', parentGroupId: levelTwoId });

					// create a membership for seedUser1@soilstack.io to group level-2 and level-3
					await request(testApp)
						.post(`/groups/${levelTwoId}/memberships`)
						.set('Authorization', getDefaultAdminBearerToken())
						.send({ userEmail: 'seedUser1@soilstack.io' })
						.expect(201);

					await request(testApp)
						.post(`/groups/${levelOneId}/memberships`)
						.set('Authorization', getDefaultAdminBearerToken())
						.send({ userEmail: 'seedUser1@soilstack.io' })
						.expect(201);

					const memberships = await prisma.membership.findMany({
						where: { userId: 'seed-user-1' },
					});

					expect(memberships).toContainEqual(expect.objectContaining({ groupId: levelOneId }));
					expect(memberships).toContainEqual(expect.objectContaining({ groupId: levelTwoId }));
					expect(memberships).toContainEqual(expect.objectContaining({ groupId: levelThreeId }));
				});

				it('returns 404 if the requesting user is not a member of the group', (done) => {
					request(testApp)
						.post('/groups/seed-group-2/memberships')
						.set('Authorization', getDefaultUserBearerToken())
						.send({ userEmail: 'seedUser2@soilstack.io' })
						.expect(404, done);
				});
			});

			describe('when the requesting user is a super admin', () => {
				beforeEach(async () => {
					await registerUser({
						id: 'seed-super-admin-1',
						email: 'seedSuperAdmin1@soilstack.io',
						password: 'password',
						groupIds: [],
						isSuperAdmin: true,
					});
				});

				it('returns 201 with the new membership, creating the user when they do not exist, and sending them an email to create an account, even when the requesting user is not a member of the group', async (done) => {
					const userBefore = await prisma.user.findUnique({ where: { email: 'seeduser2@soilstack.io' } });
					expect(userBefore).toBeNull();

					request(testApp)
						.post('/groups/seed-group-1/memberships')
						.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
						.send({ userEmail: 'seedUser2@soilstack.io' })
						.end(async (err, res) => {
							expect201AndValidateResponse()(err, res);
							expect(res.body[0]).toHaveProperty('groupId', 'seed-group-1');
							expect(res.body[0]).toHaveProperty('user.email', 'seeduser2@soilstack.io');

							const userAfter = await prisma.user.findUnique({ where: { email: 'seeduser2@soilstack.io' } });
							expect(userAfter).not.toBeNull();
							expect(userAfter?.seen).toBe(false);

							expect(sendEmail).toHaveBeenCalled();

							done();
						});
				});

				it('returns 404 if the group does not exist', (done) => {
					request(testApp)
						.post('/groups/does-not-exist/memberships')
						.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
						.send({ userEmail: 'seedUser2@soilstack.io' })
						.end(async (err, res) => {
							expect404AndValidateResponse()(err, res);
							expect(sendEmail).not.toHaveBeenCalled();
							done();
						});
				});
			});
		});
	});

	describe('DELETE', () => {
		describe('/groups/:groupId/memberships/:membershipId', () => {
			describe('when the requesting user is NOT a super admin', () => {
				it('returns 204 and deletes the membership on success', async () => {
					const membership = await prisma.membership.findUnique({
						where: {
							userId_groupId: {
								groupId: 'seed-group-1',
								userId: 'seed-user-1',
							},
						},
					});

					await request(testApp)
						.delete(`/groups/seed-group-1/memberships/${membership?.id}`)
						.set('Authorization', getDefaultUserBearerToken())
						.expect(204);

					const membershipAfter = await prisma.membership.findUnique({
						where: {
							userId_groupId: {
								groupId: 'seed-group-1',
								userId: 'seed-user-1',
							},
						},
					});
					expect(membershipAfter).toBeNull();
				});

				it('deletes the memberships for the user in all descendant groups of the group in the path', async () => {
					const {
						body: { id: levelOneId },
					} = await postMockGroup({ name: 'level-1', parentGroupId: null });
					const {
						body: { id: levelTwoId },
					} = await postMockGroup({ name: 'level-2', parentGroupId: levelOneId });
					const {
						body: { id: levelThreeId },
					} = await postMockGroup({ name: 'level-3', parentGroupId: levelTwoId });
					const { body: memberships } = await request(testApp)
						.post(`/groups/${levelOneId}/memberships`)
						.set('Authorization', getDefaultAdminBearerToken())
						.send({ userEmail: 'seedUser1@soilstack.io' })
						.expect(201);

					const levelOneMembershipId = memberships.find((membership: any) => membership.groupId === levelOneId)?.id;

					await request(testApp)
						.delete(`/groups/${levelOneId}/memberships/${levelOneMembershipId}`)
						.set('Authorization', getDefaultAdminBearerToken())
						.expect(204);

					const membershipCount = await prisma.membership.count({
						where: {
							userId: 'seed-user-1',
							groupId: { in: [levelOneId, levelTwoId, levelThreeId] },
						},
					});

					expect(membershipCount).toBe(0);
				});

				it('returns 404 if the requesting user is not a member of the group', async (done) => {
					const membership = await prisma.membership.findUnique({
						where: {
							userId_groupId: {
								groupId: 'seed-group-2',
								userId: 'seed-admin-1',
							},
						},
					});

					request(testApp)
						.delete(`/groups/seed-group-2/memberships/${membership?.id}`)
						.set('Authorization', getDefaultUserBearerToken())
						.expect(404, done);
				});

				it('returns 404 if the group does not exist', (done) => {
					request(testApp)
						.delete('/groups/non-existent-group/memberships/seed-membership-1')
						.set('Authorization', getDefaultUserBearerToken())
						.expect(404, done);
				});

				it('returns 404 if the membership does not exist', (done) => {
					request(testApp)
						.delete('/groups/seed-group-1/memberships/non-existent-membership')
						.set('Authorization', getDefaultUserBearerToken())
						.expect(404, done);
				});

				it('returns 404 if the membership does not belong to the group', async (done) => {
					const membership = await prisma.membership.create({
						data: {
							groupId: 'seed-group-2',
							userId: 'seed-user-1',
						},
					});

					request(testApp)
						.delete(`/groups/seed-group-1/memberships/${membership.id}`)
						.set('Authorization', getDefaultUserBearerToken())
						.expect(404, done);
				});

				it('returns 409 if the user belongs to the parent group of the group in the path', async (done) => {
					const {
						body: { id: subGroupId },
					} = await postMockGroup({ name: 'sub-group', parentGroupId: 'seed-group-1' });
					const {
						body: [{ id: membershipId }],
					} = await postMockMembership({ groupId: subGroupId, userEmail: 'seedUser1@soilstack.io' });

					request(testApp)
						.delete(`/groups/${subGroupId}/memberships/${membershipId}`)
						.set('Authorization', getDefaultUserBearerToken())
						.expect(409, done);
				});
			});

			describe('when the requesting user is a super admin', () => {
				beforeEach(async () => {
					await registerUser({
						id: 'seed-super-admin-1',
						email: 'seedSuperAdmin1@soilstack.io',
						password: 'password',
						groupIds: [],
						isSuperAdmin: true,
					});
				});

				it('returns 204 and deletes the membership on success, even when the requesting user is not a member of the group', async () => {
					const membership = await prisma.membership.findUnique({
						where: {
							userId_groupId: {
								groupId: 'seed-group-1',
								userId: 'seed-user-1',
							},
						},
					});

					await request(testApp)
						.delete(`/groups/seed-group-1/memberships/${membership?.id}`)
						.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
						.expect(204);

					const membershipAfter = await prisma.membership.findUnique({
						where: {
							userId_groupId: {
								groupId: 'seed-group-1',
								userId: 'seed-user-1',
							},
						},
					});
					expect(membershipAfter).toBeNull();
				});
			});
		});
	});
});
