import request from 'supertest';
import { prismaMock } from '../singleton';
import {
	createMockPrismaMembershipWithIncludes,
	createMockPrismaUser,
	getDefaultUserBearerToken,
	testApp,
} from '../test-utils';

describe('Membership Endpoints', () => {
	describe('GET', () => {
		describe('/groups/:groupId/memberships', () => {
			it('returns 500 when prisma.membership.findMany throws an unexpected error', (done) => {
				prismaMock.membership.findMany.mockRejectedValueOnce(new Error('Unexpected error'));

				request(testApp)
					.get('/groups/seed-group-1/memberships')
					.set('Authorization', getDefaultUserBearerToken())
					.expect(500, done);
			});
		});
	});

	describe('POST', () => {
		describe('/groups/:groupId/memberships', () => {
			it('returns 500 when prisma.user.findUnique throws an unexpected error', (done) => {
				(prismaMock.user.findUnique as any).mockRejectedValue(new Error('Unexpected error'));

				request(testApp)
					.post('/groups/seed-group-1/memberships')
					.set('Authorization', getDefaultUserBearerToken())
					.send({ userEmail: 'seedUser2@soilstack.io' })
					.expect(500, done);
			});

			it('returns 500 when prisma.user.create throws an unexpected error', (done) => {
				(prismaMock.user.findUnique as any).mockResolvedValue(null);
				prismaMock.user.create.mockRejectedValueOnce(new Error('Unexpected error'));

				request(testApp)
					.post('/groups/seed-group-1/memberships')
					.set('Authorization', getDefaultUserBearerToken())
					.send({ userEmail: 'seedUser2@soilstack.io' })
					.expect(500, done);
			});

			it('returns 500 when prisma.$queryraw throws an unexpected error', (done) => {
				(prismaMock.user.findUnique as any).mockResolvedValue(createMockPrismaUser());
				prismaMock.$queryRaw.mockRejectedValueOnce(new Error('Unexpected error'));

				request(testApp)
					.post('/groups/seed-group-1/memberships')
					.set('Authorization', getDefaultUserBearerToken())
					.send({ userEmail: 'seedUser2@soilstack.io' })
					.expect(500, done);
			});

			it('returns 500 when prisma.membership.createMany throws an unexpected error', (done) => {
				(prismaMock.user.findUnique as any).mockResolvedValue(createMockPrismaUser());
				prismaMock.membership.createMany.mockRejectedValueOnce(new Error('Unexpected error'));

				request(testApp)
					.post('/groups/seed-group-1/memberships')
					.set('Authorization', getDefaultUserBearerToken())
					.send({ userEmail: 'seedUser2@soilstack.io' })
					.expect(500, done);
			});

			it('returns 500 when prisma.membership.findMany throws an unexpected error', (done) => {
				(prismaMock.user.findUnique as any).mockResolvedValue(createMockPrismaUser());
				prismaMock.membership.createMany.mockResolvedValue({ count: 1 });
				prismaMock.membership.findMany.mockRejectedValueOnce(new Error('Unexpected error'));

				request(testApp)
					.post('/groups/seed-group-1/memberships')
					.set('Authorization', getDefaultUserBearerToken())
					.send({ userEmail: 'seedUser2@soilstack.io' })
					.expect(500, done);
			});
		});
	});

	describe('DELETE', () => {
		describe('/groups/:groupId/memberships/:membershipId', () => {
			it('returns 500 when prisma.membership.findUnique throw an unexpected error', (done) => {
				prismaMock.membership.findUnique.mockRejectedValueOnce(new Error('Unexpected error'));

				request(testApp)
					.delete('/groups/seed-group-1/memberships/seed-membership-1')
					.set('Authorization', getDefaultUserBearerToken())
					.expect(500, done);
			});

			it('returns 500 when prisma.$queryRaw throws an unexpected error', async (done) => {
				prismaMock.membership.findUnique.mockResolvedValueOnce(await createMockPrismaMembershipWithIncludes());
				prismaMock.$queryRaw.mockRejectedValueOnce(new Error('Unexpected error'));

				request(testApp)
					.delete('/groups/seed-group-1/memberships/seed-membership-1')
					.set('Authorization', getDefaultUserBearerToken())
					.expect(500, done);
			});

			it('returns 500 when prisma.membership.deleteMany throws an unexpected error', async (done) => {
				prismaMock.membership.findUnique.mockResolvedValueOnce(await createMockPrismaMembershipWithIncludes());
				prismaMock.membership.deleteMany.mockRejectedValueOnce(new Error('Unexpected error'));

				request(testApp)
					.delete('/groups/seed-group-1/memberships/seed-membership-1')
					.set('Authorization', getDefaultUserBearerToken())
					.expect(500, done);
			});
		});
	});
});
