import { Membership as PrismaMembership } from '@prisma/client';
import { NextFunction, Request, Response, Router } from 'express';
import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';
import { prismaMembershipToMembership } from '../transforms';
import { PrismaMembershipWithUser } from '../types/prisma';

const router = Router();

router.get('/', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const memberships = res.locals.user.memberships
			.map(
				(membership: PrismaMembership): PrismaMembershipWithUser => ({
					...membership,
					user: res.locals.user,
				})
			)
			.map(prismaMembershipToMembership);

		res.status(200).json(memberships);
	} catch (error) {
		next(error);
	}
});

const openApiPaths: OpenAPIV3.PathsObject = {
	'/memberships': {
		get: {
			tags: ['Memberships'],
			summary: 'Get a list of memberships belonging to the requesting user',
			responses: {
				200: {
					description: 'A list of memberships belonging to the requesting user',
					content: {
						'application/json': {
							schema: {
								type: 'array',
								items: { $ref: '#/components/schemas/ResponseMembership' },
							},
						},
					},
				},
				401: { $ref: '#/components/responses/401' },
			},
		},
	},
};

export { openApiPaths, router };
