import request from 'supertest';
import prisma from '../prismaClient';
import {
	expect403AndValidateResponse,
	expect404AndValidateResponse,
	getDefaultAdminBearerToken,
	getDefaultUserBearerToken,
	getUserBearerToken,
	postMockField,
	postMockSamplingCollection,
	postMockStratification,
	registerUser,
	testApp,
} from '../test-utils';

describe('Reassign Endpoints', () => {
	describe('PATCH', () => {
		describe('/reassign', () => {
			describe('when the requesting user is a super admin', () => {
				beforeEach(async () => {
					await registerUser({
						id: 'seed-super-admin-1',
						email: 'seedSuperAdmin1@soilstack.io',
						password: 'password',
						groupIds: [],
						isSuperAdmin: true,
					});
				});

				describe('bulk reassign (when fieldId is an array of field ids)', () => {
					it('returns 200 on success and updates the group id of all resources related to the fields, even when the requesting user is not a member of the groups being assigned from and to', async () => {
						const {
							body: {
								fields: [{ id: fieldId1 }],
								areas: [{ id: areaId1 }],
							},
						} = await postMockField();
						const {
							body: {
								fields: [{ id: fieldId2 }],
								areas: [{ id: areaId2 }],
							},
						} = await postMockField();
						const {
							body: {
								locationCollections: [{ id: locationCollectionId1 }],
								locations: [{ id: locationId1 }],
								stratifications: [{ id: stratificationId1 }],
							},
						} = await postMockStratification({ areaId: areaId1 });
						const {
							body: {
								locationCollections: [{ id: locationCollectionId2 }],
								locations: [{ id: locationId2 }],
								stratifications: [{ id: stratificationId2 }],
							},
						} = await postMockStratification({ areaId: areaId2 });
						const {
							body: {
								samplingCollections: [{ id: samplingCollectionId1 }],
								samplings: [{ id: samplingId1 }],
								samples: [{ id: sampleId1 }],
							},
						} = await postMockSamplingCollection(locationCollectionId1, locationId1);
						const {
							body: {
								samplingCollections: [{ id: samplingCollectionId2 }],
								samplings: [{ id: samplingId2 }],
								samples: [{ id: sampleId2 }],
							},
						} = await postMockSamplingCollection(locationCollectionId2, locationId2);

						await request(testApp)
							.patch(`/reassign`)
							.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
							.send({ fieldId: [fieldId1, fieldId2], groupId: 'seed-group-2' })
							.expect(200);

						const resources = await Promise.all([
							prisma.field.findUnique({ where: { id: fieldId1 } }),
							prisma.field.findUnique({ where: { id: fieldId2 } }),
							prisma.area.findUnique({ where: { id: areaId1 } }),
							prisma.area.findUnique({ where: { id: areaId2 } }),
							prisma.locationCollection.findUnique({ where: { id: locationCollectionId1 } }),
							prisma.locationCollection.findUnique({ where: { id: locationCollectionId2 } }),
							prisma.location.findUnique({ where: { id: locationId1 } }),
							prisma.location.findUnique({ where: { id: locationId2 } }),
							prisma.stratification.findUnique({ where: { id: stratificationId1 } }),
							prisma.stratification.findUnique({ where: { id: stratificationId2 } }),
							prisma.samplingCollection.findUnique({ where: { id: samplingCollectionId1 } }),
							prisma.samplingCollection.findUnique({ where: { id: samplingCollectionId2 } }),
							prisma.sampling.findUnique({ where: { id: samplingId1 } }),
							prisma.sampling.findUnique({ where: { id: samplingId2 } }),
							prisma.sample.findUnique({ where: { id: sampleId1 } }),
							prisma.sample.findUnique({ where: { id: sampleId2 } }),
						]);

						resources.forEach((resource) => {
							expect(resource).toHaveProperty('groupId', 'seed-group-2');
						});
					});
				});

				it('returns 200 on success and updates the group id of all resources related to the field, even when the requesting user is not a member of the groups being assigned from and to', async () => {
					const {
						body: {
							fields: [{ id: fieldId }],
							areas: [{ id: areaId }],
						},
					} = await postMockField();
					const {
						body: {
							locationCollections: [{ id: locationCollectionId }],
							locations: [{ id: locationId }],
							stratifications: [{ id: stratificationId }],
						},
					} = await postMockStratification({ areaId });
					const {
						body: {
							samplingCollections: [{ id: samplingCollectionId }],
							samplings: [{ id: samplingId }],
							samples: [{ id: sampleId }],
						},
					} = await postMockSamplingCollection(locationCollectionId, locationId);

					await request(testApp)
						.patch(`/reassign`)
						.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
						.send({ fieldId, groupId: 'seed-group-2' })
						.expect(200);

					const resources = await Promise.all([
						prisma.field.findUnique({ where: { id: fieldId } }),
						prisma.area.findUnique({ where: { id: areaId } }),
						prisma.locationCollection.findUnique({ where: { id: locationCollectionId } }),
						prisma.location.findUnique({ where: { id: locationId } }),
						prisma.stratification.findUnique({ where: { id: stratificationId } }),
						prisma.samplingCollection.findUnique({ where: { id: samplingCollectionId } }),
						prisma.sampling.findUnique({ where: { id: samplingId } }),
						prisma.sample.findUnique({ where: { id: sampleId } }),
					]);

					resources.forEach((resource) => {
						expect(resource).toHaveProperty('groupId', 'seed-group-2');
					});
				});
			});

			describe('bulk reassign (when fieldId is an array of field ids)', () => {
				it('returns 200 on success and updates the group id of all resources related to the fields', async (done) => {
					const {
						body: {
							fields: [{ id: fieldId1 }],
							areas: [{ id: areaId1 }],
						},
					} = await postMockField();
					const {
						body: {
							fields: [{ id: fieldId2 }],
							areas: [{ id: areaId2 }],
						},
					} = await postMockField();
					const {
						body: {
							locationCollections: [{ id: locationCollectionId1 }],
							locations: [{ id: locationId1 }],
							stratifications: [{ id: stratificationId1 }],
						},
					} = await postMockStratification({ areaId: areaId1 });
					const {
						body: {
							locationCollections: [{ id: locationCollectionId2 }],
							locations: [{ id: locationId2 }],
							stratifications: [{ id: stratificationId2 }],
						},
					} = await postMockStratification({ areaId: areaId2 });
					const {
						body: {
							samplingCollections: [{ id: samplingCollectionId1 }],
							samplings: [{ id: samplingId1 }],
							samples: [{ id: sampleId1 }],
						},
					} = await postMockSamplingCollection(locationCollectionId1, locationId1);
					const {
						body: {
							samplingCollections: [{ id: samplingCollectionId2 }],
							samplings: [{ id: samplingId2 }],
							samples: [{ id: sampleId2 }],
						},
					} = await postMockSamplingCollection(locationCollectionId2, locationId2);

					await request(testApp)
						.patch(`/reassign`)
						.set('Authorization', getDefaultAdminBearerToken())
						.send({ fieldId: [fieldId1, fieldId2], groupId: 'seed-group-2' })
						.expect(200);

					const resources = await Promise.all([
						prisma.field.findUnique({ where: { id: fieldId1 } }),
						prisma.field.findUnique({ where: { id: fieldId2 } }),
						prisma.area.findUnique({ where: { id: areaId1 } }),
						prisma.area.findUnique({ where: { id: areaId2 } }),
						prisma.locationCollection.findUnique({ where: { id: locationCollectionId1 } }),
						prisma.locationCollection.findUnique({ where: { id: locationCollectionId2 } }),
						prisma.location.findUnique({ where: { id: locationId1 } }),
						prisma.location.findUnique({ where: { id: locationId2 } }),
						prisma.stratification.findUnique({ where: { id: stratificationId1 } }),
						prisma.stratification.findUnique({ where: { id: stratificationId2 } }),
						prisma.samplingCollection.findUnique({ where: { id: samplingCollectionId1 } }),
						prisma.samplingCollection.findUnique({ where: { id: samplingCollectionId2 } }),
						prisma.sampling.findUnique({ where: { id: samplingId1 } }),
						prisma.sampling.findUnique({ where: { id: samplingId2 } }),
						prisma.sample.findUnique({ where: { id: sampleId1 } }),
						prisma.sample.findUnique({ where: { id: sampleId2 } }),
					]);

					resources.forEach((resource) => {
						expect(resource).toHaveProperty('groupId', 'seed-group-2');
					});

					done();
				});

				it('returns 404 and reassigns nothing if one or more fields do not exist', async () => {
					const {
						body: {
							fields: [{ id: fieldId }],
							areas: [{ id: areaId }],
						},
					} = await postMockField();

					await request(testApp)
						.patch(`/reassign`)
						.set('Authorization', getDefaultAdminBearerToken())
						.send({ fieldId: [fieldId, 'non-existent-id'], groupId: 'seed-group-2' })
						.expect(404);

					const resources = await Promise.all([
						prisma.field.findUnique({ where: { id: fieldId } }),
						prisma.area.findUnique({ where: { id: areaId } }),
					]);

					resources.forEach((resource) => {
						expect(resource).not.toHaveProperty('groupId', 'seed-group-2');
					});
				});

				it('returns 403 and reassigns nothing if the user does not have access to every group the fields are in', async () => {
					const {
						body: {
							fields: [{ id: fieldId1 }],
							areas: [{ id: areaId1 }],
						},
					} = await postMockField({ groupId: 'seed-group-1' });
					const {
						body: {
							fields: [{ id: fieldId2 }],
							areas: [{ id: areaId2 }],
						},
					} = await postMockField({ groupId: 'seed-group-2' });

					await request(testApp)
						.patch(`/reassign`)
						.set('Authorization', getDefaultUserBearerToken())
						.send({ fieldId: [fieldId1, fieldId2], groupId: 'seed-group-1' })
						.expect(403);

					const [field1, field2, area1, area2] = await Promise.all([
						prisma.field.findUnique({ where: { id: fieldId1 } }),
						prisma.field.findUnique({ where: { id: fieldId2 } }),
						prisma.area.findUnique({ where: { id: areaId1 } }),
						prisma.area.findUnique({ where: { id: areaId2 } }),
					]);

					expect(field1).toHaveProperty('groupId', 'seed-group-1');
					expect(field2).toHaveProperty('groupId', 'seed-group-2');
					expect(area1).toHaveProperty('groupId', 'seed-group-1');
					expect(area2).toHaveProperty('groupId', 'seed-group-2');
				});
			});

			it('returns 200 on success and updates the group id of all resources related to the field', async (done) => {
				const {
					body: {
						fields: [{ id: fieldId }],
						areas: [{ id: areaId }],
					},
				} = await postMockField();
				const {
					body: {
						locationCollections: [{ id: locationCollectionId }],
						locations: [{ id: locationId }],
						stratifications: [{ id: stratificationId }],
					},
				} = await postMockStratification({ areaId });
				const {
					body: {
						samplingCollections: [{ id: samplingCollectionId }],
						samplings: [{ id: samplingId }],
						samples: [{ id: sampleId }],
					},
				} = await postMockSamplingCollection(locationCollectionId, locationId);

				await request(testApp)
					.patch(`/reassign`)
					.set('Authorization', getDefaultAdminBearerToken())
					.send({ fieldId, groupId: 'seed-group-2' })
					.expect(200);

				const resources = await Promise.all([
					prisma.field.findUnique({ where: { id: fieldId } }),
					prisma.area.findUnique({ where: { id: areaId } }),
					prisma.locationCollection.findUnique({ where: { id: locationCollectionId } }),
					prisma.location.findUnique({ where: { id: locationId } }),
					prisma.stratification.findUnique({ where: { id: stratificationId } }),
					prisma.samplingCollection.findUnique({ where: { id: samplingCollectionId } }),
					prisma.sampling.findUnique({ where: { id: samplingId } }),
					prisma.sample.findUnique({ where: { id: sampleId } }),
				]);

				resources.forEach((resource) => {
					expect(resource).toHaveProperty('groupId', 'seed-group-2');
				});

				done();
			});

			it('returns 404 if the field does not exist', (done) => {
				request(testApp)
					.patch(`/reassign`)
					.set('Authorization', getDefaultUserBearerToken())
					.send({ fieldId: 'non-existent-id', groupId: 'seed-group-1' })
					.end(expect404AndValidateResponse(done));
			});

			it('returns 403 if the group does not exist', async (done) => {
				const {
					body: {
						fields: [{ id: fieldId }],
					},
				} = await postMockField();

				request(testApp)
					.patch(`/reassign`)
					.set('Authorization', getDefaultUserBearerToken())
					.send({ fieldId, groupId: 'non-existent-id' })
					.end(expect403AndValidateResponse(done));
			});

			it('returns 403 if the user does not have access to the group the field is in', async (done) => {
				const {
					body: {
						fields: [{ id: fieldId }],
					},
				} = await postMockField({ groupId: 'seed-group-2' });

				request(testApp)
					.patch(`/reassign`)
					.set('Authorization', getDefaultUserBearerToken())
					.send({ fieldId, groupId: 'seed-group-1' })
					.end(expect403AndValidateResponse(done));
			});

			it('returns 403 if the user does not have access to the group they are requesting to reassign to', async (done) => {
				const {
					body: {
						fields: [{ id: fieldId }],
					},
				} = await postMockField({ groupId: 'seed-group-1' });

				request(testApp)
					.patch(`/reassign`)
					.set('Authorization', getDefaultUserBearerToken())
					.send({ fieldId, groupId: 'seed-group-2' })
					.end(expect403AndValidateResponse(done));
			});
		});
	});
});
