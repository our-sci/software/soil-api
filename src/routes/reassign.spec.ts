import { Prisma } from '@prisma/client';
import request from 'supertest';
import { prismaMock } from '../singleton';
import { getDefaultAdminBearerToken, testApp } from '../test-utils';

type PrismaFieldWithResourceIds = Prisma.FieldGetPayload<{
	select: {
		areas: { select: { id: true } };
		locationCollections: {
			select: {
				id: true;
				locations: { select: { id: true } };
				stratificationId: true;
			};
		};
		samplingCollections: {
			select: {
				id: true;
				samplings: { select: { id: true } };
			};
		};
		samples: { select: { id: true } };
		groupId: true;
	};
}>;

describe('/reassign', () => {
	describe('PATCH', () => {
		it(`returns 500 when prisma.$transaction throws an unexpected error`, (done) => {
			const mockPrismaResponse: PrismaFieldWithResourceIds = {
				areas: [{ id: '1' }],
				locationCollections: [{ id: '1', locations: [{ id: '1' }], stratificationId: '1' }],
				samplingCollections: [{ id: '1', samplings: [{ id: '1' }] }],
				samples: [{ id: '1' }],
				groupId: 'seed-group-1',
			};
			(prismaMock.field.findUnique as any).mockResolvedValue(mockPrismaResponse);

			prismaMock.$transaction.mockRejectedValue(new Error('Unexpected error'));

			request(testApp)
				.patch(`/reassign`)
				.set('Authorization', getDefaultAdminBearerToken())
				.send({ fieldId: 'mock field id', groupId: 'seed-group-2' })
				.expect(500, done);
		});

		it('returns 500 when prisma.field.findUnique throws an unexpected error', (done) => {
			prismaMock.field.findUnique.mockRejectedValueOnce(new Error('Unexpected error'));

			request(testApp)
				.patch(`/reassign`)
				.set('Authorization', getDefaultAdminBearerToken())
				.send({ fieldId: 'mock field id', groupId: 'seed-group-2' })
				.expect(500, done);
		});
	});
});
