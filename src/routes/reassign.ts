import { Membership as PrismaMembership } from '@prisma/client';
import { NextFunction, Request, Response, Router } from 'express';
import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';
import prisma from '../prismaClient';
import { paths } from '../types/generated/openapi-types';

const router = Router();

router.patch('/', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const { groupId, fieldId } = req.body as paths['/reassign']['patch']['requestBody']['content']['application/json'];
		const fieldIds = Array.isArray(fieldId) ? fieldId : [fieldId];
		const userGroupIds = res.locals.user.memberships.map((m: PrismaMembership) => m.groupId);
		const isSuperAdmin = res.locals.user.isSuperAdmin;

		const userHasAccessToReassignToGroup = isSuperAdmin || userGroupIds.includes(groupId);
		if (!userHasAccessToReassignToGroup) {
			return res.status(403).send();
		}

		const relatedResourcesSets = await Promise.all(
			fieldIds.map((id) =>
				prisma.field.findUnique({
					where: { id },
					select: {
						areas: { select: { id: true } },
						locationCollections: {
							select: {
								id: true,
								locations: { select: { id: true } },
								stratificationId: true,
							},
						},
						samplingCollections: {
							select: {
								id: true,
								samplings: { select: { id: true } },
							},
						},
						samples: { select: { id: true } },
						groupId: true,
					},
				})
			)
		);

		if (relatedResourcesSets.includes(null)) {
			return res.status(404).send({
				message: 'Field not found.',
			});
		}

		const userHasAccessToField =
			isSuperAdmin ||
			relatedResourcesSets.every((relatedResources) => userGroupIds.includes(relatedResources!.groupId));
		if (!userHasAccessToField) {
			return res.status(403).send();
		}

		const relatedResourceIdSets = relatedResourcesSets.map((relatedResources) => {
			const areaIds = relatedResources!.areas.map((area) => area.id);
			const locationCollectionIds = relatedResources!.locationCollections.map(
				(locationCollection) => locationCollection.id
			);
			const locationIds = relatedResources!.locationCollections.flatMap((locationCollection) =>
				locationCollection?.locations.map((location) => location.id)
			);
			const stratificationIds = relatedResources!.locationCollections.map(
				(locationCollection) => locationCollection.stratificationId
			);
			const samplingCollectionIds = relatedResources!.samplingCollections.map(
				(samplingCollection) => samplingCollection.id
			);
			const samplingIds = relatedResources!.samplingCollections.flatMap((samplingCollection) =>
				samplingCollection?.samplings.map((sampling) => sampling.id)
			);
			const sampleIds = relatedResources!.samples.map((sample) => sample.id);

			return {
				areaIds,
				locationCollectionIds,
				locationIds,
				stratificationIds,
				samplingCollectionIds,
				samplingIds,
				sampleIds,
			};
		});

		const relatedResourceIds = relatedResourceIdSets.reduce(
			(r, set) => ({
				areaIds: [...r.areaIds, ...set.areaIds],
				locationCollectionIds: [...r.locationCollectionIds, ...set.locationCollectionIds],
				locationIds: [...r.locationIds, ...set.locationIds],
				stratificationIds: [...r.stratificationIds, ...set.stratificationIds],
				samplingCollectionIds: [...r.samplingCollectionIds, ...set.samplingCollectionIds],
				samplingIds: [...r.samplingIds, ...set.samplingIds],
				sampleIds: [...r.sampleIds, ...set.sampleIds],
			}),
			{
				areaIds: [],
				locationCollectionIds: [],
				locationIds: [],
				stratificationIds: [],
				samplingCollectionIds: [],
				samplingIds: [],
				sampleIds: [],
			}
		);

		await prisma.$transaction([
			prisma.field.updateMany({
				where: { id: { in: fieldIds } },
				data: { groupId },
			}),
			prisma.area.updateMany({
				where: { id: { in: relatedResourceIds.areaIds } },
				data: { groupId },
			}),
			prisma.locationCollection.updateMany({
				where: { id: { in: relatedResourceIds.locationCollectionIds } },
				data: { groupId },
			}),
			prisma.location.updateMany({
				where: { id: { in: relatedResourceIds.locationIds } },
				data: { groupId },
			}),
			prisma.stratification.updateMany({
				where: { id: { in: relatedResourceIds.stratificationIds } },
				data: { groupId },
			}),
			prisma.samplingCollection.updateMany({
				where: { id: { in: relatedResourceIds.samplingCollectionIds } },
				data: { groupId },
			}),
			prisma.sampling.updateMany({
				where: { id: { in: relatedResourceIds.samplingIds } },
				data: { groupId },
			}),
			prisma.sample.updateMany({
				where: { id: { in: relatedResourceIds.sampleIds } },
				data: { groupId },
			}),
		]);

		res.status(200).send();
	} catch (error) {
		next(error);
	}
});

const openApiPaths: OpenAPIV3.PathsObject = {
	'/reassign': {
		patch: {
			summary: 'Reassigns a field (or fields) and all related resources to a group.',
			requestBody: {
				description: 'The id(s) of the field(s) to be reassigned, and the id of the group to reassign to.',
				content: {
					'application/json': {
						schema: {
							type: 'object',
							required: ['fieldId', 'groupId'],
							additionalProperties: false,
							properties: {
								fieldId: {
									oneOf: [{ type: 'string' }, { type: 'array', items: { type: 'string' } }],
								},
								groupId: {
									type: 'string',
								},
							},
						},
					},
				},
				required: true,
			},
			responses: {
				'200': {
					description: 'Successfully reassigned the field(s) and its related resources to the group.',
				},
				'404': {
					description: 'The field or group was not found.',
					content: {
						'application/json': {
							schema: {
								$ref: '#/components/schemas/Error',
							},
						},
					},
				},
				401: { $ref: '#/components/responses/401' },
			},
		},
	},
};

export { openApiPaths, router };
