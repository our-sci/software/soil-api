import request from 'supertest';
import {
	getDefaultUserBearerToken,
	getUserBearerToken,
	postMockApiKey,
	postMockGroup,
	registerUser,
	testApp,
} from '../test-utils';
import { expect200AndValidateResponse, postMockSamplingCollectionAndDependencies } from '../test-utils';

describe('Resources Endpoints', () => {
	describe('GET', () => {
		describe('/resources', () => {
			let apiOnlyGroupId: string;
			beforeEach(async () => {
				await registerUser({
					id: 'seed-super-admin-1',
					email: 'seedSuperAdmin1@soilstack.io',
					password: 'password',
					groupIds: [],
					isSuperAdmin: true,
				});
				({
					body: { id: apiOnlyGroupId },
				} = await postMockGroup({
					name: 'API Only Group',
					apiOnly: true,
					parentGroupId: 'seed-group-1',
					authorizationHeaderValue: getUserBearerToken('seedSuperAdmin1@soilstack.io'),
				}));
			});

			describe('when the authorization scheme is bearer (frontend requests)', () => {
				it('returns 200 on success when no resources are present', (done) => {
					request(testApp)
						.get('/resources')
						.set('Authorization', getDefaultUserBearerToken())
						.end(expect200AndValidateResponse(done));
				});

				it('returns 200 with the resources the user has access to, except for those belonging to api only groups, on success', async (done) => {
					// add a sampling collection to a group the user belongs to
					await postMockSamplingCollectionAndDependencies();
					// add a sampling collection to a group the user does NOT belong to
					await postMockSamplingCollectionAndDependencies({ groupId: 'seed-group-2', referenceId: 'ref id 2' });
					// add a sampling collection to an api only group
					await postMockSamplingCollectionAndDependencies({ groupId: apiOnlyGroupId, referenceId: 'ref id 3' });

					request(testApp)
						.get('/resources')
						.set('Authorization', getDefaultUserBearerToken())
						.end((err, res) => {
							expect(res.body.fields).toHaveLength(1);
							expect(res.body.areas).toHaveLength(1);
							expect(res.body.stratifications).toHaveLength(1);
							expect(res.body.locationCollections).toHaveLength(1);
							expect(res.body.locations).toHaveLength(1);
							expect(res.body.samplingCollections).toHaveLength(1);
							expect(res.body.samplings).toHaveLength(1);
							expect(res.body.samples).toHaveLength(1);
							expect200AndValidateResponse(done)(err, res);
						});
				});

				it('returns 200 with all resources, except for those belonging to api only groups, when the user is a super admin', async (done) => {
					await postMockSamplingCollectionAndDependencies();
					await postMockSamplingCollectionAndDependencies({ groupId: 'seed-group-2', referenceId: 'ref id 2' });
					// add a sampling collection to an api only group
					await postMockSamplingCollectionAndDependencies({ groupId: apiOnlyGroupId, referenceId: 'ref id 3' });

					request(testApp)
						.get('/resources')
						.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
						.end((err, res) => {
							expect(res.body.fields).toHaveLength(2);
							expect(res.body.areas).toHaveLength(2);
							expect(res.body.stratifications).toHaveLength(2);
							expect(res.body.locationCollections).toHaveLength(2);
							expect(res.body.locations).toHaveLength(2);
							expect(res.body.samplingCollections).toHaveLength(2);
							expect(res.body.samplings).toHaveLength(2);
							expect(res.body.samples).toHaveLength(2);
							expect200AndValidateResponse(done)(err, res);
						});
				});
			});

			describe('when the authorization scheme is apikey (non frontend requests)', () => {
				it('returns 200 with the resources the user has access to, including those belonging to api only groups, on success', async (done) => {
					// add a sampling collection to a group the user belongs to
					await postMockSamplingCollectionAndDependencies();
					// add a sampling collection to a group the user does NOT belong to
					await postMockSamplingCollectionAndDependencies({ groupId: 'seed-group-2', referenceId: 'ref id 2' });
					// add a sampling collection to an api only group
					await postMockSamplingCollectionAndDependencies({ groupId: apiOnlyGroupId, referenceId: 'ref id 3' });
					const {
						body: { apiKey },
					} = await postMockApiKey({
						authToken: getDefaultUserBearerToken(),
					});

					request(testApp)
						.get('/resources')
						.set('Authorization', `apikey ${apiKey}`)
						.end((err, res) => {
							expect(res.body.fields).toHaveLength(2);
							expect(res.body.areas).toHaveLength(2);
							expect(res.body.stratifications).toHaveLength(2);
							expect(res.body.locationCollections).toHaveLength(2);
							expect(res.body.locations).toHaveLength(2);
							expect(res.body.samplingCollections).toHaveLength(2);
							expect(res.body.samplings).toHaveLength(2);
							expect(res.body.samples).toHaveLength(2);
							expect200AndValidateResponse(done)(err, res);
						});
				});

				it('returns 200 with all resources, including those belonging to api only groups, when the user is a super admin', async (done) => {
					await postMockSamplingCollectionAndDependencies();
					await postMockSamplingCollectionAndDependencies({ groupId: 'seed-group-2', referenceId: 'ref id 2' });
					// add a sampling collection to an api only group
					await postMockSamplingCollectionAndDependencies({ groupId: apiOnlyGroupId, referenceId: 'ref id 3' });
					const {
						body: { apiKey },
					} = await postMockApiKey({
						authToken: getUserBearerToken('seedSuperAdmin1@soilstack.io'),
					});

					request(testApp)
						.get('/resources')
						.set('Authorization', `apikey ${apiKey}`)
						.end((err, res) => {
							expect(res.body.fields).toHaveLength(3);
							expect(res.body.areas).toHaveLength(3);
							expect(res.body.stratifications).toHaveLength(3);
							expect(res.body.locationCollections).toHaveLength(3);
							expect(res.body.locations).toHaveLength(3);
							expect(res.body.samplingCollections).toHaveLength(3);
							expect(res.body.samplings).toHaveLength(3);
							expect(res.body.samples).toHaveLength(3);
							expect200AndValidateResponse(done)(err, res);
						});
				});
			});
		});
	});
});
