import request from 'supertest';
import { prismaMock } from '../singleton';
import { getDefaultUserBearerToken, testApp } from '../test-utils';

describe('/resources', () => {
	describe('GET', () => {
		const resources = [
			'field',
			'area',
			'stratification',
			'locationCollection',
			'location',
			'samplingCollection',
			'sampling',
			'sample',
		];

		resources.forEach((resource) => {
			it(`returns 500 when prisma throws an unexpected error when getting ${resource}s`, (done) => {
				// throw an error for the resource under test
				(prismaMock as any)[resource].findMany.mockRejectedValueOnce(new Error('Unexpected error'));
				// return an empty array for all other resources
				resources
					.filter((r) => r !== resource)
					.forEach((r) => {
						(prismaMock as any)[r].findMany.mockResolvedValueOnce([]);
					});

				request(testApp).get(`/resources`).set('Authorization', getDefaultUserBearerToken()).expect(500, done);
			});
		});
	});
});
