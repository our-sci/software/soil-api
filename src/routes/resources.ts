import { Membership as PrismaMembership } from '@prisma/client';
import { NextFunction, Request, Response, Router } from 'express';
import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';
import prisma from '../prismaClient';
import {
	prismaAreaToArea,
	prismaFieldToField,
	prismaLocationCollectionToLocationCollection,
	prismaLocationToLocation,
	prismaSampleToSample,
	prismaSamplingCollectionToSamplingCollection,
	prismaSamplingToSampling,
	prismaStratificationToStratification,
} from '../transforms';
import { PrismaFlatSampling } from '../types/prisma';

const router = Router();

router.get('/', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const userGroupIds = res.locals.user.memberships.map((m: PrismaMembership) => m.groupId);
		const where: any = res.locals.user.isSuperAdmin ? {} : { group: { id: { in: userGroupIds } } };
		if (res.locals.authorizationScheme === 'bearer') {
			where.group = {
				...where.group,
				apiOnly: false,
			};
		}

		const [
			prismaFields,
			prismaAreas,
			prismaStratifications,
			prismaLocationCollections,
			prismaLocations,
			prismaSamplingCollections,
			prismaSamplings,
			prismaSamples,
		] = await Promise.all([
			prisma.field.findMany({
				where,
				include: { areas: true, referenceIds: true },
			}),
			prisma.area.findMany({
				where,
			}),
			prisma.stratification.findMany({
				where,
				include: {
					algorithm: true,
					area: { select: { fieldId: true } },
				},
			}),
			prisma.locationCollection.findMany({
				where,
				include: { locations: { select: { id: true } } },
			}),
			prisma.location.findMany({
				where,
			}),
			prisma.samplingCollection.findMany({
				where,
				include: {
					samplings: { select: { id: true } },
					referenceIds: true,
				},
			}),
			prisma.sampling.findMany({
				where,
				include: { samples: { select: { id: true } } },
			}),
			prisma.sample.findMany({
				where,
			}),
		]);

		res.status(200).json({
			fields: prismaFields.map(prismaFieldToField),
			areas: prismaAreas.map(prismaAreaToArea),
			stratifications: prismaStratifications.map(prismaStratificationToStratification),
			locationCollections: prismaLocationCollections.map(prismaLocationCollectionToLocationCollection),
			locations: prismaLocations.map(prismaLocationToLocation),
			samplingCollections: prismaSamplingCollections.map(prismaSamplingCollectionToSamplingCollection),
			samplings: (prismaSamplings as PrismaFlatSampling[]).map(prismaSamplingToSampling),
			samples: prismaSamples.map(prismaSampleToSample),
		});
	} catch (error) {
		next(error);
	}
});

const openApiPaths: OpenAPIV3.PathsObject = {
	'/resources': {
		get: {
			'x-internal': true,
			summary: 'Get all resources.',
			responses: {
				'200': {
					description:
						'An object keyed by resource, with values that are arrays containing all the resources of each type you have access to.',
					content: {
						'application/json': {
							schema: {
								type: 'object',
								required: [
									'fields',
									'areas',
									'stratifications',
									'locationCollections',
									'locations',
									'samples',
									'samplings',
									'samplingCollections',
								],
								properties: {
									fields: { type: 'array', items: { $ref: '#/components/schemas/ResponseField' } },
									areas: { type: 'array', items: { $ref: '#/components/schemas/ResponseArea' } },
									stratifications: { type: 'array', items: { $ref: '#/components/schemas/ResponseStratification' } },
									locationCollections: {
										type: 'array',
										items: { $ref: '#/components/schemas/ResponseLocationCollection' },
									},
									locations: { type: 'array', items: { $ref: '#/components/schemas/ResponseLocation' } },
									samples: { type: 'array', items: { $ref: '#/components/schemas/ResponseSample' } },
									samplings: { type: 'array', items: { $ref: '#/components/schemas/ResponseSampling' } },
									samplingCollections: {
										type: 'array',
										items: { $ref: '#/components/schemas/ResponseSamplingCollection' },
									},
								},
							},
						},
					},
				},
				401: { $ref: '#/components/responses/401' },
			},
		} as OpenAPIV3.OperationObject,
	},
};

export { openApiPaths, router };
