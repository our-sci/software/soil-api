import request from 'supertest';
import { ResourceTypes } from '../constants';
import prisma from '../prismaClient';
import { getDefaultUserBearerToken, testApp } from '../test-utils';
import {
	expect200AndValidateResponse,
	expect404AndValidateResponse,
	postMockSamplingCollectionAndDependencies,
	testGetResourceByIdEndpoint,
} from '../test-utils';

describe('Sample Endpoints', () => {
	describe('GET', () => {
		describe('/samples', () => {
			it('returns 200 with samples the user has access to on success', async (done) => {
				// add a sampling collection to a group the user belongs to
				const {
					body: {
						samples: [{ id: sampleId }],
					},
				} = await postMockSamplingCollectionAndDependencies();
				// add a sampling collection to a group the user does NOT belong to
				await postMockSamplingCollectionAndDependencies({ groupId: 'seed-group-2', referenceId: 'ref id 2' });

				request(testApp)
					.get('/samples')
					.set('Authorization', getDefaultUserBearerToken())
					.end((err, res) => {
						expect(res.body).toHaveLength(1);
						expect(res.body[0]).toHaveProperty('id', sampleId);
						expect200AndValidateResponse(done)(err, res);
					});
			});

			it('returns 200 on success when no samples are not present', (done) => {
				request(testApp)
					.get('/samples')
					.set('Authorization', getDefaultUserBearerToken())
					.end(expect200AndValidateResponse(done));
			});
		});

		testGetResourceByIdEndpoint({
			endpointPath: '/samples',
			resourceName: 'sample',
			resourceType: ResourceTypes.Sample,
		});
	});

	describe('PATCH', () => {
		describe('/samples/:id', () => {
			const updateSampleRequest = {
				name: 'updated name',
				soDepth: {
					minValue: { value: 555, unit: 'unit:CM' },
					maxValue: { value: 666, unit: 'unit:CM' },
				},
			};

			it('returns 204 and updates the sample on success', async (done) => {
				const {
					body: {
						samples: [{ id }],
					},
				} = await postMockSamplingCollectionAndDependencies();

				await request(testApp)
					.patch(`/samples/${id}`)
					.set('Authorization', getDefaultUserBearerToken())
					.send(updateSampleRequest)
					.expect(204);

				const updatedSample = await prisma.sample.findUnique({ where: { id } });
				expect(updatedSample).toHaveProperty('name', updateSampleRequest.name);
				expect(updatedSample).toHaveProperty('soDepth', updateSampleRequest.soDepth);

				done();
			});

			it('returns 404 when the sample is not found', (done) => {
				request(testApp)
					.patch('/samples/non-existent-id')
					.set('Authorization', getDefaultUserBearerToken())
					.send(updateSampleRequest)
					.end(expect404AndValidateResponse(done));
			});

			it('returns 404 when the sample is not in a group the user has access to', async (done) => {
				const {
					body: {
						samples: [{ id }],
					},
				} = await postMockSamplingCollectionAndDependencies({ groupId: 'seed-group-2' });

				request(testApp)
					.patch(`/samples/${id}`)
					.set('Authorization', getDefaultUserBearerToken())
					.send(updateSampleRequest)
					.end(expect404AndValidateResponse(done));
			});
		});
	});
});
