import request from 'supertest';
import { prismaMock } from '../singleton';
import { createMockPrismaSample, getDefaultUserBearerToken, testApp } from '../test-utils';
import { createMockSample } from '../test-utils';
import { components } from '../types/generated/openapi-types';

describe('Sample Endpoints', () => {
	describe('GET', () => {
		describe('/samples', () => {
			it('returns 500 when prismaClient returns an unexpected error', (done) => {
				prismaMock.sample.findMany.mockRejectedValueOnce(new Error('unexpected error'));

				request(testApp).get('/samples').set('Authorization', getDefaultUserBearerToken()).expect(500, done);
			});
		});

		describe('/samples/:id', () => {
			it('returns 500 when prisma.sample.findFirst throws an unexpected error', (done) => {
				prismaMock.sample.findFirst.mockRejectedValueOnce(new Error('unexpected error'));

				request(testApp)
					.get('/samples/non-existent-id')
					.set('Authorization', getDefaultUserBearerToken())
					.expect(500, done);
			});
		});
	});

	describe('PATCH', () => {
		describe('/samples/:id', () => {
			const sampleId = '123456789012345678901234';
			const mockSample = createMockSample({ id: sampleId });
			const { name, soDepth } = mockSample;
			const mockEditableSample: components['schemas']['EditableSample'] = {
				name,
				soDepth: {
					minValue: { ...soDepth.minValue, unit: 'unit:CM' },
					maxValue: { ...soDepth.maxValue, unit: 'unit:CM' },
				},
			};

			it('returns 500 when prisma.sample.update throws an unexpected error', (done) => {
				prismaMock.sample.findUnique.mockResolvedValueOnce(createMockPrismaSample());
				prismaMock.sample.update.mockRejectedValueOnce(new Error('unexpected error'));

				return request(testApp)
					.patch(`/samples/${sampleId}`)
					.set('Authorization', getDefaultUserBearerToken())
					.send(mockEditableSample)
					.expect(500, done);
			});

			it('returns 500 when prisma.sample.findUnique throws an unexpected error', (done) => {
				prismaMock.sample.findUnique.mockRejectedValueOnce(new Error('unexpected error'));
				prismaMock.sample.update.mockResolvedValueOnce(createMockPrismaSample());

				return request(testApp)
					.patch(`/samples/${sampleId}`)
					.set('Authorization', getDefaultUserBearerToken())
					.send(mockEditableSample)
					.expect(500, done);
			});
		});
	});
});
