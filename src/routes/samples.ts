import { Prisma } from '@prisma/client';
import { Membership as PrismaMembership } from '@prisma/client';
import { NextFunction, Request, Response, Router } from 'express';
import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';
import { ResourceTypes } from '../constants';
import prisma from '../prismaClient';
import { editableSampleToPrismaSampleUpdateInput, prismaSampleToSample } from '../transforms';
import { components } from '../types/generated/openapi-types';
import createGetResourceByIdController from './common/createGetResourceByIdController';

const router = Router();

router.get('/', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const userGroupIds = res.locals.user.memberships.map((m: PrismaMembership) => m.groupId);

		const prismaSamples = await prisma.sample.findMany({
			where: { groupId: { in: userGroupIds } },
		});

		const samples = prismaSamples.map(prismaSampleToSample);

		res.status(200).json(samples);
	} catch (error) {
		next(error);
	}
});

router.get('/:id', createGetResourceByIdController(ResourceTypes.Sample));

router.patch('/:id', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const id = req.params.id;
		const body = req.body as components['schemas']['EditableSample'];
		const userGroupIds = res.locals.user.memberships.map((m: PrismaMembership) => m.groupId);

		const sample = await prisma.sample.findUnique({ where: { id } });
		const isAuthorized = userGroupIds.includes(sample?.groupId);

		if (!isAuthorized) {
			return res.status(404).send();
		}

		await prisma.sample.update({
			where: { id },
			data: editableSampleToPrismaSampleUpdateInput(body),
		});

		return res.status(204).send();
	} catch (error) {
		if (error instanceof Prisma.PrismaClientKnownRequestError) {
			// P2025 is the Prisma Client error code for RecordNotFound
			if (error.code === 'P2025') {
				return res.status(404).send();
			}
		}

		next(error);
	}
});

const openApiPaths: OpenAPIV3.PathsObject = {
	'/samples': {
		get: {
			tags: ['Samples'],
			summary: 'Get all samples.',
			responses: {
				'200': {
					description: 'An array of the samples you have access to.',
					content: {
						'application/json': {
							schema: {
								type: 'array',
								items: { $ref: '#/components/schemas/ResponseSample' },
							},
						},
					},
				},
				401: { $ref: '#/components/responses/401' },
			},
		},
	},
	'/samples/{sampleId}': {
		get: {
			tags: ['Samples'],
			summary: 'Get a sample by its id.',
			parameters: [
				{
					name: 'sampleId',
					in: 'path',
					description: 'The id of the sample to get.',
					required: true,
					schema: { type: 'string' },
				},
			],
			responses: {
				'200': {
					description: 'The sample with the given id.',
					content: {
						'application/json': {
							schema: {
								$ref: '#/components/schemas/ResponseSample',
							},
						},
					},
				},
				'404': {
					description: 'No sample with the given id exists.',
				},
				401: { $ref: '#/components/responses/401' },
			},
		},
		patch: {
			tags: ['Samples'],
			summary: 'Updates a sample.',
			parameters: [
				{
					name: 'sampleId',
					in: 'path',
					description: 'The id of the sample to edit.',
					required: true,
					schema: { type: 'string' },
				},
			],
			requestBody: {
				description: 'The sample object to be saved.',
				required: true,
				content: {
					'application/json': {
						schema: {
							$ref: '#/components/schemas/EditableSample',
						},
					},
				},
			},
			responses: {
				'204': {
					description: 'The sample was saved successfully.',
				},
				'404': {
					description: 'No sample with the given id exists.',
				},
				401: { $ref: '#/components/responses/401' },
			},
		},
	},
};

export { openApiPaths, router };
