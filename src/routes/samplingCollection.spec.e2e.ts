import request from 'supertest';
import { ResourceTypes } from '../constants';
import { getDefaultAdminBearerToken, getDefaultUserBearerToken, testApp } from '../test-utils';
import {
	createMockPopulatedSamplingCollection,
	expect200AndValidateResponse,
	expect201AndValidateResponse,
	expect400AndValidateResponse,
	expect404AndValidateResponse,
	postMockField,
	postMockSamplingCollection,
	postMockSamplingCollectionAndDependencies,
	postMockStratification,
	postMockStratificationAndDependencies,
	testGetResourceByIdEndpoint,
} from '../test-utils';

describe('Sampling Collection Endpoints', () => {
	describe('POST', () => {
		describe('/location-collections/:locationCollectionId/sampling-collections', () => {
			it('returns 201 on success', async (done) => {
				const {
					body: {
						areas: [{ id: areaId }],
						fields: [{ id: fieldId }],
					},
				} = await postMockField();
				const {
					body: {
						locationCollections: [{ id: locationCollectionId }],
						locations: [{ id: locationId }],
					},
				} = await postMockStratification({ areaId });

				const postSamplingCollectionRequestBody = createMockPopulatedSamplingCollection([locationId]);

				request(testApp)
					.post(`/location-collections/${locationCollectionId}/sampling-collections`)
					.set('Authorization', getDefaultUserBearerToken())
					.send(postSamplingCollectionRequestBody)
					.end((err, res) => {
						const samplingCollections = res.body.samplingCollections;
						const samplings = res.body.samplings;
						const samples = res.body.samples;

						expect(samplingCollections).toHaveLength(1);

						expect(
							samplingCollections[0],
							'samplingCollection.featureOfInterest should match the id of the field it belongs to'
						).toHaveProperty('featureOfInterest', fieldId);

						expect(
							samplingCollections[0],
							'samplingCollection.object should match the id of the location collection'
						).toHaveProperty('object', locationCollectionId);

						expect(
							samplingCollections[0],
							'samplingCollection.meta.groupId should match the id of group the location collection belongs to'
						).toHaveProperty('meta.groupId', 'seed-group-1');

						expect(
							samplingCollections[0].meta.referenceIds,
							'samplingCollection.meta.referenceIds should contain the passed in reference id'
						).toContainEqual({ id: 'mock reference id', owner: 'soilstack-draft' });

						expect(samplings).toHaveLength(1);

						expect(
							samplingCollections[0].members,
							'samplingCollection.members should reference the ids of the samplings it contains'
						).toContain(samplings[0].id);

						expect(
							samplings[0],
							'sampling.featureOfInterest should match the id of the location it belongs to'
						).toHaveProperty('featureOfInterest', locationId);

						expect(
							samplings[0],
							'sampling.memberOf should match the id of the sampling collection it belongs to'
						).toHaveProperty('memberOf', samplingCollections[0].id);

						expect(
							samplings[0],
							'sampling.meta.groupId should match the id of group the location collection it relates to'
						).toHaveProperty('meta.groupId', 'seed-group-1');

						expect(samples).toHaveLength(1);

						expect(
							samplings[0].results,
							'sampling.results should reference the ids of the samples it contains'
						).toContain(samples[0].id);

						expect(samples[0], 'sample.resultOf should match the id of the sampling it belongs to').toHaveProperty(
							'resultOf',
							samplings[0].id
						);

						expect(samples[0], 'sample.sampleOf should match the id of the field it belongs to').toHaveProperty(
							'sampleOf',
							fieldId
						);

						expect(
							samples[0],
							'sample.meta.groupId should match the id of group the location collection it relates to'
						).toHaveProperty('meta.groupId', 'seed-group-1');

						expect201AndValidateResponse(done)(err, res);
					});
			});

			it('returns 404 when the location collection referenced in the path does not exist', (done) => {
				const postSamplingCollectionRequestBody = createMockPopulatedSamplingCollection(['location-id']);

				request(testApp)
					.post(`/location-collections/non-existent-id/sampling-collections`)
					.set('Authorization', getDefaultUserBearerToken())
					.send(postSamplingCollectionRequestBody)
					.end(expect404AndValidateResponse(done));
			});

			it('returns 404 when the location collection referenced in the path is in a group the user does not have access to', async (done) => {
				const {
					body: {
						locationCollections: [{ id: locationCollectionId }],
					},
				} = await postMockStratificationAndDependencies({ groupId: 'seed-group-2' });
				const postSamplingCollectionRequestBody = createMockPopulatedSamplingCollection(['location-id']);

				request(testApp)
					.post(`/location-collections/${locationCollectionId}/sampling-collections`)
					.set('Authorization', getDefaultUserBearerToken())
					.send(postSamplingCollectionRequestBody)
					.end(expect404AndValidateResponse(done));
			});

			it('returns 400 when locations referenced by sampling.featureOfInterest do not belong to the location collection referenced in the path', async (done) => {
				const {
					body: {
						locationCollections: [locationCollection],
					},
				} = await postMockStratificationAndDependencies();

				const samplingCollectionRequest = createMockPopulatedSamplingCollection(['non-existent-location-id']);

				request(testApp)
					.post(`/location-collections/${locationCollection.id}/sampling-collections`)
					.set('Authorization', getDefaultAdminBearerToken())
					.send(samplingCollectionRequest)
					.end(expect400AndValidateResponse(done));
			});
		});
	});

	describe('GET', () => {
		describe('/sampling-collections', () => {
			it('returns 200 with sampling collections the user has access to on success', async (done) => {
				// add a sampling collection to a group the user belongs to
				const {
					body: {
						samplingCollections: [{ id: samplingCollectionId }],
					},
				} = await postMockSamplingCollectionAndDependencies();
				// add a sampling collection to a group the user does NOT belong to
				await postMockSamplingCollectionAndDependencies({ groupId: 'seed-group-2', referenceId: 'ref id 2' });

				request(testApp)
					.get('/sampling-collections')
					.set('Authorization', getDefaultUserBearerToken())
					.end((err, res) => {
						expect(res.body).toHaveLength(1);
						expect(res.body[0]).toHaveProperty('id', samplingCollectionId);
						expect(res.body[0]).toHaveProperty('meta.groupId', 'seed-group-1');
						expect200AndValidateResponse(done)(err, res);
					});
			});

			it('returns 200 on success when no sampling collections are present', (done) => {
				request(testApp)
					.get('/sampling-collections')
					.set('Authorization', getDefaultUserBearerToken())
					.end(expect200AndValidateResponse(done));
			});

			describe("when passing 'submittedAfter' query paramter", () => {
				it("only returns sampling collections created after the 'submittedAfter' time, ordered most recent first", async (done) => {
					const {
						body: {
							locationCollections: [{ id: locationCollectionId }],
							locations: [{ id: locationId }],
						},
					} = await postMockStratificationAndDependencies();
					const {
						body: {
							samplingCollections: [samplingCollectionOne],
						},
					} = await postMockSamplingCollection(locationCollectionId, locationId, 'ref1');
					const {
						body: {
							samplingCollections: [samplingCollectionTwo],
						},
					} = await postMockSamplingCollection(locationCollectionId, locationId, 'ref2');
					const {
						body: {
							samplingCollections: [samplingCollectionThree],
						},
					} = await postMockSamplingCollection(locationCollectionId, locationId, 'ref3');

					const submittedAfter = encodeURIComponent(samplingCollectionOne.meta.submittedAt);

					request(testApp)
						.get(`/sampling-collections?submittedAfter=${submittedAfter}`)
						.set('Authorization', getDefaultUserBearerToken())
						.end((err, res) => {
							expect(res.body).toHaveLength(2);
							expect(res.body[0]).toHaveProperty('id', samplingCollectionThree.id);
							expect(res.body[1]).toHaveProperty('id', samplingCollectionTwo.id);
							expect200AndValidateResponse(done)(err, res);
						});
				});
			});
		});

		testGetResourceByIdEndpoint({
			endpointPath: '/sampling-collections',
			resourceName: 'sampling collection',
			resourceType: ResourceTypes.SamplingCollection,
		});
	});
});
