import request from 'supertest';
import { prismaMock } from '../singleton';
import { getDefaultUserBearerToken, testApp } from '../test-utils';
import {
	createMockPopulatedSamplingCollection,
	createMockPrismaPopulatedLocationCollectionWithFieldAndLocationIds,
	createMockPrismaPopulatedSampling,
	expect400AndValidateResponse,
} from '../test-utils';

describe('Sampling Collection Endpoints', () => {
	describe('POST', () => {
		describe('/location-collections/:locationCollectionId/sampling-collections', () => {
			const mockPopulatedSamplingCollection = createMockPopulatedSamplingCollection(['location-id']);

			it('returns 500 when prismaClient returns an unexpected error when creating samples', (done) => {
				prismaMock.locationCollection.findUnique.mockResolvedValueOnce(
					createMockPrismaPopulatedLocationCollectionWithFieldAndLocationIds()
				);
				prismaMock.$transaction.mockRejectedValueOnce(new Error('unkown prisma error'));

				request(testApp)
					.post('/location-collections/mock-id/sampling-collections')
					.set('Authorization', getDefaultUserBearerToken())
					.send(mockPopulatedSamplingCollection)
					.expect(500, done);
			});

			it('returns 500 when prismaClient returns an unexpected error when creating the sampling collection', (done) => {
				prismaMock.locationCollection.findUnique.mockResolvedValueOnce(
					createMockPrismaPopulatedLocationCollectionWithFieldAndLocationIds()
				);
				prismaMock.$transaction.mockResolvedValue([createMockPrismaPopulatedSampling()]);
				prismaMock.samplingCollection.create.mockRejectedValueOnce(new Error('unexpected error'));

				request(testApp)
					.post('/location-collections/mock-id/sampling-collections')
					.set('Authorization', getDefaultUserBearerToken())
					.send(mockPopulatedSamplingCollection)
					.expect(500, done);
			});

			it('attemps to rollback (delete) created samplings when sampling collection creation fails', (done) => {
				const mockPrismaSampling = createMockPrismaPopulatedSampling();
				prismaMock.locationCollection.findUnique.mockResolvedValueOnce(
					createMockPrismaPopulatedLocationCollectionWithFieldAndLocationIds()
				);
				prismaMock.$transaction.mockResolvedValue([mockPrismaSampling]);
				prismaMock.samplingCollection.create.mockRejectedValueOnce(new Error('unexpected error'));

				request(testApp)
					.post('/location-collections/mock-id/sampling-collections')
					.set('Authorization', getDefaultUserBearerToken())
					.send(mockPopulatedSamplingCollection)
					.end((err, res) => {
						expect(prismaMock.sampling.deleteMany).toHaveBeenCalledWith({
							where: { id: { in: [mockPrismaSampling.id] } },
						});
						done();
					});
			});

			it('returns 500 when prismaClient returns an unexpected error when finding the location collection', (done) => {
				prismaMock.locationCollection.findUnique.mockRejectedValueOnce(new Error('unexpected error'));

				request(testApp)
					.post('/location-collections/mock-id/sampling-collections')
					.set('Authorization', getDefaultUserBearerToken())
					.send(mockPopulatedSamplingCollection)
					.expect(500, done);
			});
		});
	});

	describe('GET', () => {
		describe('/sampling-collections', () => {
			it('returns 500 when prisma client throws an unexpected error', (done) => {
				prismaMock.samplingCollection.findMany.mockRejectedValueOnce(new Error('unexpected error'));

				request(testApp)
					.get(`/sampling-collections`)
					.set('Authorization', getDefaultUserBearerToken())
					.expect(500, done);
			});

			const invalidSubmittedAfterValues = [
				{ description: 'empty string', submittedAfter: '' },
				{ description: 'invalid date', submittedAfter: '2020-01-01T00:00:00.000' },
				{ description: 'ms since epoch', submittedAfter: '1577836800000' },
			];

			invalidSubmittedAfterValues.forEach(({ description, submittedAfter }) => {
				it(`returns 400 when ${description} is passed in the \'submittedAfter\' query paramter`, (done) => {
					request(testApp)
						.get('/sampling-collections')
						.set('Authorization', getDefaultUserBearerToken())
						.query({ submittedAfter })
						.send()
						.end(expect400AndValidateResponse(done));
				});
			});
		});

		describe('/sampling-collections/:id', () => {
			it('returns 500 when prisma.samplingCollection.findFirst throws an unexpected error', (done) => {
				prismaMock.samplingCollection.findFirst.mockRejectedValueOnce(new Error('unexpected error'));

				request(testApp)
					.get('/sampling-collections/non-existent-id')
					.set('Authorization', getDefaultUserBearerToken())
					.expect(500, done);
			});
		});
	});
});
