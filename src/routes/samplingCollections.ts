import { Membership as PrismaMembership } from '@prisma/client';
import { NextFunction, Request, Response, Router } from 'express';
import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';
import { ResourceTypes } from '../constants';
import prisma from '../prismaClient';
import { prismaSamplingCollectionToSamplingCollection } from '../transforms';
import createGetResourceByIdController from './common/createGetResourceByIdController';

const router = Router();

router.get('/', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const submittedAfter = req.query.submittedAfter as string;
		const userGroupIds = res.locals.user.memberships.map((m: PrismaMembership) => m.groupId);
		const defaultWhere = { groupId: { in: userGroupIds } };

		const samplingCollections = await prisma.samplingCollection.findMany({
			where: submittedAfter ? { ...defaultWhere, createdAt: { gt: new Date(submittedAfter) } } : defaultWhere,
			orderBy: { createdAt: 'desc' },
			include: {
				samplings: { select: { id: true } },
				referenceIds: true,
			},
		});

		res.status(200).json(samplingCollections.map(prismaSamplingCollectionToSamplingCollection));
	} catch (error) {
		next(error);
	}
});

router.get('/:id', createGetResourceByIdController(ResourceTypes.SamplingCollection));

const openApiPaths: OpenAPIV3.PathsObject = {
	'/sampling-collections': {
		get: {
			tags: ['Sampling Collections'],
			summary: 'Get all sampling collections.',
			parameters: [
				{
					name: 'submittedAfter',
					in: 'query',
					description: 'Only return sampling collections submitted after this ISO8601 timestamp.',
					required: false,
					schema: {
						type: 'string',
						format: 'date-time',
					},
				},
			],
			responses: {
				'200': {
					description: 'An array of the sampling collections you have access to.',
					content: {
						'application/json': {
							schema: {
								type: 'array',
								items: { $ref: '#/components/schemas/ResponseSamplingCollection' },
							},
						},
					},
				},
				401: { $ref: '#/components/responses/401' },
			},
		},
	},
	'/sampling-collections/{samplingCollectionId}': {
		get: {
			tags: ['Sampling Collections'],
			summary: 'Get a sampling collection by its id.',
			parameters: [
				{
					name: 'samplingCollectionId',
					in: 'path',
					description: 'The id of the sampling collection to get.',
					required: true,
					schema: { type: 'string' },
				},
			],
			responses: {
				'200': {
					description: 'The sampling collection with the given id.',
					content: {
						'application/json': {
							schema: {
								$ref: '#/components/schemas/ResponseSamplingCollection',
							},
						},
					},
				},
				'404': {
					description: 'No sampling collection with the given id exists.',
				},
				401: { $ref: '#/components/responses/401' },
			},
		},
	},
};

export { openApiPaths, router };
