import request from 'supertest';
import { ResourceTypes } from '../constants';
import { getDefaultUserBearerToken, testApp } from '../test-utils';
import {
	expect200AndValidateResponse,
	postMockSamplingCollectionAndDependencies,
	testGetResourceByIdEndpoint,
} from '../test-utils';

describe('Sampling Endpoints', () => {
	describe('GET', () => {
		describe('/samplings', () => {
			it('returns 200 with samplings the user has access to on success', async (done) => {
				// add a sampling collection to a group the user belongs to
				const {
					body: {
						samplings: [{ id: samplingId }],
					},
				} = await postMockSamplingCollectionAndDependencies();
				// add a sampling collection to a group the user does NOT belong to
				await postMockSamplingCollectionAndDependencies({ groupId: 'seed-group-2', referenceId: 'ref id 2' });

				request(testApp)
					.get('/samplings')
					.set('Authorization', getDefaultUserBearerToken())
					.end((err, res) => {
						expect(res.body).toHaveLength(1);
						expect(res.body[0]).toHaveProperty('id', samplingId);
						expect200AndValidateResponse(done)(err, res);
					});
			});

			it('returns 200 on success when no samplings are not present', (done) => {
				request(testApp)
					.get('/samplings')
					.set('Authorization', getDefaultUserBearerToken())
					.end(expect200AndValidateResponse(done));
			});
		});

		testGetResourceByIdEndpoint({
			endpointPath: '/samplings',
			resourceName: 'sampling',
			resourceType: ResourceTypes.Sampling,
		});
	});
});
