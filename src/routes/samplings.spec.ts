import request from 'supertest';
import { prismaMock } from '../singleton';
import { getDefaultUserBearerToken, testApp } from '../test-utils';

describe('Sampling Endpoints', () => {
	describe('GET', () => {
		describe('/samplings', () => {
			it('returns 500 when prismaClient returns an unexpected error', (done) => {
				prismaMock.sampling.findMany.mockRejectedValueOnce(new Error('unexpected error'));

				request(testApp).get('/samplings').set('Authorization', getDefaultUserBearerToken()).expect(500, done);
			});
		});

		describe('/samplings/:id', () => {
			it('returns 500 when prisma.sampling.findFirst throws an unexpected error', (done) => {
				prismaMock.sampling.findFirst.mockRejectedValueOnce(new Error('unexpected error'));

				request(testApp)
					.get('/samplings/non-existent-id')
					.set('Authorization', getDefaultUserBearerToken())
					.expect(500, done);
			});
		});
	});
});
