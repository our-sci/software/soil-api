import { Membership as PrismaMembership } from '@prisma/client';
import { NextFunction, Request, Response, Router } from 'express';
import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';
import { ResourceTypes } from '../constants';
import prisma from '../prismaClient';
import { prismaSamplingToSampling } from '../transforms';
import { PrismaFlatSampling } from '../types/prisma';
import createGetResourceByIdController from './common/createGetResourceByIdController';

const router = Router();

router.get('/', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const userGroupIds = res.locals.user.memberships.map((m: PrismaMembership) => m.groupId);

		const prismaSamplings = await prisma.sampling.findMany({
			where: { groupId: { in: userGroupIds } },
			include: { samples: { select: { id: true } } },
		});

		const samplings = (prismaSamplings as PrismaFlatSampling[]).map(prismaSamplingToSampling);

		res.status(200).json(samplings);
	} catch (error) {
		next(error);
	}
});

router.get('/:id', createGetResourceByIdController(ResourceTypes.Sampling));

const openApiPaths: OpenAPIV3.PathsObject = {
	'/samplings': {
		get: {
			tags: ['Samplings'],
			summary: 'Get all samplings.',
			responses: {
				'200': {
					description: 'An array of the samplings you have access to.',
					content: {
						'application/json': {
							schema: {
								type: 'array',
								items: { $ref: '#/components/schemas/ResponseSampling' },
							},
						},
					},
				},
				401: { $ref: '#/components/responses/401' },
			},
		},
	},
	'/samplings/{samplingId}': {
		get: {
			tags: ['Samplings'],
			summary: 'Get a sampling by its id.',
			parameters: [
				{
					name: 'samplingId',
					in: 'path',
					description: 'The id of the sampling to get.',
					required: true,
					schema: { type: 'string' },
				},
			],
			responses: {
				'200': {
					description: 'The sampling with the given id.',
					content: {
						'application/json': {
							schema: {
								$ref: '#/components/schemas/ResponseSampling',
							},
						},
					},
				},
				'404': {
					description: 'No sampling with the given id exists.',
				},
				401: { $ref: '#/components/responses/401' },
			},
		},
	},
};

export { openApiPaths, router };
