import { Prisma } from '@prisma/client';
import request from 'supertest';
import * as constants from '../constants';
import prisma from '../prismaClient';
import * as stratificationService from '../services/stratification';
import {
	createMockStratificationConfig,
	createMockStratificationJobFailure,
	createMockStratificationJobSuccess,
	createMockStratificationServiceStratification,
	createMockStratifyRequest,
	createMockStratServicePostStratificationResponse,
	expect200AndValidateResponse,
	expect404AndValidateResponse,
	expect409AndValidateResponse,
	getUserBearerToken,
	postMockApiKey,
	postMockField,
	postMockGroup,
	postMockStratification,
	registerUser,
	testApp,
} from '../test-utils';
import { stratServiceJobToPrismaStratificationJobCreate } from '../transforms';

jest.mock('../services/stratification');

const postMockStratifyAndDependencies = async ({ autoApprove } = { autoApprove: false }) => {
	await registerUser({
		id: 'seed-super-admin-1',
		email: 'seedSuperAdmin1@soilstack.io',
		password: 'password',
		groupIds: [],
		isSuperAdmin: true,
	});

	const {
		body: {
			areas: [{ id: areaId }],
		},
	} = await postMockField();

	(stratificationService.postStratification as jest.Mock).mockResolvedValueOnce({
		status: 200,
		data: createMockStratServicePostStratificationResponse(),
	});
	return request(testApp)
		.post('/areas/stratify')
		.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
		.send(createMockStratifyRequest({ areaIds: [areaId], autoApprove }))
		.expect(201);
};

describe('Stratification Job Endpoints', () => {
	describe('/stratification-jobs/resolve', () => {
		describe('POST', () => {
			it('returns 404 if the job does not exist', (done) => {
				request(testApp)
					.post(`/stratification-jobs/resolve?token=${encodeURIComponent(constants.STRATIFICATION_SERVICE_KEY)}`)
					.send(createMockStratificationJobSuccess())
					.expect(404, done);
			});

			it('returns 409 if the job exists and is already resolved', async (done) => {
				const {
					body: {
						areas: [{ id: areaId }],
					},
				} = await postMockField();
				const jobId = 'mock-job-id';

				await prisma.stratificationJob.create({
					data: stratServiceJobToPrismaStratificationJobCreate({
						jobId,
						jobStatus: 'SUCCESS',
						areaId,
						stratificationConfig: createMockStratificationConfig(),
						isClosed: false,
					}),
				});

				request(testApp)
					.post(`/stratification-jobs/resolve?token=${encodeURIComponent(constants.STRATIFICATION_SERVICE_KEY)}`)
					.send(createMockStratificationJobSuccess({ taskId: jobId }))
					.expect(409, done);
			});

			it("returns 200 and updates the job to a success state when the resolve request body has state = 'SUCCESS'", async (done) => {
				const {
					body: [{ id }],
				} = await postMockStratifyAndDependencies();
				const mockStratificationJobSuccess = createMockStratificationJobSuccess();

				await request(testApp)
					.post(`/stratification-jobs/resolve?token=${encodeURIComponent(constants.STRATIFICATION_SERVICE_KEY)}`)
					.send(mockStratificationJobSuccess)
					.expect(200);

				const stratificationJob = await prisma.stratificationJob.findFirst({ where: { id } });
				expect(stratificationJob?.externalJobStatus).toBe('SUCCESS');
				expect(stratificationJob?.jobResult).toEqual(mockStratificationJobSuccess.result);
				done();
			});

			it("returns 200 and updates the job to a failure state when the resolve request body has state = 'FAILURE'", async (done) => {
				const {
					body: [{ id }],
				} = await postMockStratifyAndDependencies();
				const mockStratificationJobFailure = createMockStratificationJobFailure();

				await request(testApp)
					.post(`/stratification-jobs/resolve?token=${encodeURIComponent(constants.STRATIFICATION_SERVICE_KEY)}`)
					.send(mockStratificationJobFailure)
					.expect(200);

				const stratificationJob = await prisma.stratificationJob.findFirst({ where: { id } });
				expect(stratificationJob?.externalJobStatus).toBe('FAILURE');
				expect(stratificationJob?.error).toEqual(mockStratificationJobFailure.result);
				done();
			});

			describe('when the job has autoApprove set to true', () => {
				describe('if the job is already closed', () => {
					it('returns 200 and updates the job but does NOT create a stratification', async (done) => {
						const {
							body: [{ id }],
						} = await postMockStratifyAndDependencies({ autoApprove: true });
						await request(testApp)
							.post(`/stratification-jobs/${id}/close`)
							.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
							.send({ closedReason: 'CANCELLED' })
							.expect(200);
						const mockStratificationJobSuccess = createMockStratificationJobSuccess();

						await request(testApp)
							.post(`/stratification-jobs/resolve?token=${encodeURIComponent(constants.STRATIFICATION_SERVICE_KEY)}`)
							.send(mockStratificationJobSuccess)
							.expect(200);

						const stratificationJob = await prisma.stratificationJob.findFirst({ where: { id } });
						expect(stratificationJob?.externalJobStatus).toBe('SUCCESS');
						expect(stratificationJob?.jobResult).toEqual(mockStratificationJobSuccess.result);
						const associatedStratification = await prisma.stratification.findFirst({
							where: {
								originStratificationJob: { id },
							},
						});
						expect(associatedStratification).toBeNull();
						done();
					});
				});

				describe('if the job is not closed', () => {
					it('returns 200, updates the job, creates a stratification, and closes the job with closedReason = "APPROVED"', async (done) => {
						const {
							body: [{ id }],
						} = await postMockStratifyAndDependencies({ autoApprove: true });
						const mockStratificationJobSuccess = createMockStratificationJobSuccess();

						await request(testApp)
							.post(`/stratification-jobs/resolve?token=${encodeURIComponent(constants.STRATIFICATION_SERVICE_KEY)}`)
							.send(mockStratificationJobSuccess)
							.expect(200);

						const stratificationJob = await prisma.stratificationJob.findFirst({ where: { id } });
						expect(stratificationJob?.externalJobStatus).toBe('SUCCESS');
						expect(stratificationJob?.jobResult).toEqual(mockStratificationJobSuccess.result);
						expect(stratificationJob?.isClosed).toBe(true);
						expect(stratificationJob?.closedReason).toBe('APPROVED');
						const associatedStratification = await prisma.stratification.findFirst({
							where: {
								originStratificationJob: { id },
							},
						});
						expect(associatedStratification).toBeDefined();
						done();
					});

					it('if the related field is already stratified it returns 200 and updates the job, but does NOT create a stratification or mark the job as closed', async (done) => {
						const {
							body: [{ id, areaId }],
						} = await postMockStratifyAndDependencies({ autoApprove: true });
						await postMockStratification({ areaId });
						const mockStratificationJobSuccess = createMockStratificationJobSuccess();

						await request(testApp)
							.post(`/stratification-jobs/resolve?token=${encodeURIComponent(constants.STRATIFICATION_SERVICE_KEY)}`)
							.send(mockStratificationJobSuccess)
							.expect(200);

						const stratificationJob = await prisma.stratificationJob.findFirst({ where: { id } });
						expect(stratificationJob?.externalJobStatus).toBe('SUCCESS');
						expect(stratificationJob?.jobResult).toEqual(mockStratificationJobSuccess.result);
						expect(stratificationJob?.isClosed).toBe(false);
						const associatedStratification = await prisma.stratification.findFirst({
							where: {
								originStratificationJob: { id },
							},
						});
						expect(associatedStratification).toBeNull();
						done();
					});
				});
			});
		});
	});

	describe('/stratification-jobs', () => {
		describe('GET', () => {
			let areaId: string;
			const originalStratificationJobTimeoutMs = constants.STRATIFICATION_JOB_TIMEOUT_MS;
			beforeEach(async () => {
				await registerUser({
					id: 'seed-super-admin-1',
					email: 'seedSuperAdmin1@soilstack.io',
					password: 'password',
					groupIds: [],
					isSuperAdmin: true,
				});
				const postFieldResponse = await postMockField();
				areaId = postFieldResponse.body.areas[0].id;
			});
			afterEach(() => {
				(constants.STRATIFICATION_JOB_TIMEOUT_MS as any) = originalStratificationJobTimeoutMs;
			});

			it('excludes stratification jobs associated with areas in apiOnly groups when authorization scheme is bearer', async (done) => {
				const {
					body: { id: apiOnlyGroupId },
				} = await postMockGroup({
					name: 'API Only Group',
					apiOnly: true,
					parentGroupId: 'seed-group-1',
					authorizationHeaderValue: getUserBearerToken('seedSuperAdmin1@soilstack.io'),
				});
				const {
					body: {
						areas: [{ id: areaId1 }],
					},
				} = await postMockField();
				const {
					body: {
						areas: [{ id: areaId2 }],
					},
				} = await postMockField({ groupId: apiOnlyGroupId });
				await prisma.stratificationJob.create({
					data: stratServiceJobToPrismaStratificationJobCreate({
						jobId: 'mock-job-id',
						jobStatus: 'SUCCESS',
						areaId: areaId1,
						stratificationConfig: createMockStratificationConfig(),
						isClosed: true,
						closedReason: 'CANCELLED',
					}),
				});
				await prisma.stratificationJob.create({
					data: stratServiceJobToPrismaStratificationJobCreate({
						jobId: 'mock-job-id-apionly-group',
						jobStatus: 'SUCCESS',
						areaId: areaId2,
						stratificationConfig: createMockStratificationConfig(),
						isClosed: true,
						closedReason: 'CANCELLED',
					}),
				});

				request(testApp)
					.get('/stratification-jobs')
					.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
					.end((err, res) => {
						expect(res.body.length).toBe(1);
						expect200AndValidateResponse(done)(err, res);
					});
			});

			it('includes stratification jobs associated with areas in apiOnly groups when authorization scheme is apikey', async (done) => {
				const {
					body: { apiKey },
				} = await postMockApiKey({
					authToken: getUserBearerToken('seedSuperAdmin1@soilstack.io'),
				});
				const {
					body: { id: apiOnlyGroupId },
				} = await postMockGroup({
					name: 'API Only Group',
					apiOnly: true,
					parentGroupId: 'seed-group-1',
					authorizationHeaderValue: getUserBearerToken('seedSuperAdmin1@soilstack.io'),
				});
				const {
					body: {
						areas: [{ id: areaId1 }],
					},
				} = await postMockField();
				const {
					body: {
						areas: [{ id: areaId2 }],
					},
				} = await postMockField({ groupId: apiOnlyGroupId });
				await prisma.stratificationJob.create({
					data: stratServiceJobToPrismaStratificationJobCreate({
						jobId: 'mock-job-id',
						jobStatus: 'SUCCESS',
						areaId: areaId1,
						stratificationConfig: createMockStratificationConfig(),
						isClosed: true,
						closedReason: 'CANCELLED',
					}),
				});
				await prisma.stratificationJob.create({
					data: stratServiceJobToPrismaStratificationJobCreate({
						jobId: 'mock-job-id-apionly-group',
						jobStatus: 'SUCCESS',
						areaId: areaId2,
						stratificationConfig: createMockStratificationConfig(),
						isClosed: true,
						closedReason: 'CANCELLED',
					}),
				});

				request(testApp)
					.get('/stratification-jobs')
					.set('Authorization', `apikey ${apiKey}`)
					.end((err, res) => {
						expect(res.body.length).toBe(2);
						expect200AndValidateResponse(done)(err, res);
					});
			});

			it('returns jobs that were already closed before the request', async (done) => {
				await prisma.stratificationJob.create({
					data: stratServiceJobToPrismaStratificationJobCreate({
						jobId: 'mock-job-id',
						jobStatus: 'SUCCESS',
						areaId,
						stratificationConfig: createMockStratificationConfig(),
						isClosed: true,
						closedReason: 'CANCELLED',
					}),
				});

				request(testApp)
					.get('/stratification-jobs')
					.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
					.end((err, res) => {
						expect(res.body.length).toBe(1);
						expect200AndValidateResponse(done)(err, res);
					});
			});

			it('returns open, pending jobs that were not updated', async (done) => {
				const job = await prisma.stratificationJob.create({
					data: stratServiceJobToPrismaStratificationJobCreate({
						jobId: 'mock-task-id',
						jobStatus: 'PENDING',
						areaId,
						stratificationConfig: createMockStratificationConfig(),
						isClosed: false,
					}),
				});
				(stratificationService.getStratification as jest.Mock).mockResolvedValueOnce({
					data: {
						taskId: 'mock-task-id',
						state: 'PENDING',
					},
				});

				request(testApp)
					.get('/stratification-jobs')
					.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
					.end((err, res) => {
						expect(res.body.length).toBe(1);
						expect(res.body[0].updatedAt).toBe(job.updatedAt.toISOString());
						expect200AndValidateResponse(done)(err, res);
					});
			});

			it('returns open, non-pending jobs that were not updated', async (done) => {
				const job = await prisma.stratificationJob.create({
					data: stratServiceJobToPrismaStratificationJobCreate({
						jobId: 'mock-task-id',
						jobStatus: 'SUCCESS',
						areaId,
						stratificationConfig: createMockStratificationConfig(),
						isClosed: false,
					}),
				});

				request(testApp)
					.get('/stratification-jobs')
					.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
					.end((err, res) => {
						expect(res.body.length).toBe(1);
						expect(res.body[0].updatedAt).toBe(job.updatedAt.toISOString());
						expect200AndValidateResponse(done)(err, res);
					});
			});

			it('returns jobs that were pending, and were updated to success', async (done) => {
				await prisma.stratificationJob.create({
					data: stratServiceJobToPrismaStratificationJobCreate({
						jobId: 'mock-task-id',
						jobStatus: 'PENDING',
						areaId,
						stratificationConfig: createMockStratificationConfig(),
						isClosed: false,
					}),
				});
				const mockStratificationJobSuccess = createMockStratificationJobSuccess();
				(stratificationService.getStratification as jest.Mock).mockResolvedValueOnce({
					data: mockStratificationJobSuccess,
				});

				request(testApp)
					.get('/stratification-jobs')
					.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
					.end((err, res) => {
						expect(res.body.length).toBe(1);
						expect(res.body[0].externalJobStatus).toBe('SUCCESS');
						expect(res.body[0].jobResult).toEqual(mockStratificationJobSuccess.result);
						expect200AndValidateResponse(done)(err, res);
					});
			});

			it('returns jobs that were pending, and were updated to failure', async (done) => {
				await prisma.stratificationJob.create({
					data: stratServiceJobToPrismaStratificationJobCreate({
						jobId: 'mock-task-id',
						jobStatus: 'PENDING',
						areaId,
						stratificationConfig: createMockStratificationConfig(),
						isClosed: false,
					}),
				});
				const mockStratificationJobFailure = createMockStratificationJobFailure();
				(stratificationService.getStratification as jest.Mock).mockResolvedValueOnce({
					data: mockStratificationJobFailure,
				});

				request(testApp)
					.get('/stratification-jobs')
					.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
					.end((err, res) => {
						expect(res.body.length).toBe(1);
						expect(res.body[0].externalJobStatus).toBe('FAILURE');
						expect(res.body[0].error).toEqual(mockStratificationJobFailure.result);
						expect200AndValidateResponse(done)(err, res);
					});
			});

			it('returns jobs that were pending, and were updated to timed out due to failed request to stratification service', async (done) => {
				await prisma.stratificationJob.create({
					data: stratServiceJobToPrismaStratificationJobCreate({
						jobId: 'mock-task-id',
						jobStatus: 'PENDING',
						areaId,
						stratificationConfig: createMockStratificationConfig(),
						isClosed: false,
					}),
				});
				(stratificationService.getStratification as jest.Mock).mockRejectedValueOnce({});
				(constants.STRATIFICATION_JOB_TIMEOUT_MS as any) = 0;

				request(testApp)
					.get('/stratification-jobs')
					.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
					.end((err, res) => {
						expect(res.body.length).toBe(1);
						expect(res.body[0].isClosed).toBe(true);
						expect(res.body[0].closedReason).toBe('TIMED_OUT');
						expect200AndValidateResponse(done)(err, res);
					});
			});

			it("returns jobs that were pending, and were updated to timed out due to response from stratification service containing a job state of 'PENDING'", async (done) => {
				await prisma.stratificationJob.create({
					data: stratServiceJobToPrismaStratificationJobCreate({
						jobId: 'mock-task-id',
						jobStatus: 'PENDING',
						areaId,
						stratificationConfig: createMockStratificationConfig(),
						isClosed: false,
					}),
				});
				(stratificationService.getStratification as jest.Mock).mockResolvedValueOnce({
					data: {
						taskId: 'mock-task-id',
						state: 'PENDING',
					},
				});
				(constants.STRATIFICATION_JOB_TIMEOUT_MS as any) = 0;

				request(testApp)
					.get('/stratification-jobs')
					.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
					.end((err, res) => {
						expect(res.body.length).toBe(1);
						expect(res.body[0].isClosed).toBe(true);
						expect(res.body[0].closedReason).toBe('TIMED_OUT');
						expect200AndValidateResponse(done)(err, res);
					});
			});

			describe('when a job that was pending is updated to success, and the job has autoApprove set to true', () => {
				it('updates the job, creates a stratification, and closes the job with closedReason = "APPROVED", then returns the fully updated job', async (done) => {
					await prisma.stratificationJob.create({
						data: stratServiceJobToPrismaStratificationJobCreate({
							jobId: 'mock-task-id',
							jobStatus: 'PENDING',
							areaId,
							stratificationConfig: createMockStratificationConfig(),
							isClosed: false,
							autoApprove: true,
						}),
					});
					const mockStratificationJobSuccess = createMockStratificationJobSuccess();
					(stratificationService.getStratification as jest.Mock).mockResolvedValueOnce({
						data: mockStratificationJobSuccess,
					});

					const response = await request(testApp)
						.get('/stratification-jobs')
						.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
						.expect(200);

					expect(response.body.length).toBe(1);
					expect(response.body[0].externalJobStatus).toBe('SUCCESS');
					expect(response.body[0].jobResult).toEqual(mockStratificationJobSuccess.result);
					expect(response.body[0].isClosed).toBe(true);
					expect(response.body[0].closedReason).toBe('APPROVED');
					const stratification = await prisma.stratification.findFirst({
						where: { originStratificationJob: { id: response.body[0].id } },
					});
					expect(stratification).not.toBeNull();
					done();
				});

				it('if the related field is already stratified it updates the job, but does NOT create a stratification or mark the job as closed, and returns the updated job', async (done) => {
					await postMockStratification({ areaId });
					await prisma.stratificationJob.create({
						data: stratServiceJobToPrismaStratificationJobCreate({
							jobId: 'mock-task-id',
							jobStatus: 'PENDING',
							areaId,
							stratificationConfig: createMockStratificationConfig(),
							isClosed: false,
							autoApprove: true,
						}),
					});
					const mockStratificationJobSuccess = createMockStratificationJobSuccess();
					(stratificationService.getStratification as jest.Mock).mockResolvedValueOnce({
						data: mockStratificationJobSuccess,
					});

					request(testApp)
						.get('/stratification-jobs')
						.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
						.end((err, res) => {
							expect(res.body.length).toBe(1);
							expect(res.body[0].externalJobStatus).toBe('SUCCESS');
							expect(res.body[0].jobResult).toEqual(mockStratificationJobSuccess.result);
							expect200AndValidateResponse(done)(err, res);
						});
				});
			});
		});
	});

	describe('/stratification-jobs/:id/close', () => {
		describe('POST', () => {
			beforeEach(async () => {
				await registerUser({
					id: 'seed-super-admin-1',
					email: 'seedSuperAdmin1@soilstack.io',
					password: 'password',
					groupIds: [],
					isSuperAdmin: true,
				});
			});

			it('returns 404 if the stratification job does not exist', (done) => {
				request(testApp)
					.post('/stratification-jobs/non-existent-id/close')
					.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
					.send({ closedReason: 'APPROVED' })
					.end(expect404AndValidateResponse(done));
			});

			it('returns 409 if the stratification job is already closed', async (done) => {
				const postFieldResponse = await postMockField();
				const areaId = postFieldResponse.body.areas[0].id;
				const { id } = await prisma.stratificationJob.create({
					data: stratServiceJobToPrismaStratificationJobCreate({
						jobId: 'mock-job-id',
						jobStatus: 'SUCCESS',
						areaId,
						stratificationConfig: createMockStratificationConfig(),
						isClosed: true,
						closedReason: 'CANCELLED',
					}),
				});

				request(testApp)
					.post(`/stratification-jobs/${id}/close`)
					.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
					.send({ closedReason: 'APPROVED' })
					.end(expect409AndValidateResponse(done));
			});

			it('returns 409 if the area already has a stratification associated with it', async (done) => {
				const postFieldResponse = await postMockField();
				const areaId = postFieldResponse.body.areas[0].id;
				await postMockStratification({ areaId });
				const { id } = await prisma.stratificationJob.create({
					data: stratServiceJobToPrismaStratificationJobCreate({
						jobId: 'mock-job-id',
						jobStatus: 'SUCCESS',
						areaId,
						stratificationConfig: createMockStratificationConfig(),
					}),
				});

				request(testApp)
					.post(`/stratification-jobs/${id}/close`)
					.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
					.send({ closedReason: 'APPROVED' })
					.end(expect409AndValidateResponse(done));
			});

			it('returns 200, updates the stratification job and does NOT create a stratification when closedReason is "CANCELLED" or "REJECTED"', async (done) => {
				const postFieldResponse = await postMockField();
				const areaId = postFieldResponse.body.areas[0].id;
				const { id } = await prisma.stratificationJob.create({
					data: stratServiceJobToPrismaStratificationJobCreate({
						jobId: 'mock-job-id',
						jobStatus: 'FAILURE',
						areaId,
						stratificationConfig: createMockStratificationConfig(),
						isClosed: false,
					}),
				});
				const { id: id2 } = await prisma.stratificationJob.create({
					data: stratServiceJobToPrismaStratificationJobCreate({
						jobId: 'mock-job-id-2',
						jobStatus: 'FAILURE',
						areaId,
						stratificationConfig: createMockStratificationConfig(),
						isClosed: false,
					}),
				});

				await request(testApp)
					.post(`/stratification-jobs/${id}/close`)
					.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
					.send({ closedReason: 'CANCELLED' })
					.expect(200);
				await request(testApp)
					.post(`/stratification-jobs/${id2}/close`)
					.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
					.send({ closedReason: 'REJECTED' })
					.expect(200);

				const updatedJob1 = await prisma.stratificationJob.findUnique({ where: { id } });
				expect(updatedJob1?.isClosed).toBe(true);
				expect(updatedJob1?.closedReason).toBe('CANCELLED');
				const associatedStratification1 = await prisma.stratification.findFirst({
					where: {
						originStratificationJob: { id },
					},
				});
				expect(associatedStratification1).toBeNull();
				const updatedJob2 = await prisma.stratificationJob.findUnique({ where: { id: id2 } });
				expect(updatedJob2?.isClosed).toBe(true);
				expect(updatedJob2?.closedReason).toBe('REJECTED');
				const associatedStratification2 = await prisma.stratification.findFirst({
					where: {
						originStratificationJob: { id: id2 },
					},
				});
				expect(associatedStratification2).toBeNull();

				done();
			});

			it('returns 200, updates the stratification job and creates a stratification when closedReason is "APPROVED"', async (done) => {
				const postFieldResponse = await postMockField();
				const areaId = postFieldResponse.body.areas[0].id;
				const { id } = await prisma.stratificationJob.create({
					data: {
						...stratServiceJobToPrismaStratificationJobCreate({
							jobId: 'mock-job-id',
							jobStatus: 'SUCCESS',
							areaId,
							stratificationConfig: createMockStratificationConfig(),
							isClosed: false,
						}),
						jobResult: createMockStratificationServiceStratification() as Prisma.JsonObject,
					},
				});

				await request(testApp)
					.post(`/stratification-jobs/${id}/close`)
					.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
					.send({ closedReason: 'APPROVED' })
					.expect(200);

				const updatedJob = await prisma.stratificationJob.findUnique({ where: { id } });
				expect(updatedJob?.isClosed).toBe(true);
				expect(updatedJob?.closedReason).toBe('APPROVED');
				const associatedStratification = await prisma.stratification.findFirst({
					where: {
						originStratificationJob: { id },
					},
				});
				expect(associatedStratification).toBeDefined();

				done();
			});
		});
	});
});
