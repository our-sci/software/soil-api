import request from 'supertest';
import { STRATIFICATION_SERVICE_KEY } from '../constants';
import { prismaMock } from '../singleton';
import {
	createMockPrismaStratification,
	createMockPrismaStratificationJob,
	createMockStratificationJobSuccess,
	createMockStratificationServiceStratification,
	expect401AndValidateResponse,
	getDefaultSuperAdminBearerToken,
	getDefaultUserBearerToken,
	testApp,
} from '../test-utils';

describe('Stratification Job Endpoints', () => {
	describe('/stratification-jobs/resolve', () => {
		describe('POST', () => {
			it('does not require an Authorization header', (done) => {
				request(testApp)
					.post(`/stratification-jobs/resolve?token=${encodeURIComponent(STRATIFICATION_SERVICE_KEY)}`)
					.send(createMockStratificationJobSuccess())
					.end((err, res) => {
						expect(res.status).not.toBe(401);
						done();
					});
			});

			it('returns 401 when the token query param is not valid', (done) => {
				request(testApp)
					.post(`/stratification-jobs/resolve?token=invalid-token`)
					.send(createMockStratificationJobSuccess())
					.expect(401, done);
			});

			it('returns 500 when prisma.stratificationJob.findFirst throws an unexpected error', (done) => {
				prismaMock.stratificationJob.findFirst.mockRejectedValueOnce(new Error('unexpected error'));

				request(testApp)
					.post(`/stratification-jobs/resolve?token=${encodeURIComponent(STRATIFICATION_SERVICE_KEY)}`)
					.send(createMockStratificationJobSuccess())
					.expect(500, done);
			});

			it('returns 500 when prisma.stratificationJob.update throws an unexpected error', (done) => {
				prismaMock.stratificationJob.findFirst.mockResolvedValueOnce(
					createMockPrismaStratificationJob({ externalJobStatus: 'PENDING' })
				);
				prismaMock.stratificationJob.update.mockRejectedValueOnce(new Error('unexpected error'));

				request(testApp)
					.post(`/stratification-jobs/resolve?token=${encodeURIComponent(STRATIFICATION_SERVICE_KEY)}`)
					.send(createMockStratificationJobSuccess())
					.expect(500, done);
			});
		});
	});

	describe('/stratification-jobs', () => {
		describe('GET', () => {
			it('returns 401 when the user is not a super admin', (done) => {
				request(testApp)
					.get('/stratification-jobs')
					.set('Authorization', getDefaultUserBearerToken())
					.end(expect401AndValidateResponse(done));
			});

			it('returns 500 when prisma.stratificationJob.findMany throws an unexpected error', (done) => {
				prismaMock.stratificationJob.findMany.mockRejectedValueOnce(new Error('unexpected error'));

				request(testApp)
					.get('/stratification-jobs')
					.set('Authorization', getDefaultSuperAdminBearerToken())
					.expect(500, done);
			});

			it('returns 500 when prisma.$transaction throws an unexpected error', (done) => {
				prismaMock.stratificationJob.findMany.mockResolvedValueOnce([]);
				prismaMock.$transaction.mockRejectedValueOnce(new Error('unexpected error'));

				request(testApp)
					.get('/stratification-jobs')
					.set('Authorization', getDefaultSuperAdminBearerToken())
					.expect(500, done);
			});
		});
	});

	describe('/stratification-jobs/:id/close', () => {
		describe('POST', () => {
			it('returns 401 when the user is not a super admin', (done) => {
				request(testApp)
					.post('/stratification-jobs/id/close')
					.set('Authorization', getDefaultUserBearerToken())
					.send({ closedReason: 'APPROVED' })
					.end(expect401AndValidateResponse(done));
			});

			it('returns 500 when prisma.stratificationJob.findUnique throws an unexpected error', (done) => {
				prismaMock.stratificationJob.findUnique.mockRejectedValueOnce(new Error('unexpected error'));

				request(testApp)
					.post('/stratification-jobs/id/close')
					.set('Authorization', getDefaultSuperAdminBearerToken())
					.send({ closedReason: 'APPROVED' })
					.expect(500, done);
			});

			it('returns 500 when prisma.stratification.create throws an unexpected error', (done) => {
				(prismaMock.stratificationJob.findUnique as any).mockResolvedValueOnce({
					...createMockPrismaStratificationJob({ jobResult: createMockStratificationServiceStratification() as any }),
					area: {
						_count: { stratifications: 0 },
						field: { name: 'name', id: 'id' },
						groupId: 'groupId',
					},
				});
				prismaMock.stratification.create.mockRejectedValueOnce(new Error('unexpected error'));

				request(testApp)
					.post('/stratification-jobs/id/close')
					.set('Authorization', getDefaultSuperAdminBearerToken())
					.send({ closedReason: 'APPROVED' })
					.expect(500, done);
			});

			it('returns 500 when prisma.stratificationJob.update throws an unexpected error', (done) => {
				(prismaMock.stratificationJob.findUnique as any).mockResolvedValueOnce({
					...createMockPrismaStratificationJob({ jobResult: createMockStratificationServiceStratification() as any }),
					area: {
						_count: { stratifications: 0 },
						field: { name: 'name', id: 'id' },
						groupId: 'groupId',
					},
				});
				prismaMock.stratification.create.mockResolvedValueOnce(createMockPrismaStratification());
				prismaMock.stratificationJob.update.mockRejectedValueOnce(new Error('unexpected error'));

				request(testApp)
					.post('/stratification-jobs/id/close')
					.set('Authorization', getDefaultSuperAdminBearerToken())
					.send({ closedReason: 'APPROVED' })
					.expect(500, done);
			});
		});
	});
});
