import { Prisma, StratificationJob as PrismaStratificationJob } from '@prisma/client';
import { AxiosResponse } from 'axios';
import { NextFunction, Request, Response, Router } from 'express';
import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';
import { STRATIFICATION_JOB_TIMEOUT_MS, STRATIFICATION_SERVICE_KEY } from '../constants';
import prisma from '../prismaClient';
import * as stratificationService from '../services/stratification';
import {
	prismaStratificationJobToStratificationJob,
	stratificationServiceJobResultToPrismaCreateInput,
} from '../transforms';
import { paths } from '../types/generated/openapi-types';
import { components as stratServiceComponents } from '../types/generated/stratification-service-types';
import {
	dateDiffInMs,
	isFulfilled,
	isRejected,
	isStratificationJobFailure,
	isStratificationJobPending,
	isStratificationJobSuccess,
} from '../utils';

type StratificationJobSuccess = stratServiceComponents['schemas']['StratificationJobSuccess'];
type StratificationJobFailure = stratServiceComponents['schemas']['StratificationJobFailure'];
type StratificationJobPending = stratServiceComponents['schemas']['StratificationJobPending'];

interface StratificationJobWithJobId extends PrismaStratificationJob {
	jobId: string;
}

const router = Router();

router.post('/resolve', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const { token } = req.query as paths['/stratification-jobs/resolve']['post']['parameters']['query'];
		const body = req.body as paths['/stratification-jobs/resolve']['post']['requestBody']['content']['application/json'];
		const jobId = body.taskId;

		if (token !== STRATIFICATION_SERVICE_KEY) {
			return res.status(401).send();
		}

		const stratificationJob = await prisma.stratificationJob.findFirst({
			where: { jobId },
			include: {
				area: {
					include: {
						_count: {
							select: { stratifications: true },
						},
						field: {
							select: { name: true, id: true },
						},
					},
				},
			},
		});

		if (!stratificationJob) {
			return res.status(404).send();
		}

		if (stratificationJob.externalJobStatus !== 'PENDING') {
			return res.status(409).send();
		}

		if (isStratificationJobSuccess(body)) {
			await prisma.stratificationJob.update({
				where: { jobId },
				data: {
					externalJobStatus: 'SUCCESS',
					jobResult: body.result as Prisma.JsonObject,
				},
			});

			const isAutoApprovable =
				stratificationJob.autoApprove &&
				!stratificationJob.isClosed &&
				stratificationJob.area._count.stratifications === 0;
			if (isAutoApprovable) {
				const jobResult = body.result as StratificationJobSuccess['result'];

				try {
					await prisma.$transaction([
						prisma.stratification.create({
							data: {
								...stratificationServiceJobResultToPrismaCreateInput({
									jobResult,
									areaId: stratificationJob.areaId,
									fieldName: stratificationJob.area.field.name,
									fieldId: stratificationJob.area.field.id,
									groupId: stratificationJob.area.groupId,
								}),
								originStratificationJob: {
									connect: { id: stratificationJob.id },
								},
							},
						}),
						prisma.stratificationJob.update({
							where: { id: stratificationJob.id },
							data: {
								isClosed: true,
								closedReason: 'APPROVED',
							},
						}),
					]);
				} catch {
					// It's okay if this transaction fails. As long as the job updates to resolved, we return 200.
					// If we fail to auto approve the job, it can be manually approved by a user later.
				}
			}
		} else if (isStratificationJobFailure(body)) {
			await prisma.stratificationJob.update({
				where: { jobId },
				data: {
					externalJobStatus: 'FAILURE',
					error: body.result as string,
				},
			});
		}

		return res.status(200).send();
	} catch (error) {
		next(error);
	}
});

router.get('/', async (req: Request, res: Response, next: NextFunction) => {
	try {
		if (!res.locals.user.isSuperAdmin) {
			return res.status(401).send();
		}

		const where =
			res.locals.authorizationScheme === 'bearer'
				? {
						area: {
							group: {
								apiOnly: false,
							},
						},
				  }
				: {};

		const stratificationJobs = await prisma.stratificationJob.findMany({ where });

		const jobsToUpdate = stratificationJobs.filter(
			(job): job is StratificationJobWithJobId =>
				typeof job.jobId === 'string' && job.externalJobStatus === 'PENDING' && !job.isClosed
		);

		const updates = await Promise.allSettled<{
			currentRecord: PrismaStratificationJob;
			axiosResult: Awaited<ReturnType<typeof stratificationService.getStratification>>;
		}>(
			jobsToUpdate.map(
				(job) =>
					new Promise(async (resolve, reject) => {
						try {
							resolve({
								currentRecord: job,
								axiosResult: await stratificationService.getStratification(job.jobId),
							});
						} catch (error) {
							reject({
								currentRecord: job,
								axiosResult: error,
							});
						}
					})
			)
		);
		const successes = updates.filter(isFulfilled).map((update) => update.value);
		const failures = updates.filter(isRejected).map((update) => update.reason);

		const updatesWithSuccessState = successes.filter(({ axiosResult }) =>
			isStratificationJobSuccess(axiosResult.data)
		) as { axiosResult: AxiosResponse<StratificationJobSuccess>; currentRecord: PrismaStratificationJob }[];
		const updatesWithFailureState = successes.filter(({ axiosResult }) =>
			isStratificationJobFailure(axiosResult.data)
		) as { axiosResult: AxiosResponse<StratificationJobFailure>; currentRecord: PrismaStratificationJob }[];
		const updatesWithPendingState = successes.filter(({ axiosResult }) =>
			isStratificationJobPending(axiosResult.data)
		) as { axiosResult: AxiosResponse<StratificationJobPending>; currentRecord: PrismaStratificationJob }[];

		const updateToTimedOut = [...updatesWithPendingState, ...failures]
			.filter(({ currentRecord }) => dateDiffInMs(currentRecord.createdAt, new Date()) > STRATIFICATION_JOB_TIMEOUT_MS)
			.map(({ currentRecord }) =>
				prisma.stratificationJob.update({
					where: { id: currentRecord.id },
					data: {
						isClosed: true,
						closedReason: 'TIMED_OUT',
					},
				})
			);
		const updateToSuccess = updatesWithSuccessState.map(({ currentRecord, axiosResult }) =>
			prisma.stratificationJob.update({
				where: { id: currentRecord.id },
				data: {
					externalJobStatus: 'SUCCESS',
					jobResult: axiosResult.data.result as Prisma.JsonObject,
				},
			})
		);
		const updateToFailure = updatesWithFailureState.map(({ currentRecord, axiosResult }) =>
			prisma.stratificationJob.update({
				where: { id: currentRecord.id },
				data: {
					externalJobStatus: 'FAILURE',
					error: axiosResult.data.result as string,
				},
			})
		);

		const updatedJobs = await prisma.$transaction([...updateToTimedOut, ...updateToSuccess, ...updateToFailure]);
		const updateToApproveAndCreateStratification = updatesWithSuccessState.map(async ({ currentRecord }) => {
			const stratificationJob = await prisma.stratificationJob.findUnique({
				where: { id: currentRecord.id },
				include: {
					area: {
						include: {
							_count: {
								select: { stratifications: true },
							},
							field: {
								select: { name: true, id: true },
							},
						},
					},
				},
			});
			if (!stratificationJob) {
				return;
			}

			const isAutoApprovable =
				stratificationJob.autoApprove &&
				!stratificationJob.isClosed &&
				stratificationJob.area._count.stratifications === 0;
			if (!isAutoApprovable) {
				return;
			}

			return prisma.$transaction([
				prisma.stratification.create({
					data: {
						...stratificationServiceJobResultToPrismaCreateInput({
							jobResult: stratificationJob.jobResult as StratificationJobSuccess['result'],
							areaId: stratificationJob.areaId,
							fieldName: stratificationJob.area.field.name,
							fieldId: stratificationJob.area.field.id,
							groupId: stratificationJob.area.groupId,
						}),
						originStratificationJob: {
							connect: { id: stratificationJob.id },
						},
					},
				}),
				prisma.stratificationJob.update({
					where: { id: stratificationJob.id },
					data: {
						isClosed: true,
						closedReason: 'APPROVED',
					},
				}),
			]);
		});

		// We use Promise.allSettled here because it's okay if the auto approve transaction fails.
		// It does not affect the response from this endpoint.
		// If we fail to auto approve the job, it can be manually approved by a user later.
		await Promise.allSettled(updateToApproveAndCreateStratification);

		let responseStratificationJobs;
		if (updatedJobs.length > 0) {
			const updatedStratificationJobs = await prisma.stratificationJob.findMany({ where });
			responseStratificationJobs = updatedStratificationJobs.map(prismaStratificationJobToStratificationJob);
		} else {
			responseStratificationJobs = stratificationJobs.map(prismaStratificationJobToStratificationJob);
		}

		return res.status(200).send(responseStratificationJobs);
	} catch (error) {
		next(error);
	}
});

router.post('/:id/close', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const { id } = req.params as paths['/stratification-jobs/{id}/close']['post']['parameters']['path'];
		const {
			closedReason,
		} = req.body as paths['/stratification-jobs/{id}/close']['post']['requestBody']['content']['application/json'];

		if (!res.locals.user.isSuperAdmin) {
			return res.status(401).send();
		}

		const stratificationJob = await prisma.stratificationJob.findUnique({
			where: { id },
			include: {
				area: {
					include: {
						_count: {
							select: { stratifications: true },
						},
						field: {
							select: { name: true, id: true },
						},
					},
				},
			},
		});

		if (!stratificationJob) {
			return res.status(404).send();
		}

		if (stratificationJob.isClosed) {
			return res.status(409).send({ message: 'The stratification job is already closed.' });
		}

		if (closedReason === 'APPROVED') {
			if (stratificationJob.area._count.stratifications > 0) {
				return res
					.status(409)
					.send({ message: 'The area associated with the stratification job already has a stratification.' });
			}

			const jobResult = stratificationJob.jobResult as StratificationJobSuccess['result'];

			await prisma.stratification.create({
				data: {
					...stratificationServiceJobResultToPrismaCreateInput({
						jobResult,
						areaId: stratificationJob.areaId,
						fieldName: stratificationJob.area.field.name,
						fieldId: stratificationJob.area.field.id,
						groupId: stratificationJob.area.groupId,
					}),
					originStratificationJob: {
						connect: { id: stratificationJob.id },
					},
				},
			});
		}

		await prisma.stratificationJob.update({
			where: { id },
			data: {
				isClosed: true,
				closedReason,
			},
		});

		res.status(200).send();
	} catch (error) {
		next(error);
	}
});

const openApiPaths: OpenAPIV3.PathsObject = {
	'/stratification-jobs': {
		get: {
			'x-internal': true,
			tags: ['Stratification Jobs'],
			summary: 'Gets all stratification jobs.',
			responses: {
				200: {
					description: 'An array of stratification jobs.',
					content: {
						'application/json': {
							schema: {
								type: 'array',
								items: { $ref: '#/components/schemas/StratificationJob' },
							},
						},
					},
				},
			},
		},
	} as OpenAPIV3.OperationObject,
	'/stratification-jobs/resolve': {
		post: {
			'x-internal': true,
			tags: ['Stratification Jobs'],
			summary:
				'Resolves a stratification job to a completed status. This is the callback url the stratification service will call when a job succeeds or fails.',
			security: [],
			parameters: [
				{
					name: 'token',
					in: 'query',
					description: 'A token to authenticate the request.',
					required: true,
					schema: {
						type: 'string',
					},
				},
			],
			requestBody: {
				description: "The task id, the task's state and the task's result (or error).",
				content: {
					'application/json': {
						schema: {
							oneOf: [
								{ $ref: '#/components/schemas/StratificationServiceStratificationJobSuccess' },
								{ $ref: '#/components/schemas/StratificationServiceStratificationJobFailure' },
							],
						},
					},
				},
			},
			responses: {
				200: {
					description: 'The stratification job was successfully resolved.',
				},
				404: {
					description: 'The stratification job was not found.',
				},
				409: {
					description: 'The stratification job has already been resolved.',
				},
			},
		},
	} as OpenAPIV3.OperationObject,
	'/stratification-jobs/{id}/close': {
		post: {
			'x-internal': true,
			tags: ['Stratification Jobs'],
			summary:
				'Closes a stratification job. If the job is closed with a reason of APPROVED, the associated stratification will be created.',
			parameters: [
				{
					name: 'id',
					in: 'path',
					description: 'The id of the stratification job to close.',
					required: true,
					schema: {
						type: 'string',
					},
				},
			],
			requestBody: {
				description: 'The reason for closing the stratification job.',
				content: {
					'application/json': {
						schema: {
							type: 'object',
							required: ['closedReason'],
							additionalProperties: false,
							properties: {
								closedReason: {
									type: 'string',
									enum: ['APPROVED', 'REJECTED', 'CANCELLED'],
								},
							},
						},
					},
				},
			},
			responses: {
				200: {
					description: 'The stratification job was successfully closed.',
				},
				404: {
					description: 'The stratification job was not found.',
				},
				409: {
					description:
						'The stratification job is already closed or the area associated with the stratification job already has a stratification.',
					content: {
						'application/json': {
							schema: { $ref: '#/components/schemas/Error' },
						},
					},
				},
			},
		},
	} as OpenAPIV3.OperationObject,
};

export { openApiPaths, router };
