import request from 'supertest';
import { ResourceTypes } from '../constants';
import prisma from '../prismaClient';
import {
	expect200AndValidateResponse,
	expect401AndValidateResponse,
	expect404AndValidateResponse,
	expect409AndValidateResponse,
	getDefaultUserBearerToken,
	getUserBearerToken,
	postMockField,
	postMockSamplingCollection,
	postMockStratification,
	postMockStratificationAndDependencies,
	registerUser,
	testApp,
	testGetResourceByIdEndpoint,
} from '../test-utils';

describe('Stratification Endpoints', () => {
	describe('GET', () => {
		describe('/stratifications', () => {
			it('returns 200 with stratifications the user has access to on success', async (done) => {
				// add a stratification to a group the user belongs to
				const postFieldResponse = await postMockField();
				const areaId = postFieldResponse.body.areas[0].id;
				const fieldId = postFieldResponse.body.fields[0].id;
				await postMockStratification({ areaId });
				// add a stratification to a group the user does NOT belong to
				await postMockStratificationAndDependencies({ groupId: 'seed-group-2' });

				request(testApp)
					.get('/stratifications')
					.set('Authorization', getDefaultUserBearerToken())
					.end((err, res) => {
						expect(res.body).toHaveLength(1);
						expect(res.body[0]).toHaveProperty('featureOfInterest', fieldId);
						expect200AndValidateResponse(done)(err, res);
					});
			});

			it('returns 200 on success when no stratifications are present', (done) => {
				request(testApp)
					.get('/stratifications')
					.set('Authorization', getDefaultUserBearerToken())
					.end((err, res) => {
						expect(res.body).toHaveLength(0);
						expect200AndValidateResponse(done)(err, res);
					});
			});

			describe("when passing 'submittedAfter' query parameter", () => {
				it("only returns stratifications created after the 'submittedAfter' time, ordered most recent first", async (done) => {
					const {
						body: {
							stratifications: [stratificationOne],
						},
					} = await postMockStratificationAndDependencies();
					const {
						body: {
							stratifications: [stratificationTwo],
						},
					} = await postMockStratificationAndDependencies();
					const {
						body: {
							stratifications: [stratificationThree],
						},
					} = await postMockStratificationAndDependencies();

					const submittedAfter = encodeURIComponent(stratificationOne.meta.submittedAt);

					request(testApp)
						.get(`/stratifications?submittedAfter=${submittedAfter}`)
						.set('Authorization', getDefaultUserBearerToken())
						.end((err, res) => {
							expect(res.body).toHaveLength(2);
							expect(res.body[0]).toHaveProperty('id', stratificationThree.id);
							expect(res.body[1]).toHaveProperty('id', stratificationTwo.id);
							expect200AndValidateResponse(done)(err, res);
						});
				});
			});
		});

		testGetResourceByIdEndpoint({
			endpointPath: '/stratifications',
			resourceName: 'stratification',
			resourceType: ResourceTypes.Stratification,
		});
	});

	describe('DELETE', () => {
		describe('/stratifications/:id', () => {
			const test404OnDoesNotExistWith = (userAuthGetter: () => string) => (done: jest.DoneCallback) => {
				request(testApp)
					.delete('/stratifications/non-existent-id')
					.set('Authorization', userAuthGetter())
					.end(expect404AndValidateResponse(done));
			};

			const test204OnSuccessWith = (userAuthGetter: () => string) => async () => {
				const {
					body: {
						stratifications: [{ id: stratificationId }],
						locationCollections: [{ id: locationCollectionId }],
						locations: [{ id: locationId }],
					},
				} = await postMockStratificationAndDependencies({ isDemo: true });

				await request(testApp)
					.delete(`/stratifications/${stratificationId}`)
					.set('Authorization', userAuthGetter())
					.expect(204);

				const [stratification, locationCollection, location] = await Promise.all([
					prisma.stratification.findUnique({ where: { id: stratificationId } }),
					prisma.locationCollection.findUnique({ where: { id: locationCollectionId } }),
					prisma.location.findUnique({ where: { id: locationId } }),
				]);

				expect(stratification).toBeNull();
				expect(locationCollection).toBeNull();
				expect(location).toBeNull();
			};

			const test409OnConflictWith = (userAuthGetter: () => string) => async (done: jest.DoneCallback) => {
				const {
					body: {
						stratifications: [{ id: stratificationId }],
						locationCollections: [{ id: locationCollectionId }],
						locations: [{ id: locationId }],
					},
				} = await postMockStratificationAndDependencies({ isDemo: true });
				await postMockSamplingCollection(locationCollectionId, locationId);

				request(testApp)
					.delete(`/stratifications/${stratificationId}`)
					.set('Authorization', userAuthGetter())
					.end(expect409AndValidateResponse(done));
			};

			const test204OnForceSuccessWith = (userAuthGetter: () => string) => async () => {
				const {
					body: {
						stratifications: [{ id: stratificationId }],
						locationCollections: [{ id: locationCollectionId }],
						locations: [{ id: locationId }],
					},
				} = await postMockStratificationAndDependencies({ isDemo: true });
				const {
					body: {
						samplingCollections: [{ id: samplingCollectionId }],
						samplings: [{ id: samplingId }],
						samples: [{ id: sampleId }],
					},
				} = await postMockSamplingCollection(locationCollectionId, locationId);

				await request(testApp)
					.delete(`/stratifications/${stratificationId}?force=true`)
					.set('Authorization', userAuthGetter())
					.expect(204);

				const [stratification, locationCollection, location, samplingCollection, sampling, sample] = await Promise.all([
					prisma.stratification.findUnique({ where: { id: stratificationId } }),
					prisma.locationCollection.findUnique({ where: { id: locationCollectionId } }),
					prisma.location.findUnique({ where: { id: locationId } }),
					prisma.samplingCollection.findUnique({ where: { id: samplingCollectionId } }),
					prisma.sampling.findUnique({ where: { id: samplingId } }),
					prisma.sample.findUnique({ where: { id: sampleId } }),
				]);

				expect(stratification).toBeNull();
				expect(locationCollection).toBeNull();
				expect(location).toBeNull();
				expect(samplingCollection).toBeNull();
				expect(sampling).toBeNull();
				expect(sample).toBeNull();
			};

			describe('when the requesting user is a super admin', () => {
				beforeEach(async () => {
					await registerUser({
						id: 'seed-super-admin-1',
						email: 'seedSuperAdmin1@soilstack.io',
						password: 'password',
						groupIds: [],
						isSuperAdmin: true,
					});
				});

				it(
					'returns 404 when the stratification does not exist',
					test404OnDoesNotExistWith(() => getUserBearerToken('seedSuperAdmin1@soilstack.io'))
				);

				describe('and the stratification is NOT a demo stratification', () => {
					it('returns 204 on success and deletes the stratification, its location collection and locations when the stratification exists and has no dependent resources', async (done) => {
						const {
							body: {
								stratifications: [{ id: stratificationId }],
								locationCollections: [{ id: locationCollectionId }],
								locations: [{ id: locationId }],
							},
						} = await postMockStratificationAndDependencies();

						await request(testApp)
							.delete(`/stratifications/${stratificationId}`)
							.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
							.expect(204);

						const [locationCollection, location] = await Promise.all([
							prisma.locationCollection.findUnique({ where: { id: locationCollectionId } }),
							prisma.location.findUnique({ where: { id: locationId } }),
						]);

						expect(locationCollection).toBeNull();
						expect(location).toBeNull();

						done();
					});

					it('returns 409 when a sampling collection depends on the location collections belonging to the stratification', async (done) => {
						const {
							body: {
								stratifications: [{ id: stratificationId }],
								locationCollections: [{ id: locationCollectionId }],
								locations: [{ id: locationId }],
							},
						} = await postMockStratificationAndDependencies();
						await postMockSamplingCollection(locationCollectionId, locationId);

						request(testApp)
							.delete(`/stratifications/${stratificationId}`)
							.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
							.end(expect409AndValidateResponse(done));
					});

					it('returns 409 when a sampling collection depends on the location collections belonging to the stratification, even when force=true', async (done) => {
						const {
							body: {
								stratifications: [{ id: stratificationId }],
								locationCollections: [{ id: locationCollectionId }],
								locations: [{ id: locationId }],
							},
						} = await postMockStratificationAndDependencies();
						await postMockSamplingCollection(locationCollectionId, locationId);

						request(testApp)
							.delete(`/stratifications/${stratificationId}?force=true`)
							.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
							.end(expect409AndValidateResponse(done));
					});
				});

				describe('and the stratification is a demo stratification', () => {
					it(
						'returns 204 on success and deletes the stratification, its location collection and locations when the stratification exists and has no dependent resources',
						test204OnSuccessWith(() => getUserBearerToken('seedSuperAdmin1@soilstack.io'))
					);

					it(
						'returns 409 when a sampling collection depends on the location collections belonging to the stratification',
						test409OnConflictWith(() => getUserBearerToken('seedSuperAdmin1@soilstack.io'))
					);

					it(
						'when force=true is passed, returns 204 and deletes the stratification and related sampling collections, samplings, and samples, even when a sampling collection depends on the location collections belonging to the stratification',
						test204OnForceSuccessWith(() => getUserBearerToken('seedSuperAdmin1@soilstack.io'))
					);
				});
			});

			describe('when the requesting user is not a super admin', () => {
				it('returns 404 if the stratification does not exist', test404OnDoesNotExistWith(getDefaultUserBearerToken));

				describe('and the stratification is NOT a demo stratification', () => {
					it('returns 401', async (done) => {
						const {
							body: {
								stratifications: [{ id: stratificationId }],
							},
						} = await postMockStratificationAndDependencies({ isDemo: false });

						request(testApp)
							.delete(`/stratifications/${stratificationId}`)
							.set('Authorization', getDefaultUserBearerToken())
							.end(expect401AndValidateResponse(done));
					});
				});

				describe('and the stratification is a demo stratification', () => {
					it('returns 404 if the requesting user does not have access to the stratification', async (done) => {
						const {
							body: {
								stratifications: [{ id: stratificationId }],
							},
						} = await postMockStratificationAndDependencies({ groupId: 'seed-group-2', isDemo: true });

						request(testApp)
							.delete(`/stratifications/${stratificationId}`)
							.set('Authorization', getDefaultUserBearerToken())
							.end(expect404AndValidateResponse(done));
					});

					it(
						'returns 204 on success and deletes the stratification, its location collection and locations when the stratification exists and has no dependent resources',
						test204OnSuccessWith(getDefaultUserBearerToken)
					);

					it(
						'returns 409 when a sampling collection depends on the location collections belonging to the stratification',
						test409OnConflictWith(getDefaultUserBearerToken)
					);

					it(
						'when force=true is passed, returns 204 and deletes the stratification and related sampling collections, samplings, and samples, even when a sampling collection depends on the location collections belonging to the stratification',
						test204OnForceSuccessWith(getDefaultUserBearerToken)
					);
				});
			});
		});
	});
});
