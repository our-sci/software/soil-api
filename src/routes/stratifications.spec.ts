import request from 'supertest';
import { prismaMock } from '../singleton';
import {
	createMockPrismaStratificationWithSamplingCollectionIds,
	expect400AndValidateResponse,
	getDefaultSuperAdminBearerToken,
	getDefaultUserBearerToken,
	testApp,
} from '../test-utils';

describe('Stratification Endpoints', () => {
	describe('GET', () => {
		describe('/stratifications', () => {
			it('returns 500 when prisma client throws an unexpected error', (done) => {
				prismaMock.stratification.findMany.mockRejectedValue(new Error('unexpected error'));

				request(testApp).get('/stratifications').set('Authorization', getDefaultUserBearerToken()).expect(500, done);
			});

			const invalidSubmittedAfterValues = [
				{ description: 'empty string', submittedAfter: '' },
				{ description: 'invalid date', submittedAfter: '2020-01-01T00:00:00.000' },
				{ description: 'ms since epoch', submittedAfter: '1577836800000' },
			];

			invalidSubmittedAfterValues.forEach(({ description, submittedAfter }) => {
				it(`returns 400 when ${description} is passed in the \'submittedAfter\' query paramter`, (done) => {
					request(testApp)
						.get('/stratifications')
						.set('Authorization', getDefaultUserBearerToken())
						.query({ submittedAfter })
						.send()
						.end(expect400AndValidateResponse(done));
				});
			});
		});

		describe('/stratifications/:id', () => {
			it('returns 500 when prisma.stratification.findFirst throws an unexpected error', (done) => {
				prismaMock.stratification.findFirst.mockRejectedValueOnce(new Error('unexpected error'));

				request(testApp)
					.get('/stratifications/non-existent-id')
					.set('Authorization', getDefaultUserBearerToken())
					.expect(500, done);
			});
		});
	});

	describe('DELETE', () => {
		describe('/stratifications/:id', () => {
			it('returns 500 when prisma.stratification.findUnique throws an unexpected error', (done) => {
				prismaMock.stratification.findUnique.mockRejectedValueOnce('error');

				request(testApp)
					.delete('/stratifications/123456789012345678901234')
					.set('Authorization', getDefaultSuperAdminBearerToken())
					.send()
					.expect(500, done);
			});

			it('returns 500 when prisma.stratification.delete throws an unexpected error', (done) => {
				prismaMock.stratification.findUnique.mockResolvedValueOnce(
					createMockPrismaStratificationWithSamplingCollectionIds()
				);
				prismaMock.stratification.delete.mockRejectedValueOnce('error');

				request(testApp)
					.delete('/stratifications/123456789012345678901234')
					.set('Authorization', getDefaultSuperAdminBearerToken())
					.send()
					.expect(500, done);
			});
		});
	});
});
