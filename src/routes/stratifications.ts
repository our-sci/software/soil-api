import { Membership as PrismaMembership, Prisma } from '@prisma/client';
import { NextFunction, Request, Response, Router } from 'express';
import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';
import { ResourceTypes } from '../constants';
import prisma from '../prismaClient';
import { prismaStratificationToStratification } from '../transforms';
import createGetResourceByIdController from './common/createGetResourceByIdController';

const router = Router();

router.get('/', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const submittedAfter = req.query.submittedAfter as string;
		const userGroupIds = res.locals.user.memberships.map((m: PrismaMembership) => m.groupId);
		const defaultWhere = { groupId: { in: userGroupIds } };

		const stratifications = await prisma.stratification.findMany({
			where: submittedAfter ? { ...defaultWhere, createdAt: { gt: new Date(submittedAfter) } } : defaultWhere,
			orderBy: { createdAt: 'desc' },
			include: {
				algorithm: true,
				area: { select: { fieldId: true } },
			},
		});

		res.status(200).json(stratifications.map(prismaStratificationToStratification));
	} catch (error) {
		next(error);
	}
});

router.get('/:id', createGetResourceByIdController(ResourceTypes.Stratification));

router.delete('/:id', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const id = req.params.id;
		const stratification = await prisma.stratification.findUnique({
			where: { id },
			include: {
				locationCollection: {
					select: {
						id: true,
						samplingCollections: {
							select: {
								id: true,
							},
						},
					},
				},
			},
		});

		const isAuthorized = res.locals.user.memberships.some(
			(m: PrismaMembership) => m.groupId === stratification?.groupId
		);
		const isSuperAdmin = res.locals.user.isSuperAdmin;

		if (!stratification || (!isSuperAdmin && !isAuthorized)) {
			return res.status(404).send();
		}

		if (!isSuperAdmin && !stratification.isDemo) {
			return res.status(401).send();
		}

		const stratificationHasRelatedSamplingCollection = stratification.locationCollection?.samplingCollections?.length;
		if (stratificationHasRelatedSamplingCollection) {
			if (!stratification.isDemo) {
				return res.status(409).send({
					message: 'Cannot delete a stratification that has a sampling collection depending on it.',
				});
			}

			if (!req.query.force) {
				return res.status(409).send({
					message:
						'Cannot delete a stratification that has a sampling collection depending on it. Because this is a demo stratification, you may include the query parameter force=true to bypass this and delete the attached sampling collections, samplings, and samples.',
				});
			}
		}

		await prisma.stratification.delete({ where: { id } });

		res.status(204).send();
	} catch (error) {
		if (error instanceof Prisma.PrismaClientKnownRequestError) {
			// P2025 is the Prisma Client error code for RecordNotFound
			if (error.code === 'P2025') {
				return res.status(404).send();
			}
		}

		next(error);
	}
});

const openApiPaths: OpenAPIV3.PathsObject = {
	'/stratifications': {
		get: {
			tags: ['Stratifications'],
			summary: 'Get all stratifications',
			parameters: [
				{
					name: 'submittedAfter',
					in: 'query',
					description: 'Only return stratifications submitted after this ISO8601 timestamp.',
					required: false,
					schema: {
						type: 'string',
						format: 'date-time',
					},
				},
			],
			responses: {
				'200': {
					description: 'An array of the stratifications you have access to.',
					content: {
						'application/json': {
							schema: {
								type: 'array',
								items: { $ref: '#/components/schemas/ResponseStratification' },
							},
						},
					},
				},
				401: { $ref: '#/components/responses/401' },
			},
		},
	},
	'/stratifications/{stratificationId}': {
		get: {
			tags: ['Stratifications'],
			summary: 'Get a stratification by its id.',
			parameters: [
				{
					name: 'stratificationId',
					in: 'path',
					description: 'The id of the stratification to get.',
					required: true,
					schema: { type: 'string' },
				},
			],
			responses: {
				'200': {
					description: 'The stratification with the given id.',
					content: {
						'application/json': {
							schema: {
								$ref: '#/components/schemas/ResponseStratification',
							},
						},
					},
				},
				'404': {
					description: 'No stratification with the given id exists.',
				},
				401: { $ref: '#/components/responses/401' },
			},
		},
		delete: {
			'x-internal': true,
			tags: ['Stratifications'],
			summary: 'Delete a stratification by its id.',
			parameters: [
				{
					name: 'stratificationId',
					in: 'path',
					description: 'The id of the stratification to delete.',
					required: true,
					schema: { type: 'string' },
				},
				{
					name: 'force',
					in: 'query',
					description:
						'When true, allows the deletion of stratifications with a related sampling collection. The related sampling collections, sampling and samples will also be deleted. This parameter is disregarded if the stratification being deleted is not a demo stratification.',
					required: false,
					schema: {
						type: 'boolean',
					},
				},
			],
			responses: {
				'204': {
					description: 'The stratification was deleted successfully.',
				},
				'404': {
					description: 'No stratification with the given id exists.',
				},
				'409': {
					description: 'Cannot delete a stratification that has resources that depend on it.',
					content: {
						'application/json': {
							schema: {
								$ref: '#/components/schemas/Error',
							},
						},
					},
				},
				401: { $ref: '#/components/responses/401' },
			},
		} as OpenAPIV3.OperationObject,
	},
};

export { openApiPaths, router };
