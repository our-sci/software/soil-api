import request from 'supertest';
import { expect200AndValidateResponse, getDefaultUserBearerToken, testApp } from '../test-utils';

describe('User Endpoints', () => {
	describe('GET', () => {
		describe('/user', () => {
			it('returns 200 with a logged-in user on success', (done) => {
				request(testApp)
					.get('/user')
					.set('Authorization', getDefaultUserBearerToken())
					.end((err, res) => {
						expect200AndValidateResponse()(err, res);
						expect(res.body).toEqual(expect.objectContaining({ id: 'seed-user-1' }));
						done();
					});
			});
		});
	});
});
