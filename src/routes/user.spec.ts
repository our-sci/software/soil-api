import request from 'supertest';
import { prismaMock } from '../singleton';
import { getDefaultUserBearerToken, testApp } from '../test-utils';

describe('User Endpoints', () => {
	describe('GET', () => {
		describe('/user', () => {
			it('returns 500 when prisma.user.findUnique throws an unexpected error', (done) => {
				prismaMock.user.findUnique.mockRejectedValue(new Error('unexpected error'));

				request(testApp).get('/user').set('Authorization', getDefaultUserBearerToken()).expect(500, done);
			});
		});
	});
});
