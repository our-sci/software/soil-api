import { NextFunction, Request, Response, Router } from 'express';
import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';
import prisma from '../prismaClient';
import { prismaUserToUser } from '../transforms';

const router = Router();

router.get('/', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const prismaUser = await prisma.user.findUnique({
			where: { id: res.locals.user.id },
			include: { apiKeys: true },
		});

		if (!prismaUser) {
			return res.status(404).send();
		}

		res.status(200).json(prismaUserToUser(prismaUser));
	} catch (error) {
		next(error);
	}
});

const openApiPaths: OpenAPIV3.PathsObject = {
	'/user': {
		get: {
			tags: ['Users'],
			summary: 'Get the requesting user.',
			responses: {
				200: {
					description: 'A user.',
					content: {
						'application/json': {
							schema: {
								$ref: '#/components/schemas/ResponseUser',
							},
						},
					},
				},
				401: { $ref: '#/components/responses/401' },
			},
		},
	},
};

export { openApiPaths, router };
