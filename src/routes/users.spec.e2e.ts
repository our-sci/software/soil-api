import request from 'supertest';
import {
	expect200AndValidateResponse,
	expect401AndValidateResponse,
	getDefaultUserBearerToken,
	getUserBearerToken,
	registerUser,
	testApp,
} from '../test-utils';

describe('Users Endpoints', () => {
	describe('GET', () => {
		describe('/users', () => {
			describe('when the requesting user is a super admin', () => {
				it('returns 200 with a list of users on success', async (done) => {
					await registerUser({
						id: 'seed-super-admin-1',
						email: 'seedSuperAdmin1@soilstack.io',
						password: 'password',
						groupIds: [],
						isSuperAdmin: true,
					});

					request(testApp)
						.get(`/users`)
						.set('Authorization', getUserBearerToken('seedSuperAdmin1@soilstack.io'))
						.end((err, res) => {
							expect200AndValidateResponse()(err, res);
							expect(res.body).toHaveLength(3);
							expect(res.body).toContainEqual(expect.objectContaining({ id: 'seed-user-1' }));
							expect(res.body).toContainEqual(expect.objectContaining({ id: 'seed-admin-1' }));
							expect(res.body).toContainEqual(expect.objectContaining({ id: 'seed-super-admin-1' }));
							done();
						});
				});
			});

			describe('when the requesting user is not a super admin', () => {
				it('returns 401', (done) => {
					request(testApp)
						.get('/users')
						.set('Authorization', getDefaultUserBearerToken())
						.end(expect401AndValidateResponse(done));
				});
			});
		});
	});
});
