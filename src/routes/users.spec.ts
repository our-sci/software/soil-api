import request from 'supertest';
import { prismaMock } from '../singleton';
import { getDefaultSuperAdminBearerToken, testApp } from '../test-utils';

describe('Users Endpoints', () => {
	describe('GET', () => {
		describe('/users', () => {
			it('returns 500 when prisma.user.findMany throws an unexpected error', (done) => {
				prismaMock.user.findMany.mockRejectedValue(new Error('unexpected error'));

				request(testApp).get('/users').set('Authorization', getDefaultSuperAdminBearerToken()).expect(500, done);
			});
		});
	});
});
