import { NextFunction, Request, Response, Router } from 'express';
import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';
import prisma from '../prismaClient';
import { prismaUserToUser } from '../transforms';

const router = Router();

router.get('/', async (req: Request, res: Response, next: NextFunction) => {
	try {
		if (!res.locals.user.isSuperAdmin) {
			return res.status(401).send();
		}

		const prismaUsers = await prisma.user.findMany({
			include: { apiKeys: true },
		});

		const users = prismaUsers.map(prismaUserToUser);

		res.status(200).json(users);
	} catch (error) {
		next(error);
	}
});

const openApiPaths: OpenAPIV3.PathsObject = {
	'/users': {
		get: {
			'x-internal': true,
			tags: ['Users'],
			summary: 'Get all users',
			responses: {
				200: {
					description: 'A list of all users.',
					content: {
						'application/json': {
							schema: {
								type: 'array',
								items: { $ref: '#/components/schemas/ResponseUser' },
							},
						},
					},
				},
				401: { $ref: '#/components/responses/401' },
			},
		} as OpenAPIV3.OperationObject,
	},
};

export { openApiPaths, router };
