import { SendEmailCommand, SESClient } from '@aws-sdk/client-ses';
import { ENABLE_EMAIL, SES_ACCESS_KEY_ID, SES_SECRET_ACCESS_KEY } from '../../constants';
export * from './templates';

const SES_REGION = 'us-east-1';

const SESClienConfig = {
	region: SES_REGION,
	credentials: {
		accessKeyId: SES_ACCESS_KEY_ID,
		secretAccessKey: SES_SECRET_ACCESS_KEY,
	},
};

const sesClient = new SESClient(SESClienConfig);

const sendEmail = ({ to, subject, body }: { to: string; subject: string; body: string }) => {
	if (!ENABLE_EMAIL) {
		console.log('Emails are disabled due to the value of ENABLE_EMAIL environment variable. Not sending email.');
		return;
	}

	const sendEmailCommand = new SendEmailCommand({
		Destination: {
			ToAddresses: [to],
		},
		Message: {
			Body: {
				Html: {
					Charset: 'UTF-8',
					Data: body,
				},
			},
			Subject: {
				Charset: 'UTF-8',
				Data: subject,
			},
		},
		Source: 'SoilStack.io <noreply@soilstack.io>',
	});

	return sesClient.send(sendEmailCommand);
};

export { sendEmail };
