import { singleCTA } from './';

describe('singleCTA', () => {
	it('matches snapshot', () => {
		// prevent unintended changes to the template
		expect(singleCTA()).toMatchSnapshot();
	});
});
