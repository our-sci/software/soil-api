const SOILSTACK_BRANDING_COLORS = {
	DARK_GREEN: '#003B36',
	BLUE: '#086788',
};

/*
  This function does not handle sanitization.
  This allows the caller to add html if they need to.
  The caller should sanitize any user input before passing it to this function, to prevent html injection.
*/
const singleCTA = ({
	title = 'Something relevant to you happened on SoilStack.io',
	preheaderText = 'This is preheader text. Some clients will show this text as a preview.',
	preCtaParagraphs = [
		'Hi there,',
		'Sometimes you just want to send a simple HTML email with a simple design and clear call to action. This is it.',
	],
	ctaHref = 'https://soilstack.io',
	ctaText = 'Click here',
	postCtaParagraphs = [
		'This is a really simple email template. Its sole purpose is to get the recipient to click the button with no distractions.',
		'Good luck! Hope it works.',
	],
	colors = {
		buttonBackground: '#3498db', // non soilstack blue
		buttonText: '#ffffff',
		headerBackground: SOILSTACK_BRANDING_COLORS.BLUE,
	},
} = {}) => `
<!doctype html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>${title}</title>
    <style>
@media only screen and (max-width: 620px) {
  table.body h1 {
    font-size: 28px !important;
    margin-bottom: 10px !important;
  }

  table.body p,
table.body ul,
table.body ol,
table.body td,
table.body span,
table.body a {
    font-size: 16px !important;
  }

  table.body .wrapper,
table.body .article {
    padding: 10px !important;
  }

  table.body .content {
    padding: 0 !important;
  }

  table.body .container {
    padding: 0 !important;
    width: 100% !important;
  }

  table.body .main {
    border-left-width: 0 !important;
    border-radius: 0 !important;
    border-right-width: 0 !important;
  }

  table.body .btn table {
    width: 100% !important;
  }

  table.body .btn a {
    width: 100% !important;
  }

  table.body .img-responsive {
    height: auto !important;
    max-width: 100% !important;
    width: auto !important;
  }
}
@media all {
  .ExternalClass {
    width: 100%;
  }

  .ExternalClass,
.ExternalClass p,
.ExternalClass span,
.ExternalClass font,
.ExternalClass td,
.ExternalClass div {
    line-height: 100%;
  }

  .apple-link a {
    color: inherit !important;
    font-family: inherit !important;
    font-size: inherit !important;
    font-weight: inherit !important;
    line-height: inherit !important;
    text-decoration: none !important;
  }

  #MessageViewBody a {
    color: inherit;
    text-decoration: none;
    font-size: inherit;
    font-family: inherit;
    font-weight: inherit;
    line-height: inherit;
  }

  .btn-primary table td:hover {
    background-color: ${colors.buttonBackground} !important;
  }

  .btn-primary a:hover {
    background-color: ${colors.buttonBackground} !important;
    border-color: ${colors.buttonBackground} !important;
  }
}
</style>
  </head>
  <body style="background-color: #f6f6f6; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
    <span class="preheader" style="color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;">${preheaderText}</span>
    <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #f6f6f6; width: 100%;" width="100%" bgcolor="#f6f6f6">
      <tr>
        <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;" valign="top">&nbsp;</td>
        <td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; max-width: 580px; padding: 10px; width: 580px; margin: 0 auto;" width="580" valign="top">
          <div class="content" style="box-sizing: border-box; display: block; margin: 0 auto; max-width: 580px; padding: 10px;">

            <!-- START CENTERED WHITE CONTAINER -->
            <table role="presentation" class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background: #ffffff; border-radius: 3px; width: 100%;" width="100%">

              <!-- logo header -->
              <tr>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" role="presentation" dir="ltr">
                  <tbody>
                    <tr>

                      <td align="left" valign="middle" width="100%" style="background-color:${
												colors.headerBackground
											};border:0;padding:0;width:100%"
                        dir="ltr">
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" role="presentation" dir="ltr">
                          <tbody>
                            <tr>
                              <td valign="middle" width="100%"
                                style="background-color:${
																	colors.headerBackground
																};border:0;padding:12px 0 12px 25px;text-align:left;width:100%">
                                <table border="0" cellpadding="0" cellspacing="0" align="left" role="presentation">
                                  <tbody>
                                    <tr>
                                      <td><img
                                          src="https://soilstack-static-assets.s3.us-east-1.amazonaws.com/SoilStack_Logo_Horizontal_White.png"
                                          alt="SoilStack.io" height="" width="250"
                                          style="display:block;padding:0px;text-align:center;height:px;width:250px;border:0px"
                                          data-bit="iit"><img
                                          src="https://soilstack-static-assets.s3.us-east-1.amazonaws.com/SoilStack_Logo_Horizontal_White.png"
                                          alt="SoilStack.io" height="" width="250"
                                          style="display:block;padding:0px;text-align:center;height:px;width:250px;border:0px;display:none"
                                          data-bit="iit" tabindex="0">
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </tr>

              <!-- START MAIN CONTENT AREA -->
              <tr>
                <td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;" valign="top">
                  <table role="presentation" border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%">
                    <tr>
                      <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;" valign="top">
                        ${preCtaParagraphs
													.map(
														(paragraph) =>
															`<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">${paragraph}</p>`
													)
													.join('\n')}
                        <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; box-sizing: border-box; width: 100%;" width="100%">
                          <tbody>
                            <tr>
                              <td align="left" style="font-family: sans-serif; font-size: 14px; vertical-align: top; padding-bottom: 15px;" valign="top">
                                <table role="presentation" border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: auto;">
                                  <tbody>
                                    <tr>
                                      <td style="font-family: sans-serif; font-size: 14px; vertical-align: top; border-radius: 5px; text-align: center; background-color: ${
																				colors.buttonBackground
																			};" valign="top" align="center" bgcolor="${
	colors.buttonBackground
}"> <a href="${ctaHref}" target="_blank" style="border: solid 1px ${
	colors.buttonBackground
}; border-radius: 5px; box-sizing: border-box; cursor: pointer; display: inline-block; font-size: 14px; font-weight: bold; margin: 0; padding: 12px 25px; text-decoration: none; text-transform: capitalize; background-color: ${
	colors.buttonBackground
}; border-color: ${colors.buttonBackground}; color: ${colors.buttonText};">${ctaText}</a> </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        ${postCtaParagraphs
													.map(
														(paragraph) =>
															`<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">${paragraph}</p>`
													)
													.join('\n')}
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>

            <!-- END MAIN CONTENT AREA -->
            </table>
            <!-- END CENTERED WHITE CONTAINER -->
          </div>
        </td>
        <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;" valign="top">&nbsp;</td>
      </tr>
    </table>
  </body>
</html>
`;

export default singleCTA;
