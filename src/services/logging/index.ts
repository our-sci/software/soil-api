import { Client } from '@elastic/elasticsearch';
import {
	ELASTIC_AUTH_PASSWORD,
	ELASTIC_AUTH_USERNAME,
	ELASTIC_CLOUD_ID,
	ELASTIC_ENABLE_LOGGING,
	ELASTIC_INDEX_NAME,
} from '../../constants';

let client: Client;
let isAuthenticationValid = true;

if (ELASTIC_ENABLE_LOGGING) {
	try {
		client = new Client({
			cloud: {
				id: ELASTIC_CLOUD_ID,
			},
			auth: {
				username: ELASTIC_AUTH_USERNAME,
				password: ELASTIC_AUTH_PASSWORD,
			},
		});
	} catch (error: any) {
		if (error?.code === 'ERR_INVALID_URL') {
			console.error('Error: ELASTIC_CLOUD_ID is not valid.');
		} else {
			console.error('Unexpected Error initializing Elasticsearch client:', error);
		}
	}
}

function log(message: any): void {
	if (ELASTIC_ENABLE_LOGGING && client && isAuthenticationValid) {
		client
			.index({
				index: ELASTIC_INDEX_NAME,
				body: message,
			})
			.catch((error) => {
				if (error?.meta?.statusCode === 401) {
					console.error('Error: ELASTIC_AUTH_USERNAME and/or ELASTIC_AUTH_PASSWORD are not valid.');
					isAuthenticationValid = false;
				} else {
					console.error('Unexpected Error attempting to log to Elasticsearch:', error);
				}
			});
	}
}

export { log };
