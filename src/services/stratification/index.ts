import axios from 'axios';
import { STRATIFICATION_SERVICE_URL } from '../../constants';
import { paths } from '../../types/generated/stratification-service-types';

type postStratificationRequest = paths['/stratifications']['post']['requestBody']['content']['application/json'];
type postStratificationResponse = paths['/stratifications']['post']['responses']['200']['content']['application/json'];
type getStratificationResponse = paths['/stratifications/{id}']['get']['responses']['200']['content']['application/json'];

function postStratification(requestBody: postStratificationRequest) {
	return axios.post<postStratificationResponse>(`${STRATIFICATION_SERVICE_URL}/stratifications`, requestBody);
}

function getStratification(taskId: string) {
	return axios.get<getStratificationResponse>(`${STRATIFICATION_SERVICE_URL}/stratifications/${taskId}`);
}

export { getStratification, postStratification };
