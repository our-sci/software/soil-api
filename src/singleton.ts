import { PrismaClient } from '@prisma/client';
import { DeepMockProxy, mockDeep, mockReset } from 'jest-mock-extended';
import prisma from './prismaClient';
import {
	createMockPrismaAuthTokenWithUserWithMemberships,
	createMockPrismaMembership,
	defaultAdminToken,
	defaultSuperAdminToken,
	defaultUserToken,
} from './test-utils';
import { hash } from './utils';

jest.mock('./services/stratification');

jest.mock('./prismaClient', () => ({
	__esModule: true,
	default: mockDeep<PrismaClient>(),
}));

// In the event you need to affect the usage of prisma.authToken.findFirst within a controller
// without interefering with this mock that aids the authenticateUser middleware, you can override
// the mock implementation like so:
/*
	prismaMock.authToken.findFirst.mockImplementation((args: any) => {
		authTokenFindFirstMockImplementation(args);
		// Implement more mock behavior here as long as it doesn't expect
		// the authToken to match one of the tokens below
	})
*/
const hashedUserToken = hash(defaultUserToken);
const hashedAdminToken = hash(defaultAdminToken);
const hashedSuperAdminToken = hash(defaultSuperAdminToken);
export const authTokenFindFirstMockImplementation = (args: any) => {
	if (args.where.hashedToken === hashedUserToken) {
		return createMockPrismaAuthTokenWithUserWithMemberships({
			hashedToken: hashedUserToken,
			userId: 'seed-user-1',
			memberships: [createMockPrismaMembership({ groupId: 'seed-group-1' })],
		});
	}

	if (args.where.hashedToken === hashedAdminToken) {
		return createMockPrismaAuthTokenWithUserWithMemberships({
			hashedToken: hashedUserToken,
			userId: 'seed-admin-1',
			memberships: [
				createMockPrismaMembership({ userId: 'seed-admin-1', groupId: 'seed-group-1' }),
				createMockPrismaMembership({ userId: 'seed-admin-1', groupId: 'seed-group-2' }),
			],
		});
	}

	if (args.where.hashedToken === hashedSuperAdminToken) {
		return createMockPrismaAuthTokenWithUserWithMemberships({
			hashedToken: hashedSuperAdminToken,
			userId: 'seed-super-admin-1',
			isSuperAdmin: true,
			memberships: [],
		});
	}
};

beforeEach(() => {
	mockReset(prismaMock);
	// Allow seedUser1@soilstack.io and seedAdmin1@soilstack.io to be resolved in the authenticateUser middleware, so that the user is available on res.locals.user
	// Can be overridden in individual tests if needed.
	(prismaMock.authToken.findFirst as any).mockImplementation(authTokenFindFirstMockImplementation);
});

export const prismaMock = (prisma as unknown) as DeepMockProxy<PrismaClient>;
