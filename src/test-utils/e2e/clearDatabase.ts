import { clearDatabaseData, seedDatabase } from './database';

beforeEach(async () => {
	await clearDatabaseData();
	await seedDatabase();
});
