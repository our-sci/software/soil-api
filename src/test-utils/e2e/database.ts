import prisma from '../../prismaClient';
import { clearUsers, registerUser } from './users';

let tablenames: undefined | Array<{ tablename: string }>;

async function clearDatabaseData() {
	if (!tablenames) {
		tablenames = await prisma.$queryRaw<
			Array<{ tablename: string }>
		>`SELECT tablename FROM pg_tables WHERE schemaname='public'`;
	}

	for (const { tablename } of tablenames) {
		if (tablename !== '_prisma_migrations') {
			try {
				await prisma.$executeRawUnsafe(`TRUNCATE TABLE "public"."${tablename}" CASCADE;`);
			} catch (error) {
				console.log({ error });
			}
		}
	}

	clearUsers();
}

async function seedDatabase() {
	const [group1, group2] = await Promise.all([
		await prisma.group.create({
			data: {
				id: 'seed-group-1',
				name: 'Seed Group 1',
			},
		}),
		await prisma.group.create({
			data: {
				id: 'seed-group-2',
				name: 'Seed Group 2',
			},
		}),
	]);

	await Promise.all([
		registerUser({
			id: 'seed-admin-1',
			email: 'seedAdmin1@soilstack.io',
			password: 'password',
			groupIds: [group1.id, group2.id],
			isSuperAdmin: false,
		}),
		registerUser({
			id: 'seed-user-1',
			email: 'seedUser1@soilstack.io',
			password: 'password',
			groupIds: [group1.id],
			isSuperAdmin: false,
		}),
	]);
}

export { clearDatabaseData, seedDatabase };
