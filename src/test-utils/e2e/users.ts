import prisma from '../../prismaClient';
import { hash, hashAndSalt, randomHex } from '../../utils';

type UserSecrets = {
	authToken: string;
	password: string;
};

type RegisterUserConfig = {
	id: string;
	email: string;
	password: string;
	groupIds: string[];
	isSuperAdmin: boolean;
};

let _users: { [email: string]: UserSecrets } = {};

const _register = async ({ id, email, password, groupIds, isSuperAdmin }: RegisterUserConfig) => {
	const hashedPassword = await hashAndSalt(password);
	const authToken = randomHex();
	const hashedAuthToken = hash(authToken);

	await prisma.user.create({
		data: {
			id,
			email: email.toLowerCase(),
			hashedPassword,
			seen: true,
			isSuperAdmin,
			authTokens: {
				create: {
					hashedToken: hashedAuthToken,
				},
			},
			memberships: {
				createMany: {
					data: groupIds.map((groupId) => ({ groupId })),
				},
			},
		},
	});

	return authToken;
};

const registerUser = async (config: RegisterUserConfig) => {
	const { email, password } = config;
	const authToken = await _register(config);
	_users[email.toLowerCase()] = { authToken, password };
};

const _getUserSecrets = (email: string): UserSecrets => {
	return {
		..._users[email.toLowerCase()],
	};
};

const clearUsers = () => {
	_users = {};
};

const getUserBearerToken = (email: string) => `Bearer ${_getUserSecrets(email).authToken}`;

const defaultUserToken = randomHex();
const defaultAdminToken = randomHex();
const defaultSuperAdminToken = randomHex();
const getDefaultUserBearerToken = () =>
	(global as any)?.__E2E_TESTS__ ? getUserBearerToken('seedUser1@soilstack.io') : `Bearer ${defaultUserToken}`;
const getDefaultAdminBearerToken = () =>
	(global as any)?.__E2E_TESTS__ ? getUserBearerToken('seedAdmin1@soilstack.io') : `Bearer ${defaultAdminToken}`;
const getDefaultSuperAdminBearerToken = () => {
	if ((global as any)?.__E2E_TESTS__) {
		throw new Error(
			'This function does not support e2e tests. The super admin user is not needed by enough e2e tests to warrant being seeded for all tests. If you need a super admin user in an e2e test, use "registerUser" to create one and "getUserBearerToken" to get the token.'
		);
	}
	return `Bearer ${defaultSuperAdminToken}`;
};

export {
	clearUsers,
	defaultAdminToken,
	defaultSuperAdminToken,
	defaultUserToken,
	getDefaultAdminBearerToken,
	getDefaultSuperAdminBearerToken,
	getDefaultUserBearerToken,
	getUserBearerToken,
	registerUser,
};
