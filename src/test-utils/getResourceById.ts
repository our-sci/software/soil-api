import request from 'supertest';
import { ResourceTypes } from '../constants';
import { getDefaultUserBearerToken } from './e2e/users';
import {
	postMockField,
	postMockSamplingCollectionAndDependencies,
	postMockStratificationAndDependencies,
} from './requests';
import { testApp } from './testApp';
import { expect200AndValidateResponse, expect404AndValidateResponse } from './validation';

const testConfigsByResourceType: { [key in ResourceTypes]: any } = {
	[ResourceTypes.Field]: {
		seedData: postMockField,
		getResourceFromPostResponse: (postResponse: any) => postResponse.body.fields[0],
	},
	[ResourceTypes.Area]: {
		seedData: postMockField,
		getResourceFromPostResponse: (postResponse: any) => postResponse.body.areas[0],
	},
	[ResourceTypes.Stratification]: {
		seedData: postMockStratificationAndDependencies,
		getResourceFromPostResponse: (postResponse: any) => postResponse.body.stratifications[0],
	},
	[ResourceTypes.LocationCollection]: {
		seedData: postMockStratificationAndDependencies,
		getResourceFromPostResponse: (postResponse: any) => postResponse.body.locationCollections[0],
	},
	[ResourceTypes.Location]: {
		seedData: postMockStratificationAndDependencies,
		getResourceFromPostResponse: (postResponse: any) => postResponse.body.locations[0],
	},
	[ResourceTypes.SamplingCollection]: {
		seedData: postMockSamplingCollectionAndDependencies,
		getResourceFromPostResponse: (postResponse: any) => postResponse.body.samplingCollections[0],
	},
	[ResourceTypes.Sampling]: {
		seedData: postMockSamplingCollectionAndDependencies,
		getResourceFromPostResponse: (postResponse: any) => postResponse.body.samplings[0],
	},
	[ResourceTypes.Sample]: {
		seedData: postMockSamplingCollectionAndDependencies,
		getResourceFromPostResponse: (postResponse: any) => postResponse.body.samples[0],
	},
};

const testGetResourceByIdEndpoint = ({
	endpointPath,
	resourceName,
	resourceType,
}: {
	endpointPath: string;
	resourceName: string;
	resourceType: ResourceTypes;
}): void => {
	describe(`${endpointPath}/:id`, () => {
		it(`returns 200 when a ${resourceName} is found`, async (done) => {
			const testConfig = testConfigsByResourceType[resourceType];
			const postResponse = await testConfig.seedData();
			const postedResource = testConfig.getResourceFromPostResponse(postResponse);

			request(testApp)
				.get(`${endpointPath}/${postedResource.id}`)
				.set('Authorization', getDefaultUserBearerToken())
				.end((err, res) => {
					expect(res.body).toHaveProperty('id', postedResource.id);
					expect200AndValidateResponse(done)(err, res);
				});
		});

		it(`returns 404 when a ${resourceName} is not found`, (done) => {
			request(testApp)
				.get(`${endpointPath}/non-existent-id`)
				.set('Authorization', getDefaultUserBearerToken())
				.end(expect404AndValidateResponse(done));
		});

		it(`returns 404 when the user does not have permission to access the ${resourceName}`, async (done) => {
			const testConfig = testConfigsByResourceType[resourceType];
			const postResponse = await testConfig.seedData({ groupId: 'seed-group-2' });
			const postedResource = testConfig.getResourceFromPostResponse(postResponse);

			request(testApp)
				.get(`${endpointPath}/${postedResource.id}`)
				.set('Authorization', getDefaultUserBearerToken())
				.end(expect404AndValidateResponse(done));
		});
	});
};

export { testGetResourceByIdEndpoint };
