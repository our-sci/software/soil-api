import { Point } from 'geojson';
import { components, paths } from '../types/generated/openapi-types';
import {
	components as stratServiceComponents,
	paths as stratServicePaths,
} from '../types/generated/stratification-service-types';

export function createMockPolygon({
	coordinates = [
		[
			[40, 80],
			[41, 81],
			[40, 80],
		],
	],
} = {}): components['schemas']['Polygon'] {
	return {
		type: 'Polygon',
		coordinates,
	};
}

export function createMockPoint({ coordinates = [40, 80] } = {}): Point {
	return {
		type: 'Point',
		coordinates,
	};
}

function createMockSoDepthValue({ value = 10 } = {}): components['schemas']['SoDepthValue'] {
	return {
		value,
		unit: 'unit:CM',
	};
}

export function createMockSoDepth({
	minValue = createMockSoDepthValue(),
	maxValue = createMockSoDepthValue(),
} = {}): components['schemas']['SoDepth'] {
	return {
		minValue,
		maxValue,
	};
}

export function createMockSample(
	{
		id,
		name = 'mock name',
		sampleOf = 'mock field id',
		resultOf = 'mock sampling id',
		soDepth = createMockSoDepth(),
		meta,
	}: Partial<components['schemas']['ResponseSample']> = {},
	{ omitSampleOf = false, omitResultOf = false } = {}
): components['schemas']['Sample'] {
	return {
		...(id ? { id } : {}),
		name,
		...(omitSampleOf ? {} : { sampleOf }),
		...(omitResultOf ? {} : { resultOf }),
		soDepth,
		...(meta ? { meta } : {}),
	};
}

export function createMockSampling(
	{
		id,
		resultTime = new Date(0).toISOString(),
		geometry = createMockPoint(),
		featureOfInterest = 'mock location id',
		memberOf = 'mock sampling collection id',
		results = ['mock sample id'],
		comment = 'mock comment',
		procedures = ['mock procedure'],
		properties = {},
		soDepth = createMockSoDepth(),
		meta,
	}: Partial<components['schemas']['ResponseSampling']> = {},
	{ omitMemberOf = false } = {}
): components['schemas']['Sampling'] | components['schemas']['ResponseSampling'] {
	return {
		...(id ? { id } : {}),
		resultTime,
		geometry,
		featureOfInterest,
		...(omitMemberOf ? {} : { memberOf }),
		results,
		comment,
		procedures,
		properties,
		soDepth,
		...(meta ? { meta } : {}),
	};
}

export function createMockSamplingCollection(
	{
		id,
		name = 'mock sampling collection',
		featureOfInterest = 'mock field id',
		object = 'mock location collection id',
		members = ['mock sampling id'],
		soDepth = createMockSoDepth(),
		sampleDepths = [createMockSoDepth()],
		dateCreated = new Date(0).toISOString(),
		dateModified = new Date(0).toISOString(),
		meta = {
			referenceIds: [{ owner: 'soilstack-draft', id: 'mock draft id' }],
		},
	}: Partial<components['schemas']['ResponseSamplingCollection']> = {},
	{ omitObject = false, omitFeatureOfInterest = false } = {}
): components['schemas']['SamplingCollection'] | components['schemas']['ResponseSamplingCollection'] {
	return id
		? {
				id,
				name,
				...(omitFeatureOfInterest ? {} : { featureOfInterest }),
				...(omitObject ? {} : { object }),
				members,
				soDepth,
				sampleDepths,
				dateCreated,
				dateModified,
				meta,
		  }
		: {
				name,
				...(omitFeatureOfInterest ? {} : { featureOfInterest }),
				...(omitObject ? {} : { object }),
				members,
				soDepth,
				sampleDepths,
				dateCreated,
				dateModified,
				meta,
		  };
}

export function createMockArea({
	id,
	properties = {},
	geometry = createMockPolygon(),
	meta,
}: Partial<components['schemas']['ResponseArea']> = {}):
	| components['schemas']['Area']
	| components['schemas']['ResponseArea'] {
	return id
		? {
				id,
				type: 'Feature',
				properties,
				geometry,
				...(meta ? { meta } : {}),
		  }
		: {
				type: 'Feature',
				properties,
				geometry,
				...(meta ? { meta } : {}),
		  };
}

export function createMockLocation({
	id,
	properties = { stratum: 0 },
	geometry = createMockPoint(),
	meta,
}: Partial<components['schemas']['ResponseLocation']> = {}):
	| components['schemas']['Location']
	| components['schemas']['ResponseLocation'] {
	return id
		? {
				id,
				type: 'Feature',
				properties,
				geometry,
				...(meta ? { meta } : {}),
		  }
		: {
				type: 'Feature',
				properties,
				geometry,
				...(meta ? { meta } : {}),
		  };
}

export function createMockAddress({
	streetAddress = 'mock streetAddress',
	addressLocality = 'mock addressLocality',
	addressRegion = 'mock addressRegion',
	addressCountry = 'mock addressCountry',
	postalCode = 'mock postalCode',
}: Partial<components['schemas']['Address']> = {}): components['schemas']['Address'] {
	return {
		streetAddress,
		addressLocality,
		addressRegion,
		addressCountry,
		postalCode,
	};
}

export function createMockContactPoint({
	telephone = 'mock telephone',
	email = 'mock email',
	name = 'mock name',
	contactType = 'mock contactType',
	organization = 'mock organization',
}: Partial<components['schemas']['ContactPoint']> = {}): components['schemas']['ContactPoint'] {
	return {
		telephone,
		email,
		name,
		contactType,
		organization,
	};
}

export function createMockField({
	id,
	name = 'mock field name',
	alternateName = 'mock alternate name',
	producerName = 'mock producer name',
	address = createMockAddress(),
	contactPoints = [createMockContactPoint()],
	areas = [] as string[],
	meta = {},
}: Partial<components['schemas']['ResponseField']> = {}):
	| components['schemas']['Field']
	| components['schemas']['ResponseField'] {
	return id
		? {
				id,
				name,
				alternateName,
				producerName,
				address,
				contactPoints,
				areas,
				meta,
		  }
		: {
				name,
				alternateName,
				producerName,
				address,
				contactPoints,
				areas,
				meta,
		  };
}

function createMockAlgorithm({
	name = 'mock name',
	alternateName = 'mock alternateName',
	codeRepository = 'mock codeRepository',
	version = 'mock version',
	doi = 'mock doi',
} = {}): components['schemas']['Algorithm'] {
	return {
		name,
		alternateName,
		codeRepository,
		version,
		doi,
	};
}

function createMockStratificationInput({
	name = 'mock name',
	value = 'mock value',
} = {}): components['schemas']['StratificationInput'] {
	return {
		name,
		value,
	};
}

function createMockStratificationResult({
	name = 'mock name',
	value = 'mock value',
} = {}): components['schemas']['StratificationResult'] {
	return {
		name,
		value,
	};
}

export function createMockStratification(
	{
		id,
		name = 'mock name',
		agent = 'mock agent',
		dateCreated = new Date(0).toISOString(),
		provider = 'mock provider',
		object = 'mock object',
		isDemo,
		algorithm = createMockAlgorithm(),
		input = [createMockStratificationInput()],
		result = [createMockStratificationResult()],
		meta,
	}: Partial<components['schemas']['ResponseStratification']> = {},
	{ omitObject = false } = {}
): components['schemas']['Stratification'] | components['schemas']['ResponseStratification'] {
	return {
		...(id ? { id } : {}),
		name,
		agent,
		dateCreated,
		provider,
		...(isDemo !== null && typeof isDemo !== 'undefined' ? { isDemo } : {}),
		...(omitObject ? {} : { object }),
		algorithm,
		input,
		result,
		...(meta ? { meta } : {}),
	};
}

export function createMockLocationCollection(
	{
		id,
		object = 'mock object',
		resultOf = 'mock resultOf',
		featureOfInterest = 'mock featureOfInterest',
		features = ['mock feature'],
		meta,
	}: Partial<components['schemas']['ResponseLocationCollection']> = {},
	{ omitResultOf = false, omitFeatureOfInterest = false, omitObject = false } = {}
): Partial<components['schemas']['ResponseLocationCollection']> {
	return {
		...(id ? { id } : {}),
		...(omitObject ? {} : { object }),
		...(omitResultOf ? {} : { resultOf }),
		...(omitFeatureOfInterest ? {} : { featureOfInterest }),
		features,
		...(meta ? { meta } : {}),
	};
}

export function createMockPopulatedField({ meta = {} } = {}): components['schemas']['PopulatedField'] {
	const mockField = createMockField({ meta });
	const mockArea = {
		...createMockArea(),
		properties: {
			...createMockArea().properties,
			drawn: true,
		},
	};
	const mockPopulatedField = {
		...mockField,
		areas: [mockArea],
	};

	return mockPopulatedField;
}

export function createMockPopulatedLocationCollection(): components['schemas']['PopulatedLocationCollection'] {
	const mockLocationCollection = createMockLocationCollection(
		{},
		{
			omitResultOf: true,
			omitFeatureOfInterest: true,
			omitObject: true,
		}
	);
	const mockLocation = createMockLocation();
	const mockPopulatedLocationCollection = {
		...mockLocationCollection,
		features: [mockLocation],
	};

	return mockPopulatedLocationCollection;
}

export function createMockPopulatedSamplingCollection(
	locationIds: string[],
	referenceId = 'mock reference id'
): components['schemas']['PopulatedSamplingCollection'] {
	const mockSamplingCollection = createMockSamplingCollection(
		{ meta: { referenceIds: [{ owner: 'soilstack-draft', id: referenceId }] } },
		{ omitObject: true, omitFeatureOfInterest: true }
	) as components['schemas']['SamplingCollection'];
	const mockSamplings = locationIds.map((locationId) => ({
		...createMockSampling({ featureOfInterest: locationId }, { omitMemberOf: true }),
		results: [createMockSample({}, { omitSampleOf: true, omitResultOf: true })],
	}));
	return {
		...mockSamplingCollection,
		members: mockSamplings,
	};
}

type PostStratificationRequest = paths['/areas/{areaId}/stratifications']['post']['requestBody']['content']['application/json'];

export function createMockPostStratificationRequest(
	stratificationOverrides: Partial<components['schemas']['ResponseStratification']> = {}
): PostStratificationRequest {
	const stratification = createMockStratification(stratificationOverrides, { omitObject: true });
	const locationCollection = createMockPopulatedLocationCollection();
	return {
		stratification,
		locationCollection,
	};
}

export function createMockStratificationConfig(): stratServiceComponents['schemas']['StratificationConfig'] {
	return {
		inputs: [
			{
				source: 'SENTINEL_2',
				params: {
					dates: '2021-07-01/2021-07-30',
					masked_ratio_threshold: 0.3,
					ndvi_max: 1.0,
				},
			},
			{
				source: 'COPERNICUS',
				params: {},
			},
		],
		params: {
			sampling_densities: [
				{
					min_area: 0,
					max_area: 9999999999,
					units: 'HECTARES',
					density: 0.617763453667925,
					per: 'STRATUM',
				},
			],
			field_boundary_raster_mask_buffer: {
				type: 'DYNAMIC',
				buffer_max: -10,
				buffer_min: -30,
				buffer_interval: 5,
				threshold: 0.6,
			},
			bbox_padding_degrees: 0.001,
			n_strata: {
				type: 'DYNAMIC',
				value: [
					{
						min: 1,
						max: 5,
						rate: 0.2,
						min_area: 0,
						max_area: 9007199254740991,
						offset: 0,
					},
				],
				span: 4,
				pad_min_length: 5,
			},
		},
	};
}

type PostStratifyRequest = paths['/areas/stratify']['post']['requestBody']['content']['application/json'];

export function createMockStratifyRequest({
	areaIds = ['mock area id'],
	stratificationConfig = createMockStratificationConfig(),
	autoApprove,
}: Partial<PostStratifyRequest> = {}): PostStratifyRequest {
	const request: PostStratifyRequest = {
		areaIds,
		stratificationConfig,
	};
	if (autoApprove !== undefined) {
		request.autoApprove = autoApprove;
	}
	return request;
}

export function createMockStratificationServiceStratification(): stratServiceComponents['schemas']['Stratification'] {
	return {
		locations: [
			{
				type: 'Feature',
				geometry: {
					type: 'Point',
					coordinates: [-84.491258, 39.207344],
				},
				properties: {
					stratum: '0',
				},
			},
			{
				type: 'Feature',
				geometry: {
					type: 'Point',
					coordinates: [-84.49183, 39.207536],
				},
				properties: {
					stratum: '1',
				},
			},
		],
		algorithm: {
			name: 'OUR_SCI_STRATIFICATION',
			alternateName: 'SKLEARN_KMEANS_RANGE_CLHS',
			version: '0.10.0',
			doi: 'mock-doi',
			codeRepository: 'mock-code-repository',
		},
		input: [
			{
				name: 'INPUT_LAYER_SENTINEL_2_L2A',
				value: 'https://sentinel.esa.int/web/sentinel/user-guides/sentinel-2-msi/product-types/level-2a',
			},
			{
				name: 'INPUT_LAYER_SENTINEL_2_L2A_SCL_MASKED_RATIO_THRESHOLD',
				value: 0.3,
			},
			{
				name: 'INPUT_LAYER_SENTINEL_2_L2A_NDVI_MAX_VALUE',
				value: 1,
			},
			{
				name: 'INPUT_LAYER_SENTINEL_2_L2A_NDVI_DATE_RANGE',
				value: '2021-07-01/2021-07-30',
			},
			{
				name: 'INPUT_LAYER_SENTINEL_2_L2A_SEARCH_QUERY',
				value: {
					platform: {
						eq: 'sentinel-2a',
					},
				},
			},
			{
				name: 'INPUT_LAYER_COPERNICUS_GLO_30',
				value: 'https://portal.opentopography.org/datasetMetadata?otCollectionID=OT.032021.4326.1',
			},
			{
				name: 'RANDOM_SEED',
				value: 573085,
			},
			{
				name: 'CLHS_MAX_ITERATIONS',
				value: 1000,
			},
			{
				name: 'CLUSTERING_NUMBER_OF_CLUSTERS',
				value: 2,
			},
			{
				name: 'SAMPLE_DENSITY',
				value: [
					{
						min_area: 0,
						max_area: 9999999999,
						units: 'HECTARES',
						per: 'STRATUM',
						density: 0.617763453667925,
					},
				],
			},
			{
				name: 'FIELD_BOUNDARY_RASTER_MASK_BUFFER_METERS',
				value: -10,
			},
		],
		result: [],
	};
}

type PostStratificationResponse = stratServicePaths['/stratifications']['post']['responses']['200']['content']['application/json'];
export function createMockStratServicePostStratificationResponse({
	state = 'PENDING',
	taskId = 'mock-task-id',
}: Partial<PostStratificationResponse> = {}): PostStratificationResponse {
	return {
		state,
		taskId,
	};
}

type StratificationJobSuccess = stratServiceComponents['schemas']['StratificationJobSuccess'];
export function createMockStratificationJobSuccess({
	taskId = 'mock-task-id',
	result = createMockStratificationServiceStratification(),
}: Partial<StratificationJobSuccess> = {}): StratificationJobSuccess {
	return {
		taskId,
		state: 'SUCCESS',
		result,
	};
}

type StratificationJobFailure = stratServiceComponents['schemas']['StratificationJobFailure'];
export function createMockStratificationJobFailure({
	taskId = 'mock-task-id',
	result = 'mock error',
	traceback = '',
}: Partial<StratificationJobFailure> = {}): StratificationJobFailure {
	return {
		taskId,
		state: 'FAILURE',
		result,
		traceback,
	};
}
