import {
	ApiKey as PrismaApiKey,
	Area as PrismaArea,
	AuthToken as PrismaAuthToken,
	Group as PrismaGroup,
	Location as PrismaLocation,
	LocationCollection as PrismaLocationCollection,
	Membership as PrismaMembership,
	PasswordResetToken as PrismaPasswordResetToken,
	Prisma,
	Sample as PrismaSample,
	Stratification as PrismaStratification,
	StratificationJob as PrismaStratificationJob,
	User as PrismaUser,
} from '@prisma/client';
import {
	PrismaApiKeyWithUserWithMemberships,
	PrismaAuthTokenWithUserWithMemberships,
	PrismaMembershipWithIncludes,
	PrismaMembershipWithUser,
	PrismaPasswordResetTokenWithUser,
	PrismaPopulatedField,
	PrismaPopulatedLocationCollection,
	PrismaPopulatedSampling,
	PrismaPopulatedSamplingCollection,
	PrismaPopulatedStratification,
	PrismaStratificationWithSamplingCollectionIds,
	PrismaUserWithApiKeys,
	PrismaUserWithMemberships,
} from '../types/prisma';
import { hashAndSalt } from '../utils';
import { createMockPoint, createMockSoDepth, createMockStratificationConfig } from './mockGenerators';

export const createMockPrismaPasswordResetTokenWithUser = async ({
	id = 'password-reset-token-id',
	hashedToken = 'password-reset-token-hashed-token',
	expiresAt = new Date(Date.now() + 1000 * 60 * 60 * 24),
}: Partial<PrismaPasswordResetToken> = {}): Promise<PrismaPasswordResetTokenWithUser> => {
	const user = await createMockPrismaUser();
	return {
		id,
		hashedToken,
		expiresAt,
		user,
		userId: user.id,
		createdAt: new Date(0),
		updatedAt: new Date(0),
	};
};

export const createMockPrismaMembership = ({
	id = 'membership-id',
	userId = 'seed-user-1',
	groupId = 'seed-group-1',
	role = 'USER',
}: Partial<PrismaMembership> = {}) => ({
	id,
	userId,
	groupId,
	role,
	createdAt: new Date(0),
	updatedAt: new Date(0),
});

export const createMockPrismaApiKey = ({
	id = 'api-key-id',
	label = 'api-key-label',
	userId = 'user-id',
	hashedKey = 'hashed-key',
}: Partial<PrismaApiKey> = {}): PrismaApiKey => ({
	id,
	label,
	userId,
	hashedKey,
	createdAt: new Date(0),
	updatedAt: new Date(0),
});

export const createMockPrismaApiKeyWithUserWithMemberships = async ({
	id = 'api-key-id',
	label = 'api-key-label',
	userId = 'user-id',
	hashedKey = 'hashed-key',
}: Partial<PrismaApiKey> = {}): Promise<PrismaApiKeyWithUserWithMemberships> => ({
	id,
	label,
	userId,
	hashedKey,
	user: await createMockPrismaUserWithMemberships(),
	createdAt: new Date(0),
	updatedAt: new Date(0),
});

export const createMockPrismaAuthTokenWithUserWithMemberships = async ({
	id = 'auth-token-id',
	hashedToken = 'auth-token-hashed-token',
	userId = 'seed-user-1',
	userEmail = 'seedUser1@soilstack.io',
	memberships = [],
	isSuperAdmin = false,
}: Partial<
	PrismaAuthToken & { memberships: PrismaMembership[]; userEmail: string; isSuperAdmin: boolean }
> = {}): Promise<PrismaAuthTokenWithUserWithMemberships> => ({
	id,
	hashedToken,
	userId,
	user: await createMockPrismaUserWithMemberships({ id: userId, email: userEmail, memberships, isSuperAdmin }),
	createdAt: new Date(0),
	updatedAt: new Date(0),
});

export const createMockPrismaMembershipWithUser = async (): Promise<PrismaMembershipWithUser> => ({
	...createMockPrismaMembership(),
	user: await createMockPrismaUser(),
});

export const createMockPrismaMembershipWithIncludes = async ({
	memberships = [],
	parentId = null,
} = {}): Promise<PrismaMembershipWithIncludes> => ({
	...createMockPrismaMembership(),
	user: await createMockPrismaUserWithMemberships({ memberships }),
	group: {
		parentId,
	},
});

// TODO: consolidate the next few functions. If they call call createMockPrismaUser to start we can save some code
export const createMockPrismaUser = async ({
	id = 'seed-user-1',
	email = 'seedUser1@soilstack.io',
	password = 'password',
	seen = true,
	isSuperAdmin = false,
}: Partial<PrismaUser & { password: string }> = {}): Promise<PrismaUser> => ({
	id,
	email,
	seen,
	isSuperAdmin,
	hashedPassword: await hashAndSalt(password),
	createdAt: new Date(0),
	updatedAt: new Date(0),
});

export const createMockPrismaUserWithMemberships = async ({
	id = 'seed-user-1',
	email = 'seedUser1@soilstack.io',
	password = 'password',
	seen = true,
	isSuperAdmin = false,
	memberships = [],
}: Partial<PrismaUserWithMemberships & { password: string }> = {}): Promise<PrismaUserWithMemberships> => ({
	id,
	email,
	seen,
	isSuperAdmin,
	hashedPassword: await hashAndSalt(password),
	memberships,
	createdAt: new Date(0),
	updatedAt: new Date(0),
});

export const createMockPrismaUserWithApiKeys = async ({
	id = 'seed-user-1',
	email = 'seedUser1@soilstack.io',
	password = 'password',
	seen = true,
	apiKeys = [],
	isSuperAdmin = false,
}: Partial<PrismaUserWithApiKeys & { password: string }> = {}): Promise<PrismaUserWithApiKeys> => ({
	id,
	email,
	seen,
	isSuperAdmin,
	hashedPassword: await hashAndSalt(password),
	apiKeys,
	createdAt: new Date(0),
	updatedAt: new Date(0),
});

export const createMockPrismaGroupWithMemberships = ({
	id = 'seed-group-1',
	memberships = [],
}: Partial<{ id: string; memberships: PrismaMembership[] }> = {}) => ({
	...createMockPrismaGroup({ id }),
	memberships,
});

export const createMockPrismaGroup = ({
	id = 'seed-group-1',
	name = 'group-name',
	parentId = null,
}: Partial<PrismaGroup> = {}): PrismaGroup => ({
	id,
	name,
	parentId,
	apiOnly: false,
	createdAt: new Date(0),
	updatedAt: new Date(0),
});

export const createMockPrismaArea = ({ id = 'area-id' } = {}): PrismaArea => ({
	id,
	type: 'Feature',
	properties: null,
	geometry: {
		type: 'Polygon',
		coordinates: [
			[
				[-86.8644505695512, 38.547271392686866],
				[-86.8644505695512, 38.54570258301999],
				[-86.86185889289783, 38.54570258301999],
				[-86.86185889289783, 38.547271392686866],
				[-86.8644505695512, 38.547271392686866],
			],
		],
	},
	createdAt: new Date(0),
	updatedAt: new Date(0),
	fieldId: '123',
	groupId: 'seed-group-1',
});

export const createMockPrismaField = ({ id = 'field-id' } = {}): PrismaPopulatedField => ({
	id,
	name: 'Test Field',
	alternateName: 'alternate name',
	producerName: 'Bobby Producer',
	streetAddress: '123 Cherry St.',
	addressLocality: 'Fargo',
	addressRegion: 'North Dakota',
	addressCountry: 'United States',
	postalCode: '54321',
	contactPoints: [],
	createdAt: new Date(0),
	updatedAt: new Date(0),
	groupId: 'seed-group-1',
	areas: [createMockPrismaArea()],
	referenceIds: [
		{
			id: '987',
			owner: 'soilstack-draft',
			value: '123456',
			createdAt: new Date(0),
			updatedAt: new Date(0),
			fieldId: id,
			samplingCollectionId: null,
		},
	],
});

export const createMockPrismaLocation = ({ id = 'location-id' } = {}): PrismaLocation => ({
	id,
	locationCollectionId: 'location-collection-id',
	type: 'Feature',
	geometry: {},
	properties: {},
	groupId: 'seed-group-1',
	createdAt: new Date(0),
	updatedAt: new Date(0),
});

export const createMockPrismaLocationCollection = (): PrismaLocationCollection => ({
	id: 'location-collection-id',
	areaId: 'area-id',
	stratificationId: 'stratification-id',
	fieldId: 'field-id',
	groupId: 'seed-group-1',
	createdAt: new Date(0),
	updatedAt: new Date(0),
});

export const createMockPrismaPopulatedLocationCollection = (): PrismaPopulatedLocationCollection => ({
	...createMockPrismaLocationCollection(),
	locations: [createMockPrismaLocation()],
});

type PrismaLocationCollectionWithFieldAndLocationIds = Prisma.LocationCollectionGetPayload<{
	include: {
		field: { select: { id: true } };
		locations: { select: { id: true } };
	};
}>;
export const createMockPrismaPopulatedLocationCollectionWithFieldAndLocationIds = (): PrismaLocationCollectionWithFieldAndLocationIds => ({
	...createMockPrismaLocationCollection(),
	field: { id: 'field-id' },
	locations: [{ id: 'location-id' }],
});

export const createMockPrismaStratification = (): PrismaStratification => ({
	id: 'stratification-id',
	name: 'stratification-name',
	agent: 'stratification-agent',
	provider: 'stratification-provider',
	isDemo: false,
	areaId: 'area-id',
	algorithmId: 'algorithm-id',
	input: [
		{
			name: 'input-name',
			value: 'input-value',
		},
	],
	result: [
		{
			name: 'result-name',
			value: 'result-value',
		},
	],
	groupId: 'seed-group-1',
	createdAt: new Date(0),
	updatedAt: new Date(0),
});

export const createMockPrismaPopulatedStratification = (): PrismaPopulatedStratification => ({
	id: 'stratification-id',
	name: 'stratification-name',
	agent: 'stratification-agent',
	provider: 'stratification-provider',
	isDemo: false,
	area: { fieldId: 'field-id' },
	areaId: 'area-id',
	algorithm: {
		id: 'algorithm-id',
		name: 'algorithm-name',
		alternateName: 'algorithm-alternate-name',
		codeRepository: 'algorithm-code-repository',
		version: 'algorithm-version',
		doi: 'algorithm-doi',
		createdAt: new Date(0),
		updatedAt: new Date(0),
	},
	algorithmId: 'algorithm-id',
	input: [
		{
			name: 'input-name',
			value: 'input-value',
		},
	],
	result: [
		{
			name: 'result-name',
			value: 'result-value',
		},
	],
	groupId: 'seed-group-1',
	locationCollection: createMockPrismaPopulatedLocationCollection(),
	createdAt: new Date(0),
	updatedAt: new Date(0),
});

export const createMockPrismaStratificationWithSamplingCollectionIds = (
	ids = []
): PrismaStratificationWithSamplingCollectionIds => ({
	...createMockPrismaPopulatedStratification(),
	locationCollection: {
		id: 'location-collection-id',
		samplingCollections: ids,
	},
});

export const createMockPrismaSample = (): PrismaSample => ({
	id: 'sample-id',
	name: 'sample-name',
	soDepth: (createMockSoDepth() as unknown) as Prisma.JsonObject,
	fieldId: 'field-id',
	samplingId: 'sampling-id',
	groupId: 'seed-group-1',
	createdAt: new Date(0),
	updatedAt: new Date(0),
});

export const createMockPrismaPopulatedSampling = (): PrismaPopulatedSampling => ({
	id: 'sampling-id',
	resultTime: new Date(0),
	geometry: (createMockPoint() as unknown) as Prisma.JsonObject,
	samples: [createMockPrismaSample()],
	locationId: 'location-id',
	samplingCollectionId: 'sampling-collection-id',
	comment: 'sampling-comment',
	procedures: ['sampling-procedure'],
	properties: {},
	soDepth: (createMockSoDepth() as unknown) as Prisma.JsonObject,
	groupId: 'seed-group-1',
	createdAt: new Date(0),
	updatedAt: new Date(0),
});

export const createMockPrismaPopulatedSamplingCollection = (): PrismaPopulatedSamplingCollection => ({
	id: 'sampling-collection-id',
	name: 'sampling-collection-name',
	fieldId: 'field-id',
	locationCollectionId: 'location-collection-id',
	samplings: [createMockPrismaPopulatedSampling()],
	groupId: 'seed-group-1',
	soDepth: (createMockSoDepth() as unknown) as Prisma.JsonObject,
	sampleDepths: [
		(createMockSoDepth() as unknown) as Prisma.JsonObject,
		(createMockSoDepth() as unknown) as Prisma.JsonObject,
	],
	referenceIds: [
		{
			id: '987',
			owner: 'soilstack-draft',
			value: 'reference-id-value',
			createdAt: new Date(0),
			updatedAt: new Date(0),
			fieldId: null,
			samplingCollectionId: 'sampling-collection-id',
		},
	],
	createdAt: new Date(0),
	updatedAt: new Date(0),
	externalCreatedAt: new Date(0),
	externalUpdatedAt: new Date(0),
});

export const createMockPrismaStratificationJob = ({
	jobResult = null,
	error = null,
	closedReason = null,
	externalJobStatus = null,
	stratificationId = null,
	isClosed = false,
	autoApprove = false,
}: Partial<PrismaStratificationJob> = {}): PrismaStratificationJob => ({
	id: 'stratification-job-id',
	jobId: 'stratification-job-job-id',
	areaId: 'area-id',
	error,
	isClosed,
	closedReason,
	externalJobStatus,
	jobResult,
	stratificationConfig: JSON.stringify(createMockStratificationConfig()),
	stratificationId,
	autoApprove,
	createdAt: new Date(0),
	updatedAt: new Date(0),
});
