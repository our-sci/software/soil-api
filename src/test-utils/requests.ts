import request from 'supertest';
import { paths } from '../types/generated/openapi-types';
import { getDefaultAdminBearerToken } from './e2e';
import {
	createMockPopulatedField,
	createMockPopulatedSamplingCollection,
	createMockPostStratificationRequest,
} from './mockGenerators';
import { testApp } from './testApp';

const testRequest = request(testApp);

const postMockApiKey = ({
	label = 'mock label',
	authToken,
}: {
	label?: string;
	authToken?: string;
} = {}) =>
	testRequest
		.post('/api-keys')
		.set('Authorization', authToken ?? getDefaultAdminBearerToken())
		.send({ label })
		.expect(201);

const postMockMembership = ({ groupId = 'seed-group-1', userEmail = 'seedUser2@soilstack.io' }) =>
	testRequest
		.post(`/groups/${groupId}/memberships`)
		.set('Authorization', getDefaultAdminBearerToken())
		.send({ userEmail })
		.expect(201);

const postMockGroup = ({
	name = 'Mock Group',
	parentGroupId = 'seed-group-1',
	apiOnly,
	authorizationHeaderValue,
}: {
	name?: string;
	parentGroupId?: string | null;
	apiOnly?: boolean;
	authorizationHeaderValue?: string;
} = {}) => {
	const body: paths['/groups']['post']['requestBody']['content']['application/json'] = { name };
	if (apiOnly !== undefined) {
		body.apiOnly = apiOnly;
	}
	return testRequest
		.post(`/groups${parentGroupId ? `?parentGroupId=${parentGroupId}` : ''}`)
		.set('Authorization', authorizationHeaderValue ?? getDefaultAdminBearerToken())
		.send(body)
		.expect(201);
};

const postMockField = ({ groupId = 'seed-group-1', meta = {} } = {}) =>
	testRequest
		.post(`/fields?groupId=${groupId}`)
		.set('Authorization', getDefaultAdminBearerToken())
		.send(createMockPopulatedField({ meta }))
		.expect(201);

const postMockStratification = ({ areaId, isDemo }: { areaId?: string; isDemo?: boolean } = {}) => {
	const stratificationRequestBody = createMockPostStratificationRequest({ isDemo });

	return testRequest
		.post(`/areas/${areaId}/stratifications`)
		.set('Authorization', getDefaultAdminBearerToken())
		.send(stratificationRequestBody)
		.expect(201);
};

const postMockStratificationAndDependencies = async ({ groupId = 'seed-group-1', isDemo = false } = {}) => {
	const {
		body: {
			areas: [{ id }],
		},
	} = await postMockField({ groupId });
	return postMockStratification({ areaId: id, isDemo });
};

const postMockSamplingCollection = (
	locationCollectionId: string,
	locationId: string,
	referenceId = 'mock reference id'
) => {
	const samplingCollectionRequestBody = createMockPopulatedSamplingCollection([locationId], referenceId);

	return testRequest
		.post(`/location-collections/${locationCollectionId}/sampling-collections`)
		.set('Authorization', getDefaultAdminBearerToken())
		.send(samplingCollectionRequestBody)
		.expect(201);
};

const postMockSamplingCollectionAndDependencies = async ({
	groupId = 'seed-group-1',
	referenceId = 'mock reference id',
} = {}) => {
	const {
		body: {
			locationCollections: [{ id: locationCollectionId }],
			locations: [{ id: locationId }],
		},
	} = await postMockStratificationAndDependencies({ groupId });
	return postMockSamplingCollection(locationCollectionId, locationId, referenceId);
};

export {
	postMockApiKey,
	postMockField,
	postMockGroup,
	postMockMembership,
	postMockSamplingCollection,
	postMockSamplingCollectionAndDependencies,
	postMockStratification,
	postMockStratificationAndDependencies,
};
