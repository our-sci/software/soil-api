// jest-expect-message is wired up in jest.config.js
// This import just lets typescript know that it's being used
import 'jest-expect-message';

// captures validation errors thrown by express-openapi-validator and makes them visible in jest test output
const expectAndValidateResponse = (statusCode: number) => (done?: any) => (err: any, res: any) => {
	expect(res.status, res.text).toEqual(statusCode);
	done?.();
};

const expect200AndValidateResponse = expectAndValidateResponse(200);
const expect201AndValidateResponse = expectAndValidateResponse(201);
const expect204AndValidateResponse = expectAndValidateResponse(204);
const expect400AndValidateResponse = expectAndValidateResponse(400);
const expect401AndValidateResponse = expectAndValidateResponse(401);
const expect403AndValidateResponse = expectAndValidateResponse(403);
const expect404AndValidateResponse = expectAndValidateResponse(404);
const expect409AndValidateResponse = expectAndValidateResponse(409);

export {
	expect200AndValidateResponse,
	expect201AndValidateResponse,
	expect204AndValidateResponse,
	expect400AndValidateResponse,
	expect401AndValidateResponse,
	expect403AndValidateResponse,
	expect404AndValidateResponse,
	expect409AndValidateResponse,
};
