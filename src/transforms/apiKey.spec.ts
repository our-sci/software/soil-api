import { createMockPrismaApiKey } from '../test-utils';
import { prismaApiKeyToApiKey, prismaApiKeyToPostApiKeyResponse } from './apiKey';

describe('prismaApiKeyToPostApiKeyResponse', () => {
	const prismaApiKey = createMockPrismaApiKey();
	const rawApiKey = 'example-api-key';
	const postApiKeyResponse = prismaApiKeyToPostApiKeyResponse(prismaApiKey, rawApiKey);

	it('maps id', () => {
		expect(postApiKeyResponse.id).toEqual(prismaApiKey.id);
	});

	it('maps label', () => {
		expect(postApiKeyResponse.label).toEqual(prismaApiKey.label);
	});

	it('maps rawApiKey', () => {
		expect(postApiKeyResponse.apiKey).toEqual(rawApiKey);
	});
});

describe('prismaApiKeyToApiKey', () => {
	const prismaApiKey = createMockPrismaApiKey();
	const apiKey = prismaApiKeyToApiKey(prismaApiKey);

	it('maps id', () => {
		expect(apiKey.id).toEqual(prismaApiKey.id);
	});

	it('maps label', () => {
		expect(apiKey.label).toEqual(prismaApiKey.label);
	});
});
