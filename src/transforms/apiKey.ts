import { ApiKey as PrismaApiKey } from '@prisma/client';
import { components } from '../types/generated/openapi-types';

type ApiKey = components['schemas']['ApiKey'];
type PostApiKeyResponse = components['schemas']['PostApiKeyResponse'];

const prismaApiKeyToPostApiKeyResponse = ({ id, label }: PrismaApiKey, apiKey: string): PostApiKeyResponse => ({
	id,
	label,
	apiKey,
});

const prismaApiKeyToApiKey = ({ id, label }: PrismaApiKey): ApiKey => ({
	id,
	label,
});

export { prismaApiKeyToApiKey, prismaApiKeyToPostApiKeyResponse };
