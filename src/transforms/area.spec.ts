import { createMockArea, createMockPrismaArea } from '../test-utils';
import { areaToPrismaArea, editableAreaToPrismaAreaUpdateInput, prismaAreaToArea } from './area';

describe('areaToPrismaArea', () => {
	const mockArea = createMockArea();
	const prismaArea = areaToPrismaArea(mockArea, 'group-id');

	describe('type', () => {
		it("is always 'Feature'", () => {
			expect(prismaArea.type).toEqual('Feature');
		});
	});

	describe('geometry', () => {
		it('maps geometry', () => {
			expect(prismaArea.geometry).toEqual(mockArea.geometry);
		});
	});

	describe('properties', () => {
		it('maps properties', () => {
			expect(prismaArea.properties).toEqual(mockArea.properties);
		});
	});

	describe('groupId', () => {
		it('includes groupId', () => {
			expect(prismaArea.groupId).toEqual('group-id');
		});
	});
});

describe('prismaAreaToArea', () => {
	const mockPrismaArea = createMockPrismaArea();
	const actual = prismaAreaToArea(mockPrismaArea);

	describe('id', () => {
		it('maps id', () => {
			expect(actual.id).toEqual(mockPrismaArea.id);
		});
	});

	describe('type', () => {
		it('maps type', () => {
			expect(actual.type).toEqual(mockPrismaArea.type);
		});
	});

	describe('geometry', () => {
		it('maps geometry', () => {
			expect(actual.geometry).toEqual(mockPrismaArea.geometry);
		});
	});

	describe('properties', () => {
		it('maps properties, adding featureOfInterest', () => {
			expect(actual.properties).toEqual({
				...(mockPrismaArea.properties as Record<string, unknown>),
				featureOfInterest: mockPrismaArea.fieldId,
			});
		});
	});

	describe('meta', () => {
		it('includes groupId', () => {
			expect(actual.meta.groupId).toEqual(mockPrismaArea.groupId);
		});

		it('maps updatedAt to submittedAt, converting to ISO 8601', () => {
			expect(actual.meta.submittedAt).toEqual(mockPrismaArea.updatedAt.toISOString());
		});
	});
});

describe('editableAreaToPrismaAreaUpdateInput', () => {
	const mockArea = createMockArea();
	const { type, geometry, properties } = mockArea;
	const editableArea = { type, geometry, properties };
	const existingArea = {
		...createMockPrismaArea(),
		properties: {
			drawn: true,
		},
	};

	const areaUpdateInput = editableAreaToPrismaAreaUpdateInput(existingArea, editableArea);

	describe('geometry', () => {
		it(`maps geometry when present on editableArea`, () => {
			expect(areaUpdateInput).toHaveProperty('geometry', editableArea.geometry);
		});

		it(`omits geometry when not present on editableArea`, () => {
			const { geometry: _, ...editableAreaWithoutProperty } = editableArea;

			const areaUpdateInputWithoutProperty = editableAreaToPrismaAreaUpdateInput(
				existingArea,
				editableAreaWithoutProperty
			);
			expect(areaUpdateInputWithoutProperty).not.toHaveProperty('geometry');
		});
	});

	describe('properties', () => {
		it('preserves properties on the existing area that are not present on the editable area', () => {
			expect(areaUpdateInput.properties).toHaveProperty('drawn', true);
		});

		it('includes properties present on the editable area', () => {
			expect(areaUpdateInput.properties).toHaveProperty('name', editableArea.properties?.name);
			expect(areaUpdateInput.properties).toHaveProperty('description', editableArea.properties?.description);
		});

		it('preserves properties on the existing area when the editable area has no properties', () => {
			const areaUpdateInputWithoutProperties = editableAreaToPrismaAreaUpdateInput(existingArea, {
				...editableArea,
				properties: undefined,
			});
			expect(areaUpdateInputWithoutProperties.properties).toHaveProperty('drawn', true);
		});
	});
});
