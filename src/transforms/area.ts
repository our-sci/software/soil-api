import { Area as PrismaArea, Prisma } from '@prisma/client';
import { components } from '../types/generated/openapi-types';

type RequestArea = components['schemas']['Area'];
type ResponseArea = components['schemas']['ResponseArea'];
type EditableArea = components['schemas']['EditableArea'];
type AreaGeometry = components['schemas']['Polygon'] | components['schemas']['MultiPolygon'];

const areaToPrismaArea = (area: RequestArea, groupId: string): Prisma.AreaCreateManyFieldInput => ({
	type: 'Feature',
	geometry: area.geometry,
	properties: area.properties ?? {},
	groupId,
});

const prismaAreaToArea = (area: PrismaArea): ResponseArea => ({
	id: area.id,
	type: 'Feature',
	geometry: area.geometry as AreaGeometry,
	properties: {
		...(area.properties as Record<string, unknown>),
		featureOfInterest: area.fieldId,
	},
	meta: {
		groupId: area.groupId,
		submittedAt: area.updatedAt.toISOString(),
	},
});

const editableAreaToPrismaAreaUpdateInput = (
	existingArea: PrismaArea,
	editableArea: EditableArea
): Prisma.AreaUpdateInput => {
	const areaUpdateInput: Prisma.AreaUpdateInput = {};
	if (editableArea.geometry) {
		areaUpdateInput.geometry = editableArea.geometry;
	}

	areaUpdateInput.properties = editableArea.properties
		? {
				...(existingArea.properties as Record<string, unknown>),
				...editableArea.properties,
		  }
		: (existingArea.properties as Prisma.InputJsonValue);

	return areaUpdateInput;
};

export { areaToPrismaArea, editableAreaToPrismaAreaUpdateInput, prismaAreaToArea };
