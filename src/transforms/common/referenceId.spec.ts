import { ReferenceId as PrismaReferenceId } from '@prisma/client';
import { prismaReferenceIdToReferenceId, referenceIdToPrismaReferenceId } from './referenceId';

describe('referenceIdToPrismaReferenceId', () => {
	// TODO: use referenceId type and remove 'any' once referenceId is defined in open api schema
	const mockReferenceId = {
		owner: 'soilstack-draft' as any,
		id: '123456',
	};

	const prismaReferenceId = referenceIdToPrismaReferenceId(mockReferenceId);

	it('maps owner to owner', () => {
		expect(prismaReferenceId.owner).toEqual(mockReferenceId.owner);
	});

	it('maps id to value', () => {
		expect(prismaReferenceId.value).toEqual(mockReferenceId.id);
	});
});

describe('prismaReferenceIdToReferenceId', () => {
	const mockPrismaReferenceId: PrismaReferenceId = {
		id: 'mock id',
		owner: 'soilstack-draft',
		value: '123456',
		createdAt: new Date(),
		updatedAt: new Date(),
		fieldId: 'mock field id',
		samplingCollectionId: null,
	};

	const referenceId = prismaReferenceIdToReferenceId(mockPrismaReferenceId);

	it('maps owner to owner', () => {
		expect(referenceId.owner).toEqual(mockPrismaReferenceId.owner);
	});

	it('maps value to id', () => {
		expect(referenceId.id).toEqual(mockPrismaReferenceId.value);
	});
});
