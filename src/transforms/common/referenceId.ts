import { Prisma, ReferenceId as PrismaReferenceId } from '@prisma/client';
import { components } from '../../types/generated/openapi-types';

type ResourceReferenceId = components['schemas']['ResourceReferenceIds'][0];

const referenceIdToPrismaReferenceId = (referenceId: ResourceReferenceId): Prisma.ReferenceIdCreateManyFieldInput => ({
	owner: referenceId.owner,
	value: referenceId.id,
});

const prismaReferenceIdToReferenceId = (prismaReferenceId: PrismaReferenceId): ResourceReferenceId => ({
	owner: prismaReferenceId.owner as ResourceReferenceId['owner'],
	id: prismaReferenceId.value,
});

export { prismaReferenceIdToReferenceId, referenceIdToPrismaReferenceId };
