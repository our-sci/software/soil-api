import {
	createMockAddress,
	createMockArea,
	createMockContactPoint,
	createMockField,
	createMockPrismaField,
} from '../test-utils';
import { components } from '../types/generated/openapi-types';
import {
	editableFieldToPrismaFieldUpdateInput,
	populatedFieldRequestToPrismaFieldCreateInput,
	prismaFieldToField,
} from './field';

type PopulatedField = components['schemas']['PopulatedField'];
type EditableField = components['schemas']['EditableField'];
type Address = PopulatedField['address'];

const addressProperties: (keyof NonNullable<Address>)[] = [
	'streetAddress',
	'addressLocality',
	'addressRegion',
	'addressCountry',
	'postalCode',
];

describe('populatedFieldRequestToPrismaFieldCreateInput', () => {
	const mockField = createMockField();
	const mockArea = {
		...createMockArea(),
		properties: {
			...createMockArea().properties,
			drawn: true,
		},
	};
	const mockPopulatedField: PopulatedField = {
		...mockField,
		areas: [mockArea],
	};

	const prismaField = populatedFieldRequestToPrismaFieldCreateInput(mockPopulatedField, 'group-id');

	describe('name', () => {
		it('maps name value', () => {
			expect(prismaField.name).toBe(mockPopulatedField.name);
		});
	});

	describe('alternateName', () => {
		it('maps alternateName value', () => {
			expect(prismaField.alternateName).toBe(mockPopulatedField.alternateName);
		});

		it('maps alternateName to null when absent', () => {
			const fieldWithoutAlternateName = {
				...mockPopulatedField,
				alternateName: undefined,
			};

			const prismaField = populatedFieldRequestToPrismaFieldCreateInput(fieldWithoutAlternateName, 'group-id');

			expect(prismaField.alternateName).toBe(null);
		});
	});

	describe('producerName', () => {
		it('maps producerName value', () => {
			expect(prismaField.producerName).toBe(mockPopulatedField.producerName);
		});

		it('maps producerName to null when absent', () => {
			const fieldWithoutProducerName = {
				...mockPopulatedField,
				producerName: undefined,
			};

			const prismaField = populatedFieldRequestToPrismaFieldCreateInput(fieldWithoutProducerName, 'group-id');

			expect(prismaField.producerName).toBe(null);
		});
	});

	describe('address', () => {
		addressProperties.forEach((property) => {
			it(`maps ${property} to root level of prisma field`, () => {
				expect(prismaField[property]).toBe((mockPopulatedField.address as any)[property]);
			});

			it(`maps ${property} to null when absent`, () => {
				const fieldWithoutAddressProperty = {
					...mockPopulatedField,
					address: {
						...mockPopulatedField.address,
						[property]: undefined,
					},
				};

				const prismaField = populatedFieldRequestToPrismaFieldCreateInput(fieldWithoutAddressProperty, 'group-id');

				expect(prismaField[property]).toBe(null);
			});
		});
	});

	describe('contactPoints', () => {
		it('maps contactPoints', () => {
			expect(prismaField.contactPoints).toEqual(mockPopulatedField.contactPoints);
		});
	});

	describe('group', () => {
		it('connects group', () => {
			expect(prismaField.group).toEqual({
				connect: { id: 'group-id' },
			});
		});
	});
});

describe('prismaFieldToField', () => {
	const mockPrismaField = createMockPrismaField();
	const field = prismaFieldToField(mockPrismaField);

	it('maps id to id', () => {
		expect(field.id).toEqual(mockPrismaField.id);
	});

	it('maps name to name', () => {
		expect(field.name).toEqual(mockPrismaField.name);
	});

	it('maps alternateName to alternateName', () => {
		expect(field.alternateName).toEqual(mockPrismaField.alternateName);
	});

	it('maps alternateName to empty string when alternateName is null', () => {
		const fieldWithNullAltName = {
			...mockPrismaField,
			alternateName: null,
		};

		const field = prismaFieldToField(fieldWithNullAltName);

		expect(field.alternateName).toEqual('');
	});

	it('maps producerName to producerName', () => {
		expect(field.producerName).toEqual(mockPrismaField.producerName);
	});

	it('maps producerName to empty string when producerName is null', () => {
		const fieldWithNullProducerName = {
			...mockPrismaField,
			producerName: null,
		};

		const field = prismaFieldToField(fieldWithNullProducerName);

		expect(field.producerName).toEqual('');
	});

	it('maps streetAddress to address.streetAddress', () => {
		expect(field.address.streetAddress).toEqual(mockPrismaField.streetAddress);
	});

	it('maps streetAddress to empty string when streetAddress is null', () => {
		const fieldWithNullStreetAddress = {
			...mockPrismaField,
			streetAddress: null,
		};

		const field = prismaFieldToField(fieldWithNullStreetAddress);

		expect(field.address.streetAddress).toEqual('');
	});

	it('maps addressLocality to address.addressLocality', () => {
		expect(field.address.addressLocality).toEqual(mockPrismaField.addressLocality);
	});

	it('maps addressLocality to empty string when addressLocality is null', () => {
		const fieldWithNullAddressLocality = {
			...mockPrismaField,
			addressLocality: null,
		};

		const field = prismaFieldToField(fieldWithNullAddressLocality);

		expect(field.address.addressLocality).toEqual('');
	});

	it('maps addressRegion to address.addressRegion', () => {
		expect(field.address.addressRegion).toEqual(mockPrismaField.addressRegion);
	});

	it('maps addressRegion to empty string when addressRegion is null', () => {
		const fieldWithNullAddressRegion = {
			...mockPrismaField,
			addressRegion: null,
		};

		const field = prismaFieldToField(fieldWithNullAddressRegion);

		expect(field.address.addressRegion).toEqual('');
	});

	it('maps addressCountry to address.addressCountry', () => {
		expect(field.address.addressCountry).toEqual(mockPrismaField.addressCountry);
	});

	it('maps addressCountry to empty string when addressCountry is null', () => {
		const fieldWithNullAddressCountry = {
			...mockPrismaField,
			addressCountry: null,
		};

		const field = prismaFieldToField(fieldWithNullAddressCountry);

		expect(field.address.addressCountry).toEqual('');
	});

	it('maps postalCode to address.postalCode', () => {
		expect(field.address.postalCode).toEqual(mockPrismaField.postalCode);
	});

	it('maps postalCode to empty string when postalCode is null', () => {
		const fieldWithNullPostalCode = {
			...mockPrismaField,
			postalCode: null,
		};

		const field = prismaFieldToField(fieldWithNullPostalCode);

		expect(field.address.postalCode).toEqual('');
	});

	it('maps contactPoints to contactPoints', () => {
		expect(field.contactPoints).toEqual(mockPrismaField.contactPoints);
	});

	it('maps createdAt to meta.submittedAt', () => {
		expect(field.meta.submittedAt).toEqual(mockPrismaField.createdAt.toISOString());
	});

	it('maps groupId to meta.groupId', () => {
		expect(field.meta.groupId).toEqual(mockPrismaField.groupId);
	});
});

describe('editableFieldToPrismaFieldUpdateInput', () => {
	let mockEditableField: EditableField;
	beforeEach(() => {
		mockEditableField = {
			name: 'edited name',
			producerName: 'edited producer name',
			address: createMockAddress(),
			contactPoints: [createMockContactPoint()],
		};
	});

	describe('name', () => {
		it('maps name to name', () => {
			const prismaFieldUpdateInput = editableFieldToPrismaFieldUpdateInput(mockEditableField);

			expect(prismaFieldUpdateInput.name).toEqual(mockEditableField.name);
		});

		it('omits name when name is not present', () => {
			delete mockEditableField.name;

			const prismaFieldUpdateInput = editableFieldToPrismaFieldUpdateInput(mockEditableField);

			expect(prismaFieldUpdateInput).not.toHaveProperty('name');
		});
	});

	describe('producerName', () => {
		it('maps producerName to producerName', () => {
			const prismaFieldUpdateInput = editableFieldToPrismaFieldUpdateInput(mockEditableField);

			expect(prismaFieldUpdateInput.producerName).toEqual(mockEditableField.producerName);
		});

		it('omits producerName when producerName is not present', () => {
			delete mockEditableField.producerName;

			const prismaFieldUpdateInput = editableFieldToPrismaFieldUpdateInput(mockEditableField);

			expect(prismaFieldUpdateInput).not.toHaveProperty('producerName');
		});
	});

	describe('address', () => {
		it(`omits address when address is not present`, () => {
			delete mockEditableField.address;

			const prismaFieldUpdateInput = editableFieldToPrismaFieldUpdateInput(mockEditableField);

			expect(prismaFieldUpdateInput).not.toHaveProperty('address');
		});

		addressProperties.forEach((addressProperty) => {
			describe(addressProperty, () => {
				it(`maps ${addressProperty} to ${addressProperty}`, () => {
					const prismaFieldUpdateInput = editableFieldToPrismaFieldUpdateInput(mockEditableField);

					expect(prismaFieldUpdateInput[addressProperty]).toEqual(mockEditableField.address?.[addressProperty]);
				});

				it(`maps ${addressProperty} to null when ${addressProperty} is empty string`, () => {
					const editableFieldWithEmptyAddressProperty = {
						...mockEditableField,
						address: {
							...mockEditableField.address,
							[addressProperty]: '',
						},
					};

					const prismaFieldUpdateInput = editableFieldToPrismaFieldUpdateInput(editableFieldWithEmptyAddressProperty);

					expect(prismaFieldUpdateInput[addressProperty]).toBeNull();
				});
			});
		});
	});

	describe('contactPoints', () => {
		it(`maps contactPoints to contactPoints`, () => {
			const prismaFieldUpdateInput = editableFieldToPrismaFieldUpdateInput(mockEditableField);

			expect(prismaFieldUpdateInput.contactPoints).toEqual(mockEditableField.contactPoints);
		});

		it(`omits contactPoints when contactPoints is not present`, () => {
			delete mockEditableField.contactPoints;

			const prismaFieldUpdateInput = editableFieldToPrismaFieldUpdateInput(mockEditableField);

			expect(prismaFieldUpdateInput).not.toHaveProperty('contactPoints');
		});
	});
});
