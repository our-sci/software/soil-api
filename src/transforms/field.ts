import { Prisma } from '@prisma/client';
import { components } from '../types/generated/openapi-types';
import { PrismaFlatField, PrismaPopulatedField } from '../types/prisma';
import { prismaReferenceIdToReferenceId } from './common';

type PopulatedField = components['schemas']['PopulatedField'];
type ResponseField = components['schemas']['ResponseField'];
type EditableField = components['schemas']['EditableField'];
type ContactPoint = components['schemas']['ContactPoint'];

const prismaFieldToField = (prismaField: PrismaPopulatedField | PrismaFlatField): ResponseField => ({
	id: prismaField.id,
	name: prismaField.name,
	alternateName: prismaField.alternateName ?? '',
	producerName: prismaField.producerName ?? '',
	address: {
		streetAddress: prismaField.streetAddress ?? '',
		addressLocality: prismaField.addressLocality ?? '',
		addressRegion: prismaField.addressRegion ?? '',
		addressCountry: prismaField.addressCountry ?? '',
		postalCode: prismaField.postalCode ?? '',
	},
	// TODO: remove type cast here once ContactPoint has its own Prisma model
	contactPoints: prismaField.contactPoints as ContactPoint[],
	areas: prismaField.areas.map(({ id }) => id),
	meta: {
		referenceIds: prismaField.referenceIds.map(prismaReferenceIdToReferenceId),
		submittedAt: prismaField.createdAt.toISOString(),
		groupId: prismaField.groupId,
	},
});

const populatedFieldRequestToPrismaFieldCreateInput = (
	populatedField: PopulatedField,
	groupId: string
): Prisma.FieldCreateInput => ({
	name: populatedField.name,
	alternateName: populatedField.alternateName ?? null,
	producerName: populatedField.producerName ?? null,
	streetAddress: populatedField.address?.streetAddress ?? null,
	addressLocality: populatedField.address?.addressLocality ?? null,
	addressRegion: populatedField.address?.addressRegion ?? null,
	addressCountry: populatedField.address?.addressCountry ?? null,
	postalCode: populatedField.address?.postalCode ?? null,
	contactPoints: populatedField.contactPoints ?? [],
	group: { connect: { id: groupId } },
});

const editableFieldToPrismaFieldUpdateInput = (editableField: EditableField): Prisma.FieldUpdateInput => {
	const updateInput: Prisma.FieldUpdateInput = {};
	if (editableField.name) {
		updateInput.name = editableField.name;
	}
	if (editableField.producerName) {
		updateInput.producerName = editableField.producerName;
	}
	if (editableField.address) {
		updateInput.streetAddress = editableField.address.streetAddress || null;
		updateInput.addressLocality = editableField.address.addressLocality || null;
		updateInput.addressRegion = editableField.address.addressRegion || null;
		updateInput.addressCountry = editableField.address.addressCountry || null;
		updateInput.postalCode = editableField.address.postalCode || null;
	}
	if (editableField.contactPoints) {
		updateInput.contactPoints = editableField.contactPoints;
	}
	return updateInput;
};

export { editableFieldToPrismaFieldUpdateInput, populatedFieldRequestToPrismaFieldCreateInput, prismaFieldToField };
