import { Group as PrismaGroup } from '@prisma/client';
import { components } from '../types/generated/openapi-types';

const slugify = (str: string): string =>
	str
		.toLowerCase()
		.replace(/[^a-z0-9]+/g, '-')
		.replace(/-$/, '')
		.replace(/^-/, '');

const getGroupPath = (group: PrismaGroup, groups: PrismaGroup[], basePath?: string): string => {
	const parentGroup = groups.find((g) => g.id === group.parentId);

	if (!parentGroup) {
		if (!basePath) {
			return `/${slugify(group.name)}/`;
		}
		return `/${basePath}/`;
	}

	return getGroupPath(parentGroup, groups, `${slugify(parentGroup.name)}/${basePath ?? slugify(group.name)}`);
};

const prismaGroupToGroup = (group: PrismaGroup, groups: PrismaGroup[]): components['schemas']['ResponseGroup'] => ({
	id: group.id,
	name: group.name,
	path: getGroupPath(group, groups),
	apiOnly: group.apiOnly,
	meta: {
		submittedAt: group.createdAt.toISOString(),
	},
});

export { getGroupPath, prismaGroupToGroup, slugify };
