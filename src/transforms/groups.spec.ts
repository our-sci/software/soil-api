import { createMockPrismaGroup } from '../test-utils';
import { getGroupPath, prismaGroupToGroup, slugify } from './group';

describe('slugify', () => {
	const slug = slugify('Group Name 1');

	it('slugifies name', () => {
		expect(slug).toBe('group-name-1');
	});
});

describe('getGroupPath', () => {
	it('returns group name as path if no parent group is present in groups', () => {
		const group1 = createMockPrismaGroup({
			parentId: 'parent-group',
		});
		const group2 = createMockPrismaGroup({
			id: 'not-parent-group',
		});
		const group3 = createMockPrismaGroup({
			id: 'not-parent-group-2',
		});

		const actual = getGroupPath(group1, [group1, group2, group3]);

		expect(actual).toBe('/group-name/');
	});

	it('returns group name as path if the group has no parent group', () => {
		const group1 = createMockPrismaGroup({
			parentId: null,
		});
		const group2 = createMockPrismaGroup({
			id: 'not-parent-group',
		});
		const group3 = createMockPrismaGroup({
			id: 'not-parent-group-2',
		});

		const actual = getGroupPath(group1, [group1, group2, group3]);

		expect(actual).toBe('/group-name/');
	});

	it('returns path with group name and all ancestor group names when ancestor groups are present', () => {
		const group1 = createMockPrismaGroup({
			parentId: 'parent-group',
		});
		const group2 = createMockPrismaGroup({
			id: 'parent-group',
			name: 'Parent Group Name',
			parentId: 'grandparent-group',
		});
		const group3 = createMockPrismaGroup({
			id: 'grandparent-group',
			name: 'Grandparent Group Name',
			parentId: null,
		});

		const actual = getGroupPath(group1, [group1, group2, group3]);

		expect(actual).toBe('/grandparent-group-name/parent-group-name/group-name/');
	});
});

describe('prismaGroupToGroup', () => {
	const prismaGroup1 = createMockPrismaGroup({
		parentId: 'parent-group',
	});
	const prismaGroup2 = createMockPrismaGroup({
		id: 'parent-group',
		name: 'parent-group-name',
		parentId: 'grandparent-group',
	});
	const prismaGroup3 = createMockPrismaGroup({
		id: 'grandparent-group',
		name: 'grandparent-group-name',
		parentId: null,
	});

	const actual = prismaGroupToGroup(prismaGroup1, [prismaGroup1, prismaGroup2, prismaGroup3]);

	describe('id', () => {
		it('maps id to id', () => {
			expect(actual).toHaveProperty('id', prismaGroup1.id);
		});
	});

	describe('name', () => {
		it('maps name to name', () => {
			expect(actual).toHaveProperty('name', prismaGroup1.name);
		});
	});

	describe('path', () => {
		it('derives group path', () => {
			expect(actual).toHaveProperty('path', '/grandparent-group-name/parent-group-name/group-name/');
		});
	});

	describe('apiOnly', () => {
		it('maps apiOnly to apiOnly', () => {
			expect(actual).toHaveProperty('apiOnly', prismaGroup1.apiOnly);
		});
	});

	describe('meta', () => {
		it('maps createdAt to submittedAt', () => {
			expect(actual.meta).toHaveProperty('submittedAt', prismaGroup1.createdAt.toISOString());
		});
	});
});
