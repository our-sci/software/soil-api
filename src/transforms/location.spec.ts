import { Prisma } from '@prisma/client';
import { createMockLocation, createMockPrismaLocation } from '../test-utils';
import { locationToPrismaCreateInput, prismaLocationToLocation } from './location';

describe('locationToPrismaCreateInput', () => {
	const location = createMockLocation();
	const prismaCreateInput = locationToPrismaCreateInput(location, 'group-id');

	describe('type', () => {
		it('is always "Feature"', () => {
			expect(prismaCreateInput.type).toEqual('Feature');
		});
	});

	describe('geometry', () => {
		it('maps geometry', () => {
			expect(prismaCreateInput.geometry).toEqual(location.geometry);
		});
	});

	describe('properties', () => {
		it('maps properties when present', () => {
			expect(prismaCreateInput.properties).toEqual(location.properties);
		});

		it('maps properties to Prisma.DbNull when not present', () => {
			const { properties, ...locationWithoutProperties } = location;

			const prismaCreateInputWithoutProperties = locationToPrismaCreateInput(locationWithoutProperties, 'group-id');

			expect(prismaCreateInputWithoutProperties.properties).toBe(Prisma.DbNull);
		});
	});

	describe('groupId', () => {
		it('maps groupId', () => {
			expect(prismaCreateInput.groupId).toEqual('group-id');
		});
	});
});

describe('prismalocationToLocation', () => {
	const mockPrismaLocation = createMockPrismaLocation();
	const location = prismaLocationToLocation(mockPrismaLocation);

	describe('id', () => {
		it('maps id', () => {
			expect(location.id).toEqual(mockPrismaLocation.id);
		});
	});

	describe('type', () => {
		it("is 'Feature'", () => {
			expect(location.type).toEqual('Feature');
		});
	});

	describe('geometry', () => {
		it('maps geometry', () => {
			expect(location.geometry).toEqual(mockPrismaLocation.geometry);
		});
	});

	describe('properties', () => {
		it('maps properties when present', () => {
			expect(location.properties).toEqual(mockPrismaLocation.properties);
		});

		it('maps properties to empty object when not present', () => {
			const mockPrismaLocationWithoutProperties = {
				...mockPrismaLocation,
				properties: null,
			};

			const locationWithoutProperties = prismaLocationToLocation(mockPrismaLocationWithoutProperties);

			expect(locationWithoutProperties.properties).toEqual({});
		});
	});

	describe('meta', () => {
		describe('submittedAt', () => {
			it('sets submittedAt to createdAt in ISO 8601', () => {
				expect(location.meta?.submittedAt).toEqual(mockPrismaLocation.createdAt.toISOString());
			});
		});

		describe('groupId', () => {
			it('maps groupId', () => {
				expect(location.meta?.groupId).toEqual(mockPrismaLocation.groupId);
			});
		});
	});
});
