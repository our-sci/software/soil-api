import { Location as PrismaLocation, Prisma } from '@prisma/client';
import { components } from '../types/generated/openapi-types';

type Location = components['schemas']['Location'];
const locationToPrismaCreateInput = (
	location: Location,
	groupId: string
): Prisma.LocationCreateManyLocationCollectionInput => {
	return {
		type: 'Feature',
		geometry: location.geometry,
		properties: ((location.properties as unknown) as Prisma.InputJsonObject) ?? Prisma.DbNull,
		groupId,
	};
};

type ResponseLocation = components['schemas']['ResponseLocation'];
type Point = components['schemas']['Point'];
const prismaLocationToLocation = (prismaLocation: PrismaLocation): ResponseLocation => {
	return {
		id: prismaLocation.id,
		type: 'Feature',
		geometry: prismaLocation.geometry as Point,
		properties: (prismaLocation.properties as { [key: string]: unknown }) ?? {},
		meta: {
			submittedAt: prismaLocation.createdAt.toISOString(),
			groupId: prismaLocation.groupId,
		},
	};
};

export { locationToPrismaCreateInput, prismaLocationToLocation };
