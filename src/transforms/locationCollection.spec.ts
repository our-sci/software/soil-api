import { createMockPrismaPopulatedLocationCollection } from '../test-utils';
import {
	populatedLocationCollectionToPrismaCreateInput,
	prismaLocationCollectionToLocationCollection,
} from './locationCollection';

describe('populatedLocationCollectionToPrismaCreateInput', () => {
	const prismaCreateInput = populatedLocationCollectionToPrismaCreateInput('group-id', 'field-id', 'area-id');

	describe('area', () => {
		it('creates connect argument referencing areaId', () => {
			expect(prismaCreateInput.area).toEqual({
				connect: { id: 'area-id' },
			});
		});
	});

	describe('field', () => {
		it('creates connect argument referencing fieldId', () => {
			expect(prismaCreateInput.field).toEqual({
				connect: { id: 'field-id' },
			});
		});
	});

	describe('group', () => {
		it('creates connect argument referencing groupId', () => {
			expect(prismaCreateInput.group).toEqual({
				connect: { id: 'group-id' },
			});
		});
	});
});

describe('prismaLocationCollectionToLocationCollection', () => {
	const mockPrismaLocationCollection = createMockPrismaPopulatedLocationCollection();
	const locationCollection = prismaLocationCollectionToLocationCollection(mockPrismaLocationCollection);

	describe('id', () => {
		it('maps id value', () => {
			expect(locationCollection.id).toEqual(mockPrismaLocationCollection.id);
		});
	});

	describe('object', () => {
		it('is the areaId', () => {
			expect(locationCollection.object).toEqual(mockPrismaLocationCollection.areaId);
		});
	});

	describe('resultOf', () => {
		it('is the stratificationId', () => {
			expect(locationCollection.resultOf).toEqual(mockPrismaLocationCollection.stratificationId);
		});
	});

	describe('featureOfInterest', () => {
		it('is the fieldId', () => {
			expect(locationCollection.featureOfInterest).toEqual(mockPrismaLocationCollection.fieldId);
		});
	});

	describe('features', () => {
		it('is an array of the location ids', () => {
			expect(locationCollection.features).toEqual(mockPrismaLocationCollection.locations.map((loc) => loc.id));
		});
	});

	describe('meta', () => {
		describe('submittedAt', () => {
			it('is createdAt in ISO 8601 format', () => {
				expect(locationCollection.meta?.submittedAt).toEqual(mockPrismaLocationCollection.createdAt.toISOString());
			});
		});

		describe('groupId', () => {
			it('maps groupId', () => {
				expect(locationCollection.meta?.groupId).toEqual(mockPrismaLocationCollection.groupId);
			});
		});
	});
});
