import { Prisma } from '@prisma/client';
import { components } from '../types/generated/openapi-types';
import { PrismaFlatLocationCollection, PrismaPopulatedLocationCollection } from '../types/prisma';

type ResponseLocationCollection = components['schemas']['ResponseLocationCollection'];

const populatedLocationCollectionToPrismaCreateInput = (
	groupId: string,
	fieldId: string,
	areaId: string
): Prisma.LocationCollectionCreateWithoutStratificationInput => {
	return {
		area: { connect: { id: areaId } },
		field: { connect: { id: fieldId } },
		group: { connect: { id: groupId } },
	};
};

const prismaLocationCollectionToLocationCollection = (
	prismaLocationCollection: PrismaPopulatedLocationCollection | PrismaFlatLocationCollection
): ResponseLocationCollection => {
	return {
		id: prismaLocationCollection.id,
		object: prismaLocationCollection.areaId,
		resultOf: prismaLocationCollection.stratificationId,
		featureOfInterest: prismaLocationCollection.fieldId,
		features: prismaLocationCollection.locations.map((location) => location.id),
		meta: {
			submittedAt: prismaLocationCollection.createdAt.toISOString(),
			groupId: prismaLocationCollection.groupId,
		},
	};
};

export { populatedLocationCollectionToPrismaCreateInput, prismaLocationCollectionToLocationCollection };
