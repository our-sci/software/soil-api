import { createMockPrismaMembershipWithUser } from '../test-utils';
import { components } from '../types/generated/openapi-types';
import { PrismaMembershipWithUser } from '../types/prisma';
import { prismaMembershipToMembership } from './membership';

describe('prismaMembershipToMembership', () => {
	let prismaMembership: PrismaMembershipWithUser;
	let responseMembership: components['schemas']['ResponseMembership'];

	beforeAll(async () => {
		prismaMembership = await createMockPrismaMembershipWithUser();
		responseMembership = prismaMembershipToMembership(prismaMembership);
	});

	it('maps id', () => {
		expect(responseMembership.id).toEqual(prismaMembership.id);
	});

	it('maps groupId', () => {
		expect(responseMembership.groupId).toEqual(prismaMembership.groupId);
	});

	it('maps role', () => {
		expect(responseMembership.role).toEqual(prismaMembership.role);
	});

	it('maps userId to user.id', () => {
		expect(responseMembership.user.id).toEqual(prismaMembership.userId);
	});

	it('maps user.email', () => {
		expect(responseMembership.user.email).toEqual(prismaMembership.user.email);
	});

	it('maps user.seen', () => {
		expect(responseMembership.user.seen).toEqual(prismaMembership.user.seen);
	});
});
