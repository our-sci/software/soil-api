import { components } from '../types/generated/openapi-types';
import { PrismaMembershipWithUser } from '../types/prisma';

const prismaMembershipToMembership = (
	prismaMembership: PrismaMembershipWithUser
): components['schemas']['ResponseMembership'] => ({
	id: prismaMembership.id,
	groupId: prismaMembership.groupId,
	role: prismaMembership.role,
	user: {
		id: prismaMembership.userId,
		email: prismaMembership.user.email,
		seen: prismaMembership.user.seen,
	},
});

export { prismaMembershipToMembership };
