import { createMockPrismaSample, createMockSample } from '../test-utils';
import { components } from '../types/generated/openapi-types';
import { editableSampleToPrismaSampleUpdateInput, prismaSampleToSample, sampleToPrismaCreateInput } from './sample';

describe('sampleToPrismaCreateInput', () => {
	const mockSample = createMockSample(
		{},
		{ omitSampleOf: true, omitResultOf: true }
	) as components['schemas']['Sample'];
	const prismaCreateInput = sampleToPrismaCreateInput(mockSample, 'group-id', 'field-id');

	describe('name', () => {
		it('maps name to name', () => {
			expect(prismaCreateInput.name).toEqual(mockSample.name);
		});
	});

	describe('soDepth', () => {
		it('maps soDepth to soDepth when present', () => {
			expect(prismaCreateInput.soDepth).toEqual(mockSample.soDepth);
		});
	});

	describe('groupId', () => {
		it('sets groupId', () => {
			expect(prismaCreateInput.groupId).toEqual('group-id');
		});
	});

	describe('fieldId', () => {
		it('sets fieldId', () => {
			expect(prismaCreateInput.fieldId).toEqual('field-id');
		});
	});
});

describe('prismaSampletoSample', () => {
	const mockPrismaSample = createMockPrismaSample();
	const responseSample = prismaSampleToSample(mockPrismaSample);

	describe('soDepth', () => {
		it('maps soDepth to soDepth when present', () => {
			expect(responseSample.soDepth).toEqual(mockPrismaSample.soDepth);
		});

		it('sets soDepth to {} when soDepth is null', () => {
			const prismaSampleWithoutSoDepth = {
				...mockPrismaSample,
				soDepth: null,
			};

			const responseSampleWithoutSoDepth = prismaSampleToSample(prismaSampleWithoutSoDepth);

			expect(responseSampleWithoutSoDepth.soDepth).toEqual({});
		});
	});

	describe('meta', () => {
		it('maps createdAt to meta.submittedAt', () => {
			expect(responseSample.meta?.submittedAt).toEqual(mockPrismaSample.createdAt.toISOString());
		});

		it('maps groupId', () => {
			expect(responseSample.meta?.groupId).toEqual(mockPrismaSample.groupId);
		});
	});

	const directMappings: [keyof typeof responseSample, keyof typeof mockPrismaSample][] = [
		['id', 'id'],
		['name', 'name'],
		['resultOf', 'samplingId'],
		['sampleOf', 'fieldId'],
	];

	directMappings.forEach(([responseKey, prismaKey]) => {
		it(`maps ${prismaKey} to ${responseKey}`, () => {
			expect(responseSample[responseKey]).toEqual(mockPrismaSample[prismaKey]);
		});
	});
});

describe('editableSampleToPrismaSampleUpdateInput', () => {
	type EditableSample = components['schemas']['EditableSample'];
	const mockEditableSample: EditableSample = {
		name: 'updated name',
		soDepth: {
			minValue: { value: 555, unit: 'unit:CM' },
			maxValue: { value: 666, unit: 'unit:CM' },
		},
	};
	const prismaUpdateInput = editableSampleToPrismaSampleUpdateInput(mockEditableSample);

	const optionalProperties: (keyof EditableSample)[] = ['name', 'soDepth'];

	optionalProperties.forEach((property) => {
		describe(property, () => {
			it(`maps ${property} to ${property} when present`, () => {
				expect(prismaUpdateInput.name).toBe(mockEditableSample.name);
			});

			it(`omits ${property} when not present`, () => {
				const { [property]: _, ...editableSampleWithoutProperty } = mockEditableSample;

				const prismaUpdateInputWithoutProperty = editableSampleToPrismaSampleUpdateInput(editableSampleWithoutProperty);

				expect(prismaUpdateInputWithoutProperty).not.toHaveProperty(property);
			});
		});
	});
});
