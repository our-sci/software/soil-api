import { Prisma, Sample as PrismaSample } from '@prisma/client';
import { components } from '../types/generated/openapi-types';

const sampleToPrismaCreateInput = (
	sample: components['schemas']['Sample'],
	groupId: string,
	fieldId: string
): Prisma.SampleCreateManySamplingInput => ({
	name: sample.name,
	soDepth: sample.soDepth,
	groupId,
	fieldId,
});

const prismaSampleToSample = (prismaSample: PrismaSample): components['schemas']['ResponseSample'] => ({
	id: prismaSample.id,
	name: prismaSample.name,
	soDepth: (prismaSample.soDepth as components['schemas']['SoDepth']) ?? {},
	resultOf: prismaSample.samplingId,
	sampleOf: prismaSample.fieldId,
	meta: {
		submittedAt: prismaSample.createdAt.toISOString(),
		groupId: prismaSample.groupId,
	},
});

const editableSampleToPrismaSampleUpdateInput = (
	editableSample: components['schemas']['EditableSample']
): Prisma.SampleUpdateInput => {
	const updateInput: Prisma.SampleUpdateInput = {};
	if (editableSample.name) {
		updateInput.name = editableSample.name;
	}
	if (editableSample.soDepth) {
		updateInput.soDepth = editableSample.soDepth;
	}
	return updateInput;
};

export { editableSampleToPrismaSampleUpdateInput, prismaSampleToSample, sampleToPrismaCreateInput };
