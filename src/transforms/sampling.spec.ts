import { createMockPopulatedSamplingCollection, createMockPrismaPopulatedSampling } from '../test-utils';
import { components } from '../types/generated/openapi-types';
import { PrismaPopulatedSampling } from '../types/prisma';
import { prismaSamplingToSampling, samplingToPrismaCreateInput } from './sampling';

describe('samplingToPrismaCreateInput', () => {
	const mockPopulatedSamplingCollection = createMockPopulatedSamplingCollection(['location-id']);
	const mockSampling = mockPopulatedSamplingCollection.members?.[0] as components['schemas']['PopulatedSampling'];
	const prismaCreateInput = samplingToPrismaCreateInput(mockSampling, 'group-id');

	const requiredProperties: [keyof typeof prismaCreateInput, keyof typeof mockSampling][] = [
		['resultTime', 'resultTime'],
		['geometry', 'geometry'],
		['locationId', 'featureOfInterest'],
	];

	requiredProperties.forEach(([prismaProperty, requestProperty]) => {
		describe(prismaProperty, () => {
			it(`maps ${requestProperty} to ${prismaProperty}`, () => {
				expect(prismaCreateInput[prismaProperty]).toEqual(mockSampling[requestProperty]);
			});
		});
	});

	const optionalProperties: [keyof typeof prismaCreateInput, keyof typeof mockSampling, any][] = [
		['comment', 'comment', null],
		['procedures', 'procedures', []],
		['properties', 'properties', {}],
		['soDepth', 'soDepth', {}],
	];

	optionalProperties.forEach(([prismaProperty, requestProperty, defaultValue]) => {
		describe(prismaProperty, () => {
			it(`maps ${requestProperty} to ${prismaProperty} when present`, () => {
				expect(prismaCreateInput[prismaProperty]).toEqual(mockSampling[requestProperty]);
			});

			it(`sets ${prismaProperty} to ${defaultValue} when ${requestProperty} is not present`, () => {
				const requestWithoutProperty = { ...mockSampling };
				delete requestWithoutProperty[requestProperty];

				const prismaCreateInput = samplingToPrismaCreateInput(requestWithoutProperty, 'group-id');

				expect(prismaCreateInput[prismaProperty]).toEqual(defaultValue);
			});
		});
	});

	describe('groupId', () => {
		it('includes groupId', () => {
			expect(prismaCreateInput.groupId).toEqual('group-id');
		});
	});
});

describe('prismaSamplingToSampling', () => {
	const prismaPopulatedSampling = createMockPrismaPopulatedSampling();
	const responseSampling = prismaSamplingToSampling(prismaPopulatedSampling as PrismaPopulatedSampling);

	describe('resultTime', () => {
		it('maps resultTime to resultTime', () => {
			expect(responseSampling.resultTime).toEqual(prismaPopulatedSampling.resultTime.toISOString());
		});
	});

	describe('results', () => {
		it('maps sample ids to results', () => {
			expect(responseSampling.results).toEqual(prismaPopulatedSampling.samples.map((sample) => sample.id));
		});
	});

	describe('meta', () => {
		it('maps createdAt to submittedAt', () => {
			expect(responseSampling.meta.submittedAt).toEqual(prismaPopulatedSampling.createdAt.toISOString());
		});

		it('maps groupId to groupId', () => {
			expect(responseSampling.meta.groupId).toEqual(prismaPopulatedSampling.groupId);
		});
	});

	const directMappings: [keyof typeof responseSampling, keyof typeof prismaPopulatedSampling][] = [
		['id', 'id'],
		['geometry', 'geometry'],
		['featureOfInterest', 'locationId'],
		['memberOf', 'samplingCollectionId'],
	];

	directMappings.forEach(([responseProperty, prismaProperty]) => {
		describe(responseProperty, () => {
			it(`maps ${prismaProperty} to ${responseProperty}`, () => {
				expect(responseSampling[responseProperty]).toEqual(prismaPopulatedSampling[prismaProperty]);
			});
		});
	});

	const optionalMappings: [keyof typeof responseSampling, keyof typeof prismaPopulatedSampling, any][] = [
		['comment', 'comment', ''],
		['procedures', 'procedures', []],
		['properties', 'properties', {}],
		['soDepth', 'soDepth', {}],
	];

	optionalMappings.forEach(([responseProperty, prismaProperty, defaultValue]) => {
		describe(responseProperty, () => {
			it(`maps ${prismaProperty} to ${responseProperty} when present`, () => {
				expect(responseSampling[responseProperty]).toEqual(prismaPopulatedSampling[prismaProperty]);
			});

			it(`sets ${responseProperty} to ${defaultValue} when ${prismaProperty} is null`, () => {
				const prismaSamplingWithoutProperty = {
					...prismaPopulatedSampling,
					[prismaProperty]: null,
				};

				const responseSampling = prismaSamplingToSampling(prismaSamplingWithoutProperty as PrismaPopulatedSampling);

				expect(responseSampling[responseProperty]).toEqual(defaultValue);
			});
		});
	});
});
