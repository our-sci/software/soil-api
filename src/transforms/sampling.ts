import { Prisma } from '@prisma/client';
import { components } from '../types/generated/openapi-types';
import { PrismaFlatSampling, PrismaPopulatedSampling } from '../types/prisma';

const samplingToPrismaCreateInput = (
	sampling: components['schemas']['PopulatedSampling'],
	groupId: string
): Prisma.SamplingUncheckedCreateInput => ({
	resultTime: sampling.resultTime,
	geometry: sampling.geometry,
	locationId: sampling.featureOfInterest,
	comment: sampling.comment ?? null,
	procedures: sampling.procedures ?? [],
	properties: (sampling.properties ?? {}) as Prisma.InputJsonObject,
	soDepth: sampling.soDepth ?? {},
	groupId,
});

const prismaSamplingToSampling = (
	prismaSampling: PrismaPopulatedSampling | PrismaFlatSampling
): components['schemas']['ResponseSampling'] => ({
	id: prismaSampling.id,
	resultTime: prismaSampling.resultTime.toISOString(),
	geometry: prismaSampling.geometry as components['schemas']['Point'],
	featureOfInterest: prismaSampling.locationId,
	results: prismaSampling.samples.map(({ id }) => id),
	comment: prismaSampling.comment ?? '',
	procedures: (prismaSampling.procedures as string[]) ?? [],
	properties: (prismaSampling.properties as { [key: string]: unknown }) ?? {},
	soDepth: (prismaSampling.soDepth as components['schemas']['SoDepth']) ?? {},
	memberOf: prismaSampling.samplingCollectionId,
	meta: {
		submittedAt: prismaSampling.createdAt.toISOString(),
		groupId: prismaSampling.groupId,
	},
});

export { prismaSamplingToSampling, samplingToPrismaCreateInput };
