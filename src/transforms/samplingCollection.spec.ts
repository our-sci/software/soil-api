import { createMockPopulatedSamplingCollection, createMockPrismaPopulatedSamplingCollection } from '../test-utils';
import {
	postSamplingCollectionRequestToPrismaCreateInput,
	prismaSamplingCollectionToSamplingCollection,
} from './samplingCollection';

describe('postSamplingCollectionRequestToPrismaCreateInput', () => {
	const mockPostSamplingCollectionRequest = createMockPopulatedSamplingCollection(['location-id']);
	const prismaCreateInput = postSamplingCollectionRequestToPrismaCreateInput(
		mockPostSamplingCollectionRequest,
		'group-id',
		'field-id',
		'location-collection-id'
	);

	const requiredProperties: [keyof typeof prismaCreateInput, keyof typeof mockPostSamplingCollectionRequest][] = [
		['soDepth', 'soDepth'],
		['sampleDepths', 'sampleDepths'],
		['externalCreatedAt', 'dateCreated'],
		['externalUpdatedAt', 'dateModified'],
	];

	const optionalProperties: [keyof typeof prismaCreateInput, keyof typeof mockPostSamplingCollectionRequest][] = [
		['name', 'name'],
	];

	optionalProperties.forEach(([prismaProperty, requestProperty]) => {
		describe(prismaProperty, () => {
			it(`maps ${requestProperty} to ${prismaProperty} when present`, () => {
				expect(prismaCreateInput[prismaProperty]).toEqual(mockPostSamplingCollectionRequest[requestProperty]);
			});

			it(`sets ${prismaProperty} to null when ${requestProperty} is not present`, () => {
				const requestWithoutProperty = { ...mockPostSamplingCollectionRequest };
				delete requestWithoutProperty[requestProperty];

				const prismaCreateInputWithoutName = postSamplingCollectionRequestToPrismaCreateInput(
					requestWithoutProperty,
					'group-id',
					'field-id',
					'location-collection-id'
				);

				expect(prismaCreateInputWithoutName[prismaProperty]).toBeNull();
			});
		});
	});

	requiredProperties.forEach(([prismaProperty, requestProperty]) => {
		describe(prismaProperty, () => {
			it(`maps ${requestProperty} to ${prismaProperty}`, () => {
				expect(prismaCreateInput[prismaProperty]).toEqual(mockPostSamplingCollectionRequest[requestProperty]);
			});
		});
	});

	describe('fieldId', () => {
		it('includes fieldId', () => {
			expect(prismaCreateInput.fieldId).toEqual('field-id');
		});
	});

	describe('groupId', () => {
		it('includes groupId', () => {
			expect(prismaCreateInput.groupId).toEqual('group-id');
		});
	});

	describe('locationCollectionId', () => {
		it('includes locationCollectionId', () => {
			expect(prismaCreateInput.locationCollectionId).toEqual('location-collection-id');
		});
	});
});

describe('prismaSamplingCollectionToSamplingCollection', () => {
	const prismaPopulatedSamplingCollection = createMockPrismaPopulatedSamplingCollection();
	const responseSamplingCollection = prismaSamplingCollectionToSamplingCollection(prismaPopulatedSamplingCollection);

	describe('members', () => {
		it('maps sampling ids to members', () => {
			expect(responseSamplingCollection.members).toEqual(prismaPopulatedSamplingCollection.samplings.map((s) => s.id));
		});
	});

	describe('dateCreated', () => {
		it('maps externalCreatedAt to dateCreated', () => {
			expect(responseSamplingCollection.dateCreated).toEqual(
				prismaPopulatedSamplingCollection.externalCreatedAt.toISOString()
			);
		});
	});

	describe('dateModified', () => {
		it('maps externalUpdatedAt to dateModified', () => {
			expect(responseSamplingCollection.dateModified).toEqual(
				prismaPopulatedSamplingCollection.externalUpdatedAt.toISOString()
			);
		});
	});

	describe('meta', () => {
		it('maps createdAt to submittedAt', () => {
			expect(responseSamplingCollection.meta.submittedAt).toEqual(
				prismaPopulatedSamplingCollection.createdAt.toISOString()
			);
		});

		it('maps groupId', () => {
			expect(responseSamplingCollection.meta.groupId).toEqual(prismaPopulatedSamplingCollection.groupId);
		});
	});

	const directMappings: [keyof typeof responseSamplingCollection, keyof typeof prismaPopulatedSamplingCollection][] = [
		['id', 'id'],
		['name', 'name'],
		['soDepth', 'soDepth'],
		['sampleDepths', 'sampleDepths'],
		['featureOfInterest', 'fieldId'],
		['object', 'locationCollectionId'],
	];

	directMappings.forEach(([responseProperty, prismaProperty]) => {
		describe(responseProperty, () => {
			it(`maps ${prismaProperty} to ${responseProperty}`, () => {
				expect(responseSamplingCollection[responseProperty]).toEqual(prismaPopulatedSamplingCollection[prismaProperty]);
			});
		});
	});
});
