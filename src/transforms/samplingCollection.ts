import { Prisma } from '@prisma/client';
import { components } from '../types/generated/openapi-types';
import { PrismaFlatSamplingCollection, PrismaPopulatedSamplingCollection } from '../types/prisma';
import { prismaReferenceIdToReferenceId } from './common';

type SoDepth = components['schemas']['SoDepth'];

type PopulatedSamplingCollection = components['schemas']['PopulatedSamplingCollection'];
const postSamplingCollectionRequestToPrismaCreateInput = (
	samplingCollectionRequest: PopulatedSamplingCollection,
	groupId: string,
	fieldId: string,
	locationCollectionId: string
): Prisma.SamplingCollectionUncheckedCreateInput => ({
	name: samplingCollectionRequest.name ?? null,
	fieldId,
	locationCollectionId,
	groupId,
	soDepth: samplingCollectionRequest.soDepth,
	sampleDepths: samplingCollectionRequest.sampleDepths,
	externalCreatedAt: samplingCollectionRequest.dateCreated,
	externalUpdatedAt: samplingCollectionRequest.dateModified,
});

const prismaSamplingCollectionToSamplingCollection = (
	prismaSamplingCollection: PrismaPopulatedSamplingCollection | PrismaFlatSamplingCollection
): components['schemas']['ResponseSamplingCollection'] => ({
	id: prismaSamplingCollection.id,
	name: prismaSamplingCollection.name ?? '',
	members: prismaSamplingCollection.samplings.map(({ id }) => id),
	soDepth: prismaSamplingCollection.soDepth as SoDepth,
	sampleDepths: prismaSamplingCollection.sampleDepths as SoDepth[],
	dateCreated: prismaSamplingCollection.externalCreatedAt.toISOString(),
	dateModified: prismaSamplingCollection.externalUpdatedAt.toISOString(),
	featureOfInterest: prismaSamplingCollection.fieldId,
	object: prismaSamplingCollection.locationCollectionId,
	meta: {
		submittedAt: prismaSamplingCollection.createdAt.toISOString(),
		groupId: prismaSamplingCollection.groupId,
		referenceIds: prismaSamplingCollection.referenceIds.map(prismaReferenceIdToReferenceId),
	},
});

export { postSamplingCollectionRequestToPrismaCreateInput, prismaSamplingCollectionToSamplingCollection };
