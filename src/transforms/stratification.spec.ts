import { Prisma } from '@prisma/client';
import {
	createMockPostStratificationRequest,
	createMockPrismaPopulatedStratification,
	createMockStratificationServiceStratification,
} from '../test-utils';
import { components } from '../types/generated/openapi-types';
import { locationToPrismaCreateInput } from './location';
import { populatedLocationCollectionToPrismaCreateInput } from './locationCollection';
import {
	postStratificationRequestToPrismaCreateInput,
	prismaStratificationToStratification,
	stratificationServiceJobResultToPrismaCreateInput,
} from './stratification';

type Algorithm = components['schemas']['Algorithm'];
const algorithmProperties: (keyof Algorithm)[] = ['name', 'alternateName', 'codeRepository', 'version', 'doi'];

describe('postStratificationRequestToPrismaCreateInput', () => {
	const mockPostStratificationRequest = createMockPostStratificationRequest();
	const prismaStratification = postStratificationRequestToPrismaCreateInput(
		mockPostStratificationRequest,
		'group-id',
		'area-id'
	);

	describe('name', () => {
		it('maps name from request', () => {
			expect(prismaStratification.name).toEqual(mockPostStratificationRequest.stratification.name);
		});
	});

	describe('agent', () => {
		it('maps agent from request', () => {
			expect(prismaStratification.agent).toEqual(mockPostStratificationRequest.stratification.agent);
		});
	});

	describe('provider', () => {
		it('maps provider from request', () => {
			expect(prismaStratification.provider).toEqual(mockPostStratificationRequest.stratification.provider);
		});
	});

	describe('isDemo', () => {
		it('maps isDemo from request when present', () => {
			const mockPostStratificationRequestWithIsDemo = createMockPostStratificationRequest({ isDemo: true });
			const prismaStratificationWithIsDemo = postStratificationRequestToPrismaCreateInput(
				mockPostStratificationRequestWithIsDemo,
				'group-id',
				'area-id'
			);
			expect(prismaStratificationWithIsDemo.isDemo).toEqual(
				mockPostStratificationRequestWithIsDemo.stratification.isDemo
			);
		});

		it('sets isDemo to false when not present', () => {
			expect(prismaStratification.isDemo).toBe(false);
		});
	});

	describe('input', () => {
		it('maps input from request when present', () => {
			expect(prismaStratification.input).toEqual(mockPostStratificationRequest.stratification.input);
		});

		it('sets input to Prisma.DbNull when not present', () => {
			const {
				stratification: { input: _, ...stratificationWithoutInput },
			} = mockPostStratificationRequest;
			const requestWithoutInput = {
				...mockPostStratificationRequest,
				stratification: stratificationWithoutInput,
			};

			const prismaStratificationWithoutName = postStratificationRequestToPrismaCreateInput(
				requestWithoutInput,
				'group-id',
				'area-id'
			);

			expect(prismaStratificationWithoutName.input).toBe(Prisma.DbNull);
		});
	});

	describe('result', () => {
		it('maps result from request when present', () => {
			expect(prismaStratification.result).toEqual(mockPostStratificationRequest.stratification.result);
		});

		it('sets result to Prisma.DbNull when not present', () => {
			const {
				stratification: { result: _, ...stratificationWithoutResult },
			} = mockPostStratificationRequest;
			const requestWithoutResult = {
				...mockPostStratificationRequest,
				stratification: stratificationWithoutResult,
			};

			const prismaStratificationWithoutResult = postStratificationRequestToPrismaCreateInput(
				requestWithoutResult,
				'group-id',
				'area-id'
			);

			expect(prismaStratificationWithoutResult.result).toBe(Prisma.DbNull);
		});
	});

	describe('area', () => {
		it('connects the area referenced by areaId', () => {
			expect(prismaStratification.area).toEqual({
				connect: { id: 'area-id' },
			});
		});
	});

	describe('group', () => {
		it('connects the group referenced by group-id', () => {
			expect(prismaStratification.group).toEqual({
				connect: { id: 'group-id' },
			});
		});
	});

	describe('algorithm', () => {
		algorithmProperties.forEach((property) => {
			describe(property, () => {
				it(`maps ${property} from request when present`, () => {
					expect(prismaStratification.algorithm.create?.[property]).toEqual(
						mockPostStratificationRequest.stratification.algorithm[property]
					);
				});

				it(`sets ${property} to null when not present`, () => {
					const {
						stratification: {
							algorithm: { [property]: _, ...algorithmWithoutProperty },
						},
					} = mockPostStratificationRequest;
					const requestWithoutAlgorithmName = {
						...mockPostStratificationRequest,
						stratification: {
							...mockPostStratificationRequest.stratification,
							algorithm: algorithmWithoutProperty,
						},
					};

					const prismaStratificationWithoutAlgorithm = postStratificationRequestToPrismaCreateInput(
						requestWithoutAlgorithmName,
						'group-id',
						'area-id'
					);

					expect(prismaStratificationWithoutAlgorithm.algorithm.create?.[property]).toBeNull();
				});
			});
		});
	});
});

describe('stratificationServiceJobResultToPrismaCreateInput', () => {
	const jobResult = createMockStratificationServiceStratification();
	const prismaCreateInput = stratificationServiceJobResultToPrismaCreateInput({
		jobResult,
		fieldName: 'field-name',
		fieldId: 'field-id',
		areaId: 'area-id',
		groupId: 'group-id',
	});

	describe('name', () => {
		it('creates a name based on the field name', () => {
			expect(prismaCreateInput.name).toEqual('field-name Stratification');
		});
	});

	describe('agent', () => {
		it('sets agent to null', () => {
			expect(prismaCreateInput.agent).toBeNull();
		});
	});

	describe('provider', () => {
		it('sets provider to null', () => {
			expect(prismaCreateInput.provider).toBeNull();
		});
	});

	describe('isDemo', () => {
		it('sets isDemo to false', () => {
			expect(prismaCreateInput.isDemo).toBe(false);
		});
	});

	describe('algorithm', () => {
		algorithmProperties.forEach((property) => {
			describe(property, () => {
				it(`maps ${property} from request when present`, () => {
					expect(prismaCreateInput.algorithm.create?.[property]).toEqual(jobResult.algorithm[property]);
				});

				it(`sets ${property} to null when not present`, () => {
					const {
						algorithm: { [property]: _, ...algorithmWithoutProperty },
					} = jobResult;
					const jobResultWithoutAlgorithmName = {
						...jobResult,
						algorithm: algorithmWithoutProperty,
					};

					const prismaStratificationWithoutAlgorithm = stratificationServiceJobResultToPrismaCreateInput({
						jobResult: jobResultWithoutAlgorithmName,
						fieldName: 'field-name',
						fieldId: 'field-id',
						areaId: 'area-id',
						groupId: 'group-id',
					});

					expect(prismaStratificationWithoutAlgorithm.algorithm.create?.[property]).toBeNull();
				});
			});
		});
	});

	describe('area', () => {
		it('connects the area referenced by areaId', () => {
			expect(prismaCreateInput.area).toEqual({
				connect: { id: 'area-id' },
			});
		});
	});

	describe('group', () => {
		it('connects the group referenced by group-id', () => {
			expect(prismaCreateInput.group).toEqual({
				connect: { id: 'group-id' },
			});
		});
	});

	describe('input', () => {
		it('sets input', () => {
			expect(prismaCreateInput.input).toEqual(jobResult.input);
		});
	});

	describe('result', () => {
		it('sets result', () => {
			expect(prismaCreateInput.result).toEqual(jobResult.result);
		});
	});

	describe('locationCollection', () => {
		it('creates a location collection', () => {
			expect(prismaCreateInput.locationCollection?.create).toEqual(
				expect.objectContaining(populatedLocationCollectionToPrismaCreateInput('group-id', 'field-id', 'area-id'))
			);
		});

		describe('locations', () => {
			it('creates locations', () => {
				expect(prismaCreateInput.locationCollection?.create?.locations?.createMany?.data).toEqual(
					expect.arrayContaining(
						jobResult.locations.map((location) => locationToPrismaCreateInput(location as any, 'group-id'))
					)
				);
			});
		});
	});
});

describe('prismaStratificationToStratification', () => {
	const mockPrismaPopulatedStratification = createMockPrismaPopulatedStratification();

	const stratification = prismaStratificationToStratification(mockPrismaPopulatedStratification);

	describe('id', () => {
		it('maps id from prisma stratification', () => {
			expect(stratification.id).toEqual(mockPrismaPopulatedStratification.id);
		});
	});

	describe('name', () => {
		it('maps name from prisma stratification when present', () => {
			expect(stratification.name).toEqual(mockPrismaPopulatedStratification.name);
		});

		it("sets name to empty string when it's null", () => {
			const prismaStratificationWithoutName = {
				...mockPrismaPopulatedStratification,
				name: null,
			};

			const stratificationWithoutName = prismaStratificationToStratification(prismaStratificationWithoutName);

			expect(stratificationWithoutName.name).toEqual('');
		});
	});

	describe('agent', () => {
		it('maps agent from prisma stratification when present', () => {
			expect(stratification.agent).toEqual(mockPrismaPopulatedStratification.agent);
		});

		it("sets agent to empty string when it's null", () => {
			const prismaStratificationWithoutAgent = {
				...mockPrismaPopulatedStratification,
				agent: null,
			};

			const stratificationWithoutAgent = prismaStratificationToStratification(prismaStratificationWithoutAgent);

			expect(stratificationWithoutAgent.agent).toEqual('');
		});
	});

	describe('dateCreated', () => {
		it('maps createdAt from prisma stratification, converting to ISO 8601', () => {
			expect(stratification.dateCreated).toEqual(mockPrismaPopulatedStratification.createdAt.toISOString());
		});
	});

	describe('provider', () => {
		it('maps provider from prisma stratification when present', () => {
			expect(stratification.provider).toEqual(mockPrismaPopulatedStratification.provider);
		});

		it("sets provider to empty string when it's null", () => {
			const prismaStratificationWithoutProvider = {
				...mockPrismaPopulatedStratification,
				provider: null,
			};

			const stratificationWithoutProvider = prismaStratificationToStratification(prismaStratificationWithoutProvider);

			expect(stratificationWithoutProvider.provider).toEqual('');
		});
	});

	describe('object', () => {
		it('maps areaId from prisma stratification', () => {
			expect(stratification.object).toEqual(mockPrismaPopulatedStratification.areaId);
		});
	});

	describe('isDemo', () => {
		it('maps isDemo from prisma stratification', () => {
			expect(stratification.isDemo).toEqual(mockPrismaPopulatedStratification.isDemo);
		});
	});

	describe('algorithm', () => {
		algorithmProperties.forEach((property) => {
			describe(property, () => {
				it(`maps ${property} from prisma stratification algorithm when present`, () => {
					expect(stratification.algorithm[property]).toEqual(mockPrismaPopulatedStratification.algorithm[property]);
				});

				it(`sets ${property} to empty string when it's null`, () => {
					const prismaStratificationWithoutAlgorithm = {
						...mockPrismaPopulatedStratification,
						algorithm: {
							...mockPrismaPopulatedStratification.algorithm,
							[property]: null,
						},
					};

					const stratificationWithoutAlgorithm = prismaStratificationToStratification(
						prismaStratificationWithoutAlgorithm
					);

					expect(stratificationWithoutAlgorithm.algorithm[property]).toEqual('');
				});
			});
		});
	});

	describe('input', () => {
		it('maps input from prisma stratification when present', () => {
			expect(stratification.input).toEqual(mockPrismaPopulatedStratification.input);
		});

		it("sets input to empty array when it's null", () => {
			const prismaStratificationWithoutInput = {
				...mockPrismaPopulatedStratification,
				input: null,
			};

			const stratificationWithoutInput = prismaStratificationToStratification(prismaStratificationWithoutInput);

			expect(stratificationWithoutInput.input).toEqual([]);
		});
	});

	describe('result', () => {
		it('maps result from prisma stratification when present', () => {
			expect(stratification.result).toEqual(mockPrismaPopulatedStratification.result);
		});

		it("sets result to empty array when it's null", () => {
			const prismaStratificationWithoutResult = {
				...mockPrismaPopulatedStratification,
				result: null,
			};

			const stratificationWithoutResult = prismaStratificationToStratification(prismaStratificationWithoutResult);

			expect(stratificationWithoutResult.result).toEqual([]);
		});
	});

	describe('featureOfInterest', () => {
		it("maps featureOfInterest from the area's fieldId", () => {
			expect(stratification.featureOfInterest).toEqual(mockPrismaPopulatedStratification.area.fieldId);
		});
	});

	describe('meta', () => {
		describe('submittedAt', () => {
			it("maps submittedAt from the prisma stratification's createdAt, converting to ISO 8601", () => {
				expect(stratification.meta.submittedAt).toEqual(mockPrismaPopulatedStratification.createdAt.toISOString());
			});
		});

		describe('groupId', () => {
			it('maps groupId', () => {
				expect(stratification.meta.groupId).toEqual(mockPrismaPopulatedStratification.groupId);
			});
		});
	});
});
