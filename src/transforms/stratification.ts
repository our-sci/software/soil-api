import { Prisma } from '@prisma/client';
import { components } from '../types/generated/openapi-types';
import { components as stratServiceComponents } from '../types/generated/stratification-service-types';
import { PrismaFlatStratification, PrismaPopulatedStratification } from '../types/prisma';
import { locationToPrismaCreateInput } from './location';
import { populatedLocationCollectionToPrismaCreateInput } from './locationCollection';

type ResponseStratification = components['schemas']['ResponseStratification'];
type StratificationInput = components['schemas']['StratificationInput'];
type StratificationResult = components['schemas']['StratificationResult'];
type Location = components['schemas']['Location'];

type PostStratificationRequest = {
	stratification: components['schemas']['Stratification'];
	locationCollection: components['schemas']['PopulatedLocationCollection'];
};
const postStratificationRequestToPrismaCreateInput = (
	postStratificationRequest: PostStratificationRequest,
	groupId: string,
	areaId: string
): Prisma.StratificationCreateInput => {
	return {
		name: postStratificationRequest.stratification.name,
		agent: postStratificationRequest.stratification.agent,
		provider: postStratificationRequest.stratification.provider,
		isDemo: postStratificationRequest.stratification.isDemo ?? false,
		algorithm: {
			create: {
				name: postStratificationRequest.stratification.algorithm.name ?? null,
				alternateName: postStratificationRequest.stratification.algorithm.alternateName ?? null,
				codeRepository: postStratificationRequest.stratification.algorithm.codeRepository ?? null,
				version: postStratificationRequest.stratification.algorithm.version ?? null,
				doi: postStratificationRequest.stratification.algorithm.doi ?? null,
			},
		},
		area: { connect: { id: areaId } },
		group: { connect: { id: groupId } },
		input: ((postStratificationRequest.stratification.input as unknown) as Prisma.InputJsonObject) ?? Prisma.DbNull,
		result: ((postStratificationRequest.stratification.result as unknown) as Prisma.InputJsonObject) ?? Prisma.DbNull,
	};
};

const stratificationServiceJobResultToPrismaCreateInput = ({
	jobResult,
	fieldName,
	fieldId,
	areaId,
	groupId,
}: {
	jobResult: stratServiceComponents['schemas']['StratificationJobSuccess']['result'];
	fieldName: string;
	fieldId: string;
	areaId: string;
	groupId: string;
}): Prisma.StratificationCreateInput => {
	return {
		name: `${fieldName} Stratification`,
		agent: null,
		provider: null,
		isDemo: false,
		algorithm: {
			create: {
				name: jobResult.algorithm.name ?? null,
				alternateName: jobResult.algorithm.alternateName ?? null,
				codeRepository: jobResult.algorithm.codeRepository ?? null,
				version: jobResult.algorithm.version ?? null,
				doi: jobResult.algorithm.doi ?? null,
			},
		},
		area: { connect: { id: areaId } },
		group: { connect: { id: groupId } },
		input: (jobResult.input as unknown) as Prisma.InputJsonObject,
		result: (jobResult.result as unknown) as Prisma.InputJsonObject,
		locationCollection: {
			create: {
				...populatedLocationCollectionToPrismaCreateInput(groupId, fieldId, areaId),
				locations: {
					createMany: {
						data: (jobResult.locations as Required<typeof jobResult.locations[number]>[])
							.map(
								(location): Location => ({
									type: 'Feature',
									geometry: {
										type: 'Point',
										coordinates: location.geometry.coordinates as number[],
									},
									properties: location.properties,
								})
							)
							.map((location) => locationToPrismaCreateInput(location, groupId)),
					},
				},
			},
		},
	};
};

const prismaStratificationToStratification = (
	prismaStratification: PrismaPopulatedStratification | PrismaFlatStratification
): ResponseStratification => {
	return {
		id: prismaStratification.id,
		name: prismaStratification.name ?? '',
		agent: prismaStratification.agent ?? '',
		dateCreated: prismaStratification.createdAt.toISOString(),
		provider: prismaStratification.provider ?? '',
		object: prismaStratification.areaId,
		isDemo: prismaStratification.isDemo,
		algorithm: {
			name: prismaStratification.algorithm.name ?? '',
			alternateName: prismaStratification.algorithm.alternateName ?? '',
			codeRepository: prismaStratification.algorithm.codeRepository ?? '',
			version: prismaStratification.algorithm.version ?? '',
			doi: prismaStratification.algorithm.doi ?? '',
		},
		input: (prismaStratification.input ?? []) as StratificationInput[],
		result: (prismaStratification.result ?? []) as StratificationResult[],
		featureOfInterest: prismaStratification.area.fieldId,
		meta: {
			submittedAt: prismaStratification.createdAt.toISOString(),
			groupId: prismaStratification.groupId,
		},
	};
};

export {
	postStratificationRequestToPrismaCreateInput,
	prismaStratificationToStratification,
	stratificationServiceJobResultToPrismaCreateInput,
};
