import { Prisma } from '@prisma/client';
import {
	createMockPrismaStratificationJob,
	createMockStratificationConfig,
	createMockStratificationServiceStratification,
} from '../test-utils';
import {
	prismaStratificationJobToStratificationJob,
	stratServiceJobToPrismaStratificationJobCreate,
} from './stratificationJob';

describe('stratServiceJobToPrismaStratificationJobCreate', () => {
	const stratJobCreate = stratServiceJobToPrismaStratificationJobCreate({
		jobId: 'jobId',
		areaId: 'areaId',
		stratificationConfig: createMockStratificationConfig(),
	});

	describe('jobId', () => {
		it('sets the given jobId', () => {
			expect(stratJobCreate.jobId).toEqual('jobId');
		});
	});

	describe('areaId', () => {
		it('sets the given areaId', () => {
			expect(stratJobCreate.areaId).toEqual('areaId');
		});
	});

	describe('stratificationConfig', () => {
		it('sets the given stratificationConfig', () => {
			expect(stratJobCreate.stratificationConfig).toEqual(createMockStratificationConfig());
		});
	});

	describe('jobStatus', () => {
		it('does not set jobStatus if not provided', () => {
			expect(stratJobCreate).not.toHaveProperty('jobStatus');
		});

		it('sets jobStatus if provided', () => {
			const stratJobCreateWithJobStatus = stratServiceJobToPrismaStratificationJobCreate({
				jobId: 'jobId',
				areaId: 'areaId',
				stratificationConfig: createMockStratificationConfig(),
				jobStatus: 'PENDING',
			});

			expect(stratJobCreateWithJobStatus.externalJobStatus).toEqual('PENDING');
		});
	});

	describe('isClosed', () => {
		it('does not set isClosed if not provided', () => {
			expect(stratJobCreate).not.toHaveProperty('isClosed');
		});

		it('sets isClosed if provided', () => {
			const stratJobCreateWithIsClosed = stratServiceJobToPrismaStratificationJobCreate({
				jobId: 'jobId',
				areaId: 'areaId',
				stratificationConfig: createMockStratificationConfig(),
				isClosed: true,
				closedReason: 'CANCELLED',
			});

			expect(stratJobCreateWithIsClosed.isClosed).toEqual(true);
		});

		it('throws if isClosed is true but closedReason is not provided', () => {
			expect(() => {
				stratServiceJobToPrismaStratificationJobCreate({
					jobId: 'jobId',
					areaId: 'areaId',
					stratificationConfig: createMockStratificationConfig(),
					isClosed: true,
				});
			}).toThrowError('closedReason must be provided if isClosed is true');
		});
	});

	describe('closedReason', () => {
		it('does not set closedReason if not provided', () => {
			expect(stratJobCreate).not.toHaveProperty('closedReason');
		});

		it('sets closedReason if provided', () => {
			const stratJobCreateWithClosedReason = stratServiceJobToPrismaStratificationJobCreate({
				jobId: 'jobId',
				areaId: 'areaId',
				stratificationConfig: createMockStratificationConfig(),
				isClosed: true,
				closedReason: 'CANCELLED',
			});

			expect(stratJobCreateWithClosedReason.closedReason).toEqual('CANCELLED');
		});
	});

	describe('error', () => {
		it('does not set error if not provided', () => {
			expect(stratJobCreate).not.toHaveProperty('error');
		});

		it('sets error if provided', () => {
			const stratJobCreateWithError = stratServiceJobToPrismaStratificationJobCreate({
				jobId: 'jobId',
				areaId: 'areaId',
				stratificationConfig: createMockStratificationConfig(),
				error: 'ERROR',
			});

			expect(stratJobCreateWithError.error).toEqual('ERROR');
		});
	});

	describe('autoApprove', () => {
		it('sets autoApprove to false if not provided', () => {
			expect(stratJobCreate.autoApprove).toEqual(false);
		});

		it('sets autoApprove if provided', () => {
			const stratJobCreateWithAutoApprove = stratServiceJobToPrismaStratificationJobCreate({
				jobId: 'jobId',
				areaId: 'areaId',
				stratificationConfig: createMockStratificationConfig(),
				autoApprove: true,
			});

			expect(stratJobCreateWithAutoApprove.autoApprove).toEqual(true);
		});
	});
});

describe('prismaStratificationJobToStratificationJob', () => {
	const mockPrismaStratificationJob = createMockPrismaStratificationJob();
	const stratificationJob = prismaStratificationJobToStratificationJob(mockPrismaStratificationJob);

	describe('id', () => {
		it('sets id', () => {
			expect(stratificationJob.id).toEqual(mockPrismaStratificationJob.id);
		});
	});

	describe('areaId', () => {
		it('sets areaId', () => {
			expect(stratificationJob.areaId).toEqual(mockPrismaStratificationJob.areaId);
		});
	});

	describe('isClosed', () => {
		it('sets isClosed', () => {
			expect(stratificationJob.isClosed).toEqual(mockPrismaStratificationJob.isClosed);
		});
	});

	describe('stratificationConfig', () => {
		it('stringifies and sets stratificationConfig', () => {
			expect(stratificationJob.stratificationConfig).toEqual(
				JSON.stringify(mockPrismaStratificationJob.stratificationConfig)
			);
		});
	});

	describe('createdAt', () => {
		it('sets createdAt as ISO string', () => {
			expect(stratificationJob.createdAt).toEqual(mockPrismaStratificationJob.createdAt.toISOString());
		});
	});

	describe('updatedAt', () => {
		it('sets updatedAt as ISO string', () => {
			expect(stratificationJob.updatedAt).toEqual(mockPrismaStratificationJob.updatedAt.toISOString());
		});
	});

	describe('autoApprove', () => {
		it('sets autoApprove', () => {
			expect(stratificationJob.autoApprove).toEqual(mockPrismaStratificationJob.autoApprove);
		});
	});

	describe('jobResult', () => {
		it('does not set jobResult if not provided', () => {
			expect(stratificationJob).not.toHaveProperty('jobResult');
		});

		it('sets jobResult if provided', () => {
			const mockStratServiceStratification = createMockStratificationServiceStratification();

			const mockPrismaStratificationJobWithJobResult = createMockPrismaStratificationJob({
				jobResult: mockStratServiceStratification as Prisma.JsonObject,
			});

			const stratificationJobWithJobResult = prismaStratificationJobToStratificationJob(
				mockPrismaStratificationJobWithJobResult
			);
			expect(stratificationJobWithJobResult.jobResult).toEqual(mockStratServiceStratification);
		});
	});

	describe('error', () => {
		it('does not set error if not provided', () => {
			expect(stratificationJob).not.toHaveProperty('error');
		});

		it('sets error if provided', () => {
			const mockPrismaStratificationJobWithError = createMockPrismaStratificationJob({
				error: 'ERROR',
			});

			const stratificationJobWithError = prismaStratificationJobToStratificationJob(
				mockPrismaStratificationJobWithError
			);

			expect(stratificationJobWithError.error).toEqual('ERROR');
		});
	});

	describe('closedReason', () => {
		it('does not set closedReason if not provided', () => {
			expect(stratificationJob).not.toHaveProperty('closedReason');
		});

		it('sets closedReason if provided', () => {
			const mockPrismaStratificationJobWithClosedReason = createMockPrismaStratificationJob({
				closedReason: 'CANCELLED',
				isClosed: true,
			});

			const stratificationJobWithClosedReason = prismaStratificationJobToStratificationJob(
				mockPrismaStratificationJobWithClosedReason
			);

			expect(stratificationJobWithClosedReason.closedReason).toEqual('CANCELLED');
		});
	});

	describe('externalJobStatus', () => {
		it('does not set externalJobStatus if not provided', () => {
			expect(stratificationJob).not.toHaveProperty('externalJobStatus');
		});

		it('sets externalJobStatus if provided', () => {
			const mockPrismaStratificationJobWithExternalJobStatus = createMockPrismaStratificationJob({
				externalJobStatus: 'PENDING',
			});

			const stratificationJobWithExternalJobStatus = prismaStratificationJobToStratificationJob(
				mockPrismaStratificationJobWithExternalJobStatus
			);

			expect(stratificationJobWithExternalJobStatus.externalJobStatus).toEqual('PENDING');
		});
	});

	describe('stratificationId', () => {
		it('does not set stratificationId if not provided', () => {
			expect(stratificationJob).not.toHaveProperty('stratificationId');
		});

		it('sets stratificationId if provided', () => {
			const mockPrismaStratificationJobWithStratificationId = createMockPrismaStratificationJob({
				stratificationId: 'stratificationId',
			});

			const stratificationJobWithStratificationId = prismaStratificationJobToStratificationJob(
				mockPrismaStratificationJobWithStratificationId
			);

			expect(stratificationJobWithStratificationId.stratificationId).toEqual('stratificationId');
		});
	});
});
