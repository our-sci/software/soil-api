import { Prisma, StratificationJob as PrismaStratificationJob } from '@prisma/client';
import { components } from '../types/generated/openapi-types';
import {
	components as stratServiceComponents,
	paths as stratServicePaths,
} from '../types/generated/stratification-service-types';

type PostStratificationResponse = stratServicePaths['/stratifications']['post']['responses']['200']['content']['application/json'];
type StratificationConfig = stratServiceComponents['schemas']['StratificationConfig'];
type StratificationJob = components['schemas']['StratificationJob'];
type StratificationServiceStratification = stratServiceComponents['schemas']['Stratification'];

const stratServiceJobToPrismaStratificationJobCreate = ({
	jobId,
	jobStatus,
	areaId,
	stratificationConfig,
	isClosed,
	closedReason,
	error,
	autoApprove,
}: {
	jobStatus?: PostStratificationResponse['state'];
	jobId: string | null;
	areaId: string;
	stratificationConfig: StratificationConfig;
	isClosed?: boolean;
	closedReason?: StratificationJob['closedReason'];
	error?: string;
	autoApprove?: boolean;
}): Prisma.StratificationJobUncheckedCreateInput => {
	const stratificationJobCreate: Prisma.StratificationJobUncheckedCreateInput = {
		jobId,
		areaId,
		stratificationConfig: stratificationConfig as Prisma.JsonObject,
	};

	if (jobStatus) {
		stratificationJobCreate.externalJobStatus = jobStatus;
	}

	if (isClosed) {
		if (!closedReason) {
			throw new Error('closedReason must be provided if isClosed is true');
		}
		stratificationJobCreate.isClosed = isClosed;
		stratificationJobCreate.closedReason = closedReason;
	}

	if (error) {
		stratificationJobCreate.error = error;
	}

	stratificationJobCreate.autoApprove = autoApprove ?? false;

	return stratificationJobCreate;
};

const prismaStratificationJobToStratificationJob = (
	prismaStratificationJob: PrismaStratificationJob
): StratificationJob => {
	const stratificationJob: StratificationJob = {
		id: prismaStratificationJob.id,
		areaId: prismaStratificationJob.areaId,
		isClosed: prismaStratificationJob.isClosed,
		stratificationConfig: JSON.stringify(prismaStratificationJob.stratificationConfig),
		createdAt: prismaStratificationJob.createdAt.toISOString(),
		updatedAt: prismaStratificationJob.updatedAt.toISOString(),
		autoApprove: prismaStratificationJob.autoApprove,
	};

	if (prismaStratificationJob.jobResult) {
		stratificationJob.jobResult = prismaStratificationJob.jobResult as StratificationServiceStratification;
	}
	if (prismaStratificationJob.error) {
		stratificationJob.error = prismaStratificationJob.error;
	}
	if (prismaStratificationJob.closedReason) {
		stratificationJob.closedReason = prismaStratificationJob.closedReason;
	}
	if (prismaStratificationJob.externalJobStatus) {
		stratificationJob.externalJobStatus = prismaStratificationJob.externalJobStatus;
	}
	if (prismaStratificationJob.stratificationId) {
		stratificationJob.stratificationId = prismaStratificationJob.stratificationId;
	}

	return stratificationJob;
};

export { prismaStratificationJobToStratificationJob, stratServiceJobToPrismaStratificationJobCreate };
