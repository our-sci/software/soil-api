import { ApiKey as PrismaApiKey } from '@prisma/client';
import { createMockPrismaApiKey, createMockPrismaUserWithApiKeys } from '../test-utils';
import { components } from '../types/generated/openapi-types';
import { PrismaUserWithApiKeys } from '../types/prisma';
import { prismaUserToUser } from './user';

describe('prismaUserToUser', () => {
	let prismaApiKey: PrismaApiKey;
	let prismaUser: PrismaUserWithApiKeys;
	let responseUser: components['schemas']['ResponseUser'];

	beforeAll(async () => {
		prismaApiKey = createMockPrismaApiKey();
		prismaUser = await createMockPrismaUserWithApiKeys({
			apiKeys: [prismaApiKey],
		});
		responseUser = prismaUserToUser(prismaUser);
	});

	it('maps id', () => {
		expect(responseUser.id).toEqual(prismaUser.id);
	});

	it('maps email', () => {
		expect(responseUser.email).toEqual(prismaUser.email);
	});

	it('maps seen', () => {
		expect(responseUser.seen).toEqual(prismaUser.seen);
	});

	it('maps apiKeys with id, label props only', () => {
		expect(responseUser.apiKeys.length).toBe(1);
		expect(responseUser.apiKeys[0].id).toBe(prismaApiKey.id);
		expect(responseUser.apiKeys[0].label).toBe(prismaApiKey.label);
		expect(responseUser.apiKeys[0]).not.toHaveProperty('hashedKey');
	});
});
