import { components } from '../types/generated/openapi-types';
import { PrismaUserWithApiKeys } from '../types/prisma';
import { prismaApiKeyToApiKey } from './apiKey';

const prismaUserToUser = (prismaUser: PrismaUserWithApiKeys): components['schemas']['ResponseUser'] => ({
	id: prismaUser.id,
	email: prismaUser.email,
	seen: prismaUser.seen,
	apiKeys: prismaUser.apiKeys.map(prismaApiKeyToApiKey),
});

export { prismaUserToUser };
