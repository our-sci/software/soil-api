import { Prisma } from '@prisma/client';

export type PrismaPasswordResetTokenWithUser = Prisma.PasswordResetTokenGetPayload<{
	include: { user: true };
}>;

export type PrismaMembershipWithUser = Prisma.MembershipGetPayload<{
	include: { user: true };
}>;

export type PrismaMembershipWithIncludes = Prisma.MembershipGetPayload<{
	include: {
		user: {
			include: {
				memberships: true;
			};
		};
		group: {
			select: { parentId: true };
		};
	};
}>;

export type PrismaUserWithMemberships = Prisma.UserGetPayload<{
	include: { memberships: true };
}>;

export type PrismaUserWithApiKeys = Prisma.UserGetPayload<{
	include: { apiKeys: true };
}>;

export type PrismaApiKeyWithUserWithMemberships = Prisma.ApiKeyGetPayload<{
	include: { user: { include: { memberships: true } } };
}>;

export type PrismaAuthTokenWithUserWithMemberships = Prisma.AuthTokenGetPayload<{
	include: { user: { include: { memberships: true } } };
}>;

export type PrismaPopulatedField = Prisma.FieldGetPayload<{
	include: { areas: true; referenceIds: true };
}>;

export type PrismaFlatField = Prisma.FieldGetPayload<{
	include: {
		areas: { select: { id: true } };
		referenceIds: true;
	};
}>;

export type PrismaPopulatedStratification = Prisma.StratificationGetPayload<{
	include: {
		algorithm: true;
		locationCollection: { include: { locations: true } };
		area: { select: { fieldId: true } };
	};
}>;

export type PrismaFlatStratification = Prisma.StratificationGetPayload<{
	include: {
		algorithm: true;
		area: { select: { fieldId: true } };
	};
}>;

export type PrismaPopulatedLocationCollection = Prisma.LocationCollectionGetPayload<{
	include: { locations: true };
}>;

export type PrismaFlatLocationCollection = Prisma.LocationCollectionGetPayload<{
	include: { locations: { select: { id: true } } };
}>;

export type PrismaPopulatedSampling = Prisma.SamplingGetPayload<{
	include: {
		samples: true;
	};
}> & { samplingCollectionId: string };

export type PrismaFlatSampling = Prisma.SamplingGetPayload<{
	include: {
		samples: { select: { id: true } };
	};
}> & { samplingCollectionId: string };

export type PrismaPopulatedSamplingCollection = Prisma.SamplingCollectionGetPayload<{
	include: {
		samplings: {
			include: {
				samples: true;
			};
		};
		referenceIds: true;
	};
}>;

export type PrismaFlatSamplingCollection = Prisma.SamplingCollectionGetPayload<{
	include: {
		samplings: { select: { id: true } };
		referenceIds: true;
	};
}>;

export type PrismaStratificationWithSamplingCollectionIds = Prisma.StratificationGetPayload<{
	include: {
		locationCollection: {
			select: {
				id: true;
				samplingCollections: {
					select: {
						id: true;
					};
				};
			};
		};
	};
}>;
