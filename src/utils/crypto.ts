import { createHash, randomBytes } from 'crypto';
import bcrypt from 'bcryptjs';
import { SALT_ROUNDS } from '../constants';

function hash(value: string): string {
	return createHash('sha512').update(value).digest('hex');
}

function randomHex(): string {
	return randomBytes(32).toString('hex');
}

function hashAndSalt(value: string): Promise<string> {
	return bcrypt.hash(value, SALT_ROUNDS);
}

function compare(value: string, hash: string): Promise<boolean> {
	return bcrypt.compare(value, hash);
}

export { compare, hash, hashAndSalt, randomHex };
