import sanitizeHtml from 'sanitize-html';

// removes all html tags from a string
const stripHtml = (text: string) => sanitizeHtml(text, { allowedTags: [] });

export { stripHtml };
