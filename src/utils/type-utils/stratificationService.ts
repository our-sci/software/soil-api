import { components } from '../../types/generated/stratification-service-types';
type StratificationJobSuccess = components['schemas']['StratificationJobSuccess'];
type StratificationJobFailure = components['schemas']['StratificationJobFailure'];
type StratificationJobPending = components['schemas']['StratificationJobPending'];

export const isStratificationJobSuccess = (
	stratificationJob: StratificationJobSuccess | StratificationJobFailure | StratificationJobPending
): stratificationJob is StratificationJobSuccess => stratificationJob.state === 'SUCCESS';

export const isStratificationJobFailure = (
	stratificationJob: StratificationJobSuccess | StratificationJobFailure | StratificationJobPending
): stratificationJob is StratificationJobFailure => stratificationJob.state === 'FAILURE';

export const isStratificationJobPending = (
	stratificationJob: StratificationJobSuccess | StratificationJobFailure | StratificationJobPending
): stratificationJob is StratificationJobPending => stratificationJob.state === 'PENDING';
