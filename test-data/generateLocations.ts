import { randomPoint } from '@turf/random';
import { bbox, Point, pointsWithinPolygon } from '@turf/turf';
import { Feature, Polygon } from 'geojson';

function generateRandomPointsInPolygon(polygon: Polygon, count = 10) {
	const polygonBbox = bbox(polygon);
	const pointsInPolygon: Feature<Point, any>[] = [];
	let remainder = Number(String(count));
	while (pointsInPolygon.length < count) {
		const points = randomPoint(remainder, { bbox: polygonBbox });
		const ptsInPoly = pointsWithinPolygon(points, polygon);
		pointsInPolygon.push(...ptsInPoly.features.slice(0, remainder));
		remainder -= ptsInPoly.features.length;
	}
	return pointsInPolygon.map((feature) => feature.geometry);
}

export { generateRandomPointsInPolygon };
