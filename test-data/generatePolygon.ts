import { components } from '../src/types/generated/openapi-types';

function generatePolygon(lng: number, lat: number, buffer = 1e-3): components['schemas']['Polygon'] {
	return {
		type: 'Polygon',
		coordinates: [
			[
				[lng - buffer, lat + buffer],
				[lng + buffer, lat + buffer],
				[lng + buffer, lat - buffer],
				[lng - buffer, lat - buffer],
				[lng - buffer, lat + buffer],
			],
		],
	};
}

export { generatePolygon };
