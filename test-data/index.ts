import axios from 'axios';
import yargs from 'yargs';
import {
	createMockArea,
	createMockLocation,
	createMockLocationCollection,
	createMockStratification,
} from '../src/test-utils';
import { components } from '../src/types/generated/openapi-types';
import { generateRandomPointsInPolygon } from './generateLocations';
import { generatePolygon } from './generatePolygon';

const argvPromise = yargs
	.command('create', 'Creates a field with an associated stratification.', {
		'api-url': {
			alias: 'u',
			description: 'The Soil API url. (ex. "http://localhost:3000")',
			type: 'string',
			demandOption: true,
			requiresArg: true,
		},
		auth: {
			alias: 'a',
			description: 'The authorization header value to pass to the Soil API. (ex. "name@example.com abcd1234")',
			type: 'string',
			demandOption: true,
			requiresArg: true,
		},
		'group-id': {
			alias: 'gid',
			description: 'The id of the group to create the survey under.',
			type: 'string',
			demandOption: true,
			requiresArg: true,
		},
		lat: {
			description: 'The latitude to center the field around.',
			type: 'number',
			demandOption: true,
			requiresArg: true,
		},
		lng: {
			description: 'The longitude to center the field around.',
			type: 'number',
			demandOption: true,
			requiresArg: true,
		},
		'dry-run': {
			alias: 'd',
			description: 'Prevent resource creation. Only log args.',
			type: 'boolean',
			default: false,
		},
	})
	.demandCommand(1, 'You must specify one of the above commands.')
	.help()
	.alias('help', 'h')
	.parse();

async function main() {
	const argv = await argvPromise;
	if (argv._.includes('create')) {
		const apiUrl = argv.apiUrl as string;
		const auth = argv.auth as string;
		const groupId = argv.groupId as string;
		const lat = argv.lat as number;
		const lng = argv.lng as number;
		const dryRun = argv.dryRun as boolean;
		console.log('Received Args:', {
			apiUrl,
			auth,
			groupId,
			lat,
			lng,
			dryRun,
		});

		if (dryRun) {
			return;
		}

		const area = createMockArea({
			geometry: generatePolygon(lng, lat),
			properties: {},
		});
		const field: components['schemas']['PopulatedField'] = {
			name: 'Test Field',
			alternateName: 'Alternate Test Name',
			producerName: 'Producer Test Name',
			address: {
				streetAddress: '123 Test Data Lane',
				addressLocality: 'Testville',
				addressRegion: 'TN',
				addressCountry: 'USA',
				postalCode: '12345',
			},
			contactPoints: [
				{
					telephone: '(800) 321 - 4567',
					email: 'testdata@example.com',
					name: 'Mrs. Test',
					contactType: 'producer',
					organization: 'Test Data Inc.',
				},
			],
			areas: [area],
		};

		let fieldId: string;
		let areaId: string;
		try {
			const fieldsResponse = await axios.post(`${apiUrl}/fields?groupId=${groupId}`, field, {
				headers: {
					'X-Authorization': auth,
				},
			});
			fieldId = fieldsResponse.data.fields[0].id;
			areaId = fieldsResponse.data.areas[0].id;
			console.log('Successfully created field:', fieldId);
			console.log('Successfully created area:', areaId);
		} catch (error) {
			console.log('Failed to create field. Aborting...');
			return;
		}

		const stratification = createMockStratification({
			object: areaId,
			dateCreated: new Date().toISOString(),
		});
		const locationCollection: any = createMockLocationCollection({
			object: areaId,
			featureOfInterest: fieldId,
		});
		locationCollection.features = generateRandomPointsInPolygon(area.geometry).map((geometry) =>
			createMockLocation({ geometry })
		);

		try {
			const stratificationsResponse = await axios.post(
				`${apiUrl}/stratifications?groupId=${groupId}`,
				{ stratification, locationCollection },
				{
					headers: { 'X-Authorization': auth },
				}
			);
			console.log('Successfully created stratification:', stratificationsResponse.data.stratifications[0].id);
			console.log('Successfully created location collection:', stratificationsResponse.data.locationCollections[0].id);
			stratificationsResponse.data.locations.forEach((location: components['schemas']['ResponseLocation']) => {
				console.log('Successfully created location:', location.id);
			});
		} catch (e) {
			console.log('Failed to create stratification. Aborting...');
			return;
		}
	}
}

main();
